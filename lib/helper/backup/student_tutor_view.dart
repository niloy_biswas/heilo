import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_tutor_details_model.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/profilePicture.dart';
import 'package:Heilo/widgets/review_tile.dart';
// import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TeacherProfileView extends StatefulWidget {
  final int tutorId;

  const TeacherProfileView({
    Key key,
    @required this.tutorId,
  }) : super(key: key);
  @override
  _TeacherProfileViewState createState() => _TeacherProfileViewState();
}

class _TeacherProfileViewState extends State<TeacherProfileView> {
  Future<StudentTutorDetails> futureTutorDetail;
  Services serv = Services();

  _getData() {
    futureTutorDetail = serv.getStudentHomeTutorDetails(
      tutorId: widget.tutorId,
      refresh: true,
    );
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      // body:
      body: FutureBuilder(
        future: futureTutorDetail,
        builder: (context, snapshot) {
          StudentTutorDetails apiData = snapshot.data;
          if (snapshot.hasError) {
            print("ERROR OCCURED WHILE FETCHING PROFILE DATA");
          } else if (snapshot.hasData) {
            return SafeArea(
              child: SingleChildScrollView(
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Container(
                          height: sHeight * 0.33,
                          width: sWidth,
                          decoration: BoxDecoration(
                            color: AssetStrings.color4,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                height: sWidth * 0.28,
                                width: sWidth * 0.28,
                                child: ProfilePictureWidget(
                                    assetString: 'assets/images/hulk.jpg'),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),

                                ///-----------------------TUTOR NAME---------------------///
                                child: Text(
                                  apiData.data.name ?? 'Not Available',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54,
                                  ),
                                ),
                              ),

                              ///---------------------SCHOOL NAME----------------------///
                              Text(
                                apiData.data.educations[0].institutionName ??
                                    'Not Available',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    FaIcon(
                                      FontAwesomeIcons.mapMarkerAlt,
                                      color: Colors.white,
                                      size: 14,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),

                                    ///---------------------LOCATION NAME----------------///
                                    Text(
                                      "${apiData.data.location.area}, ${apiData.data.location.district}" ??
                                          'Location Not Available',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                        SizedBox(
                          height: 30,
                        ),

                        /// RATING and COMPLETED TUTION COUNT ///
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 70,
                              width: 100,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 2),
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 5,
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      ///---------------- RATING NUMBER -------------------///
                                      Text(
                                        apiData.data.rating.toString() ?? '0',
                                        style: TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      FaIcon(
                                        FontAwesomeIcons.solidStar,
                                        color: AssetStrings.starColor,
                                        size: 20,
                                      ),
                                    ],
                                  ),
                                  Text(
                                    'Rating',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: sWidth * 0.15,
                            ),
                            Container(
                              height: 70,
                              width: 100,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 2),
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 5,
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ///-----------------COMPLETED TUTION-----------------///
                                  Text(
                                    apiData.data.totalClassCompleted
                                            .toString() ??
                                        '0',
                                    style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'Completed Tutions',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          padding: EdgeInsets.all(16),
                          height: sHeight * 0.2,
                          width: sWidth * 0.85,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Medium:',
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    width: sWidth * 0.4,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: AssetStrings.color4,
                                        width: 1,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),

                                    ///------------------MEDIUM OF STUDY------------------///
                                    child: Text(
                                      apiData.data.medium ?? 'Not Available',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Background:',
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    width: sWidth * 0.4,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: AssetStrings.color4,
                                        width: 1,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),

                                    ///------------------BACKGROUND OF STUDY------------------///
                                    child: Text(
                                      apiData.data.educations[0].background ??
                                          'Not Available',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Class:',
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    width: sWidth * 0.4,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: AssetStrings.color4,
                                        width: 1,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),

                                    ///------------------CLASS OF TUTION------------------///
                                    child: Text(
                                      apiData.data.educations[0].grade ??
                                          'Not Available',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),

                        SizedBox(
                          height: 30,
                        ),

                        ///-------------------------ABOUT SECTION-------------------------///
                        Container(
                          padding: EdgeInsets.all(16),
                          height: sHeight * 0.2,
                          width: sWidth * 0.85,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'About Tutor:',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),

                                ///--------------------------ABOUT AUTHOR PARAGRAPH---------------------///
                                Text(apiData.data.about ?? "Not Available"),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),

                        ///------------------------------ REVIEW SECTION ----------------------------///
                        Container(
                          padding: EdgeInsets.all(16),
                          height: sHeight * 0.4,
                          width: sWidth * 0.85,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: ListView(
                            // mainAxisSize: MainAxisSize.min,
                            shrinkWrap: true,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  'Reviews:',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              // SizedBox(
                              //   height: 10,
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.all(8.0),
                              //   child: ReviewTile(),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.all(8.0),
                              //   child: ReviewTile(),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.all(8.0),
                              //   child: ReviewTile(),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.all(8.0),
                              //   child: ReviewTile(),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.all(8.0),
                              //   child: ReviewTile(),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.all(8.0),
                              //   child: ReviewTile(),
                              // ),
                              (apiData.data.reviews.length < 1)
                                  ? Container(
                                      child: Center(
                                        child: Text("Not Available"),
                                      ),
                                    )
                                  : ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: apiData.data.reviews.length,
                                      itemBuilder: (context, index) {
                                        // return Text(
                                        //     apiData.data.reviews[index].feedback);
                                        return Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: ReviewTile(
                                            dp: apiData.data.reviews[index]
                                                .studentDetails.dp,
                                            title: apiData.data.reviews[index]
                                                .studentDetails.name,
                                            subtitle: apiData
                                                .data.reviews[index].feedback,
                                            rating: apiData
                                                .data.reviews[index].rate
                                                .toString(),
                                          ),
                                        );
                                      },
                                    ),
                            ],
                          ),
                          // child: ListView.builder(
                          //   itemCount: apiData.data.reviews.length,
                          //   itemBuilder: (context, index) {
                          //     return Text(apiData.data.reviews[index].feedback);
                          //   },
                          // ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                    Positioned(
                      top: 10,
                      left: 15,
                      child: GestureDetector(
                        ///-----------------------BACK BUTTON-----------------------///
                        child: FaIcon(
                          FontAwesomeIcons.angleLeft,
                          size: 35,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    // Positioned(
                    //   bottom: 10,
                    //   right: 10,
                    //   child: FloatingActionButton(
                    //     child: Icon(
                    //       CommunityMaterialIcons.comment_text,
                    //       color: Colors.white,
                    //       size: 30,
                    //     ),
                    //     backgroundColor: AssetStrings.color4,
                    //     onPressed: () {},
                    //   ),
                    // ),
                  ],
                ),
              ),
            );
          }
          return Center(
              child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: AssetStrings.color4,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Loading...",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ));
        },
      ),
      floatingActionButton: FloatingActionButton(
        // child: Icon(
        //   CommunityMaterialIcons.comment_text,
        //   color: Colors.white,
        //   size: 30,
        // ),
        child: Image(
          width: 26,
          height: 26,
          color: Colors.white,
          image: Svg("assets/icons/chat.svg"),
        ),
        backgroundColor: AssetStrings.color4,
        onPressed: () {},
      ),
    );
  }
}
