import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  static String uidKey = "UIDKEY";
  static String userNameKey = "USERNAMEKEY";
  static String userEmailKey = "USEREMAILKEY";
  static String userProfilePicKey = "USERPROFILEPICKEY";
  static String userTypeKey = "USERTYPEKEY";
  static String userAccessTokenKey = "USERACCESSTOKENKEY";
  static String userContactKey = "USERCONTACTKEY";
  static String tutorIdKey = "TUTORIDKEY";

  ////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
  /// Registration Information Holder ///
  static String regNameKey = "REGNAMEKEY";
  static String regEmailKey = "REGEMAILKEY";
  static String regMobileKey = "REGMOBILEKEY";
  static String regPasswordKey = "REGPASSWORDKEY";
  static String regConfirmPasswordKey = "REGCONFIRMPASSWORDKEY";
  static String regSegmentIdKey = "REGSEGMENTID";
  static String regMediumKey = "REGMEDIUMOFSTUDY";
  static String regHourlyRateKey = "REGHOURLYRATE";
  // Location //
  static String regDistrictKey = "REGDISTRICTKEY";
  static String regAreaKey = "REGAREAKEY";
  static String regRoadKey = "REGROADKEY";
  static String regDetailAddressKey = "REGDETAILADDRESSKEY";
  // SKILL //
  static String regSkillMediumKey = "REGSKILLMEDIUM";
  static String regSkillClassKey = "REGSKILLCLASSKEY";
  static String regSkillSubKey = "REGSKILLSUBKEY";
  static String regSkillSubjectIdKey = "REGSKILLSUBJECTIDKEY";

  /// FUCK YOUUU
  static String regSkillTopicKey = "REGSKILLTOPICKEY";
  static String regSkillTopicIdKey = "REGSKILLTOPICIDKEY";

  ////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////
  /// STUDENT HOME INFO ///////////////////////////////////////////
  static String locationIdKey = "LOCATIONIDKEY";
  static String segmentIdKey = "SEGMENTIDKEY";
  static String subjectIdKey = "SUBJECTIDKEY";
  static String topicIdKey = "TOPICIDKEY";

  //============================= TRIGGERS =========================//
  static String locationTriggerKey = "LOCATIONTRIGGERKEY";

  //============================ NOTIFICATION ======================//
  static String notificationKey = "NOTIFICATIONKEY";
  static String completeNotificationKey = "COMPLETENOTIFICATIONKEY";
  static String completeClassStudentIdKey = "COMPLETECLASSID";

  //============================ CHIP ==============================//
  static String chipLocationKey = "CHIPLOCATION";
  static String chipSubjectKey = "CHIPSUBJECT";
  static String chipTopicKey = "CHIPTOPIC";

  //

  Future<bool> saveChipLocation(String chipLocation) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(chipLocationKey, chipLocation);
  }

  Future<String> getChipLocation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(chipLocationKey);
  }

  Future<bool> saveChipSubject(String chipSubject) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(chipSubjectKey, chipSubject);
  }

  Future<String> getChipSubject() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(chipSubjectKey);
  }

  Future<bool> saveChipTopic(String chipTopic) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(chipTopicKey, chipTopic);
  }

  Future<String> getChipTopic() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(chipTopicKey);
  }

  //////////////////////////////// Save Data ////////////////////////////////
  ///
  Future<bool> saveNotificationKey(String notificationId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(notificationKey, notificationId);
  }

  Future<String> getNotificationKey() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(notificationKey);
  }

  Future<bool> saveCompleteNotificationKey(String completeNotificaionId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(completeNotificationKey, completeNotificaionId);
  }

  Future<String> getCompleteNotificationKey() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(completeNotificationKey);
  }

  Future<bool> saveCompleteClassStudentIdKey(String completeClassId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(completeClassStudentIdKey, completeClassId);
  }

  Future<String> getCompleteClassStudentIdKey() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(completeClassStudentIdKey);
  }

  ///
  Future<bool> saveLocationTrigger(String locationTrigger) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(locationTriggerKey, locationTrigger);
  }

  Future<String> getLocationTrigger() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(locationTriggerKey);
  }

  ///
  Future<bool> saveUid(String uid) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(uidKey, uid);
  }

  Future<bool> saveUserName(String userName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(userNameKey, userName);
  }

  Future<bool> saveUserEmail(String userEmail) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(userEmailKey, userEmail);
  }

  Future<bool> saveUserProfilePic(String userProfilePic) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(userProfilePicKey, userProfilePic);
  }

  Future<bool> saveUserType(String userType) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(userTypeKey, userType);
  }

  Future<bool> saveUserAccessToken(String userAccessToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(userAccessTokenKey, userAccessToken);
  }

  Future<bool> saveUserContact(String userContact) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(userContactKey, userContact);
  }

  Future<bool> saveTutorId(String tutorId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(tutorIdKey, tutorId);
  }

  /////////////////////////// STUDENT HOME INFO SAVE /////////////////////////////
  ///
  Future<bool> saveLocationId(String locationId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(locationIdKey, locationId);
  }

  Future<bool> saveSegmentId(String segmentId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(segmentIdKey, segmentId);
  }

  Future<bool> saveSubjectId(String subjectId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(subjectIdKey, subjectId);
  }

  Future<bool> saveTopicId(String topicId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(topicIdKey, topicId);
  }

  ///////////////////////////////////////////////////////////////////////////////
  //////////////////////////// REGISTRY INFORMATION SAVE /////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////
  Future<bool> saveRegName(String regName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regNameKey, regName);
  }

  Future<bool> saveRegEmail(String regEmail) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regEmailKey, regEmail);
  }

  Future<bool> saveRegMobile(String regMobile) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regMobileKey, regMobile);
  }

  Future<bool> saveRegPassword(String regPassword) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regPasswordKey, regPassword);
  }

  Future<bool> saveRegSegmentId(String regSegmentId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regSegmentIdKey, regSegmentId);
  }

  Future<bool> saveRegMedium(String regMediumId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regMediumKey, regMediumId);
  }

  Future<bool> saveRegHourlyRate(String regHourlyRate) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regHourlyRateKey, regHourlyRate);
  }

  Future<bool> saveRegDistrict(String regDistrict) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regDistrictKey, regDistrict);
  }

  Future<bool> saveRegArea(String regArea) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regAreaKey, regArea);
  }

  Future<bool> saveRegRoad(String regRoad) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regRoadKey, regRoad);
  }

  Future<bool> saveRegDetailAddress(String regDetailAddress) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regDetailAddressKey, regDetailAddress);
  }

  Future<bool> saveRegSkillMedium(String regSkillMedium) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regSkillMediumKey, regSkillMedium);
  }

  Future<bool> saveRegSkillClass(String regSkillClass) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regSkillClassKey, regSkillClass);
  }

  Future<bool> saveRegSkillSub(String regSkillSub) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regSkillSubKey, regSkillSub);
  }

  Future<bool> saveRegSkillSubjectId(String regSkillSubjectId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regSkillSubjectIdKey, regSkillSubjectId);
  }

  /// FUCK YOU ///

  Future<bool> saveRegSkillTopic(String regSkillTopic) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regSkillTopicKey, regSkillTopic);
  }

  Future<bool> saveRegSkillTopicId(String regSkillTopicId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(regSkillTopicIdKey, regSkillTopicId);
  }

  ////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////// GET DATA //////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  Future<String> getUserUid() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(uidKey);
  }

  Future<String> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(userNameKey);
  }

  Future<String> getUserEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(userEmailKey);
  }

  Future<String> getUserProfilePic() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(userProfilePicKey);
  }

  Future<String> getUserType() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(userTypeKey);
  }

  Future<String> getUserAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(userAccessTokenKey);
  }

  Future<String> getUserContact() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(userContactKey);
  }

  Future<String> getTutorId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(tutorIdKey);
  }

  ////////////////////////////////////////////////////////////////
  ////////////////// GET REGISTRY INFORMATION /////////////////////
  /////////////////////////////////////////////////////////////////
  Future<String> getRegName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regNameKey);
  }

  Future<String> getRegEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regEmailKey);
  }

  Future<String> getRegMobile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regMobileKey);
  }

  Future<String> getRegPassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regPasswordKey);
  }

  Future<String> getRegSegmentId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regSegmentIdKey);
  }

  Future<String> getRegMedium() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regMediumKey);
  }

  Future<String> getRegHourlyRate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regHourlyRateKey);
  }

  Future<String> getRegDistrict() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regDistrictKey);
  }

  Future<String> getRegArea() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regAreaKey);
  }

  Future<String> getRegRoad() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regRoadKey);
  }

  Future<String> getRegDetailAddress() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regDetailAddressKey);
  }

  Future<String> getRegSkillMedium() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regSkillMediumKey);
  }

  Future<String> getRegSkillClass() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regSkillClassKey);
  }

  Future<String> getRegSkillSub() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regSkillSubKey);
  }

  Future<String> getRegSkillSubjectId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regSkillSubjectIdKey);
  }

  /// FUCK YOU ///
  Future<String> getRegSkillTopic() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regSkillTopicKey);
  }

  Future<String> getRegSkillTopicId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(regSkillTopicIdKey);
  }

  //////////////////////////////////////////////////////////////////
  /// STUDENT HOME GET IDs ///
  Future<String> getLocationId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(locationIdKey);
  }

  Future<String> getSegmentId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(segmentIdKey);
  }

  Future<String> getSubjectId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(subjectIdKey);
  }

  Future<String> getTopicId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(topicIdKey);
  }
}
