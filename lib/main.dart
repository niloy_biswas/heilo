import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/common/contact_us.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:Heilo/pages/common/initial_page.dart';
import 'package:Heilo/pages/common/login_page.dart';
import 'package:Heilo/pages/common/otp_page.dart';
import 'package:Heilo/pages/common/signup_page.dart';
import 'package:Heilo/pages/common/splash.dart';
import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await AndroidAlarmManager.initialize();
  runApp(MyApp());
  configLoading();
}

void configLoading() {
  EasyLoading.instance
    // ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.ring
    ..loadingStyle = EasyLoadingStyle.custom
    ..maskType = EasyLoadingMaskType.custom
    ..indicatorSize = 40.0
    ..radius = 10.0
    ..progressColor = Colors.white
    ..backgroundColor = AssetStrings.color4
    ..indicatorColor = Colors.white
    ..textColor = Colors.white
    ..maskColor = Colors.black.withOpacity(0.5)
    ..userInteractions = true
    ..dismissOnTap = false;
  // ..customAnimation = CustomAnimation();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Heilo',
      // home: InitialPage(),
      navigatorKey: navKey,
      theme: ThemeData(
        fontFamily: 'QuickSandMedium',
        primaryColor: AssetStrings.color4,
        accentColor: AssetStrings.color4,
        cursorColor: AssetStrings.color4,
        errorColor: Colors.red[400],
        // splashColor: AssetStrings.color4,
        // cardColor: AssetStrings.color4,
        shadowColor: AssetStrings.color4,
      ),
      routes: {
        '/': (context) => Splash(),
        '/initialPage': (context) => InitialPage(),
        '/loginPage': (context) => LoginPage(),
        '/signUpPage': (context) => SignUpPage(),
        '/otpPage': (context) => OtpPage(),
        '/bottomNav': (context) => StudentBottomNav(),
        '/tutorBottomNav': (context) => TeacherBottomNav(),
        '/contactUs': (context) => ContactUs(),
      },
      debugShowCheckedModeBanner: false,
      builder: EasyLoading.init(),
    );
  }
}
