import 'package:flutter/material.dart';

class AssetStrings {
  AssetStrings._();

  // Images
  static const String mainLogo = 'assets/images/mainLogo.png';
  static const String ic_book = 'assets/images/book.png';
  static const String ic_teacher = 'assets/images/teacher.png';

  // Color
  static const Color color1 = Color(0xFFE5F6E3);
  static const Color color2 = Colors.white;
  static const Color color3 = Color(0xFF04F675);
  // static const Color color4 = Color(0xFF44CF82);
  static const Color color4 = Color(0xFF3ACD91);
  static const Color starColor = Color(0xFFFFC107);
  // static const Color backBtnColor = Colors.blueGrey[600];

  static Shader linearGradient = LinearGradient(
    colors: <Color>[Colors.cyan, Color(0xff61C15E), Color(0xff8CC63E)],
  ).createShader(Rect.fromLTWH(100.0, 30.0, 200.0, 70.0));
}
