class LocationModel {
  int code;
  String status;
  String message;
  List<Data> data;

  LocationModel({this.code, this.status, this.message, this.data});

  LocationModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int locId;
  String district;
  String area;
  String road;
  String createdAt;
  String updatedAt;

  Data(
      {this.locId,
      this.district,
      this.area,
      this.road,
      this.createdAt,
      this.updatedAt});

  Data.fromJson(Map<String, dynamic> json) {
    locId = json['loc_id'];
    district = json['district'];
    area = json['area'];
    road = json['road'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['loc_id'] = this.locId;
    data['district'] = this.district;
    data['area'] = this.area;
    data['road'] = this.road;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
