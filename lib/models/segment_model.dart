class SegmentModel {
  int code;
  String status;
  String message;
  List<Data> data;

  SegmentModel({this.code, this.status, this.message, this.data});

  SegmentModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int segmentId;
  String medium;
  String className;
  String description;

  Data({this.segmentId, this.medium, this.className, this.description});

  Data.fromJson(Map<String, dynamic> json) {
    segmentId = json['segment_id'];
    medium = json['medium'];
    className = json['class_name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['segment_id'] = this.segmentId;
    data['medium'] = this.medium;
    data['class_name'] = this.className;
    data['description'] = this.description;
    return data;
  }
}
