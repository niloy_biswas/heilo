class StudentHomeTutorListModel {
  int code;
  String status;
  String message;
  List<Data> data;

  StudentHomeTutorListModel({this.code, this.status, this.message, this.data});

  StudentHomeTutorListModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int tutorId;
  String uid;
  String name;
  String email;
  String contact;
  String medium;
  String hourlyRate;
  String altPhone;
  String status;
  String dp;
  String about;
  num rating;
  String joined;
  Institution institution;

  Data({
    this.tutorId,
    this.uid,
    this.name,
    this.email,
    this.contact,
    this.medium,
    this.hourlyRate,
    this.altPhone,
    this.status,
    this.dp,
    this.about,
    this.rating,
    this.joined,
    this.institution,
  });

  Data.fromJson(Map<String, dynamic> json) {
    tutorId = json['tutor_id'];
    uid = json['uid'];
    name = json['name'];
    email = json['email'];
    contact = json['contact'];
    medium = json['medium'];
    hourlyRate = json['hourly_rate'];
    altPhone = json['alt_phone'];
    status = json['status'];
    dp = json['dp'];
    about = json['about'];
    rating = json['rating'];
    joined = json['joined'];
    institution = json['institution'] != null
        ? new Institution.fromJson(json['institution'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tutor_id'] = this.tutorId;
    data['uid'] = this.uid;
    data['name'] = this.name;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['medium'] = this.medium;
    data['hourly_rate'] = this.hourlyRate;
    data['alt_phone'] = this.altPhone;
    data['status'] = this.status;
    data['dp'] = this.dp;
    data['about'] = this.about;
    data['rating'] = this.rating;
    data['joined'] = this.joined;
    if (this.institution != null) {
      data['institution'] = this.institution.toJson();
    }
    return data;
  }
}

class Institution {
  int eduId;
  int tutorId;
  String institutionType;
  String institutionName;
  String background;
  String grade;
  String medium;
  String detailAddress;

  Institution({
    this.eduId,
    this.tutorId,
    this.institutionType,
    this.institutionName,
    this.background,
    this.grade,
    this.medium,
    this.detailAddress,
  });

  Institution.fromJson(Map<String, dynamic> json) {
    eduId = json['edu_id'];
    tutorId = json['tutor_id'];
    institutionType = json['institution_type'];
    institutionName = json['institution_name'];
    background = json['background'];
    grade = json['grade'];
    medium = json['medium'];
    detailAddress = json['detail_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['edu_id'] = this.eduId;
    data['tutor_id'] = this.tutorId;
    data['institution_type'] = this.institutionType;
    data['institution_name'] = this.institutionName;
    data['background'] = this.background;
    data['grade'] = this.grade;
    data['medium'] = this.medium;
    data['detail_address'] = this.detailAddress;
    return data;
  }
}
