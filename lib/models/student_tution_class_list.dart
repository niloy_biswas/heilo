class StudentTutionClassList {
  int code;
  String status;
  String message;
  List<Data> data;

  StudentTutionClassList({this.code, this.status, this.message, this.data});

  StudentTutionClassList.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int classId;
  String code;
  int tutorId;
  int studentId;
  int subjectId;
  String topicId;
  String timestamp;
  String hourlyRate;
  String totalHour;
  double hourComplete;
  String payment;
  String paymentStatus;
  String status;
  String createdAt;
  String updatedAt;
  Subject subject;
  Tutor tutor;

  Data(
      {this.classId,
      this.code,
      this.tutorId,
      this.studentId,
      this.subjectId,
      this.topicId,
      this.timestamp,
      this.hourlyRate,
      this.totalHour,
      this.hourComplete,
      this.payment,
      this.paymentStatus,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.subject,
      this.tutor});

  Data.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    code = json['code'];
    tutorId = json['tutor_id'];
    studentId = json['student_id'];
    subjectId = json['subject_id'];
    topicId = json['topic_id'];
    timestamp = json['timestamp'];
    hourlyRate = json['hourly_rate'];
    totalHour = json['total_hour'];
    hourComplete = json['hour_complete'];
    payment = json['payment'];
    paymentStatus = json['payment_status'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    subject =
        json['subject'] != null ? new Subject.fromJson(json['subject']) : null;
    tutor = json['tutor'] != null ? new Tutor.fromJson(json['tutor']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['class_id'] = this.classId;
    data['code'] = this.code;
    data['tutor_id'] = this.tutorId;
    data['student_id'] = this.studentId;
    data['subject_id'] = this.subjectId;
    data['topic_id'] = this.topicId;
    data['timestamp'] = this.timestamp;
    data['hourly_rate'] = this.hourlyRate;
    data['total_hour'] = this.totalHour;
    data['hour_complete'] = this.hourComplete;
    data['payment'] = this.payment;
    data['payment_status'] = this.paymentStatus;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.subject != null) {
      data['subject'] = this.subject.toJson();
    }
    if (this.tutor != null) {
      data['tutor'] = this.tutor.toJson();
    }
    return data;
  }
}

class Subject {
  int subjectId;
  String segmentId;
  String medium;
  String subject;
  String description;

  Subject(
      {this.subjectId,
      this.segmentId,
      this.medium,
      this.subject,
      this.description});

  Subject.fromJson(Map<String, dynamic> json) {
    subjectId = json['subject_id'];
    segmentId = json['segment_id'];
    medium = json['medium'];
    subject = json['subject'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subject_id'] = this.subjectId;
    data['segment_id'] = this.segmentId;
    data['medium'] = this.medium;
    data['subject'] = this.subject;
    data['description'] = this.description;
    return data;
  }
}

class Tutor {
  int tutorId;
  String uid;
  String name;
  String email;
  String contact;
  String medium;
  String hourlyRate;
  String altPhone;
  String status;
  String dp;
  String about;
  num rating;
  String joined;
  Location location;

  Tutor(
      {this.tutorId,
      this.uid,
      this.name,
      this.email,
      this.contact,
      this.medium,
      this.hourlyRate,
      this.altPhone,
      this.status,
      this.dp,
      this.about,
      this.rating,
      this.joined,
      this.location});

  Tutor.fromJson(Map<String, dynamic> json) {
    tutorId = json['tutor_id'];
    uid = json['uid'];
    name = json['name'];
    email = json['email'];
    contact = json['contact'];
    medium = json['medium'];
    hourlyRate = json['hourly_rate'];
    altPhone = json['alt_phone'];
    status = json['status'];
    dp = json['dp'];
    about = json['about'];
    rating = json['rating'];
    joined = json['joined'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tutor_id'] = this.tutorId;
    data['uid'] = this.uid;
    data['name'] = this.name;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['medium'] = this.medium;
    data['hourly_rate'] = this.hourlyRate;
    data['alt_phone'] = this.altPhone;
    data['status'] = this.status;
    data['dp'] = this.dp;
    data['about'] = this.about;
    data['rating'] = this.rating;
    data['joined'] = this.joined;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    return data;
  }
}

class Location {
  int locId;
  int tutorId;
  String long;
  String lat;
  String district;
  String area;
  String road;
  String detailAddress;

  Location(
      {this.locId,
      this.tutorId,
      this.long,
      this.lat,
      this.district,
      this.area,
      this.road,
      this.detailAddress});

  Location.fromJson(Map<String, dynamic> json) {
    locId = json['loc_id'];
    tutorId = json['tutor_id'];
    long = json['long'];
    lat = json['lat'];
    district = json['district'];
    area = json['area'];
    road = json['road'];
    detailAddress = json['detail_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['loc_id'] = this.locId;
    data['tutor_id'] = this.tutorId;
    data['long'] = this.long;
    data['lat'] = this.lat;
    data['district'] = this.district;
    data['area'] = this.area;
    data['road'] = this.road;
    data['detail_address'] = this.detailAddress;
    return data;
  }
}
