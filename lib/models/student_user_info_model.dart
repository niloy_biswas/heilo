class StudentUserInfoModel {
  int code;
  String status;
  String message;
  Data data;

  StudentUserInfoModel({this.code, this.status, this.message, this.data});

  StudentUserInfoModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int studentId;
  String uid;
  String name;
  String email;
  String contact;
  String medium;
  int segmentId;
  String altPhone;
  num rating;
  String status;
  String dp;
  String about;
  String joined;
  List<Classes> classes;
  int totalClass;
  int totalClassCompleted;
  List<Educations> educations;
  Segment segment;
  List<Reviews> reviews;

  Data(
      {this.studentId,
      this.uid,
      this.name,
      this.email,
      this.contact,
      this.medium,
      this.segmentId,
      this.altPhone,
      this.rating,
      this.status,
      this.dp,
      this.about,
      this.joined,
      this.classes,
      this.totalClass,
      this.totalClassCompleted,
      this.educations,
      this.segment,
      this.reviews});

  Data.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    uid = json['uid'];
    name = json['name'];
    email = json['email'];
    contact = json['contact'];
    medium = json['medium'];
    segmentId = json['segment_id'];
    altPhone = json['alt_phone'];
    rating = json['rating'];
    status = json['status'];
    dp = json['dp'];
    about = json['about'];
    joined = json['joined'];
    if (json['classes'] != null) {
      classes = new List<Classes>();
      json['classes'].forEach((v) {
        classes.add(new Classes.fromJson(v));
      });
    }
    totalClass = json['total_class'];
    totalClassCompleted = json['total_class_completed'];
    if (json['educations'] != null) {
      educations = new List<Educations>();
      json['educations'].forEach((v) {
        educations.add(new Educations.fromJson(v));
      });
    }
    segment =
        json['segment'] != null ? new Segment.fromJson(json['segment']) : null;
    if (json['reviews'] != null) {
      reviews = new List<Reviews>();
      json['reviews'].forEach((v) {
        reviews.add(new Reviews.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['uid'] = this.uid;
    data['name'] = this.name;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['medium'] = this.medium;
    data['segment_id'] = this.segmentId;
    data['alt_phone'] = this.altPhone;
    data['rating'] = this.rating;
    data['status'] = this.status;
    data['dp'] = this.dp;
    data['about'] = this.about;
    data['joined'] = this.joined;
    if (this.classes != null) {
      data['classes'] = this.classes.map((v) => v.toJson()).toList();
    }
    data['total_class'] = this.totalClass;
    data['total_class_completed'] = this.totalClassCompleted;
    if (this.educations != null) {
      data['educations'] = this.educations.map((v) => v.toJson()).toList();
    }
    if (this.segment != null) {
      data['segment'] = this.segment.toJson();
    }
    if (this.reviews != null) {
      data['reviews'] = this.reviews.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Classes {
  int classId;
  String code;
  int tutorId;
  int studentId;
  int subjectId;
  String topicId;
  String timestamp;
  String hourlyRate;
  String totalHour;
  double hourComplete;
  String payment;
  String paymentStatus;
  String status;
  String createdAt;
  String updatedAt;
  bool reviewStatus;
  Subject subject;
  Tutor tutor;
  List<TutorReviews> tutorReviews;

  Classes(
      {this.classId,
      this.code,
      this.tutorId,
      this.studentId,
      this.subjectId,
      this.topicId,
      this.timestamp,
      this.hourlyRate,
      this.totalHour,
      this.hourComplete,
      this.payment,
      this.paymentStatus,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.reviewStatus,
      this.subject,
      this.tutor,
      this.tutorReviews});

  Classes.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    code = json['code'];
    tutorId = json['tutor_id'];
    studentId = json['student_id'];
    subjectId = json['subject_id'];
    topicId = json['topic_id'];
    timestamp = json['timestamp'];
    hourlyRate = json['hourly_rate'];
    totalHour = json['total_hour'];
    hourComplete = json['hour_complete'];
    payment = json['payment'];
    paymentStatus = json['payment_status'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    reviewStatus = json['review_status'];
    subject =
        json['subject'] != null ? new Subject.fromJson(json['subject']) : null;
    tutor = json['tutor'] != null ? new Tutor.fromJson(json['tutor']) : null;
    if (json['tutor_reviews'] != null) {
      tutorReviews = new List<TutorReviews>();
      json['tutor_reviews'].forEach((v) {
        tutorReviews.add(new TutorReviews.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['class_id'] = this.classId;
    data['code'] = this.code;
    data['tutor_id'] = this.tutorId;
    data['student_id'] = this.studentId;
    data['subject_id'] = this.subjectId;
    data['topic_id'] = this.topicId;
    data['timestamp'] = this.timestamp;
    data['hourly_rate'] = this.hourlyRate;
    data['total_hour'] = this.totalHour;
    data['hour_complete'] = this.hourComplete;
    data['payment'] = this.payment;
    data['payment_status'] = this.paymentStatus;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['review_status'] = this.reviewStatus;
    if (this.subject != null) {
      data['subject'] = this.subject.toJson();
    }
    if (this.tutor != null) {
      data['tutor'] = this.tutor.toJson();
    }
    if (this.tutorReviews != null) {
      data['tutor_reviews'] = this.tutorReviews.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Subject {
  int subjectId;
  String segmentId;
  String medium;
  String subject;
  String description;

  Subject(
      {this.subjectId,
      this.segmentId,
      this.medium,
      this.subject,
      this.description});

  Subject.fromJson(Map<String, dynamic> json) {
    subjectId = json['subject_id'];
    segmentId = json['segment_id'];
    medium = json['medium'];
    subject = json['subject'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subject_id'] = this.subjectId;
    data['segment_id'] = this.segmentId;
    data['medium'] = this.medium;
    data['subject'] = this.subject;
    data['description'] = this.description;
    return data;
  }
}

class Tutor {
  int tutorId;
  String uid;
  String name;
  String email;
  String contact;
  String medium;
  String hourlyRate;
  String altPhone;
  String status;
  String dp;
  String about;
  num rating;
  String joined;
  Location location;

  Tutor(
      {this.tutorId,
      this.uid,
      this.name,
      this.email,
      this.contact,
      this.medium,
      this.hourlyRate,
      this.altPhone,
      this.status,
      this.dp,
      this.about,
      this.rating,
      this.joined,
      this.location});

  Tutor.fromJson(Map<String, dynamic> json) {
    tutorId = json['tutor_id'];
    uid = json['uid'];
    name = json['name'];
    email = json['email'];
    contact = json['contact'];
    medium = json['medium'];
    hourlyRate = json['hourly_rate'];
    altPhone = json['alt_phone'];
    status = json['status'];
    dp = json['dp'];
    about = json['about'];
    rating = json['rating'];
    joined = json['joined'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tutor_id'] = this.tutorId;
    data['uid'] = this.uid;
    data['name'] = this.name;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['medium'] = this.medium;
    data['hourly_rate'] = this.hourlyRate;
    data['alt_phone'] = this.altPhone;
    data['status'] = this.status;
    data['dp'] = this.dp;
    data['about'] = this.about;
    data['rating'] = this.rating;
    data['joined'] = this.joined;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    return data;
  }
}

class Location {
  int recordId;
  int locId;
  int tutorId;
  String long;
  String lat;
  String district;
  String area;
  String road;
  String detailAddress;

  Location(
      {this.recordId,
      this.locId,
      this.tutorId,
      this.long,
      this.lat,
      this.district,
      this.area,
      this.road,
      this.detailAddress});

  Location.fromJson(Map<String, dynamic> json) {
    recordId = json['record_id'];
    locId = json['loc_id'];
    tutorId = json['tutor_id'];
    long = json['long'];
    lat = json['lat'];
    district = json['district'];
    area = json['area'];
    road = json['road'];
    detailAddress = json['detail_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['record_id'] = this.recordId;
    data['loc_id'] = this.locId;
    data['tutor_id'] = this.tutorId;
    data['long'] = this.long;
    data['lat'] = this.lat;
    data['district'] = this.district;
    data['area'] = this.area;
    data['road'] = this.road;
    data['detail_address'] = this.detailAddress;
    return data;
  }
}

class TutorReviews {
  int reviewId;
  int tutorId;
  int studentId;
  int classId;
  int prefer;
  int rate;
  String feedback;

  TutorReviews(
      {this.reviewId,
      this.tutorId,
      this.studentId,
      this.classId,
      this.prefer,
      this.rate,
      this.feedback});

  TutorReviews.fromJson(Map<String, dynamic> json) {
    reviewId = json['review_id'];
    tutorId = json['tutor_id'];
    studentId = json['student_id'];
    classId = json['class_id'];
    prefer = json['prefer'];
    rate = json['rate'];
    feedback = json['feedback'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['review_id'] = this.reviewId;
    data['tutor_id'] = this.tutorId;
    data['student_id'] = this.studentId;
    data['class_id'] = this.classId;
    data['prefer'] = this.prefer;
    data['rate'] = this.rate;
    data['feedback'] = this.feedback;
    return data;
  }
}

class Educations {
  int eduId;
  int studentId;
  String institutionType;
  String institutionName;
  String background;
  String grade;
  String medium;
  String detailAddress;
  String createdAt;
  String updatedAt;

  Educations(
      {this.eduId,
      this.studentId,
      this.institutionType,
      this.institutionName,
      this.background,
      this.grade,
      this.medium,
      this.detailAddress,
      this.createdAt,
      this.updatedAt});

  Educations.fromJson(Map<String, dynamic> json) {
    eduId = json['edu_id'];
    studentId = json['student_id'];
    institutionType = json['institution_type'];
    institutionName = json['institution_name'];
    background = json['background'];
    grade = json['grade'];
    medium = json['medium'];
    detailAddress = json['detail_address'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['edu_id'] = this.eduId;
    data['student_id'] = this.studentId;
    data['institution_type'] = this.institutionType;
    data['institution_name'] = this.institutionName;
    data['background'] = this.background;
    data['grade'] = this.grade;
    data['medium'] = this.medium;
    data['detail_address'] = this.detailAddress;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Segment {
  int segmentId;
  String medium;
  String className;
  String description;

  Segment({this.segmentId, this.medium, this.className, this.description});

  Segment.fromJson(Map<String, dynamic> json) {
    segmentId = json['segment_id'];
    medium = json['medium'];
    className = json['class_name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['segment_id'] = this.segmentId;
    data['medium'] = this.medium;
    data['class_name'] = this.className;
    data['description'] = this.description;
    return data;
  }
}

class Reviews {
  int reviewId;
  int tutorId;
  int studentId;
  int classId;
  int prefer;
  int rate;
  String feedback;
  TutorDetails tutorDetails;

  Reviews(
      {this.reviewId,
      this.tutorId,
      this.studentId,
      this.classId,
      this.prefer,
      this.rate,
      this.feedback,
      this.tutorDetails});

  Reviews.fromJson(Map<String, dynamic> json) {
    reviewId = json['review_id'];
    tutorId = json['tutor_id'];
    studentId = json['student_id'];
    classId = json['class_id'];
    prefer = json['prefer'];
    rate = json['rate'];
    feedback = json['feedback'];
    tutorDetails = json['tutor_details'] != null
        ? new TutorDetails.fromJson(json['tutor_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['review_id'] = this.reviewId;
    data['tutor_id'] = this.tutorId;
    data['student_id'] = this.studentId;
    data['class_id'] = this.classId;
    data['prefer'] = this.prefer;
    data['rate'] = this.rate;
    data['feedback'] = this.feedback;
    if (this.tutorDetails != null) {
      data['tutor_details'] = this.tutorDetails.toJson();
    }
    return data;
  }
}

class TutorDetails {
  int tutorId;
  String name;
  String dp;

  TutorDetails({this.tutorId, this.name, this.dp});

  TutorDetails.fromJson(Map<String, dynamic> json) {
    tutorId = json['tutor_id'];
    name = json['name'];
    dp = json['dp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tutor_id'] = this.tutorId;
    data['name'] = this.name;
    data['dp'] = this.dp;
    return data;
  }
}
