class SubjectModel {
  int code;
  String status;
  String message;
  List<Data> data;

  SubjectModel({this.code, this.status, this.message, this.data});

  SubjectModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int subjectId;
  String segmentId;
  String medium;
  String subject;
  String description;

  Data(
      {this.subjectId,
      this.segmentId,
      this.medium,
      this.subject,
      this.description});

  Data.fromJson(Map<String, dynamic> json) {
    subjectId = json['subject_id'];
    segmentId = json['segment_id'];
    medium = json['medium'];
    subject = json['subject'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subject_id'] = this.subjectId;
    data['segment_id'] = this.segmentId;
    data['medium'] = this.medium;
    data['subject'] = this.subject;
    data['description'] = this.description;
    return data;
  }
}
