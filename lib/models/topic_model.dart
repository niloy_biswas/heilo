class TopicModel {
  int code;
  String status;
  String message;
  List<Data> data;

  TopicModel({this.code, this.status, this.message, this.data});

  TopicModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int topicId;
  int subjectId;
  String name;
  String description;

  Data({this.topicId, this.subjectId, this.name, this.description});

  Data.fromJson(Map<String, dynamic> json) {
    topicId = json['topic_id'];
    subjectId = json['subject_id'];
    name = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['topic_id'] = this.topicId;
    data['subject_id'] = this.subjectId;
    data['name'] = this.name;
    data['description'] = this.description;
    return data;
  }
}
