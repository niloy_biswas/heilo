class TutorClassDetailModel {
  int code;
  String status;
  String message;
  Data data;

  TutorClassDetailModel({this.code, this.status, this.message, this.data});

  TutorClassDetailModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int classId;
  String code;
  int tutorId;
  int studentId;
  int subjectId;
  String topicId;
  String timestamp;
  String hourlyRate;
  String totalHour;
  double hourComplete;
  String payment;
  String paymentStatus;
  String status;
  String createdAt;
  String updatedAt;
  Subject subject;
  Student student;

  Data(
      {this.classId,
      this.code,
      this.tutorId,
      this.studentId,
      this.subjectId,
      this.topicId,
      this.timestamp,
      this.hourlyRate,
      this.totalHour,
      this.hourComplete,
      this.payment,
      this.paymentStatus,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.subject,
      this.student});

  Data.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    code = json['code'];
    tutorId = json['tutor_id'];
    studentId = json['student_id'];
    subjectId = json['subject_id'];
    topicId = json['topic_id'];
    timestamp = json['timestamp'];
    hourlyRate = json['hourly_rate'];
    totalHour = json['total_hour'];
    hourComplete = json['hour_complete'];
    payment = json['payment'];
    paymentStatus = json['payment_status'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    subject =
        json['subject'] != null ? new Subject.fromJson(json['subject']) : null;
    student =
        json['student'] != null ? new Student.fromJson(json['student']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['class_id'] = this.classId;
    data['code'] = this.code;
    data['tutor_id'] = this.tutorId;
    data['student_id'] = this.studentId;
    data['subject_id'] = this.subjectId;
    data['topic_id'] = this.topicId;
    data['timestamp'] = this.timestamp;
    data['hourly_rate'] = this.hourlyRate;
    data['total_hour'] = this.totalHour;
    data['hour_complete'] = this.hourComplete;
    data['payment'] = this.payment;
    data['payment_status'] = this.paymentStatus;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.subject != null) {
      data['subject'] = this.subject.toJson();
    }
    if (this.student != null) {
      data['student'] = this.student.toJson();
    }
    return data;
  }
}

class Subject {
  int subjectId;
  String segmentId;
  String medium;
  String subject;
  String description;

  Subject(
      {this.subjectId,
      this.segmentId,
      this.medium,
      this.subject,
      this.description});

  Subject.fromJson(Map<String, dynamic> json) {
    subjectId = json['subject_id'];
    segmentId = json['segment_id'];
    medium = json['medium'];
    subject = json['subject'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subject_id'] = this.subjectId;
    data['segment_id'] = this.segmentId;
    data['medium'] = this.medium;
    data['subject'] = this.subject;
    data['description'] = this.description;
    return data;
  }
}

class Student {
  int studentId;
  String uid;
  String name;
  String email;
  String contact;
  String medium;
  int segmentId;
  String altPhone;
  num rating;
  String status;
  String dp;
  String about;
  String joined;

  Student(
      {this.studentId,
      this.uid,
      this.name,
      this.email,
      this.contact,
      this.medium,
      this.segmentId,
      this.altPhone,
      this.rating,
      this.status,
      this.dp,
      this.about,
      this.joined});

  Student.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    uid = json['uid'];
    name = json['name'];
    email = json['email'];
    contact = json['contact'];
    medium = json['medium'];
    segmentId = json['segment_id'];
    altPhone = json['alt_phone'];
    rating = json['rating'];
    status = json['status'];
    dp = json['dp'];
    about = json['about'];
    joined = json['joined'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['uid'] = this.uid;
    data['name'] = this.name;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['medium'] = this.medium;
    data['segment_id'] = this.segmentId;
    data['alt_phone'] = this.altPhone;
    data['rating'] = this.rating;
    data['status'] = this.status;
    data['dp'] = this.dp;
    data['about'] = this.about;
    data['joined'] = this.joined;
    return data;
  }
}
