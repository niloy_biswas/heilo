class TutorStudentViewModel {
  int code;
  String status;
  String message;
  Data data;

  TutorStudentViewModel({this.code, this.status, this.message, this.data});

  TutorStudentViewModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int studentId;
  String uid;
  String name;
  String email;
  String contact;
  String medium;
  int segmentId;
  String altPhone;
  num rating;
  String status;
  String dp;
  String about;
  String joined;
  int totalClass;
  int totalClassCompleted;
  List<Educations> educations;
  List<Reviews> reviews;
  List<Classes> classes;

  Data(
      {this.studentId,
      this.uid,
      this.name,
      this.email,
      this.contact,
      this.medium,
      this.segmentId,
      this.altPhone,
      this.rating,
      this.status,
      this.dp,
      this.about,
      this.joined,
      this.totalClass,
      this.totalClassCompleted,
      this.educations,
      this.reviews,
      this.classes});

  Data.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    uid = json['uid'];
    name = json['name'];
    email = json['email'];
    contact = json['contact'];
    medium = json['medium'];
    segmentId = json['segment_id'];
    altPhone = json['alt_phone'];
    rating = json['rating'];
    status = json['status'];
    dp = json['dp'];
    about = json['about'];
    joined = json['joined'];
    totalClass = json['total_class'];
    totalClassCompleted = json['total_class_completed'];
    if (json['educations'] != null) {
      educations = new List<Educations>();
      json['educations'].forEach((v) {
        educations.add(new Educations.fromJson(v));
      });
    }
    if (json['reviews'] != null) {
      reviews = new List<Reviews>();
      json['reviews'].forEach((v) {
        reviews.add(new Reviews.fromJson(v));
      });
    }
    if (json['classes'] != null) {
      classes = new List<Classes>();
      json['classes'].forEach((v) {
        classes.add(new Classes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['uid'] = this.uid;
    data['name'] = this.name;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['medium'] = this.medium;
    data['segment_id'] = this.segmentId;
    data['alt_phone'] = this.altPhone;
    data['rating'] = this.rating;
    data['status'] = this.status;
    data['dp'] = this.dp;
    data['about'] = this.about;
    data['joined'] = this.joined;
    data['total_class'] = this.totalClass;
    data['total_class_completed'] = this.totalClassCompleted;
    if (this.educations != null) {
      data['educations'] = this.educations.map((v) => v.toJson()).toList();
    }
    if (this.reviews != null) {
      data['reviews'] = this.reviews.map((v) => v.toJson()).toList();
    }
    if (this.classes != null) {
      data['classes'] = this.classes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Educations {
  int eduId;
  int studentId;
  String institutionType;
  String institutionName;
  String background;
  String grade;
  String medium;
  String detailAddress;
  String createdAt;
  String updatedAt;

  Educations(
      {this.eduId,
      this.studentId,
      this.institutionType,
      this.institutionName,
      this.background,
      this.grade,
      this.medium,
      this.detailAddress,
      this.createdAt,
      this.updatedAt});

  Educations.fromJson(Map<String, dynamic> json) {
    eduId = json['edu_id'];
    studentId = json['student_id'];
    institutionType = json['institution_type'];
    institutionName = json['institution_name'];
    background = json['background'];
    grade = json['grade'];
    medium = json['medium'];
    detailAddress = json['detail_address'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['edu_id'] = this.eduId;
    data['student_id'] = this.studentId;
    data['institution_type'] = this.institutionType;
    data['institution_name'] = this.institutionName;
    data['background'] = this.background;
    data['grade'] = this.grade;
    data['medium'] = this.medium;
    data['detail_address'] = this.detailAddress;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Reviews {
  int reviewId;
  int tutorId;
  int studentId;
  int classId;
  int prefer;
  int rate;
  String feedback;
  TutorDetails tutorDetails;

  Reviews(
      {this.reviewId,
      this.tutorId,
      this.studentId,
      this.classId,
      this.prefer,
      this.rate,
      this.feedback,
      this.tutorDetails});

  Reviews.fromJson(Map<String, dynamic> json) {
    reviewId = json['review_id'];
    tutorId = json['tutor_id'];
    studentId = json['student_id'];
    classId = json['class_id'];
    prefer = json['prefer'];
    rate = json['rate'];
    feedback = json['feedback'];
    tutorDetails = json['tutor_details'] != null
        ? new TutorDetails.fromJson(json['tutor_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['review_id'] = this.reviewId;
    data['tutor_id'] = this.tutorId;
    data['student_id'] = this.studentId;
    data['class_id'] = this.classId;
    data['prefer'] = this.prefer;
    data['rate'] = this.rate;
    data['feedback'] = this.feedback;
    if (this.tutorDetails != null) {
      data['tutor_details'] = this.tutorDetails.toJson();
    }
    return data;
  }
}

class TutorDetails {
  int tutorId;
  String name;
  String dp;

  TutorDetails({this.tutorId, this.name, this.dp});

  TutorDetails.fromJson(Map<String, dynamic> json) {
    tutorId = json['tutor_id'];
    name = json['name'];
    dp = json['dp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tutor_id'] = this.tutorId;
    data['name'] = this.name;
    data['dp'] = this.dp;
    return data;
  }
}

class Classes {
  int classId;
  String code;
  int tutorId;
  int studentId;
  int subjectId;
  String topicId;
  String timestamp;
  String hourlyRate;
  String totalHour;
  double hourComplete;
  String payment;
  String paymentStatus;
  String status;
  String createdAt;
  String updatedAt;
  Subject subject;
  Student student;

  Classes(
      {this.classId,
      this.code,
      this.tutorId,
      this.studentId,
      this.subjectId,
      this.topicId,
      this.timestamp,
      this.hourlyRate,
      this.totalHour,
      this.hourComplete,
      this.payment,
      this.paymentStatus,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.subject,
      this.student});

  Classes.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    code = json['code'];
    tutorId = json['tutor_id'];
    studentId = json['student_id'];
    subjectId = json['subject_id'];
    topicId = json['topic_id'];
    timestamp = json['timestamp'];
    hourlyRate = json['hourly_rate'];
    totalHour = json['total_hour'];
    hourComplete = json['hour_complete'];
    payment = json['payment'];
    paymentStatus = json['payment_status'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    subject =
        json['subject'] != null ? new Subject.fromJson(json['subject']) : null;
    student =
        json['student'] != null ? new Student.fromJson(json['student']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['class_id'] = this.classId;
    data['code'] = this.code;
    data['tutor_id'] = this.tutorId;
    data['student_id'] = this.studentId;
    data['subject_id'] = this.subjectId;
    data['topic_id'] = this.topicId;
    data['timestamp'] = this.timestamp;
    data['hourly_rate'] = this.hourlyRate;
    data['total_hour'] = this.totalHour;
    data['hour_complete'] = this.hourComplete;
    data['payment'] = this.payment;
    data['payment_status'] = this.paymentStatus;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.subject != null) {
      data['subject'] = this.subject.toJson();
    }
    if (this.student != null) {
      data['student'] = this.student.toJson();
    }
    return data;
  }
}

class Subject {
  int subjectId;
  String segmentId;
  String medium;
  String subject;
  Null description;

  Subject(
      {this.subjectId,
      this.segmentId,
      this.medium,
      this.subject,
      this.description});

  Subject.fromJson(Map<String, dynamic> json) {
    subjectId = json['subject_id'];
    segmentId = json['segment_id'];
    medium = json['medium'];
    subject = json['subject'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subject_id'] = this.subjectId;
    data['segment_id'] = this.segmentId;
    data['medium'] = this.medium;
    data['subject'] = this.subject;
    data['description'] = this.description;
    return data;
  }
}

class Student {
  int studentId;
  String uid;
  String name;
  String email;
  String contact;
  String medium;
  int segmentId;
  String altPhone;
  num rating;
  String status;
  String dp;
  String about;
  String joined;

  Student(
      {this.studentId,
      this.uid,
      this.name,
      this.email,
      this.contact,
      this.medium,
      this.segmentId,
      this.altPhone,
      this.rating,
      this.status,
      this.dp,
      this.about,
      this.joined});

  Student.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    uid = json['uid'];
    name = json['name'];
    email = json['email'];
    contact = json['contact'];
    medium = json['medium'];
    segmentId = json['segment_id'];
    altPhone = json['alt_phone'];
    rating = json['rating'];
    status = json['status'];
    dp = json['dp'];
    about = json['about'];
    joined = json['joined'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['uid'] = this.uid;
    data['name'] = this.name;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['medium'] = this.medium;
    data['segment_id'] = this.segmentId;
    data['alt_phone'] = this.altPhone;
    data['rating'] = this.rating;
    data['status'] = this.status;
    data['dp'] = this.dp;
    data['about'] = this.about;
    data['joined'] = this.joined;
    return data;
  }
}
