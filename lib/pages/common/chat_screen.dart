import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/pages/common/drawer_Content.dart';
import 'package:Heilo/services/firestore_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_time_format/date_time_format.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:random_string/random_string.dart';

class ChatScreen extends StatefulWidget {
  final String chatWithContact;
  final String chatWithName;

  ChatScreen({@required this.chatWithContact, @required this.chatWithName});
  //
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  //
  String chatRoomId;
  String messageId;
  String myName, myProfilePic, myUserName, myEmail, myContact, tempContact;

  var messageTextController = TextEditingController();

  Stream messageStream;

  // Get Data from SharedPreferences
  getMyInfoFromSharedPref() async {
    myName = await SharedPreferenceHelper().getUserName();
    myProfilePic = await SharedPreferenceHelper().getUserProfilePic();
    myUserName = await SharedPreferenceHelper().getUserName();
    myEmail = await SharedPreferenceHelper().getUserEmail();
    tempContact = await SharedPreferenceHelper().getUserContact();
    myContact = tempContact.substring(3);
    print(myContact);
    setState(() {
      chatRoomId =
          getChatRoomIdByUserContacts(widget.chatWithContact, myContact);
    });
  }

  // Generate chatroom ID by using usernames
  getChatRoomIdByUserContacts(String a, String b) {
    if (int.parse(a) > int.parse(b)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }

  // Add Messages
  addMessage(bool sendClicked) {
    String message =
        messageTextController.text.trim(); //<=========== trim the messeage here
    if (message != '' || null) {
      var lastMessageTs = DateTime.now();

      Map<String, dynamic> messageInfoMap = {
        "message": message,
        "sendBy": myUserName,
        "ts": lastMessageTs,
        "profileUrl": myProfilePic
      };

      // messageId
      if (messageId == "") {
        messageId = randomAlphaNumeric(12);
      }

      DatabaseMethods()
          .addMessage(
        chatRoomId,
        messageId,
        messageInfoMap,
      )
          .then((value) {
        Map<String, dynamic> lastMessageInfoMap = {
          "lastMessage": message,
          "lastMessageSendTs": lastMessageTs,
          "lastMessageSendBy": myUserName
        };

        DatabaseMethods().updateLastMessageSend(chatRoomId, lastMessageInfoMap);

        if (sendClicked) {
          // Remove texts from texfield after sending
          messageTextController.clear();

          // Make the messageId blank to regenerate next msg send
          messageId = "";
          // setState(() {});
        }
      });
    } else {
      return;
    }
  }

  // Get previous messages/ history || realtime
  getAndSetMessages() async {
    messageStream = await DatabaseMethods().getChatRoomMessages(chatRoomId);
    setState(() {});
  }

  // Chat MessageTile
  Widget chatMessageTile(String message, bool sendByMe, Timestamp timeStamp) {
    return Column(
      children: [
        Row(
          mainAxisAlignment:
              sendByMe ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: [
            Flexible(
              child: Container(
                // color: Colors.blue,
                padding: EdgeInsets.all(16),
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: Text(
                  message,
                  style: TextStyle(
                    color: sendByMe ? Colors.white : Colors.black87,
                    fontWeight: FontWeight.bold,
                  ),
                  maxLines: 10,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                ),
                decoration: BoxDecoration(
                  color: sendByMe ? AssetStrings.color4 : Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                    bottomLeft:
                        sendByMe ? Radius.circular(30) : Radius.circular(0),
                    bottomRight:
                        sendByMe ? Radius.circular(0) : Radius.circular(30),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 2,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),

        //================================== DATE FORMAT with CHIKON BUDDHI ================================///
        Container(
          margin:
              sendByMe ? EdgeInsets.only(right: 20) : EdgeInsets.only(left: 20),
          alignment: sendByMe ? Alignment.centerRight : Alignment.centerLeft,
          child: (DateTimeFormat.format(timeStamp.toDate(), format: 'M j') ==
                  DateTimeFormat.format(Timestamp.now().toDate(),
                      format: 'M j'))
              ? Text(
                  DateTimeFormat.format(timeStamp.toDate(), format: 'h:i a'),
                  style: TextStyle(
                    fontSize: 10,
                    color: Colors.blueGrey,
                  ),
                )
              : Text(
                  DateTimeFormat.format(timeStamp.toDate(),
                      format: 'M j, h:i a'),
                  style: TextStyle(
                    fontSize: 10,
                    color: Colors.blueGrey,
                  ),
                ),
        ),
      ],
    );
  }

  // Chat Messages Widget
  Widget chatMessages() {
    return StreamBuilder(
      stream: messageStream,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                itemCount: snapshot.data.docs.length,
                itemBuilder: (context, index) {
                  DocumentSnapshot ds = snapshot.data.docs[index];
                  return chatMessageTile(
                      ds["message"], myUserName == ds["sendBy"], ds["ts"]);
                },
                reverse: true,
              )
            : Center(
                child: CircularProgressIndicator(),
              );
      },
    );
  }

  // Function for init state
  doThisOnLaunch() async {
    await getMyInfoFromSharedPref();
    getAndSetMessages();
  }

  @override
  void initState() {
    print("CHAT WITH CONTACT: ${widget.chatWithContact}");
    print("CHAT WITH NAME: ${widget.chatWithName}");

    super.initState();
    doThisOnLaunch();
  }

  // Main widget body
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      // endDrawer: DrawerContent(),
      extendBody: true,
      // appBar: AppBar(
      //   title: Text(widget.name),
      //   centerTitle: true,
      //   elevation: 0,
      //   leading: Center(
      //     child: GestureDetector(
      //       child: FaIcon(
      //         FontAwesomeIcons.angleLeft,
      //         size: 30,
      //         color: Colors.white,
      //       ),
      //       onTap: () {
      //         Navigator.of(context).pop();
      //       },
      //     ),
      //   ),
      // ),
      body: SafeArea(
        child: Container(
          child: Stack(
            fit: StackFit.expand,
            children: [
              Positioned(
                top: 0,
                child: Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  color: AssetStrings.color4,
                ),
              ),
              Positioned(
                // top: MediaQuery.of(context).size.width * 0.2,
                child: Container(
                  // alignment: Alignment.bottomCenter,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(
                    bottom: 70,
                  ),
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.2,
                    // left: 20,
                    // right: 20,
                    // bottom: 60,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                    ),
                    color: AssetStrings.color1,
                  ),
                  child: chatMessages(),
                ),
              ),
              Container(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 10,
                  ),
                  padding: EdgeInsets.symmetric(
                    horizontal: 8,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 9,
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: AssetStrings.color4,
                              width: 2,
                              style: BorderStyle.solid,
                            ),
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.white.withOpacity(0.8),
                          ),
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: TextField(
                              controller: messageTextController,
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Type a message...",
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey,
                                ),
                              ),
                              onChanged: (value) {
                                // addMessage(false);
                              },
                              onSubmitted: (value) async {
                                await Future.delayed(
                                    Duration(milliseconds: 500));

                                addMessage(true);
                              },
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () async {
                            await Future.delayed(Duration(milliseconds: 500));
                            addMessage(true);
                          },
                          // child: Icon(
                          //   Icons.send,
                          //   color: AssetStrings.color4.withOpacity(0.8),
                          // ),
                          child: Image.asset(
                            'assets/images/send.png',
                            height: 50,
                            width: 50,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                  top: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          child: FaIcon(
                            FontAwesomeIcons.angleLeft,
                            size: 35,
                            color: Colors.white,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                        Text(
                          widget.chatWithName,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Opacity(
                          opacity: 0,
                          child: FaIcon(
                            FontAwesomeIcons.angleLeft,
                            size: 35,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
