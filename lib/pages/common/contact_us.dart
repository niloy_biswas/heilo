import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/pages/common/drawer_Content.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  //
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneNumController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  ScrollController sController = ScrollController();
  //
  @override
  Widget build(BuildContext context) {
    //
    // double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      // endDrawer: DrawerContent(),
      extendBody: true,
      body: SafeArea(
        child: Stack(
          children: [
            ListView(
              // mainAxisSize: MainAxisSize.min,
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/images/contact-us.png',
                    width: sWidth * 0.9,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ContactUsInputField1(
                  controller: nameController,
                  hintText: 'NAME',
                ),
                SizedBox(
                  height: 20,
                ),
                ContactUsInputField1(
                  controller: phoneNumController,
                  hintText: 'PHONE NUMBER',
                ),
                SizedBox(
                  height: 20,
                ),
                ContactUsInputField1(
                  controller: emailController,
                  hintText: 'EMAIL',
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  // color: Colors.grey,
                  width: MediaQuery.of(context).size.width,
                  height: 120,
                  padding: EdgeInsets.symmetric(vertical: 5),
                  // color: Colors.white,
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 3),
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 3,
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: TextField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    scrollController: sController,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      hintText: 'MESSAGE',
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(20),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 120,
                  ),
                  child: MaterialButton(
                    onPressed: () {},
                    child: Text(
                      'SEND',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    shape: StadiumBorder(),
                    color: AssetStrings.color4,
                    height: 40,
                    elevation: 3,
                  ),
                ),
              ],
            ),
            Positioned(
              top: 20,
              right: 20,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: FaIcon(
                  FontAwesomeIcons.times,
                  size: 30,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ContactUsInputField1 extends StatelessWidget {
  const ContactUsInputField1({
    Key key,
    @required this.controller,
    @required this.hintText,
  }) : super(key: key);

  final TextEditingController controller;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      margin: EdgeInsets.symmetric(horizontal: 40),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 3),
            color: Colors.grey.withOpacity(0.5),
            blurRadius: 3,
            spreadRadius: 1,
          ),
        ],
      ),
      child: TextField(
        controller: controller,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
