import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/pages/common/contact_us.dart';
import 'package:Heilo/pages/common/privacy_policy.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:Heilo/pages/student/profile_view.dart';
import 'package:Heilo/pages/student/student_profile.dart';
import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:Heilo/pages/teacher/profile_view.dart';
import 'package:Heilo/pages/teacher/teacher_profile.dart';
import 'package:Heilo/services/services.dart';
// import 'package:Heilo/pages/common/test_page.dart';
import 'package:flutter/material.dart';

class DrawerContent extends StatefulWidget {
  @override
  _DrawerContentState createState() => _DrawerContentState();
}

class _DrawerContentState extends State<DrawerContent> {
  String userType;
  String userName;
  String userContact;
  Services serv = Services();
  StudentUserInfoModel futureStudentInfo = StudentUserInfoModel();
  TutorUserInfoModel futureTutorInfo = TutorUserInfoModel();

  // bool _isLoading;

  _getData() async {
    // setState(() {
    //   _isLoading = true;
    // });

    userType = await SharedPreferenceHelper().getUserType();
    userName = await SharedPreferenceHelper().getUserName();
    userContact = await SharedPreferenceHelper().getUserContact();
    if (userType == 'student') {
      await serv.getDioStudentInfo(refresh: false).then((value) {
        setState(() {
          futureStudentInfo = value;
        });
      });
      // await serv.getDioStudentInfo(refresh: true).then((value) {
      //   setState(() {
      //     futureStudentInfo = value;
      //   });
      // });
    } else if (userType == 'tutor') {
      await serv.getTutorUserInfo(refresh: false).then((value) {
        setState(() {
          futureTutorInfo = value;
        });
      });
      // await serv.getTutorUserInfo(refresh: true).then((value) {
      //   setState(() {
      //     futureTutorInfo = value;
      //   });
      // });
    }
    // setState(() {
    //   _isLoading = false;
    // });
  }

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    // Text Style declare
    TextStyle _drawItemStyle = TextStyle(
        fontFamily: 'QuickSandBold', fontSize: 20, fontWeight: FontWeight.w900);
    //

    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.7,
        height: MediaQuery.of(context).size.height * 1,
        color: AssetStrings.color1,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 3,
                        blurRadius: 3,
                      ),
                    ],
                  ),
                  child: (userType == 'student')
                      ? Align(
                          alignment: Alignment.topCenter,
                          child: CircleAvatar(
                            maxRadius: 35,
                            backgroundImage: (futureStudentInfo.data.dp == null)
                                ? AssetImage("assets/images/home.png")
                                : NetworkImage(futureStudentInfo.data.dp),
                          ),
                        )
                      : Align(
                          alignment: Alignment.topCenter,
                          child: CircleAvatar(
                              maxRadius: 35,
                              backgroundImage: (futureTutorInfo.data.dp == null)
                                  ? AssetImage("assets/images/home.png")
                                  : NetworkImage(futureTutorInfo.data.dp)),
                        ),
                ),
                SizedBox(
                  width: 20,
                ),
                Container(
                  // width: MediaQuery.of(context).size.width * 0.3,
                  // height: 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        userName ?? 'User Name',
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          fontFamily: 'QuickSandBold',
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      Text(
                        userContact ?? 'Contact No',
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          fontFamily: 'QuickSandMedium',
                          // fontWeight: FontWeight.w900,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      // Navigator.pop(context);
                      if (userType == 'student') {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => StudentBottomNav()),
                            (route) => false);
                      } else {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TeacherBottomNav()),
                            (route) => false);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'Home',
                        style: _drawItemStyle,
                      ),
                    ),
                  ),
                  Divider(
                    height: 1,
                    thickness: 0.5,
                  ),

                  //========================== PROFILE BTN ========================//
                  GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'Profile',
                        style: _drawItemStyle,
                      ),
                    ),
                    onTap: () {
                      if (userType == 'student') {
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(
                        //     builder: (context) => StudentProfile(
                        //       isPopable: true,
                        //     ),
                        //   ),
                        // );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => StudentOwnProfile(
                              isPopable: true,
                            ),
                          ),
                        );
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TutorOwnProfile(
                              isPopable: true,
                            ),
                          ),
                        );
                      }
                    },
                  ),
                  Divider(
                    height: 1,
                    thickness: 0.5,
                  ),

                  //============================= PROFILE EDIT BTN =====================//
                  GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'Edit Profile',
                        style: _drawItemStyle,
                      ),
                    ),
                    onTap: () async {
                      if (userType == 'student') {
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => StudentProfile(
                              isPopable: true,
                            ),
                          ),
                        );
                        _getData();
                      } else {
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TutorProfile()));
                        _getData();
                      }
                    },
                  ),
                  Divider(
                    height: 1,
                    thickness: 0.5,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PrivacyPolicy()));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'Privacy Policy',
                        style: _drawItemStyle,
                      ),
                    ),
                  ),
                  Divider(
                    height: 1,
                    thickness: 0.5,
                  ),
                  InkWell(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'Contact Us',
                        style: TextStyle(
                          fontFamily: 'QuickSandBold',
                          color: Colors.green,
                          fontSize: 20,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => ContactUs()));
                    },
                  ),
                  Divider(
                    height: 1,
                    thickness: 0.5,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: GestureDetector(
                      onTap: () {
                        serv.userLogOut(context);
                      },
                      child: Text(
                        'Log Out',
                        style: TextStyle(
                          fontFamily: 'QuickSandBold',
                          color: Colors.red,
                          fontSize: 20,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
