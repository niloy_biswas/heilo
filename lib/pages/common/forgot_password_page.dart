import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/common/sign_in_otp_page.dart';
import 'package:Heilo/widgets/customInputField.dart';
import 'package:Heilo/widgets/customPasswordInputField.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ForgotPasswordPage extends StatefulWidget {
  final String userType;
  const ForgotPasswordPage({
    Key key,
    @required this.userType,
  }) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  var inputPhoneController = TextEditingController();
  var inputPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            // color: AssetStrings.color1,
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // Top back button
                Row(
                  children: [
                    // BackTextButton(
                    //   color: Colors.blueGrey[600],
                    // ),
                    //////////////////////////////// BACK BUTTON //////////////////////////////////
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                      child: GestureDetector(
                        onTap: () async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          await prefs.clear();
                          Navigator.pop(context);
                          // Navigator.pushReplacement(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => InitialPage()));
                        },
                        child: Text(
                          '<',
                          style: TextStyle(
                            fontFamily: 'QuickSandMedium',
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                            color: Colors.blueGrey[600],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                // Welcome message
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(50, 40, 0, 0),
                      child: Text(
                        'Reset Password!',
                        style: TextStyle(
                            fontFamily: 'QuickSandMedium',
                            fontSize: 26,
                            color: Colors.blueGrey),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: sHeight * 0.1,
                ),

                Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      // Input Field for Phone Numbers
                      CustomInputField(
                        fieldName: 'Phone',
                        inputPhoneController: inputPhoneController,
                      ),
                      SizedBox(
                        height: 30,
                      ),

                      // Input Field for Password
                      CustomPasswordInputField(
                        fieldName: 'New Password',
                        inputPhoneController: inputPasswordController,
                      ),
                      SizedBox(
                        height: 30,
                      ),

                      // Submit Button
                      Container(
                        height: 40,
                        width: sWidth * 0.5,
                        child: MaterialButton(
                          color: AssetStrings.color4,
                          child: Text(
                            'NEXT',
                            style: TextStyle(
                              fontFamily: 'QuickSandRegular',
                              color: Colors.white,
                              fontSize: 18,
                              shadows: <Shadow>[
                                Shadow(
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 5.0,
                                  color: Colors.blueGrey.withOpacity(0.5),
                                ),
                              ],
                            ),
                          ),
                          shape: StadiumBorder(),
                          elevation: 8,
                          onPressed: () async {
                            FocusScope.of(context).unfocus();

                            print(inputPhoneController.text);
                            print(inputPasswordController.text);
                            // Save Contact No

                            var phone = inputPhoneController.text.trim();
                            var password = inputPasswordController.text.trim();

                            await SharedPreferenceHelper()
                                .saveRegMobile(inputPhoneController.text);

                            if ((phone != '' && phone.length == 11) &&
                                password != '') {
                              await Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SignInOtp(
                                    forgotPass: true,
                                    newPassword: inputPasswordController.text,
                                  ),
                                ),
                              );
                            } else {
                              customToast("Please check all the fields!");
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
