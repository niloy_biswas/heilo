import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';

class InitialPage extends StatefulWidget {
  @override
  _InitialPageState createState() => _InitialPageState();
}

class _InitialPageState extends State<InitialPage> {
  @override
  Widget build(BuildContext context) {
    //
    var sHeight = MediaQuery.of(context).size.height;
    var sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        color: AssetStrings.color1,
        height: sHeight,
        width: sWidth,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: sHeight * 0.15,
              child: Image.asset(AssetStrings.mainLogo),
            ),
            SizedBox(
              height: sHeight * 0.15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  child: InkWell(
                    onTap: () async {
                      print('study button tapped');
                      await SharedPreferenceHelper().saveUserType('student');
                      var type = await SharedPreferenceHelper().getUserType();
                      print(type);
                      Navigator.pushReplacementNamed(context, '/loginPage');
                    },
                    child: Column(
                      children: [
                        SizedBox(
                          height: sHeight * 0.08,
                          child: Image.asset(AssetStrings.ic_book),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(
                            'Study',
                            style: loginBtnText(
                              sHeight * 0.04,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      '/',
                      style: TextStyle(
                          fontSize: sHeight * 0.12,
                          color: Colors.blueGrey,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
                SizedBox(
                  child: InkWell(
                    onTap: () async {
                      print('Teach button tapped');
                      await SharedPreferenceHelper().saveUserType('tutor');
                      var type = await SharedPreferenceHelper().getUserType();
                      print(type);
                      Navigator.pushReplacementNamed(context, '/loginPage');
                    },
                    child: Column(
                      children: [
                        SizedBox(
                          height: sHeight * 0.08,
                          child: Image.asset(AssetStrings.ic_teacher),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(
                            'Teach',
                            style: loginBtnText(
                              sHeight * 0.04,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  // TextStyle for Login Buttons (Student / Teacher)
  TextStyle loginBtnText(double textsize) {
    return TextStyle(
      fontSize: textsize,
      color: Colors.grey[800],
      fontWeight: FontWeight.w400,
      fontFamily: 'QuickSandMedium',
      letterSpacing: 0,
      shadows: <Shadow>[
        Shadow(
          offset: Offset(1.0, -1.0),
          blurRadius: 3.0,
          color: Colors.blueGrey,
        ),
        Shadow(
          offset: Offset(1.0, 2.0),
          blurRadius: 8.0,
          color: Colors.blueGrey,
        ),
      ],
    );
  }
}
