import 'package:Heilo/models/location_model.dart';
import 'package:Heilo/services/services.dart';
import 'package:flutter/material.dart';

class LocationTest extends StatefulWidget {
  @override
  _LocationTestState createState() => _LocationTestState();
}

class _LocationTestState extends State<LocationTest> {
  LocationModel locModel = LocationModel();
  Services serv = Services();

  _getData() async {
    // await serv.getLocations().then((value) => locModel = value);
    await serv
        .getDioLocations(refresh: false)
        .then((value) => locModel = value);
    setState(() {});
  }

  @override
  void initState() {
    _getData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: locModel.data == null
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: locModel.data.length,
              itemBuilder: (context, index) {
                return Text(locModel.data[index].area);
              },
            ),
    );
  }
}
