import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/common/forgot_password_page.dart';
import 'package:Heilo/pages/common/initial_page.dart';
import 'package:Heilo/pages/common/signup_terms_and_conditions.dart';
import 'package:Heilo/services/services.dart';
// import 'package:Heilo/pages/student/bottom_nav.dart';
// import 'package:Heilo/pages/teacher/bottom_nav.dart';
// import 'package:Heilo/widgets/backTextButton.dart';
import 'package:Heilo/widgets/customInputField.dart';
import 'package:Heilo/widgets/customPasswordInputField.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/gradientButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // Input Controllers
  var inputPhoneController = TextEditingController();
  var inputPasswordController = TextEditingController();
  //
  Services serv = Services();

  ////////////////////////// CUSTOM TOAST /////////////////////////
  // customToast(String content) {
  //   return Fluttertoast.showToast(
  //       msg: content,
  //       toastLength: Toast.LENGTH_SHORT,
  //       gravity: ToastGravity.BOTTOM,
  //       timeInSecForIosWeb: 1,
  //       backgroundColor: Colors.red[900],
  //       textColor: Colors.white,
  //       fontSize: 16.0);
  // }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            // color: AssetStrings.color1,
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // Top back button
                Row(
                  children: [
                    // BackTextButton(
                    //   color: Colors.blueGrey[600],
                    // ),
                    //////////////////////////////// BACK BUTTON //////////////////////////////////
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                      child: GestureDetector(
                        onTap: () async {
                          // Navigator.pop(context);
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          await prefs.clear();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => InitialPage()));
                        },
                        child: Text(
                          '<',
                          style: TextStyle(
                            fontFamily: 'QuickSandMedium',
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                            color: Colors.blueGrey[600],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                // Welcome message
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(50, 40, 0, 0),
                      child: Text(
                        'Welcome back!',
                        style: TextStyle(
                            fontFamily: 'QuickSandMedium',
                            fontSize: 26,
                            color: Colors.blueGrey),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: sHeight * 0.1,
                ),

                // Input Field for Phone Numbers
                CustomInputField(
                  fieldName: 'Phone',
                  inputPhoneController: inputPhoneController,
                ),
                SizedBox(
                  height: 30,
                ),

                // Input Field for Password
                CustomPasswordInputField(
                  fieldName: 'Password',
                  inputPhoneController: inputPasswordController,
                ),
                SizedBox(
                  height: 30,
                ),

                // Login Button
                Container(
                  height: 40,
                  width: sWidth * 0.5,
                  child: MaterialButton(
                    color: AssetStrings.color3,
                    child: Text(
                      'LOG IN',
                      style: TextStyle(
                        fontFamily: 'QuickSandRegular',
                        color: Colors.white,
                        fontSize: 18,
                        shadows: <Shadow>[
                          Shadow(
                            offset: Offset(0.0, 2.0),
                            blurRadius: 5.0,
                            color: Colors.blueGrey.withOpacity(0.5),
                          ),
                        ],
                      ),
                    ),
                    shape: StadiumBorder(),
                    elevation: 8,
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      print(inputPhoneController.text);
                      print(inputPasswordController.text);
                      // Save Contact No
                      await SharedPreferenceHelper()
                          .saveRegMobile(inputPhoneController.text);

                      var userType =
                          await SharedPreferenceHelper().getUserType();
                      if (inputPhoneController.text != '' &&
                          inputPasswordController.text != '') {
                        if (userType == 'student') {
                          EasyLoading.show(status: 'Please Wait...');

                          await serv.studentLogin(
                              context,
                              "+88${inputPhoneController.text}",
                              inputPasswordController.text);
                          // Navigator.of(context).pushReplacement(MaterialPageRoute(
                          //     builder: (context) => StudentBottomNav()));
                          EasyLoading.dismiss();
                        } else {
                          EasyLoading.show(status: 'Please Wait...');

                          await serv.tutorLogin(
                              context,
                              "+88${inputPhoneController.text}",
                              inputPasswordController.text);
                          // Navigator.of(context).pushReplacement(MaterialPageRoute(
                          //     builder: (context) => TeacherBottomNav()));
                          EasyLoading.dismiss();
                        }
                      } else {
                        customToast("Please fill up all the items!");
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                TextButton(
                  child: Text("Forgot password? Click Here"),
                  onPressed: () async {
                    var userType = await SharedPreferenceHelper().getUserType();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            ForgotPasswordPage(userType: userType),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: sHeight * 0.1,
                ),

                // No account text
                Text(
                  'if you don\'t have an account,',
                  style: TextStyle(fontFamily: 'QuickSandMedium'),
                ),
                SizedBox(
                  height: 10,
                ),

                // Sign Up button
                GradientButton(
                  buttonName: 'SIGN UP',
                  sHeight: sHeight,
                  sWidth: sWidth,
                  function: () {
                    FocusScope.of(context).unfocus();
                    // print('Sign up button');
                    // Navigator.pushNamed(context, '/signUpPage');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SignUpTermsAndConditions()));
                  },
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
