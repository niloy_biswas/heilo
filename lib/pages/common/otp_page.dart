import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
// import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:Heilo/services/firestore_database.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/gradientButton.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class OtpPage extends StatefulWidget {
  // final String phoneNumber;

  // OtpPage({Key key, @required this.phoneNumber});
  //
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  String tempOtpNum;
  String _verificationCode;
  String type;
  String mobNo;
  String uid;
  String medium;
  String segmentId;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Services serv = Services();

  //////////////////////////////// SIGN UP INFO //////////////////////////////////
  String userName;
  String userEmail;
  String userPassword;
  String hourlyRate;
  //////////////////////////////// TUTOR LOCATION INFO ////////////////////////////
  String regDistrict;
  String regArea;
  String regRoad;
  String regLocId;
  String regDetailAddress;
  //////////////////////////////// TUTOR SKILL INFO ///////////////////////////////
  String regSkillMedium;
  String regSkillClass;
  String regSkillSubject;
  String regSkillSubjectId;

  _getUserInfo() async {
    type = await SharedPreferenceHelper().getUserType();
    mobNo = await SharedPreferenceHelper().getRegMobile();
    segmentId = await SharedPreferenceHelper().getRegSegmentId();
    medium = await SharedPreferenceHelper().getRegMedium();
    userName = await SharedPreferenceHelper().getRegName();
    userEmail = await SharedPreferenceHelper().getRegEmail();
    userPassword = await SharedPreferenceHelper().getRegPassword();
    hourlyRate = await SharedPreferenceHelper().getRegHourlyRate();
    regDistrict = await SharedPreferenceHelper().getRegDistrict();
    regArea = await SharedPreferenceHelper().getRegArea();
    regRoad = await SharedPreferenceHelper().getRegRoad();
    regLocId = await SharedPreferenceHelper().getLocationId();

    regDetailAddress = await SharedPreferenceHelper().getRegDetailAddress();
    regSkillMedium = await SharedPreferenceHelper().getRegSkillMedium();
    regSkillClass = await SharedPreferenceHelper().getRegSkillClass();
    regSkillSubject = await SharedPreferenceHelper().getRegSkillSub();
    regSkillSubjectId = await SharedPreferenceHelper().getRegSkillSubjectId();
    print(mobNo);
    _verifyPhone();
  }

  ///////////////////////////// FIREBASE UID /////////////////////////////////////
  _getUid() {
    setState(() {
      uid = FirebaseAuth.instance.currentUser.uid;
    });
    print(uid);
  }

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          height: sHeight,
          width: sWidth,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xff7EDD27), Color(0xff26D892)],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: sHeight * 0.030,
                    ),
                    Container(
                      height: sWidth * 0.45,
                      width: sWidth * 0.45,
                      child: Image.asset('assets/images/otpImage.png'),
                    ),
                    SizedBox(
                      height: sHeight * 0.030,
                    ),
                    Text(
                      'OTP',
                      style: TextStyle(
                        fontFamily: 'QuickSandBold',
                        fontSize: 40,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: sHeight * 0.010,
                    ),
                    Center(
                      child: FittedBox(
                        child: Text(
                          'Please enter the OTP sent to your mobile number',
                          style: TextStyle(
                            fontFamily: 'QuickSandMedium',
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: sHeight * 0.070,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  height: sHeight * 0.5,
                  width: sWidth,
                  decoration: BoxDecoration(
                    color: AssetStrings.color1,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(120.0),
                      // topRight: Radius.circular(50.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 3,
                        offset: Offset(0, -4),
                      ),
                    ],
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: sHeight * 0.12,
                        ),

                        //================================= OTP TEXTFIELD ==============================///

                        OTPTextField(
                          length: 6,
                          width: sWidth * 0.8,
                          fieldWidth: 40,
                          style: TextStyle(fontSize: 20),
                          textFieldAlignment: MainAxisAlignment.spaceAround,
                          fieldStyle: FieldStyle.box,
                          onCompleted: (pin) {
                            setState(
                              () {
                                tempOtpNum = pin;
                              },
                            );
                            print("Completed: " + pin);
                            print(tempOtpNum);
                          },
                        ),
                        SizedBox(
                          height: sHeight * 0.08,
                        ),
                        Text(
                          'Didn\'t recieve an OTP?',
                          style: TextStyle(
                            fontFamily: 'QuickSandMedium',
                            color: Colors.grey,
                          ),
                        ),
                        SizedBox(
                          height: sHeight * 0.01,
                        ),

                        // ================================== RE SEND OTP ========================================//
                        GestureDetector(
                          child: Text(
                            'Re-send OTP',
                            style: TextStyle(
                              fontFamily: 'QuickSandBold',
                              color: Colors.grey[700],
                              fontSize: 18,
                              fontWeight: FontWeight.w900,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                          onTap: () {
                            print('OTP resend function clicked!');
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OtpPage()));
                          },
                        ),
                        SizedBox(
                          height: sHeight * 0.04,
                        ),
                        GradientButton(
                          buttonName: 'SIGN UP',
                          sWidth: sWidth,
                          sHeight: sHeight,
                          function: () async {
                            print('sign up button pressed!');

                            print(type);
                            print(mobNo);

                            EasyLoading.show(status: 'Loading...');
                            try {
                              if (type == 'student') {
                                print("SIGN UP STARTED FOR TYPE $type");

                                await _onSubmit();
                                await _getUid();

                                Map<String, dynamic> userInfoMap = {
                                  "userName": userName,
                                  "contactNo": mobNo,
                                };

                                await serv.studentSignUp(
                                  name: userName,
                                  email: userEmail,
                                  mobile: "+88$mobNo",
                                  password: userPassword,
                                  // otpToken: "otptoken",
                                  medium: medium,
                                  segmentId: segmentId,
                                  uid: uid,
                                );

                                await DatabaseMethods()
                                    .addUserInfoToDB(uid, userInfoMap);

                                print('Student sign up successful');
                                //
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            StudentBottomNav()),
                                    (route) => false);
                                //
                                EasyLoading.dismiss();

                                /////////////////// FREAKING TUTOR SIGN UP //////////////////////
                                ///
                              } else if (type == 'tutor') {
                                // EasyLoading.show(status: 'Loading...');
                                print("SIGN UP STARTED FOR TYPE $type");
                                print("step 1 start");

                                await _onSubmit();
                                await _getUid();

                                Map<String, dynamic> userInfoMap = {
                                  "userName": userName,
                                  "contactNo": mobNo,
                                };

                                await serv.tutorSignUp(
                                  name: userName,
                                  email: userEmail,
                                  mobile: "+88$mobNo",
                                  password: userPassword,
                                  uid: uid,
                                  hourlyRate: hourlyRate,
                                  medium: medium,
                                );
                                print("step 1 finished");

                                print("step 2 start");

                                await DatabaseMethods()
                                    .addUserInfoToDB(uid, userInfoMap);

                                print("step 2 finish");

                                await Future.delayed(Duration(seconds: 2));

                                print("step 3 start");
                                print(
                                    "DIST: $regDistrict, AREA: $regArea, ROAD: $regRoad, DETAIL: $regDetailAddress");
                                await serv.addTutorLocation(
                                  locId: regLocId,
                                );
                                // customToast("Address Added");

                                print("step 3 finished");

                                print("step 4 started");
                                TutorUserInfoModel futureTutor =
                                    TutorUserInfoModel();
                                await serv
                                    .getTutorUserInfo(refresh: true)
                                    .then((value) {
                                  setState(() {
                                    futureTutor = value;
                                  });
                                });

                                int tutorId = futureTutor.data.tutorId;

                                print(
                                    "TUTOR ID: $tutorId || SEGMENT ID: $regSkillClass ||  SUBJECT ID: $regSkillSubjectId");
                                await serv.saveTutorSkills(
                                    tutorId: tutorId,
                                    segmentId: int.parse(regSkillClass),
                                    subjectId: int.parse(regSkillSubjectId),
                                    topicId: "");

                                print("step 4 finished");

                                print('sign up successful');
                                //
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            TeacherBottomNav()),
                                    (route) => false);
                                //

                                EasyLoading.dismiss();
                              }
                            } catch (e) {
                              print(e);
                              customToast('Something Went Wrong');
                              EasyLoading.dismiss();

                              return;
                            }
                            // EasyLoading.dismiss();
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _verifyPhone() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: '+88${mobNo.toString()}',
      verificationCompleted: (PhoneAuthCredential credential) async {
        await FirebaseAuth.instance.signInWithCredential(credential);
        //     .then((value) async {
        //   if (value != null && type == 'student') {
        //     print('user logged in');
        //     Navigator.of(context).pushAndRemoveUntil(
        //         MaterialPageRoute(builder: (context) => StudentBottomNav()),
        //         (route) => false);
        //   } else if (value != null && type == 'tutor') {
        //     Navigator.of(context).pushAndRemoveUntil(
        //         MaterialPageRoute(builder: (context) => TeacherBottomNav()),
        //         (route) => false);
        //   }
        // });
      },
      verificationFailed: (FirebaseAuthException e) {
        print(e.toString());
      },
      codeSent: (String verificationID, int resendToken) {
        setState(() {
          _verificationCode = verificationID;
        });
      },
      codeAutoRetrievalTimeout: (String verificationID) {
        setState(() {
          _verificationCode = verificationID;
        });
      },
      // timeout: Duration(seconds: 60),
    );
  }

  _onSubmit() async {
    try {
      await FirebaseAuth.instance.signInWithCredential(
        PhoneAuthProvider.credential(
          verificationId: _verificationCode,
          smsCode: tempOtpNum,
        ),
      );
      //     .then(
      //   (value) async {
      //     if (value.user != null && type == 'student') {
      //       print('pass to student home');
      //       Navigator.of(context).pushAndRemoveUntil(
      //           MaterialPageRoute(builder: (context) => StudentBottomNav()),
      //           (route) => false);
      //     } else if (value.user != null && type == 'tutor') {
      //       print('pass to tutor home');
      //       Navigator.of(context).pushAndRemoveUntil(
      //           MaterialPageRoute(builder: (context) => TeacherBottomNav()),
      //           (route) => false);
      //     }
      //   },
      // );
    } catch (e) {
      // FocusScope.of(context).unfocus();
      // _scaffoldKey.currentState.showSnackBar(
      //   SnackBar(
      //     content: Text('Invalid OTP'),
      //   ),
      // );
    }
  }
}
