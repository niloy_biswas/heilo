import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PrivacyPolicy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var sHeight = MediaQuery.of(context).size.height;
    var sWidth = MediaQuery.of(context).size.width;

    TextStyle _textStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);

    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: SafeArea(
        child: Container(
          height: sHeight,
          width: sWidth,
          child: Stack(
            alignment: Alignment.topCenter,
            fit: StackFit.loose,
            children: [
              Positioned(
                top: 0,
                child: Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  color: AssetStrings.color4,
                ),
              ),
              Positioned(
                top: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: FaIcon(
                          FontAwesomeIcons.angleLeft,
                          size: 35,
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        "Privacy Policy",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Opacity(
                        opacity: 0,
                        child: FaIcon(
                          FontAwesomeIcons.angleLeft,
                          size: 35,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                // height: sHeight,
                width: sWidth,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.width * 0.2,
                  // bottom: 20,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                  ),
                  color: AssetStrings.color1,
                ),
                child: ListView(
                  // shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  children: [
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: Text(
                          "1.  According to our privacy policy, any information of the Heilo user provided in the application (Names, Mobile Numbers, Email address, Addresses, Payment information) is not subject to be disclosed and extremely confidential.",
                          style: _textStyle,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: Text(
                          "2. Heilo is always respectful to Law, Regulations, Acts. If needed Heilo may provide assistance to the administration.",
                          style: _textStyle,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: Text(
                          "3. Heilo can change its Privacy Policy anytime.",
                          style: _textStyle,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
