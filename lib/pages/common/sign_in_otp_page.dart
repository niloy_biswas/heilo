import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/common/initial_page.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/gradientButton.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class SignInOtp extends StatefulWidget {
  final bool forgotPass;
  final String newPassword;

  const SignInOtp({Key key, this.forgotPass, this.newPassword})
      : super(key: key);

  @override
  _SignInOtpState createState() => _SignInOtpState();
}

class _SignInOtpState extends State<SignInOtp> {
  String mobNo;
  String uid;
  var sessId;
  String type;
  String tempOtpNum;
  String _verificationCode;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Services serv = Services();

  _getUserInfo() async {
    mobNo = await SharedPreferenceHelper().getRegMobile();
    print("CONTACT NO: $mobNo");
    type = await SharedPreferenceHelper().getUserType();
    print("USER TYPE: $type");
    _verifyPhone();
  }

  _getUid() async {
    setState(() {
      uid = FirebaseAuth.instance.currentUser.uid;
    });
    sessId = await FirebaseAuth.instance.currentUser.getIdToken();

    print(uid);
    print("SESSION ID TOKEN (JWT) =>  $sessId");
  }

  @override
  void initState() {
    super.initState();
    _getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: FittedBox(
            child: Container(
              alignment: Alignment.center,
              height: sHeight - MediaQuery.of(context).padding.top,
              width: sWidth,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xff7EDD27), Color(0xff26D892)],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Column(
                      children: [
                        SizedBox(
                          height: sHeight * 0.030,
                        ),
                        Container(
                          height: sWidth * 0.45,
                          width: sWidth * 0.45,
                          child: Image.asset('assets/images/otpImage.png'),
                        ),
                        SizedBox(
                          height: sHeight * 0.030,
                        ),
                        Text(
                          'OTP',
                          style: TextStyle(
                            fontFamily: 'QuickSandBold',
                            fontSize: 40,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: sHeight * 0.010,
                        ),
                        Center(
                          child: FittedBox(
                            child: Text(
                              'Please enter the OTP sent to your mobile number',
                              style: TextStyle(
                                fontFamily: 'QuickSandMedium',
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: sHeight * 0.070,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: sHeight * 0.5,
                      width: sWidth,
                      decoration: BoxDecoration(
                        color: AssetStrings.color1,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(120.0),
                          // topRight: Radius.circular(50.0),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 3,
                            offset: Offset(0, -4),
                          ),
                        ],
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: sHeight * 0.08,
                            ),
                            OTPTextField(
                              length: 6,
                              width: sWidth * 0.8,
                              fieldWidth: 40,
                              style: TextStyle(fontSize: 20),
                              textFieldAlignment: MainAxisAlignment.spaceAround,
                              fieldStyle: FieldStyle.box,
                              onCompleted: (pin) {
                                setState(
                                  () {
                                    tempOtpNum = pin;
                                  },
                                );
                                print("Completed: " + pin);
                                print(tempOtpNum);
                              },
                            ),
                            SizedBox(
                              height: sHeight * 0.08,
                            ),
                            Text(
                              'Didn\'t recieve an OTP?',
                              style: TextStyle(
                                fontFamily: 'QuickSandMedium',
                                color: Colors.grey,
                              ),
                            ),
                            SizedBox(
                              height: sHeight * 0.01,
                            ),
                            InkWell(
                              child: Text(
                                'Re-send OTP',
                                style: TextStyle(
                                  fontFamily: 'QuickSandBold',
                                  color: Colors.grey[700],
                                  fontSize: 18,
                                  fontWeight: FontWeight.w900,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                              onTap: () {
                                print('OTP resend function clicked!');
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SignInOtp()));
                              },
                            ),
                            SizedBox(
                              height: sHeight * 0.04,
                            ),
                            GradientButton(
                              buttonName: 'SIGN IN',
                              sWidth: sWidth,
                              sHeight: sHeight,
                              function: () async {
                                print(type);
                                print(mobNo);
                                try {
                                  if (type == 'student') {
                                    EasyLoading.show(status: 'Loading...');

                                    await _onSubmit();
                                    await _getUid();

                                    if (widget.forgotPass == true) {
                                      try {
                                        await serv.studentPassReset(
                                          password: widget.newPassword,
                                          token: sessId,
                                        );
                                        customToast(
                                            "Use your new password to log in!");
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    InitialPage()),
                                            (route) => false);
                                      } catch (e) {
                                        customToast(
                                            'Something went wrong, please try again later!');
                                        EasyLoading.dismiss();
                                        Navigator.pop(context);
                                        return;
                                      }
                                    } else {
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  StudentBottomNav()),
                                          (route) => false);
                                    }

                                    EasyLoading.dismiss();
                                  } else if (type == 'tutor') {
                                    EasyLoading.show(status: 'Loading...');

                                    await _onSubmit();
                                    await _getUid();

                                    if (widget.forgotPass == true) {
                                      try {
                                        await serv.tutorPassReset(
                                          password: widget.newPassword,
                                          token: sessId,
                                        );
                                        customToast(
                                            "Use your new password to log in!");
                                        EasyLoading.dismiss();
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    InitialPage()),
                                            (route) => false);
                                      } catch (e) {
                                        customToast(
                                            'Something went wrong, please try again later!');
                                        EasyLoading.dismiss();
                                        Navigator.pop(context);
                                        return;
                                      }
                                    } else {
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  TeacherBottomNav()),
                                          (route) => false);
                                      EasyLoading.dismiss();
                                    }
                                  }
                                } catch (e) {
                                  print(e);
                                  customToast('Invalid OTP');
                                  EasyLoading.dismiss();

                                  return;
                                }
                              },
                            ),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _verifyPhone() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      // phoneNumber: '+88${widget.phoneNumber}',
      phoneNumber: '+88${mobNo.toString()}',
      verificationCompleted: (PhoneAuthCredential credential) async {
        await FirebaseAuth.instance.signInWithCredential(credential);
        //     .then((value) async {
        //   if (value != null && type == 'student') {
        //     print('user logged in');
        //     Navigator.of(context).pushAndRemoveUntil(
        //         MaterialPageRoute(builder: (context) => StudentBottomNav()),
        //         (route) => false);
        //   } else if (value != null && type == 'tutor') {
        //     Navigator.of(context).pushAndRemoveUntil(
        //         MaterialPageRoute(builder: (context) => TeacherBottomNav()),
        //         (route) => false);
        //   }
        // });
      },
      verificationFailed: (FirebaseAuthException e) {
        print(e.toString());
      },
      codeSent: (String verificationID, int resendToken) {
        setState(() {
          _verificationCode = verificationID;
        });
      },
      codeAutoRetrievalTimeout: (String verificationID) {
        setState(() {
          _verificationCode = verificationID;
        });
      },
      // timeout: Duration(seconds: 60),
    );
  }

  _onSubmit() async {
    try {
      await FirebaseAuth.instance
          .signInWithCredential(
        PhoneAuthProvider.credential(
          verificationId: _verificationCode,
          smsCode: tempOtpNum,
        ),
      )
          .then((value) {
        print(value);
      });
      //     .then(
      //   (value) async {
      //     if (value.user != null && type == 'student') {
      //       print('pass to student home');
      //       Navigator.of(context).pushAndRemoveUntil(
      //           MaterialPageRoute(builder: (context) => StudentBottomNav()),
      //           (route) => false);
      //     } else if (value.user != null && type == 'tutor') {
      //       print('pass to tutor home');
      //       Navigator.of(context).pushAndRemoveUntil(
      //           MaterialPageRoute(builder: (context) => TeacherBottomNav()),
      //           (route) => false);
      //     }
      //   },
      // );
    } catch (e) {
      FocusScope.of(context).unfocus();
      // _scaffoldKey.currentState.showSnackBar(
      // SnackBar(
      // content: Text('Invalid OTP'),
      // ),
      // );
    }
  }
}
