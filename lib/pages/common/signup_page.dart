import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/segment_model.dart';
// import 'package:Heilo/pages/common/initial_page.dart';
import 'package:Heilo/pages/common/login_page.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/class_list_dialog.dart';
import 'package:Heilo/widgets/customInputField.dart';
import 'package:Heilo/widgets/customPasswordInputField.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/gradientButton.dart';
import 'package:Heilo/widgets/medium_list_dialog.dart';
import 'package:Heilo/widgets/signup/hourly_rate_dialog.dart';
// import 'package:Heilo/widgets/signup/medium_dialog.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController mobileNoController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();
  final TextEditingController hourlyRateController = TextEditingController();

  GlobalKey _scaffoldKey = GlobalKey();
  String userType;
  String segmentId;
  String mediumOfStudy;

  List<String> mediumList = ['Bangla', 'English'];
  List<int> classList = [];
  SegmentModel segmentModel = SegmentModel();

  //
  String selectedMedium;
  String selectedSegmentId;
  String mediumButtonString = "Medium";
  String classButtonString = "Class";

  // String mediumButtonText = "Medium";

  Services serv = Services();

  // _customToast(String content) {
  //   return Fluttertoast.showToast(
  //       msg: content,
  //       toastLength: Toast.LENGTH_SHORT,
  //       gravity: ToastGravity.BOTTOM,
  //       timeInSecForIosWeb: 1,
  //       backgroundColor: Colors.black,
  //       textColor: Colors.white,
  //       fontSize: 16.0);
  // }

  _getData() async {
    userType = await SharedPreferenceHelper().getUserType();
    setState(() {});
    print(userType);
  }

  @override
  void initState() {
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //

    //
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AssetStrings.color1,
      // resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Row(
                children: [
                  // BackTextButton(
                  //   color: Colors.blueGrey[600],
                  // ),
                  //////////////////////////////// BACK BUTTON //////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                    child: GestureDetector(
                      onTap: () {
                        // Navigator.pop(context);
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      },
                      child: Text(
                        '<',
                        style: TextStyle(
                          fontFamily: 'QuickSandMedium',
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                          color: Colors.blueGrey[600],
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              // Welcome message
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 40, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Welcome',
                          style: TextStyle(
                              fontFamily: 'QuickSandMedium',
                              fontSize: 20,
                              color: Colors.blueGrey),
                        ),
                        Text(
                          'Sign up',
                          style: TextStyle(
                              fontFamily: 'QuickSandMedium',
                              fontSize: 26,
                              color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: sHeight * 0.1,
              ),
              Expanded(
                child: Container(
                  height: sHeight * 0.6,
                  decoration: BoxDecoration(
                    color: AssetStrings.color4,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50.0),
                      topRight: Radius.circular(50.0),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: sHeight * 0.08,
                        ),
                        CustomInputField(
                          fieldName: 'Name',
                          inputPhoneController: nameController,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomInputField(
                          fieldName: 'E-mail',
                          inputPhoneController: emailController,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomInputField(
                          fieldName: 'Mobile Number',
                          inputPhoneController: mobileNoController,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomPasswordInputField(
                          fieldName: 'Enter Password',
                          inputPhoneController: passwordController,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomPasswordInputField(
                          fieldName: 'Confirm Password',
                          inputPhoneController: confirmPasswordController,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 50),
                          height: 40,
                          width: double.infinity,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // SizedBox(
                              //   width: 10,
                              // ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  // padding: EdgeInsets.symmetric(horizontal: 50),
                                  height: 40,
                                  width: double.infinity,

                                  ////////////////////////////////// SELECT MEDIUM /////////////////////////////
                                  ///
                                  child: GestureDetector(
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            blurRadius: 5,
                                            offset: Offset(0, 7),
                                          ),
                                        ],
                                      ),
                                      child: Container(
                                        // alignment: Alignment.centerLeft,
                                        width: double.infinity,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              mediumButtonString ?? "Medium",
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87,
                                              ),
                                            ),
                                            FaIcon(
                                              FontAwesomeIcons.caretDown,
                                              size: 14,
                                              color: Colors.black54,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    onTap: () async {
                                      FocusScope.of(context).unfocus();
                                      setState(() {
                                        classList = [];
                                        SharedPreferenceHelper()
                                            .saveRegSkillClass("Class");
                                      });

                                      classButtonString =
                                          await SharedPreferenceHelper()
                                              .getRegSkillClass();

                                      setState(() {});

                                      await mediumListDialog(
                                        context: context,
                                        mediumList: mediumList,
                                      );
                                      mediumButtonString =
                                          await SharedPreferenceHelper()
                                              .getRegSkillMedium();
                                      setState(() {});

                                      try {
                                        await serv
                                            .getSegmentData(
                                                refresh: true,
                                                medium: mediumButtonString
                                                    .toLowerCase())
                                            .then((value) {
                                          setState(() {
                                            segmentModel = value;
                                          });
                                        });
                                        for (int i = 0;
                                            i < segmentModel.data.length;
                                            i++) {
                                          print(segmentModel.data[i].segmentId);
                                          classList.add(
                                              segmentModel.data[i].segmentId);
                                        }
                                        classList.toSet();
                                        setState(() {});
                                      } catch (e) {
                                        // customToast("Something went wrong");
                                        print(e);
                                      }
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  // padding: EdgeInsets.symmetric(horizontal: 50),
                                  height: 40,
                                  width: double.infinity,

                                  ////////////////////////////////// SELECT CLASS /////////////////////////////
                                  ///
                                  child: (userType == 'student')
                                      ? GestureDetector(
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 20),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey
                                                      .withOpacity(0.5),
                                                  blurRadius: 5,
                                                  offset: Offset(0, 7),
                                                ),
                                              ],
                                            ),
                                            child: Container(
                                              // alignment: Alignment.centerLeft,
                                              width: double.infinity,
                                              child: Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    classButtonString ??
                                                        "Class",
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      color: Colors.black87,
                                                    ),
                                                  ),
                                                  FaIcon(
                                                    FontAwesomeIcons.caretDown,
                                                    size: 14,
                                                    color: Colors.black54,
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          onTap: () async {
                                            FocusScope.of(context).unfocus();
                                            if (classList.isNotEmpty) {
                                              await classListDialog(
                                                context: context,
                                                classList: classList,
                                              );
                                              classButtonString =
                                                  await SharedPreferenceHelper()
                                                      .getRegSkillClass();
                                              setState(() {});
                                            } else {
                                              customToast(
                                                  "Select Medium First!");
                                            }
                                          },
                                        )
                                      : Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 20),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.5),
                                                blurRadius: 5,
                                                offset: Offset(0, 7),
                                              ),
                                            ],
                                          ),
                                          //============================= HOURLY RATE ===============================//
                                          child: TextField(
                                            controller: hourlyRateController,
                                            textAlign: TextAlign.left,
                                            readOnly: true,
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                              hintText: 'Hourly Rate',
                                              hintStyle: TextStyle(
                                                color: Colors.black87,
                                              ),
                                              border: InputBorder.none,
                                            ),
                                            onTap: () async {
                                              hourlyRateDialog(context: context)
                                                  .then((value) {
                                                setState(() {
                                                  hourlyRateController
                                                    ..text = value;
                                                });
                                              });
                                            },
                                          ),
                                        ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        GradientButton(
                          buttonName: 'NEXT',
                          sWidth: sWidth,
                          sHeight: sHeight,
                          function: () async {
                            // Next Button Function
                            // Navigator.pushNamed(context, '/otpPage');
                            // if (passwordController.text !=
                            //     confirmPasswordController.text) {
                            //   print('Password did not match');
                            // } else if (passwordController.text ==
                            //     confirmPasswordController.text) {
                            //   print('Password matches');
                            // }
                            selectedMedium = await SharedPreferenceHelper()
                                .getRegSkillMedium();
                            selectedSegmentId = await SharedPreferenceHelper()
                                .getRegSkillClass();
                            print(selectedMedium);
                            print(selectedSegmentId);

                            if (nameController.text != '' &&
                                emailController.text != '' &&
                                mobileNoController.text != '' &&
                                passwordController.text != '' &&
                                confirmPasswordController.text != '' &&
                                (selectedMedium != null &&
                                    selectedMedium != "Medium") &&
                                ((selectedSegmentId != null &&
                                        selectedSegmentId != "Class") ||
                                    (hourlyRateController.text != ''))) {
                              if (EmailValidator.validate(
                                  emailController.text)) {
                                print('Email OK');
                                if (passwordController.text !=
                                    confirmPasswordController.text) {
                                  print('Password did not match');
                                  customToast("Passwords did not match");
                                } else {
                                  // _customToast("OTP Sent");
                                  //////////////// SAVE DATA //////////////////
                                  await SharedPreferenceHelper()
                                      .saveRegName(nameController.text);
                                  await SharedPreferenceHelper()
                                      .saveRegEmail(emailController.text);
                                  await SharedPreferenceHelper()
                                      .saveRegMobile(mobileNoController.text);
                                  await SharedPreferenceHelper()
                                      .saveRegPassword(
                                          confirmPasswordController.text);
                                  await SharedPreferenceHelper()
                                      .saveRegSegmentId(selectedSegmentId);
                                  await SharedPreferenceHelper()
                                      .saveRegMedium(selectedMedium);
                                  await SharedPreferenceHelper()
                                      .saveRegHourlyRate(
                                          hourlyRateController.text);

                                  /////////////////////////////////// STUDENT /////////////////////////////////////

                                  if (userType == 'student') {
                                    EasyLoading.show(status: 'Please Wait...');

                                    await serv.getStudentUserExist(context,
                                        "+88${mobileNoController.text}");

                                    EasyLoading.dismiss();

                                    /////////////////////////////////// TUTOR /////////////////////////////////////
                                    ///
                                  } else if (userType == 'tutor') {
                                    EasyLoading.show(status: 'Please Wait...');

                                    await serv.getTutorUserExist(context,
                                        "+88${mobileNoController.text}");

                                    EasyLoading.dismiss();
                                  }

                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => OtpPage()));
                                }
                              } else {
                                print('Email not oK');
                                customToast("Invalid Email");
                                print(emailController.text);
                              }
                            } else {
                              print('empty fields');
                              customToast('Empty fields not allowed');
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
