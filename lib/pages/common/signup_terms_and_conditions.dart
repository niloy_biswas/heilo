import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpTermsAndConditions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var sHeight = MediaQuery.of(context).size.height;
    var sWidth = MediaQuery.of(context).size.width;

    TextStyle _textStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);
    ScrollController _controller = ScrollController();

    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: Container(
        height: sHeight,
        width: sWidth,
        child: Container(
          // height: sHeight,
          width: sWidth,
          // margin: EdgeInsets.only(
          //   top: MediaQuery.of(context).size.width * 0.2,
          //   // bottom: 20,
          // ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25),
            ),
            color: AssetStrings.color1,
          ),
          child: Scrollbar(
            controller: _controller,
            isAlwaysShown: false,
            radius: Radius.circular(50),
            thickness: 10,
            child: ListView(
              // shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              children: [
                Center(
                    child: Padding(
                  padding: const EdgeInsets.only(top: 30.0, bottom: 20),
                  child: Text(
                    "Welcome to Heilo!",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "Heilo is a Android Application based tutor service provider, primarily inside Dhaka City. Heilo provides a platform both Students and Tutors to communicate each other and to hire when needed at hourly basis. By downloading Heilo, you are agreeing to the following Terms of Services and Privacy Policy.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "1.  Heilo is a application based platform for Students and Tutors. You can have an account either as Student or Tutors.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "2. If you want to get registered as a Tutor, you are advised to shared the National Identity document and educational certification to have your profile available to the Students.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "3. You will have to set your hourly rate; the rate is one of the criteria on what a student will consider you for tuition.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "4.  You have to give 15% commission of total earned amount to Heilo platform.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "5. If you want to get registered as Students, you have to give your primary contact information and Academic information accurately (Address, Educational Details, medium of payment).",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "6. You can confirm tuitions any time, but you can cancel tuitions only 4 or more hours before the tuitions.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "7. After tuition is completed, you have to pay the Tutors fee using Helio’s payment gateway within next 12 hours; failure of which will lead to profile inactivation until the payment clearances.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "8.  Students are strictly advised not to take off Heilo tuitions from the tutors. Else, Students profile will be blocked.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "9. Heilo does its part to ensure the safety for both Tutors and Students during a tuition, but both tutors and students are advised to be cautious to prevent any crime (Theft, Burglary, Sexual Harassment, Unauthorized use of corporeal punishment etc). If happens, regular law enforcement forces and policies will be applicable for both accuse and accused. Heilo, as an identity will not take any responsibility of the crime.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "10.  For any circumstances, Tutors and Students are advised to call Heilo Support Center.",
                    style: _textStyle,
                  ),
                ),
                Center(
                    child: Padding(
                  padding: const EdgeInsets.only(top: 30.0, bottom: 20),
                  child: Text(
                    "Privacy Policy",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "1.  According to our privacy policy, any information of the Heilo user provided in the application (Names, Mobile Numbers, Email address, Addresses, Payment information) is not subject to be disclosed and extremely confidential.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "2. Heilo is always respectful to Law, Regulations, Acts. If needed Heilo may provide assistance to the administration.",
                    style: _textStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    "3. Heilo can change its Privacy Policy anytime.",
                    style: _textStyle,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    MaterialButton(
                      child: Text(
                        "Decline",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      splashColor: Colors.transparent,
                      shape: StadiumBorder(),
                      highlightElevation: 0,
                      height: 45,
                      minWidth: 120,
                      color: Colors.redAccent,
                      onPressed: () {
                        // Navigator.pushReplacementNamed(context, '/initialPage');
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/initialPage', (route) => false);
                      },
                    ),
                    MaterialButton(
                      child: Text(
                        "Accept",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      splashColor: Colors.transparent,
                      shape: StadiumBorder(),
                      highlightElevation: 0,
                      height: 45,
                      minWidth: 120,
                      color: AssetStrings.color4,
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, '/signUpPage');
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
