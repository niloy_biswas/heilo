import 'dart:async';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/common/contact_us.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:Heilo/services/notifications.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  String userType;
  String userID;
  String userAccessToken;

  _getData() async {
    try {
      userType = await SharedPreferenceHelper().getUserType();
      userAccessToken = await SharedPreferenceHelper().getUserAccessToken();
      userID = FirebaseAuth.instance.currentUser.uid;
      setState(() {});
    } catch (e) {
      return;
    }
  }

  onNotificationClick(String payload) {
    //
    print('Payload $payload');
    //
    Future.delayed(Duration(seconds: 1)).then((_) async {
      await Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => ContactUs()));
    });
  }

  @override
  void initState() {
    super.initState();
    // Screen Orientation Fix
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    notificationPlugin.setOnNotificationClick(onNotificationClick);

    _getData();

    // Splash Screen Timer
    Timer(Duration(seconds: 3),
        // () => Navigator.pushReplacementNamed(context, '/initialPage'),
        () {
      if (userType == 'student' && userAccessToken != null && userID != null) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => StudentBottomNav()),
            (route) => false);
      } else if (userType == 'tutor' &&
          userAccessToken != null &&
          userID != null) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => TeacherBottomNav()),
            (route) => false);
      } else {
        Navigator.pushReplacementNamed(context, '/initialPage');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //
    var sHeight = MediaQuery.of(context).size.height;
    var sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      body: Container(
        color: AssetStrings.color1,
        height: sHeight,
        width: sWidth,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: sHeight * 0.2,
                width: sWidth * 0.5,
                child: Image.asset(AssetStrings.mainLogo),
              ),
              SizedBox(
                height: sHeight * 0.07,
              ),
              SizedBox(
                child: Text(
                  "Heilo",
                  style: TextStyle(
                    fontSize: sHeight * 0.1,
                    fontFamily: 'Antonellie',
                    foreground: Paint()..shader = AssetStrings.linearGradient,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
