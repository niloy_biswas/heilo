// // import 'package:Heilo/models/location_model.dart';
// // import 'package:Heilo/pages/common/location_test.dart';

// import 'package:Heilo/helper/sharedpref_helper.dart';
// // import 'package:Heilo/models/location_model.dart';
// import 'package:Heilo/pages/common/test_profile.dart';
// import 'package:Heilo/pages/common/thank_you_page.dart';
// // import 'package:Heilo/pages/student/timer_page.dart';
// import 'package:Heilo/pages/student/payment_page.dart';
// // import 'package:Heilo/pages/student/profile_view.dart';
// import 'package:Heilo/pages/teacher/profile_view.dart';
// // import 'package:Heilo/widgets/location_dialog%20copy.dart';
// // import 'package:Heilo/widgets/location_dialog.dart';
// import 'package:Heilo/services/services.dart';
// import 'package:Heilo/widgets/rating/tutor_rating.dart';
// import 'package:Heilo/widgets/subject_dialog.dart';
// import 'package:Heilo/widgets/testWidet.dart';
// import 'package:Heilo/widgets/topic_dialog.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// // import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:focused_menu/focused_menu.dart';
// import 'package:focused_menu/modals.dart';
// // import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// // import 'package:flutter_easyloading/flutter_easyloading.dart';

// class TestPage extends StatefulWidget {
//   @override
//   _TestPageState createState() => _TestPageState();
// }

// class _TestPageState extends State<TestPage> {
//   @override
//   Widget build(BuildContext context) {
//     TextEditingController contactController = TextEditingController();
//     Services serv = Services();
//     // LocationModel locModel = LocationModel();
//     // List locModel = ['Model1', 'Model2', 'Model3'];

//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Test Page'),
//         centerTitle: true,
//       ),
//       body: SingleChildScrollView(
//         child: Center(
//           child: Column(
//             children: [
//               // FlatButton(
//               //   child: Text('Teacher profile from Student'),
//               //   color: Colors.cyan,
//               //   onPressed: () {
//               //     Navigator.push(
//               //         context,
//               //         MaterialPageRoute(
//               //             builder: (context) => TeacherProfileView()));
//               //   },
//               // ),
//               SizedBox(
//                 height: 10,
//               ),
//               FlatButton(
//                 color: Colors.cyan,
//                 onPressed: () {
//                   Navigator.push(context,
//                       MaterialPageRoute(builder: (context) => ThankYouPage()));
//                 },
//                 child: Text('Thank you page'),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               FlatButton(
//                 color: Colors.cyan,
//                 onPressed: () {
//                   // Navigator.push(context,
//                   // MaterialPageRoute(builder: (contex) => TimerPage()));
//                 },
//                 child: Text('Stop Watch'),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               FlatButton(
//                 color: Colors.cyan,
//                 onPressed: () {
//                   Navigator.push(context,
//                       MaterialPageRoute(builder: (contex) => PaymentPage()));
//                 },
//                 child: Text('Payment Page'),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               Container(
//                 padding: EdgeInsets.all(20),
//                 child: TextField(
//                   controller: contactController,
//                   decoration: InputDecoration(
//                     hintText: "Enter number",
//                   ),
//                 ),
//               ),
//               FlatButton(
//                 child: Text('Check OTP'),
//                 // onPressed: () async {
//                 //   EasyLoading.show(status: 'loading...');
//                 //   await serv.getStudentUserExist(
//                 //       context, "+88${contactController.text}");
//                 //   EasyLoading.dismiss();
//                 // },
//                 onPressed: null,
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Location Popup'),
//                 onPressed: () async {
//                   // EasyLoading.showToast("No Internet",
//                   //     toastPosition: EasyLoadingToastPosition.bottom);
//                   List lData;
//                   serv.getDioLocations(refresh: true).then((value) {
//                     for (int i = 0; i < value.data.length; i++) {
//                       lData.add(value.data[i]);
//                     }
//                     print(lData.toSet());
//                   });

//                   // serv
//                   //     .getDioLocations(refresh: true)
//                   //     .then((value) => locationDialog(context, value));
//                   // locationDialog(context);
//                   // Navigator.push(context,
//                   //     MaterialPageRoute(builder: (context) => LocationTest()));
//                 },
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Subject Popup'),
//                 onPressed: () async {
//                   try {
//                     serv
//                         .getDioSubjects(segmentId: '8', refresh: false)
//                         .then((value) => subjectDialog(context, value));
//                   } catch (e) {
//                     EasyLoading.showToast(
//                         "Please Check your internet connection");
//                   }
//                 },
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Topic Popup'),
//                 onPressed: () async {
//                   try {
//                     serv
//                         .getDioTopics(subjectId: '9', refresh: false)
//                         .then((value) => topicDialog(context, value));
//                   } catch (e) {
//                     EasyLoading.showToast(
//                         "Please Check your internet connection");
//                   }
//                 },
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Student Info API Check'),
//                 onPressed: () async {
//                   await serv.getStudentInfo();
//                   Navigator.push(context,
//                       MaterialPageRoute(builder: (context) => TestProfile()));
//                 },
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Student Home Tutor List'),
//                 onPressed: () async {
//                   String locationId =
//                       await SharedPreferenceHelper().getLocationId();
//                   String subjectId =
//                       await SharedPreferenceHelper().getSubjectId();
//                   String topicId = await SharedPreferenceHelper().getTopicId();
//                   print(
//                       "location id: $locationId \nsubject id: $subjectId, \ntopic id: $topicId");
//                   try {
//                     await serv.getDioStudentHomeTutorList(
//                       locationId: '1',
//                       subjectId: '11',
//                       topicId: '9',
//                       refresh: true,
//                     );
//                   } catch (e) {
//                     customToast("Something went wrong!");
//                   }
//                 },
//               ),
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Student Home Tutor Details'),
//                 onPressed: () async {
//                   try {
//                     await serv.getStudentHomeTutorDetails(
//                       tutorId: 1,
//                       refresh: true,
//                     );
//                   } catch (e) {
//                     customToast("Something went wrong!");
//                   }
//                 },
//               ),
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('UID Check'),
//                 onPressed: () async {
//                   try {
//                     var uid = FirebaseAuth.instance.currentUser.uid;
//                     print(uid);
//                   } catch (e) {
//                     print(e);
//                     customToast("Something went wrong!");
//                   }
//                 },
//               ),

//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Tutor Location Update Check'),
//                 onPressed: () async {
//                   try {
//                     // var uid = FirebaseAuth.instance.currentUser.uid;
//                     // print(uid);
//                     Services serv = Services();
//                     await serv.addTutorLocation(
//                         district: "Dhaka",
//                         area: "janina",
//                         road: "Jinga tola",
//                         detailAddress: "608/1");
//                   } catch (e) {
//                     print(e);
//                     customToast("Something went wrong!");
//                   }
//                 },
//               ),

//               // secret code popup

//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('SECRET CODE Check'),
//                 onPressed: () async {
//                   // try {
//                   //   var uid = FirebaseAuth.instance.currentUser.uid;
//                   //   print(uid);
//                   // } catch (e) {
//                   //   print(e);
//                   //   customToast("Something went wrong!");
//                   // }
//                   Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                           builder: (context) => TutorOwnProfile(
//                                 isPopable: true,
//                               )));
//                 },
//               ),

//               //
//               MaterialButton(
//                 color: Colors.cyan,
//                 child: Text('Rating Check'),
//                 onPressed: () async {
//                   // try {
//                   //   var uid = FirebaseAuth.instance.currentUser.uid;
//                   //   print(uid);
//                   // } catch (e) {
//                   //   print(e);
//                   //   customToast("Something went wrong!");
//                   // }
//                   await tutorRatingDialog(context: context);
//                   customToast("Rating Complete");
//                 },
//               ),
//               //
//               fMenu(context, "this is title"),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   ////////////////////////// CUSTOM TOAST /////////////////////////
//   customToast(String content) {
//     return Fluttertoast.showToast(
//         msg: content,
//         toastLength: Toast.LENGTH_SHORT,
//         gravity: ToastGravity.BOTTOM,
//         timeInSecForIosWeb: 1,
//         backgroundColor: Colors.red[900],
//         textColor: Colors.white,
//         fontSize: 16.0);
//   }

//   /// Pop Up Menu ///
//   Widget fMenu(BuildContext context, String title) {
//     // var normColor = Colors.grey;
//     // var flashyColor = Colors.white;
//     // bool _isSelected = false;
//     return FocusedMenuHolder(
//       menuWidth: MediaQuery.of(context).size.width * 0.35,
//       blurSize: 3,
//       menuItemExtent: 50,
//       menuBoxDecoration: BoxDecoration(
//         color: Colors.grey.withOpacity(0),
//         // borderRadius: BorderRadius.all(
//         //   Radius.circular(0.0),
//         // ),
//       ),
//       // duration: Duration(milliseconds: 100),
//       animateMenuItems: false,
//       blurBackgroundColor: Colors.black54,
//       openWithTap: true, // Open Focused-Menu on Tap rather than Long Press
//       // menuOffset: 10.0, // Offset value to show menuItem from the selected item
//       bottomOffsetHeight:
//           90.0, // Offset height to consider, for showing the menu item ( for example bottom navigation bar), so that the popup menu will be shown on top of selected item.
//       child: Container(
//         // padding: const EdgeInsets.only(right: 10),
//         margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
//         child: Text('Click Here'),
//       ),

//       onPressed: () {
//         // setState(() {
//         //   _isSelected = true;
//         // });
//       },
//       menuItems: <FocusedMenuItem>[
//         // Add Each FocusedMenuItem  for Menu Options
//         FocusedMenuItem(
//           backgroundColor: Colors.grey,
//           title: Text(
//             title,
//             style: TextStyle(
//               color: Colors.white,
//             ),
//           ),
//           // trailingIcon: Icon(
//           //   Icons.account_box,
//           //   color: Colors.white,
//           // ),
//           onPressed: () {
//             print(title);
//           },
//         ),
//         FocusedMenuItem(
//             backgroundColor: Colors.grey,
//             title: Text(
//               "Settings",
//               style: TextStyle(
//                 color: Colors.white,
//               ),
//             ),
//             trailingIcon: Icon(
//               Icons.settings,
//               color: Colors.white,
//             ),
//             onPressed: () {
//               Navigator.push(context,
//                   MaterialPageRoute(builder: (context) => TestWidget()));
//             }),
//         // FocusedMenuItem(
//         //     backgroundColor: KothonColors.dialPadHeaderColor,
//         //     title: Text(
//         //       "About",
//         //       style: TextStyle(
//         //         color: KothonColors.offWhite,
//         //       ),
//         //     ),
//         //     trailingIcon: Icon(
//         //       Icons.info,
//         //       color: KothonColors.offWhite,
//         //     ),
//         //     onPressed: () {}),
//         // FocusedMenuItem(
//         //     backgroundColor: KothonColors.dialPadHeaderColor,
//         //     title: Text(
//         //       "Log Out",
//         //       style: TextStyle(
//         //         color: KothonColors.offWhite,
//         //       ),
//         //     ),
//         //     trailingIcon: Icon(
//         //       Icons.logout,
//         //       color: KothonColors.offWhite,
//         //     ),
//         //     onPressed: () {}),
//       ],
//     );
//   }
// }
