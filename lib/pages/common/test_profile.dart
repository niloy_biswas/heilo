import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/services/services.dart';
import 'package:flutter/material.dart';

class TestProfile extends StatefulWidget {
  @override
  _TestProfileState createState() => _TestProfileState();
}

class _TestProfileState extends State<TestProfile> {
  Future<StudentUserInfoModel> futureStudent;
  Services serv = Services();
  _textStyle0(BuildContext context) {
    return TextStyle(
      color: Colors.black87,
      fontFamily: "QuickSandBold",
      fontSize: 13,
    );
  }

  _textStyle1(BuildContext context) {
    return TextStyle(
      color: AssetStrings.color4,
      fontFamily: "QuickSandBold",
      fontSize: 11,
    );
  }

  _textStyle2(BuildContext context) {
    return TextStyle(
      color: Colors.black,
      fontFamily: "QuickSandMedium",
      fontSize: 11,
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      futureStudent = serv.getDioStudentInfo(refresh: false);
    });
  }

  @override
  Widget build(BuildContext context) {
    // final sHeight = MediaQuery.of(context).size.height;
    final sWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: FutureBuilder(
        future: futureStudent,
        builder: (context, snapshot) {
          StudentUserInfoModel apiData = snapshot.data;
          if (snapshot.hasData) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(apiData.data.name),
                  Text(apiData.data.segment.segmentId.toString()),
                  Text(apiData.data.segment.description ?? "Not Available"),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    width: sWidth,
                    // color: Colors.white,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 3,
                            offset: Offset(0, 4)),
                      ],
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              top: 20, right: 20, left: 20, bottom: 0),
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'EDUCATIONAL INFORMATION',
                                style: _textStyle0(context),
                              ),
                              Divider(
                                color: AssetStrings.color4,
                                height: 20,
                                thickness: 2,
                              ),
                            ],
                          ),
                        ),

                        ///! EDUCATION INFORMATION TILE ///
                        MediaQuery.removePadding(
                          context: context,
                          removeTop: true,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: BouncingScrollPhysics(),
                              itemCount: apiData.data.educations.length,
                              itemBuilder: (context, index) {
                                return educationCard(
                                  context: context,
                                  institutionName: apiData
                                      .data.educations[index].institutionName,
                                  grade: apiData.data.educations[index].grade,
                                  background:
                                      apiData.data.educations[index].background,
                                  medium: apiData.data.educations[index].medium,
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error);
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  educationCard({
    @required BuildContext context,
    @required String institutionName,
    @required String grade,
    @required String background,
    @required String medium,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      padding: EdgeInsets.all(10),
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: AssetStrings.color4,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //////////////////////////////// SCHOOL //////////////////////////////////
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ////////////////////! SCHOOL/ COLLEGE NAME ////////////////////
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'SCHOOL/COLLEGE',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      institutionName ?? "Not Avalable",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              //////////////////////! GRADE ////////////////////////////
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'GRADE',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      grade ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //////////////////////! BACKGROUND ////////////////////////////

              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'BACKGROUND',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      background ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),

              //////////////////////! MEDIUM ////////////////////////////

              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'MEDIUM',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      medium ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
