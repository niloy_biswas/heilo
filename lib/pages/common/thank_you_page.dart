import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ThankYouPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FaIcon(
                  FontAwesomeIcons.solidPaperPlane,
                  size: 80,
                  color: AssetStrings.color4,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'THANKS!',
                    style: TextStyle(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Center(
                    child: Text(
                      'YOUR FEEDBACK HAS BEEN RECORDED',
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.normal,
                        color: Colors.blueGrey,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  decoration: BoxDecoration(
                    color: AssetStrings.color4,
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Text(
                    "We will contact soon",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 26,
            right: 26,
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: FaIcon(
                FontAwesomeIcons.arrowRight,
                size: 40,
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
