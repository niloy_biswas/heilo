import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/student/profile_view.dart';
// import 'package:Heilo/pages/common/drawer_Content_home.dart';
import 'package:Heilo/pages/student/student_bell.dart';
import 'package:Heilo/pages/student/student_chat.dart';
import 'package:Heilo/pages/student/student_home.dart';
import 'package:Heilo/pages/student/student_sidebar.dart';
import 'package:community_material_icon/community_material_icon.dart';
// import 'package:Heilo/pages/teacher/teacher_home.dart';
import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';
import 'package:flutter/material.dart';

// import '../common/drawer_Content.dart';

class StudentBottomNav extends StatefulWidget {
  @override
  _StudentBottomNavState createState() => _StudentBottomNavState();
}

class _StudentBottomNavState extends State<StudentBottomNav> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget _child;

  @override
  void initState() {
    super.initState();
    setState(() {
      _child = StudentHome();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      extendBody: true,
      // endDrawer: DrawerContent(),
      body: _child,
      bottomNavigationBar: FluidNavBar(
        icons: [
          FluidNavBarIcon(
              svgPath: "assets/icons/bell.svg",
              // icon: Icons.notifications_none,
              backgroundColor: AssetStrings.color4,
              extras: {"label": "home"}),
          FluidNavBarIcon(
              svgPath: "assets/icons/user.svg",
              // icon: Icons.person,
              backgroundColor: AssetStrings.color4,
              extras: {"label": "bookmark"}),
          FluidNavBarIcon(
              // svgPath: "assets/images/hicon.svg",
              // icon: Icons.home_filled,
              icon: CommunityMaterialIcons.school,
              backgroundColor: AssetStrings.color4,
              extras: {"label": "partner"}),
          FluidNavBarIcon(
              svgPath: "assets/icons/chat.svg",
              // icon: Icons.message,
              backgroundColor: AssetStrings.color4,
              extras: {"label": "conference"}),
          FluidNavBarIcon(
              svgPath: "assets/icons/sidebar.svg",
              // icon: Icons.sort,
              backgroundColor: AssetStrings.color4,
              extras: {"label": "conference"}),
        ],
        onChange: _handleNavigationChange,
        style: FluidNavBarStyle(
          iconUnselectedForegroundColor: Colors.white,
          barBackgroundColor: AssetStrings.color4,
        ),
        scaleFactor: 1.2,
        defaultIndex: 2,
        itemBuilder: (icon, item) => Semantics(
          label: icon.extras["label"],
          child: item,
        ),
        animationFactor: 0.1,
      ),
    );
  }

  void _handleNavigationChange(int index) {
    setState(
      () {
        switch (index) {
          case 0:
            _child = StudentBell();
            break;
          case 1:
            _child = StudentOwnProfile(
              isPopable: false,
            );
            break;
          case 2:
            _child = StudentHome();
            break;
          case 3:
            _child = StudentChat();
            break;
          case 4:
            _child = StudentSideBar();
            // _child = dumdum();
            // Scaffold.of(context).openEndDrawer();
            break;
        }
        _child = AnimatedSwitcher(
          switchInCurve: Curves.easeOut,
          switchOutCurve: Curves.easeIn,
          duration: Duration(milliseconds: 100),
          child: _child,
        );
      },
    );
  }

  dumdum() {
    _scaffoldKey.currentState.openEndDrawer();
    // Scaffold.of(context).openEndDrawer();
    // print('sidebar');
  }
}
