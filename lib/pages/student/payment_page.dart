import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/common/drawer_Content.dart';
// import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/gradientButton.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PaymentPage extends StatefulWidget {
  final String payment;

  const PaymentPage({Key key, @required this.payment}) : super(key: key);
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  @override
  Widget build(BuildContext context) {
    final sHeight = MediaQuery.of(context).size.height;
    final sWidth = MediaQuery.of(context).size.width;

    TextStyle style1 = TextStyle(
      fontSize: 25,
      color: AssetStrings.color4,
    );

    bool _isBkashSelected = false;
    bool _isNagadSelected = false;
    bool _isCardSelected = false;

    return Scaffold(
      backgroundColor: AssetStrings.color4,
      endDrawer: DrawerContent(),
      extendBody: true,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    ///-----------------------BACK BUTTON-----------------------///
                    child: FaIcon(
                      FontAwesomeIcons.angleLeft,
                      size: 35,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    'Payment',
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  Container(
                    width: sWidth,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(25)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset('assets/images/path8.png'),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: sHeight * 0.2,
                          width: sWidth * 0.65,
                          margin: EdgeInsets.all(30),
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(25),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                offset: Offset(0, 3),
                                blurRadius: 3,
                                spreadRadius: 3,
                              ),
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'TOTAL',
                                style: style1,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(bottom: 10.0),
                                    child: Text(
                                      'TK',
                                      style: style1,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),

                                  ///-----------------------PAYMENT AMOUNT-----------------------///
                                  Text(
                                    widget.payment ?? '0',
                                    style: style1.copyWith(
                                      fontSize: 50,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                offset: Offset(0, 3),
                                blurRadius: 3,
                                spreadRadius: 3,
                              ),
                            ],
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 5,
                          ),
                          child: Text('SELECT METHOD'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ///------------------------BKASH SELECT---------------------///
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 15),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.white,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(30),
                                color: _isBkashSelected
                                    ? Colors.white
                                    : Colors.transparent,
                              ),
                              child: GestureDetector(
                                child: Text('Bkash'),
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),

                            ///------------------------NAGAD SELECT---------------------///
                            InkWell(
                              onTap: () {
                                setState(() {
                                  _isBkashSelected = false;
                                  _isCardSelected = false;
                                  _isNagadSelected = true;
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.white,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                  color: _isNagadSelected
                                      ? Colors.white
                                      : Colors.transparent,
                                ),
                                child: Text('Nagad'),
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),

                            ///------------------------CARD SELECT---------------------///
                            InkWell(
                              onTap: () {
                                setState(() {
                                  _isBkashSelected = false;
                                  _isCardSelected = true;
                                  _isNagadSelected = false;
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.white,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                  color: _isCardSelected
                                      ? Colors.white
                                      : Colors.transparent,
                                ),
                                child: Text('Card'),
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 100),
                    alignment: Alignment.bottomCenter,
                    child: GradientButton(
                      buttonName: 'Proceed',
                      sWidth: sWidth,
                      sHeight: sHeight,
                      function: () {
                        // customToast("Payment System Unavaible");
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
