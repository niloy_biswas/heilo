import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/pages/common/drawer_Content.dart';
// import 'package:Heilo/pages/common/drawer_Content.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
// import 'package:Heilo/widgets/profilePicture.dart';
import 'package:Heilo/widgets/review_tile.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_svg_provider/flutter_svg_provider.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StudentOwnProfile extends StatefulWidget {
  final bool isPopable;

  const StudentOwnProfile({Key key, @required this.isPopable})
      : super(key: key);
  @override
  _StudentOwnProfileState createState() => _StudentOwnProfileState();
}

class _StudentOwnProfileState extends State<StudentOwnProfile> {
  Future<StudentUserInfoModel> futureStudent;
  Services serv = Services();

  updatePage() {
    setState(() {
      futureStudent = serv.getDioStudentInfo(refresh: true);
    });
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      futureStudent = serv.getDioStudentInfo(refresh: false);
    });
    updatePage();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      endDrawer: widget.isPopable ? DrawerContent() : null,
      extendBody: true,

      // body:
      body: FutureBuilder(
        future: futureStudent,
        builder: (context, snapshot) {
          StudentUserInfoModel apiData = snapshot.data;
          if (snapshot.hasError) {
            print("ERROR OCCURED WHILE FETCHING PROFILE DATA");
          } else if (snapshot.hasData) {
            return ListView(
              shrinkWrap: true,
              children: [
                Column(
                  children: [
                    Container(
                      height: sHeight * 0.33,
                      width: sWidth,
                      decoration: BoxDecoration(
                        color: AssetStrings.color4,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(
                            height: sWidth * 0.28,
                            width: sWidth * 0.28,
                            child: Stack(
                              alignment: Alignment.center,
                              fit: StackFit.expand,
                              children: [
                                /////////////////////////////// PROFILE PICTURE ////////////////////////////////
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: CircleAvatar(
                                      maxRadius: 50,
                                      backgroundImage: (apiData.data.dp == null)
                                          ? AssetImage("assets/images/home.png")
                                          : NetworkImage(apiData.data.dp)),
                                ),
                                /////////////////////////////// RATING CHIP ////////////////////////////////
                                Positioned(
                                  // top: 0,
                                  right: 0,
                                  top: 0,

                                  child: Container(
                                    height: 20,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                          color: AssetStrings.color4, width: 2),
                                    ),
                                    child: Center(
                                        child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        /////////////////////////////// RATING TEXT ////////////////////////////////
                                        (apiData.data.rating == null)
                                            ? Text('0')
                                            : Text(
                                                apiData.data.rating
                                                    .toStringAsFixed(1),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                        Icon(
                                          Icons.star,
                                          size: 12,
                                          color: AssetStrings.color4,
                                        ),
                                      ],
                                    )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),

                            ///-----------------------TUTOR NAME---------------------///
                            child: Text(
                              apiData.data.name ?? 'Not Available',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54,
                              ),
                            ),
                          ),

                          ///---------------------SCHOOL NAME----------------------///
                          (apiData.data.educations.length < 1)
                              ? Container()
                              : Text(
                                  apiData.data.educations[0].institutionName ??
                                      "Not Available",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: Row(
                          //     mainAxisAlignment: MainAxisAlignment.center,
                          //     children: [
                          //       FaIcon(
                          //         FontAwesomeIcons.mapMarkerAlt,
                          //         color: Colors.white,
                          //         size: 14,
                          //       ),
                          //       SizedBox(
                          //         width: 5,
                          //       ),

                          //       // ///---------------------LOCATION NAME----------------///
                          //       // Text(
                          //       //   "${apiData.data.location.area}, ${apiData.data.location.district}" ??
                          //       //       'Location Not Available',
                          //       //   style: TextStyle(
                          //       //     color: Colors.white,
                          //       //   ),
                          //       // ),
                          //     ],
                          //   ),
                          // ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 30,
                    ),

                    /// RATING and COMPLETED TUTION COUNT ///
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        // SizedBox(
                        //   width: sWidth * 0.15,
                        // ),
                        Container(
                          height: 70,
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: AssetStrings.color4.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ///-----------------COMPLETED TUTION-----------------///
                              Text(
                                apiData.data.totalClassCompleted.toString() ??
                                    '0',
                                style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                'Completed Tutions',
                                style: TextStyle(fontSize: 9),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),

                        /////////////////////////// REFER BY ///////////////////////////
                        Container(
                          height: 70,
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: AssetStrings.color4.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ////////////////////// REFER NO ///////////////////////
                              Text(
                                apiData.data.reviews.length.toString() ?? '0',
                                style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                'Referred By',
                                style: TextStyle(fontSize: 9),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),

                    ///=============================== MEDIUM, BACKGROUND, CLASS ===========================///
                    Container(
                      padding: EdgeInsets.all(16),
                      height: sHeight * 0.2,
                      width: sWidth * 0.85,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 2),
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 5,
                          )
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Medium:',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                width: sWidth * 0.4,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: AssetStrings.color4,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),

                                ///------------------MEDIUM OF STUDY------------------///
                                child: Text(
                                  apiData.data.medium ?? 'Not Available',
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Background:',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                width: sWidth * 0.4,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: AssetStrings.color4,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),

                                ///------------------BACKGROUND OF STUDY------------------///
                                child: (apiData.data.educations.length < 1)
                                    ? Text(
                                        'Not Available',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      )
                                    : Text(
                                        apiData.data.educations[0].background ??
                                            'Not Available',
                                        style: TextStyle(fontSize: 20),
                                      ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Class:',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                width: sWidth * 0.4,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: AssetStrings.color4,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),

                                ///------------------CLASS OF TUTION------------------///
                                child: (apiData.data.educations.length < 1)
                                    ? Text(
                                        'Not Available',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      )
                                    : Text(
                                        apiData.data.educations[0].grade ??
                                            'Not Available',
                                        style: TextStyle(fontSize: 20),
                                      ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 30,
                    ),

                    ///-------------------------ABOUT SECTION-------------------------///
                    Container(
                      padding: EdgeInsets.all(16),
                      // height: sHeight * 0.2,
                      width: sWidth * 0.85,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 2),
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 5,
                          )
                        ],
                      ),
                      child: Column(
                        // shrinkWrap: true,
                        // physics: NeverScrollableScrollPhysics(),
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'About Student:',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),

                          ///--------------------------ABOUT AUTHOR PARAGRAPH---------------------///
                          (apiData.data.about == null)
                              ? SizedBox(
                                  height: 100,
                                  child: Center(
                                    child: Text("Not Available"),
                                  ),
                                )
                              : Text(apiData.data.about ?? "Not Available"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),

                    ///------------------------------ REVIEW SECTION ----------------------------///
                    Container(
                      padding: EdgeInsets.all(16),
                      // height: sHeight * 0.35,
                      width: sWidth * 0.85,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 2),
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 5,
                          )
                        ],
                      ),
                      child: Column(
                        // mainAxisSize: MainAxisSize.min,
                        // shrinkWrap: true,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: Text(
                                'Reviews:',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          // SizedBox(
                          //   height: 10,
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: ReviewTile(),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: ReviewTile(),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: ReviewTile(),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: ReviewTile(),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: ReviewTile(),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: ReviewTile(),
                          // ),
                          (apiData.data.reviews.length < 1)
                              ? SizedBox(
                                  height: 200,
                                  child: Center(
                                    child: Text("Not Available"),
                                  ),
                                )
                              : SizedBox(
                                  height: 200,
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: apiData.data.reviews.length,
                                    itemBuilder: (context, index) {
                                      // return Text(
                                      //     apiData.data.reviews[index].feedback);
                                      if (apiData.data.reviews[index]
                                              .tutorDetails ==
                                          null) {
                                        return Container();
                                      }
                                      return Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 4.0),
                                        child: ReviewTile(
                                          dp: apiData.data.reviews[index]
                                              .tutorDetails.dp,
                                          title: apiData.data.reviews[index]
                                              .tutorDetails.name,
                                          subtitle: apiData
                                              .data.reviews[index].feedback,
                                          rating: apiData
                                              .data.reviews[index].rate
                                              .toString(),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                        ],
                      ),
                      // child: ListView.builder(
                      //   itemCount: apiData.data.reviews.length,
                      //   itemBuilder: (context, index) {
                      //     return Text(apiData.data.reviews[index].feedback);
                      //   },
                      // ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
                // Positioned(
                //   top: 10,
                //   left: 15,
                //   child: widget.isPopable
                //       ? GestureDetector(
                //           ///-----------------------BACK BUTTON-----------------------///
                //           child: FaIcon(
                //             FontAwesomeIcons.angleLeft,
                //             size: 35,
                //             color: Colors.white,
                //           ),
                //           onTap: () {
                //             Navigator.pop(context);
                //           },
                //         )
                //       : Container(
                //           height: 35,
                //           width: 35,
                //         ),
                // ),
                // Positioned(
                //   bottom: 10,
                //   right: 10,
                //   child: FloatingActionButton(
                //     child: Icon(
                //       CommunityMaterialIcons.comment_text,
                //       color: Colors.white,
                //       size: 30,
                //     ),
                //     backgroundColor: AssetStrings.color4,
                //     onPressed: () {},
                //   ),
                // ),
              ],
            );
          }
          return CustomLoading2();
        },
      ),
      // floatingActionButton: FloatingActionButton(
      //   // child: Icon(
      //   //   CommunityMaterialIcons.comment_text,
      //   //   color: Colors.white,
      //   //   size: 30,
      //   // ),
      //   child: Image(
      //     width: 26,
      //     height: 26,
      //     color: Colors.white,
      //     image: Svg("assets/icons/chat.svg"),
      //   ),
      //   backgroundColor: AssetStrings.color4,
      //   onPressed: () {},
      // ),
    );
  }
}
