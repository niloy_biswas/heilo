import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/message_model.dart';
import 'package:Heilo/models/user_model.dart';
import 'package:flutter/material.dart';

class StudentChatScreen extends StatefulWidget {
  final User user;

  const StudentChatScreen({Key key, @required this.user}) : super(key: key);

  @override
  _StudentChatScreenState createState() => _StudentChatScreenState();
}

class _StudentChatScreenState extends State<StudentChatScreen> {
  //
  // Chat bubble custom function
  _chatBubble(Message message, bool isMe, bool isSameUser) {
    if (isMe) {
      return Column(
        children: [
          Container(
            alignment: Alignment.topRight,
            child: Container(
              constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.8,
              ),
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                color: AssetStrings.color4,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                  bottomLeft: Radius.circular(25.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 5,
                  )
                ],
              ),
              child: Text(
                message.text,
                style: TextStyle(
                    color: Colors.blueGrey[700],
                    fontFamily: 'QuickSandBold',
                    fontSize: 15),
              ),
            ),
          ),
          !isSameUser
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Text(
                        message.time,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black45,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(3),
                      margin: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 5,
                          ),
                        ],
                      ),
                      child: CircleAvatar(
                        radius: 15,
                        // backgroundImage: AssetImage('assets/images/ironman.jpeg'),
                        backgroundImage: AssetImage(message.sender.imageUrl),
                      ),
                    ),
                  ],
                )
              : Container(
                  child: null,
                ),
        ],
      );
    } else {
      return Column(
        children: [
          Column(
            children: [
              Container(
                alignment: Alignment.topLeft,
                child: Container(
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * 0.8,
                  ),
                  padding: EdgeInsets.all(20),
                  margin: EdgeInsets.symmetric(vertical: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 5,
                      )
                    ],
                  ),
                  child: Text(
                    message.text,
                    style: TextStyle(
                      color: Colors.blueGrey[700],
                      fontFamily: 'QuickSandBold',
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
              !isSameUser
                  ? Row(
                      children: [
                        Container(
                          margin: EdgeInsets.all(3),
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                              ),
                            ],
                          ),
                          child: CircleAvatar(
                            radius: 15,
                            // backgroundImage: AssetImage('assets/images/ironman.jpeg'),
                            backgroundImage:
                                AssetImage(message.sender.imageUrl),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            message.time,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.black45,
                            ),
                          ),
                        )
                      ],
                    )
                  : Container(
                      child: null,
                    )
            ],
          ),
        ],
      );
    }
  }

  // Message area custom function
  _sendMessageArea() {
    return Row(
      children: [
        // IconButton(
        //   icon: Icon(Icons.photo),
        //   iconSize: 25,
        //   color: Theme.of(context).primaryColor,
        //   onPressed: () {},
        // ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            height: 50,
            // color: Colors.white,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
              border: Border.all(color: AssetStrings.color4, width: 2),
            ),
            child: TextField(
              controller: null,
              decoration: InputDecoration(
                hintText: 'Type a message...',
                hintStyle: TextStyle(
                    fontFamily: 'QuickSandMedium',
                    fontWeight: FontWeight.w800,
                    color: Colors.grey.withOpacity(0.7),
                    fontSize: 18),
                border: InputBorder.none,
              ),
              textCapitalization: TextCapitalization.sentences,
              textAlign: TextAlign.center,
            ),
          ),
        ),
        // IconButton(
        //   icon: Icon(Icons.send),
        //   iconSize: 25,
        //   color: Theme.of(context).primaryColor,
        //   onPressed: () {},
        // ),
        GestureDetector(
          onTap: () {
            print('send button pressed');
          },
          child: Container(
            height: 40,
            width: 40,
            padding: EdgeInsets.only(right: 8),
            child: Image.asset('assets/images/send.png'),
          ),
        ),
      ],
    );
  }

  // Main build widget
  @override
  Widget build(BuildContext context) {
    int prevUserId;
    return Scaffold(
      backgroundColor: AssetStrings.color4,
      appBar: AppBar(
        brightness: Brightness.dark,
        elevation: 0,
        backgroundColor: AssetStrings.color4,
        centerTitle: true,
        title: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: [
            TextSpan(
              text: widget.user.name,
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                fontFamily: 'QuickSandBold',
              ),
            ),
            TextSpan(text: '\n'),
            widget.user.isOnline
                ? TextSpan(
                    text: 'Online',
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'QuickSandRegular',
                    ),
                  )
                : TextSpan(
                    text: 'Offine',
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'QuickSandRegular',
                    ),
                  ),
          ]),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          color: AssetStrings.color1,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25.0),
            // topRight: Radius.circular(25.0),
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                reverse: true,
                padding: EdgeInsets.all(20),
                itemCount: messages.length,
                itemBuilder: (context, index) {
                  final Message message = messages[index];
                  final bool isMe = message.sender.id == currentUser.id;
                  final bool isSameUser = prevUserId == message.sender.id;
                  prevUserId = message.sender.id;
                  return _chatBubble(message, isMe, isSameUser);
                },
              ),
            ),
            _sendMessageArea(),
          ],
        ),
      ),
    );
  }
}
