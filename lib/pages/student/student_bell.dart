import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/models/student_tution_class_list.dart';
import 'package:Heilo/pages/student/timer_page.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/student_upcoming_tution_tile.dart';
import 'package:Heilo/widgets/tution_confirm_dialog_student.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StudentBell extends StatefulWidget {
  @override
  _StudentBellState createState() => _StudentBellState();
}

class _StudentBellState extends State<StudentBell> {
  // StudentTutionClassList studentTutionClassList;

  List pendingStudentClassList = [];

  Services serv = Services();
  bool _isLoading;

  _getData() async {
    setState(() {
      _isLoading = true;
    });
    try {
      pendingStudentClassList.clear();
      await serv.getStudentClassList(refresh: true).then((value) {
        for (int i = 0; i < value.data.length; i++) {
          if (value.data[i].status == "ACCEPT" ||
              value.data[i].status == "PENDING") {
            print(value.data[i].status);
            pendingStudentClassList.add(value.data[i]);
          }
        }
        setState(() {});
      });
      setState(() {
        _isLoading = false;
      });
    } catch (e) {
      setState(() {
        _isLoading = false;
      });
      customToast("Something went wrong!");
    }
  }

  getDataWithoutLoad() async {
    try {
      await serv.getStudentClassList(refresh: true).then((value) {
        for (int i = 0; i < value.data.length; i++) {
          if (value.data[i].status == "ACCEPT" ||
              value.data[i].status == "PENDING") {
            print(value.data[i].status);
            pendingStudentClassList.add(value.data[i]);
          }
        }
        setState(() {});
      });
    } catch (e) {
      customToast("Failed to Load");
    }
  }

  @override
  void initState() {
    super.initState();
    _getData();
    print(pendingStudentClassList.length);
  }

  @override
  Widget build(BuildContext context) {
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: RefreshIndicator(
        onRefresh: () => _getData(),
        child: SafeArea(
          child: Container(
            child: Stack(
              fit: StackFit.loose,
              children: [
                Positioned(
                  top: 0,
                  child: Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    color: AssetStrings.color4,
                  ),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Opacity(
                          opacity: 0,
                          child: FaIcon(
                            FontAwesomeIcons.angleLeft,
                            size: 35,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "Activity",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Opacity(
                          opacity: 0,
                          child: FaIcon(
                            FontAwesomeIcons.angleLeft,
                            size: 35,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  // top: MediaQuery.of(context).size.width * 0.2,
                  child: Container(
                    // alignment: Alignment.bottomCenter,
                    height: sHeight,
                    width: sWidth,
                    padding: EdgeInsets.only(
                      bottom: 00,
                    ),
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.2,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                      ),
                      color: AssetStrings.color1,
                    ),

                    child: _isLoading
                        ? CustomLoading2()
                        : (pendingStudentClassList.length < 1)
                            ? Center(
                                child: Container(
                                  // padding: const EdgeInsets.only(top: 60),
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                      color: AssetStrings.color4,
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    child: Text(
                                      "No Activity",
                                      style: TextStyle(
                                        fontSize: 16,
                                        // fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : Container(
                                // margin: EdgeInsets.only(bottom: 50),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 10,
                                      left: 15,
                                      child: Container(
                                        // width: 300,
                                        alignment: Alignment.topLeft,
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 10,
                                          vertical: 5,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                              color: AssetStrings.color4
                                                  .withOpacity(0.5),
                                              blurRadius: 3,
                                              offset: Offset(0, 2),
                                              spreadRadius: 2,
                                            ),
                                          ],
                                        ),
                                        child: Center(
                                          child: Text(
                                            'UPCOMING',
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      top: 50,
                                      width: sWidth,
                                      // height: sHeight,
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 30),
                                        width: sWidth * 0.7,
                                        height: sHeight * 0.67,
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          itemCount:
                                              pendingStudentClassList.length,
                                          itemBuilder: (context, index) {
                                            return StudentTutionUpcomingTile(
                                              sWidth: sWidth,
                                              upcomingDate: formatDate(
                                                  DateTime.parse(
                                                      pendingStudentClassList[
                                                              index]
                                                          .timestamp),
                                                  [
                                                    dd,
                                                    '/',
                                                    M,
                                                  ]),
                                              subject:
                                                  pendingStudentClassList[index]
                                                      .subject
                                                      .subject,
                                              tutionTime: formatDate(
                                                DateTime.parse(
                                                    pendingStudentClassList[
                                                            index]
                                                        .timestamp),
                                                [
                                                  hh,
                                                  ':',
                                                  nn,
                                                  ' ',
                                                  am,
                                                ],
                                              ),
                                              date:
                                                  pendingStudentClassList[index]
                                                      .timestamp,
                                              address:
                                                  pendingStudentClassList[index]
                                                      .tutor
                                                      .location
                                                      .area,
                                              secretCode:
                                                  pendingStudentClassList[index]
                                                      .code,
                                              tutorId:
                                                  pendingStudentClassList[index]
                                                      .tutorId
                                                      .toString(),
                                              classId:
                                                  pendingStudentClassList[index]
                                                      .classId
                                                      .toString(),
                                              isPending:
                                                  (pendingStudentClassList[
                                                              index]
                                                          .status ==
                                                      "PENDING"),
                                              isAccepted:
                                                  (pendingStudentClassList[
                                                              index]
                                                          .status ==
                                                      "ACCEPT"),
                                              startFunc: () async {
                                                print('start button pressed');
                                                await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        TimerPage(
                                                      classId:
                                                          pendingStudentClassList[
                                                                  index]
                                                              .classId
                                                              .toString(),
                                                      code:
                                                          pendingStudentClassList[
                                                                  index]
                                                              .code,
                                                      startHour: int.parse(
                                                          pendingStudentClassList[
                                                                  index]
                                                              .totalHour
                                                              .split('.')[0]),
                                                      startMinute: int.parse(
                                                          pendingStudentClassList[
                                                                  index]
                                                              .totalHour
                                                              .split('.')[1]),
                                                    ),
                                                  ),
                                                );
                                                await Future.delayed(
                                                    Duration(seconds: 1));
                                                _getData();
                                              },
                                              cancelFunc: () async {
                                                print("cancel button pressed");
                                                await studentTutionConfirmDialog(
                                                  context: context,
                                                  classId:
                                                      pendingStudentClassList[
                                                              index]
                                                          .classId
                                                          .toString(),
                                                  code: pendingStudentClassList[
                                                          index]
                                                      .code,
                                                );
                                                await Future.delayed(
                                                    Duration(seconds: 1));
                                                _getData();
                                              },
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
