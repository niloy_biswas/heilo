import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/common/chat_screen.dart';
import 'package:Heilo/services/firestore_database.dart';
import 'package:Heilo/widgets/chat_room_list_tile.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
// import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:community_material_icon/community_material_icon.dart';
// import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StudentChat extends StatefulWidget {
  @override
  _StudentChatState createState() => _StudentChatState();
}

class _StudentChatState extends State<StudentChat> {
  bool _searchField = false;
  // final _searchController = TextEditingController();
  bool _isLoading = false;
  Stream usersStream;
  Stream chatRoomsStream;

  String myName, myProfilePic, myUserName, myEmail, tempContact, myContact;

  // Search button function
  // _onSearchBtnClicked() async {
  //   setState(() {
  //     _searchField = true;
  //   });

  //   if (_searchController.text != '') {
  //     usersStream =
  //         await DatabaseMethods().getUserByUserName(_searchController.text);
  //     setState(() {});
  //     print('search clicked');
  //   }
  // }

  // Get the chat room streams
  getChatRoomList() async {
    chatRoomsStream = await DatabaseMethods().getChatRooms();
    setState(() {});
  }

  // we wait for sharedpref data and then get the streams
  onScreenLoaded() async {
    await getMyInfoFromSharedPref();
    await getChatRoomList();
    setState(() {
      _isLoading = false;
    });
  }

  // Get Data from SharedPreferences
  getMyInfoFromSharedPref() async {
    myName = await SharedPreferenceHelper().getUserName();
    myProfilePic = await SharedPreferenceHelper().getUserProfilePic();
    myUserName = await SharedPreferenceHelper().getUserName();
    tempContact = await SharedPreferenceHelper().getUserContact();
    myContact = tempContact.substring(3);
    print(myContact);
    myEmail = await SharedPreferenceHelper().getUserEmail();
    setState(() {});
  }

  // Generate chatroom ID by using usernames
  getChatRoomIdByUserContacts(String a, String b) {
    if (int.parse(a) > int.parse(b)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }

  // Custom ListTile for chat list
  Widget searchUserListTile(
      {String profileUrl, String partnerName, String partnerContact}) {
    return GestureDetector(
      onTap: () {
        print('myname: $myContact and partner name: $partnerContact');
        var chatRoomId = getChatRoomIdByUserContacts(myContact, partnerContact);
        Map<String, dynamic> chatRoomInfoMap = {
          "users": [myUserName, partnerName]
        };

        DatabaseMethods().createChatRoom(chatRoomId, chatRoomInfoMap);

        //==================================== GET THIS FOR NEW CHAT ====================================//
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatScreen(
              chatWithName: partnerName,
              chatWithContact: partnerContact,
            ),
          ),
        );
      },
      // child: Row(
      //   children: [
      // ClipRRect(
      //   borderRadius: BorderRadius.circular(50),
      //   child: Image.network(
      //     profileUrl,
      //     height: 50,
      //     width: 50,
      //   ),
      // ),
      child: Container(
        // width: MediaQuery.of(context).size.width,
        // height: 60,
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.only(left: 10, right: 10, top: 5),
        // decoration: BoxDecoration(
        //   color: Colors.white,
        //   borderRadius: BorderRadius.circular(20),
        //   boxShadow: [
        //     BoxShadow(
        //       color: Colors.grey.withOpacity(0.5),
        //       spreadRadius: 1,
        //       blurRadius: 2,
        //       offset: Offset(0, 2),
        //     ),
        //   ],
        // ),

        ///========================================= FIX CHAT SEARCH HERE ===============================================////
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              partnerName ?? "",
              style: TextStyle(fontWeight: FontWeight.w800),
            ),
            Text(partnerContact ?? ""),
            // Text("User Name: $partnerName"),
          ],
        ),
      ),
    );
  }

  // Custom stream widget for users
  Widget searchUsersList() {
    return StreamBuilder(
      stream: usersStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.docs.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              DocumentSnapshot ds = snapshot.data.docs[index];
              return searchUserListTile(
                partnerName: ds.data()["userName"],
                partnerContact: ds.data()["contactNo"],
              );
              // return Image.network(ds["profileUrl"]);
            },
          );
        } else if (snapshot.hasError) {
          print(snapshot.error);
        }

        return CustomLoading2();
      },
    );
  }

  // Custom stream widget for chatrooms
  Widget chatRoomsList() {
    return StreamBuilder(
      stream: chatRoomsStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.docs.length < 1) {
            return Center(
              child: Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: AssetStrings.color4,
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Text(
                  "No Chat History",
                  style: TextStyle(
                    fontSize: 16,
                    // fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            );
          }

          return ListView.builder(
            shrinkWrap: true,
            itemCount: snapshot.data.docs.length,
            itemBuilder: (context, index) {
              DocumentSnapshot ds = snapshot.data.docs[index];
              // return Text(
              //   ds.id.replaceAll(myUserName, "").replaceAll("_", ""),
              // );
              return Container(
                margin: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
                child: ChatRoomListTile(
                  // profilePic: ds.data()["profilePic"],
                  lastMessage: ds.data()["lastMessage"] ?? "",
                  lastMessageSendTs:
                      ds.data()["lastMessageSendTs"] ?? Timestamp.now(),
                  // formatDate(
                  //     DateTime.parse(DateTime.now().toString()), [
                  //   dd,
                  //   '/',
                  //   mm,
                  //   '/',
                  //   yy,
                  //   ' - ',
                  //   hh,
                  //   ':',
                  //   mm,
                  //   ' ',
                  //   am
                  // ]),
                  myUserName: myUserName ?? "",
                  myContact: myContact ?? "",
                  chatRoomId: ds.id ?? "",
                ),
              );
            },
          );
        }
        return CustomLoading2();
      },
    );
  }

  @override
  void initState() {
    super.initState();
    onScreenLoaded();
  }

  @override
  Widget build(BuildContext context) {
    //
    var sHeight = MediaQuery.of(context).size.height;
    var sWidth = MediaQuery.of(context).size.width;
    //

    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: _isLoading
          ? CustomLoading2()
          : SafeArea(
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Container(
                    color: AssetStrings.color4,
                    width: double.infinity,
                    height: 200,
                    padding: EdgeInsets.all(25),
                    child: Container(
                      width: sWidth,
                      child: Text(
                        "Chat",
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   crossAxisAlignment: CrossAxisAlignment.start,
                      //   children: [
                      //     _searchField
                      //         ? Flexible(
                      //             flex: 10,
                      //             child: Container(
                      //               height: 40,
                      //               width: sWidth,
                      //               padding:
                      //                   EdgeInsets.symmetric(horizontal: 16),
                      //               decoration: BoxDecoration(
                      //                 borderRadius: BorderRadius.circular(40),
                      //                 color: Colors.white,
                      //               ),
                      //               child: Expanded(
                      //                 flex: 1,
                      //                 child: Row(
                      //                   children: [
                      //                     GestureDetector(
                      //                       child: Icon(
                      //                         CommunityMaterialIcons.close,
                      //                         color: Colors.black54,
                      //                         size: 28,
                      //                       ),
                      //                       onTap: () async {
                      //                         setState(() {
                      //                           // _isLoading = true;
                      //                           _searchField = false;
                      //                         });
                      //                         getChatRoomList();
                      //                         await Future.delayed(
                      //                             Duration(seconds: 1));
                      //                         await onScreenLoaded();
                      //                         setState(() {});
                      //                       },
                      //                     ),
                      //                     SizedBox(
                      //                       width: 10,
                      //                     ),
                      //                     Expanded(
                      //                       flex: 9,
                      //                       child: TextField(
                      //                         controller: _searchController,
                      //                         decoration: InputDecoration(
                      //                           hintText: "Search",
                      //                           border: InputBorder.none,
                      //                         ),
                      //                       ),
                      //                     ),
                      //                   ],
                      //                 ),
                      //               ),
                      //             ),
                      //           )
                      //         : Flexible(
                      //             flex: 10,
                      //             child: Text(
                      //               "Chat",
                      //               style: TextStyle(
                      //                 fontSize: 30,
                      //                 fontWeight: FontWeight.bold,
                      //                 color: Colors.white,
                      //               ),
                      //             ),
                      //           ),
                      //     SizedBox(
                      //       width: 10,
                      //     ),
                      //     Flexible(
                      //       flex: 1,
                      //       child: Padding(
                      //         padding: const EdgeInsets.only(top: 5.0),
                      //         child: GestureDetector(
                      //           child: FaIcon(
                      //             FontAwesomeIcons.search,
                      //             color: Colors.white,
                      //             size: 28,
                      //           ),
                      //           onTap: () async {
                      //             setState(() {
                      //               _searchField = true;
                      //             });
                      //             print("Search Clicked");

                      //             print(
                      //                 "Search Item: ${_searchController.text}");
                      //             _onSearchBtnClicked();
                      //           },
                      //         ),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                    ),
                  ),
                  Positioned(
                    child: Container(
                      padding: EdgeInsets.only(top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                        ),
                        color: AssetStrings.color1,
                      ),
                      height: sHeight,
                      width: sWidth,
                      margin: EdgeInsets.only(
                        top: sWidth * 0.2,
                        // bottom: 10,
                        // left: 20,
                        // right: 20,
                        // bottom: 60,
                      ),
                      alignment: Alignment.topCenter,
                      child: _searchField ? searchUsersList() : chatRoomsList(),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
