import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_home_tutor_list_model.dart';
// import 'package:Heilo/models/student_tution_class_list.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/pages/student/teacher_profile_view.dart';
import 'package:Heilo/services/notifications.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/StudentHomeButton.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/location/district_dialog.dart';
// import 'package:Heilo/widgets/location_dialog.dart';
import 'package:Heilo/widgets/subject_dialog.dart';
import 'package:Heilo/widgets/topic_dialog.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StudentHome extends StatefulWidget {
  @override
  _StudentHomeState createState() => _StudentHomeState();
}

class _StudentHomeState extends State<StudentHome> {
  Services serv = Services();
  Future<StudentHomeTutorListModel> futureTutorList;
  StudentUserInfoModel futureStudentInfo;
  bool _isLoading;

  String prefLocationId;
  String prefSubjectId;
  String prefTopicId;

  String chipLocation;
  String chipSubject;
  String chipTopic;

  int alarmId = 2;

  getTutorFilterLocalData() async {
    prefLocationId = await SharedPreferenceHelper().getLocationId();
    prefSubjectId = await SharedPreferenceHelper().getSubjectId();
    prefTopicId = await SharedPreferenceHelper().getTopicId();

    chipLocation = await SharedPreferenceHelper().getChipLocation();
    chipSubject = await SharedPreferenceHelper().getChipSubject();
    chipTopic = await SharedPreferenceHelper().getChipTopic();

    setState(() {});
  }

  //================================ NOTIFICATION SERVICE CALL ==============================//
  startBackgroundService() async {
    //==== METHOD 2 =====//
    // await AndroidAlarmManager.initialize();
    // AndroidAlarmManager.periodic(Duration(seconds: 5), 1, fireAlarm);
    AndroidAlarmManager.periodic(Duration(seconds: 60), alarmId, fireAlarm);
  }

  getTutorList() async {
    setState(() {
      _isLoading = true;
    });
    await getTutorFilterLocalData();
    print("$prefLocationId || $prefSubjectId || $prefTopicId");
    if (prefLocationId == null &&
        prefSubjectId == null &&
        prefTopicId == null) {
      setState(() {
        futureTutorList = serv.getDioStudentHomeTutorList(
          // locationId: prefLocationId,
          // subjectId: prefSubjectId,
          // topicId: prefTopicId,
          // refresh: true,

          locationId: null,
          subjectId: null,
          topicId: null,
          refresh: true,
        );
      });
    } else if (prefLocationId != null &&
        prefSubjectId == null &&
        prefTopicId == null) {
      setState(() {
        futureTutorList = serv.getDioStudentHomeTutorList(
          // locationId: prefLocationId,
          // subjectId: prefSubjectId,
          // topicId: prefTopicId,
          // refresh: true,

          locationId: prefLocationId,
          subjectId: null,
          topicId: null,
          refresh: true,
        );
      });
    } else if (prefLocationId == null &&
        prefSubjectId != null &&
        prefTopicId == null) {
      setState(() {
        futureTutorList = serv.getDioStudentHomeTutorList(
          // locationId: prefLocationId,
          // subjectId: prefSubjectId,
          // topicId: prefTopicId,
          // refresh: true,

          locationId: null,
          subjectId: prefSubjectId,
          topicId: null,
          refresh: true,
        );
      });
    } else if (prefLocationId != null &&
        prefSubjectId != null &&
        prefTopicId == null) {
      setState(() {
        futureTutorList = serv.getDioStudentHomeTutorList(
          // locationId: prefLocationId,
          // subjectId: prefSubjectId,
          // topicId: prefTopicId,
          // refresh: true,

          locationId: prefLocationId,
          subjectId: prefSubjectId,
          topicId: null,
          refresh: true,
        );
      });
    } else if (prefLocationId != null &&
        prefSubjectId != null &&
        prefTopicId != null) {
      setState(() {
        futureTutorList = serv.getDioStudentHomeTutorList(
          // locationId: prefLocationId,
          // subjectId: prefSubjectId,
          // topicId: prefTopicId,
          // refresh: true,

          locationId: prefLocationId,
          subjectId: prefSubjectId,
          topicId: prefTopicId,
          refresh: true,
        );
      });
    }
    setState(() {
      _isLoading = false;
    });
  }

  /// Save info for Chat Window
  saveUserInfo() async {
    await serv.getDioStudentInfo(refresh: false).then((value) {
      futureStudentInfo = value;
      setState(() {});
      SharedPreferenceHelper().saveUserName(futureStudentInfo.data.name);
      SharedPreferenceHelper().saveUserEmail(futureStudentInfo.data.email);
      SharedPreferenceHelper().saveUserProfilePic(futureStudentInfo.data.dp);
      SharedPreferenceHelper().saveUserContact(futureStudentInfo.data.contact);
    });
  }

  // onNotificationClick(String payload) {

  // print('Payload $payload');
  // print('hahahaha');
  // Navigator.of(context)
  // .push(MaterialPageRoute(builder: (context) => ContactUs()));
  // }

  @override
  void initState() {
    super.initState();
    serv.getDioStudentInfo(refresh: false);
    getTutorList();
    saveUserInfo();
    startBackgroundService();

    // notificationPlugin.setOnNotificationClick(onNotificationClick);
  }

  @override
  Widget build(BuildContext context) {
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Stack Window"),
      // ),
      backgroundColor: AssetStrings.color1,
      body: RefreshIndicator(
        onRefresh: () => getTutorList(),
        child: Container(
          height: sHeight,
          width: sWidth,
          color: AssetStrings.color1,
          child: Stack(
            alignment: Alignment.topCenter,
            fit: StackFit.expand,
            overflow: Overflow.clip,
            children: [
              // Container(
              //   width: sWidth,
              //   height: sHeight,
              //   // color: Colors.red,
              //   color: AssetStrings.color1,
              // ),
              Positioned(
                bottom: 0,
                child: Container(
                  width: sWidth,
                  height: sHeight * 0.8,
                  // color: Colors.white,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0),
                    ),
                  ),
                ),
              ),

              Positioned(
                top: sHeight * 0.16,
                width: sWidth * 0.98,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ///* LOCATION BUTTON ///
                    ///
                    StudentHomeButton(
                      buttonName: 'LOCATION',
                      sHeight: sHeight,
                      sWidth: sWidth,
                      function: () async {
                        print('Location button clicked!');
                        EasyLoading.show(status: "Loading...");
                        // Future.delayed(Duration(seconds: 1))
                        //     .then((_) => EasyLoading.dismiss());
                        // serv
                        //     .getDioLocations(refresh: true)
                        //     .then((value) => locationDialog(context, value));

                        //============== CODE FOR DISTINCT DISTRICT =============//
                        List distList = [];
                        await serv
                            .getDioLocations(refresh: false)
                            .then((value) {
                          for (int i = 0; i < value.data.length; i++) {
                            // print("${value.data[i].district}");
                            distList.add(value.data[i].district);
                          }
                          distList.toSet().toList();
                          print(distList.toSet().toList());
                        });

                        // //============== CODE FOR DISTINCT AREA =============//
                        // List areaList = [];
                        // await serv.getDioLocations(refresh: false).then(
                        //   (value) {
                        //     for (int i = 0; i < value.data.length; i++) {
                        //       for (var dj in distList.toSet().toList()) {
                        //         if (dj == value.data[i].district) {
                        //           areaList.add(value.data[i].area);
                        //         }
                        //       }
                        //       // print(area.toSet().toList());
                        //       // print("${value.data[i].district}");
                        //       // dist.add(value.data[i].district);
                        //     }
                        //     // print(area.toSet().toList());
                        //   },
                        // );
                        // print(areaList.toSet().toList());

                        // //============== CODE FOR DISTINCT ROAD =============//
                        // List roadList = [];
                        // await serv.getDioLocations(refresh: false).then(
                        //   (value) {
                        //     for (int i = 0; i < value.data.length; i++) {
                        //       for (var dj in areaList.toSet().toList()) {
                        //         if (dj == value.data[i].area) {
                        //           roadList.add(value.data[i].road);
                        //         }
                        //       }
                        //     }
                        //     // print(area.toSet().toList());
                        //   },
                        // );
                        // print(roadList.toSet().toList());

                        // //============== CODE FOR LOCATION ID =============//
                        // List locIdList = [];
                        // await serv.getDioLocations(refresh: false).then(
                        //   (value) {
                        //     for (int i = 0; i < value.data.length; i++) {
                        //       for (var dj in roadList.toSet().toList()) {
                        //         if (dj == value.data[i].road) {
                        //           locIdList.add(value.data[i].locId);
                        //         }
                        //       }
                        //     }
                        //     // print(area.toSet().toList());
                        //   },
                        // );
                        // print(locIdList.toSet().toList());

                        // print(roadList.toSet().toList().length);
                        // print(locIdList.toSet().toList().length);
                        EasyLoading.dismiss();

                        await districtDialog(
                          context: context,
                          districtList: distList.toSet().toList(),
                        );
                        getTutorList();
                      },
                    ),

                    ///* SUBJECT BUTTON ///
                    ///
                    StudentHomeButton(
                      buttonName: 'SUBJECT',
                      sHeight: sHeight,
                      sWidth: sWidth,
                      function: () async {
                        print('Subject Button Clicked!');
                        var segmentId =
                            await SharedPreferenceHelper().getSegmentId();
                        // var locationId =
                        //     await SharedPreferenceHelper().getLocationId();
                        // if (locationId == null) {
                        //   EasyLoading.showInfo("Select Location to Continue");
                        //   await Future.delayed(Duration(milliseconds: 1500));
                        //   EasyLoading.dismiss();
                        // } else {
                        EasyLoading.show(status: "Loading...");
                        Future.delayed(Duration(seconds: 1))
                            .then((_) => EasyLoading.dismiss());
                        serv
                            .getDioSubjects(segmentId: segmentId, refresh: true)
                            .then((value) => subjectDialog(context, value));
                        // }
                      },
                    ),

                    ///* TOPIC BUTTON ///
                    ///
                    StudentHomeButton(
                      buttonName: 'TOPIC',
                      sHeight: sHeight,
                      sWidth: sWidth,
                      function: () async {
                        print('Topic Button Clicked!');

                        var subjectId =
                            await SharedPreferenceHelper().getSubjectId();
                        if (subjectId == null) {
                          EasyLoading.showInfo("Select Subject to Continue");
                          await Future.delayed(Duration(milliseconds: 1500));
                          EasyLoading.dismiss();
                        } else {
                          EasyLoading.show(status: "Loading...");
                          Future.delayed(Duration(seconds: 1))
                              .then((_) => EasyLoading.dismiss());
                          await serv
                              .getDioTopics(subjectId: subjectId, refresh: true)
                              .then((value) => topicDialog(context, value));
                          getTutorList();
                          EasyLoading.dismiss();

                          ///======================== HERE GOES THE FINAL CODE ==============================///
                        }
                      },
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    height: 100,
                    alignment: Alignment.topCenter,
                    padding: EdgeInsets.only(top: 45),
                    child: Text(
                      "Heilo",
                      style: TextStyle(
                        fontSize: sHeight * 0.06,
                        fontFamily: 'Antonellie',
                        foreground: Paint()
                          ..shader = AssetStrings.linearGradient,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  ),

                  //================================ FILTER CHIPS ================================//
                  (prefSubjectId == null &&
                          prefLocationId == null &&
                          prefTopicId == null)
                      ? Container()
                      : Container(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          alignment: Alignment.centerLeft,
                          height: 60,
                          child: ListView(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            children: [
                              // Container(
                              //   margin: EdgeInsets.all(10),
                              //   child: _buildChip("TUTORS", Colors.white),
                              // ),
                              (prefLocationId == null)
                                  ? Container()
                                  : Container(
                                      margin: EdgeInsets.all(10),
                                      child: _buildChip(
                                          label:
                                              chipLocation ?? "Location Chip",
                                          color: Colors.white,
                                          onDeleted: () async {
                                            await SharedPreferenceHelper()
                                                .saveLocationId(null);
                                            getTutorList();
                                          }),
                                    ),
                              (prefSubjectId == null)
                                  ? Container()
                                  : Container(
                                      margin: EdgeInsets.all(10),
                                      child: _buildChip(
                                          label: chipSubject ?? "Subject Chip",
                                          color: Colors.white,
                                          onDeleted: () async {
                                            await SharedPreferenceHelper()
                                                .saveSubjectId(null);
                                            getTutorList();
                                          }),
                                    ),
                              (prefTopicId == null)
                                  ? Container()
                                  : Container(
                                      margin: EdgeInsets.all(10),
                                      child: _buildChip(
                                          label: chipTopic ?? "Topic Chip",
                                          color: Colors.white,
                                          onDeleted: () async {
                                            await SharedPreferenceHelper()
                                                .saveTopicId(null);
                                            getTutorList();
                                          }),
                                    ),
                              // Container(
                              //   margin: EdgeInsets.all(10),
                              //   child: _buildChip("BIOLOGY", Colors.white),
                              // ),
                              // Container(
                              //   margin: EdgeInsets.all(10),
                              //   child: _buildChip("HEILO", Colors.white),
                              // ),
                            ],
                          ),
                        ),

                  //============================= This is the list of teachers ==============================//
                  _isLoading
                      ? CustomLoading2()
                      : Flexible(
                          child: Container(
                            // height: sHeight * 0.65,
                            width: sWidth,
                            // margin: EdgeInsets.only(bottom: 60),
                            // margin: EdgeInsets.only(
                            //   top: 10,
                            // ),
                            // padding: EdgeInsets.symmetric(vertical: 10),
                            // decoration: BoxDecoration(
                            //   color: AssetStrings.color1,
                            //   borderRadius: BorderRadius.only(
                            //     topLeft: Radius.circular(25.0),
                            //     // topRight: Radius.circular(25.0),
                            //   ),
                            // ),
                            child: MediaQuery.removePadding(
                              context: context,
                              removeTop: true,
                              // removeBottom: true,
                              child: FutureBuilder(
                                future: futureTutorList,
                                builder: (context, snapshot) {
                                  StudentHomeTutorListModel apiData =
                                      snapshot.data;
                                  if (snapshot.hasError) {
                                    print("api error occured");
                                    customToast("Something went wrong");
                                  } else if (snapshot.hasData &&
                                      apiData.data.length > 0) {
                                    return ListView.builder(
                                      itemCount: apiData.data.length,
                                      itemBuilder: (context, index) {
                                        print(apiData.data[index].name);
                                        // return Text(apiData.data[index].name);
                                        return GestureDetector(
                                          onTap: () {
                                            //====================== DECLARE TUTOR INFO ============================//
                                            int tutorId =
                                                apiData.data[index].tutorId;
                                            String tutorContact =
                                                apiData.data[index].contact;
                                            String tutorName =
                                                apiData.data[index].name;
                                            print("Tutor Id: $tutorId");
                                            //======================= PASS TUTOR INFO ===============================//
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TeacherProfileView(
                                                          tutorId: tutorId,
                                                          tutorContact:
                                                              tutorContact,
                                                          tutorName: tutorName,
                                                        )));
                                          },
                                          child: homeTile(
                                            dp: apiData.data[index].dp,
                                            rating:
                                                apiData.data[index].rating ==
                                                        null
                                                    ? '0'
                                                    : apiData.data[index].rating
                                                        .toString(),
                                            tutorName: apiData.data[index].name,
                                            subtitle: apiData.data[index]
                                                        .institution ==
                                                    null
                                                ? ""
                                                : apiData
                                                        .data[index]
                                                        .institution
                                                        .institutionName ??
                                                    "",
                                            // address: apiData.data[index]
                                            //             .institution ==
                                            //         null
                                            //     ? "Not Available"
                                            //     : apiData.data[index].contact ??
                                            //         "Not Available",
                                            ratePerHour:
                                                "${apiData.data[index].hourlyRate}/hr" ??
                                                    "N/A",
                                          ),
                                        );
                                      },
                                    );
                                  }
                                  return Align(
                                    alignment: Alignment.topCenter,
                                    child: Center(
                                      child: Container(
                                        padding: EdgeInsets.all(8),
                                        margin: EdgeInsets.only(bottom: 100),
                                        decoration: BoxDecoration(
                                          color: AssetStrings.color4,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                        ),
                                        child: Text(
                                          "Search for tutors",
                                          style: TextStyle(
                                            fontSize: 16,
                                            // fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                  // SizedBox(
                  //   height: 60,
                  // ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  /////////////////////////// CUSTOM TILE FOR TUTOR LIST ////////////////////////////
  Container homeTile({
    @required String dp,
    @required String rating,
    @required String tutorName,
    // @required String school,
    @required String subtitle,
    @required String ratePerHour,
  }) {
    return Container(
      // height: MediaQuery.of(context).size.height * 0.15,
      margin: EdgeInsets.only(
        bottom: 6,
        left: 5,
        right: 5,
      ),
      child: Card(
        color: Color(0xFFF3F3F3),
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: ListTile(
          dense: true,
          leading: Container(
            // padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  // color: Colors.grey.withOpacity(0.5),
                  color: Colors.white,
                  spreadRadius: 4,
                  // blurRadius: 2,
                ),
              ],
            ),
            child: Container(
              height: 100,
              width: 80,
              child: Stack(
                alignment: Alignment.center,
                fit: StackFit.expand,
                children: [
                  Positioned(
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: CircleAvatar(
                        maxRadius: 35,
                        backgroundImage: (dp == null)
                            ? AssetImage("assets/images/home.png")
                            : NetworkImage(dp),
                      ),
                    ),
                  ),
                  Positioned(
                    // top: 0,
                    right: 0,
                    top: 0,

                    child: Container(
                      height: 20,
                      width: 33,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border:
                            Border.all(color: AssetStrings.color4, width: 2),
                      ),
                      child: Center(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            rating ?? '0',
                            style: TextStyle(fontSize: 12),
                          ),
                          Icon(
                            Icons.star,
                            size: 12,
                            color: AssetStrings.color4,
                          ),
                        ],
                      )),
                    ),
                  ),
                ],
              ),
            ),
          ),
          title: Text(
            tutorName ?? 'Tutor Name',
            style: TextStyle(fontFamily: "QuickSandBold", fontSize: 16),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          subtitle: Text(
            subtitle ?? 'Not Available',
            style: TextStyle(fontFamily: "QuickSandMedium", fontSize: 13),
          ),
          trailing: Column(
            children: [
              _buildChip2(
                ratePerHour ?? "350/hr",
                Colors.white,
              ),
            ],
          ),
          contentPadding: EdgeInsets.only(
            right: 10,
            top: 10,
            bottom: 10,
          ),
          // dense: true,
          isThreeLine: false,
        ),
      ),
    );
  }

  Widget _buildChip({String label, Color color, VoidCallback onDeleted}) {
    return Chip(
      // labelPadding: EdgeInsets.all(2.0),
      // avatar: CircleAvatar(
      //   backgroundColor: AssetStrings.color4,
      //   child: Text(
      //     label[0].toUpperCase(),
      //     style: TextStyle(
      //       color: Colors.white,
      //     ),
      //   ),
      // ),
      label: Text(
        label,
        style: TextStyle(
          color: Colors.black,
          fontSize: 12,
        ),
      ),
      backgroundColor: color,
      elevation: 6.0,
      shadowColor: AssetStrings.color3,
      // padding: EdgeInsets.all(10.0),
      padding: EdgeInsets.symmetric(horizontal: 10),

      deleteIcon: FaIcon(
        FontAwesomeIcons.times,
        size: 15,
        color: Colors.red,
      ),
      onDeleted: onDeleted,
      // onDeleted: () {
      //   // widget.isDelete = !widget.isDelete;
      //   print('$label delete');
      // },
      // deleteIconColor: Colors.redAccent,
    );
  }

  Widget _buildChip2(String label, Color color) {
    return Chip(
      // labelPadding: EdgeInsets.all(2.0),
      label: Text(
        label,
        style: TextStyle(
          color: Colors.black,
        ),
      ),
      backgroundColor: color,
      // elevation: 6.0,
      // shadowColor: AssetStrings.color4,
      padding: EdgeInsets.symmetric(horizontal: 10),
      deleteIcon: Icon(
        Icons.cancel,
      ),
    );
  }

  // Widget _buildChip3(String label, Color color) {
  //   return Chip(
  //     // labelPadding: EdgeInsets.all(2.0),
  //     label: Text(
  //       label,
  //       style: TextStyle(
  //         color: Colors.black,
  //       ),
  //     ),
  //     backgroundColor: color,
  //     // elevation: 6.0,
  //     // shadowColor: AssetStrings.color4,
  //     // padding: EdgeInsets.symmetric(horizontal: 10),
  //     deleteIcon: Icon(
  //       Icons.cancel,
  //     ),
  //   );
  // }
}

fireAlarm() async {
  print("BACHAO BACHAO");
  // NotificationService model = NotificationService();
  searchList(List list, String item) {
    for (int i = 0; i < list.length; i++) {
      if (list[i] == item) {
        return true;
      } else {
        return false;
      }
    }
  }

  Services serv = Services();

  List pendingStudentClassList = [];
  List doneIdList = [];

  try {
    pendingStudentClassList.clear();
    await serv.getStudentClassList(refresh: true).then((value) {
      for (int i = 0; i < value.data.length; i++) {
        // if (value.data[i].status == "ACCEPT" ||
        //     value.data[i].status == "PENDING") {
        if (value.data[i].status == "ACCEPT") {
          print(value.data[i].status);
          pendingStudentClassList.add(value.data[i]);
        } else {
          doneIdList.add(value.data[i].classId.toString());
        }
      }
    });
  } catch (e) {
    return;
  }

  String prevKey = await SharedPreferenceHelper().getNotificationKey();
  String currentKey =
      pendingStudentClassList[pendingStudentClassList.length - 1]
          .classId
          .toString();

  if (prevKey == currentKey || searchList(doneIdList, currentKey)) {
    return;
  } else {
    String title = "Request Alert";
    String content =
        "Your tution request got accepted by ${pendingStudentClassList[pendingStudentClassList.length - 1].tutor.name}";
    await notificationPlugin.initialize();
    await notificationPlugin.instantNotification(
      title: title,
      content: content,
    );
    await SharedPreferenceHelper().saveNotificationKey(currentKey);
  }

  print(pendingStudentClassList[pendingStudentClassList.length - 1]
      .classId
      .toString());
  // print(phone);
}
