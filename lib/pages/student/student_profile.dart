import 'dart:convert';
import 'dart:io' as Io;

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_user_info_model.dart';
// import 'package:Heilo/pages/common/drawer_Content.dart';
import 'package:Heilo/services/firestore_database.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/ProfileButton.dart';
import 'package:Heilo/widgets/about_update_dialog.dart';
import 'package:Heilo/widgets/custom_loading_screen.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/student_education_add_dialog.dart';
import 'package:Heilo/widgets/student_education_list_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:Heilo/widgets/student_personal_info_update_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StudentProfile extends StatefulWidget {
  final bool isPopable;

  StudentProfile({Key key, @required this.isPopable}) : super(key: key);

  @override
  _StudentProfileState createState() => _StudentProfileState();
}

class _StudentProfileState extends State<StudentProfile> {
  Future<StudentUserInfoModel> futureStudent;
  Services serv = Services();

  TextEditingController aboutController = TextEditingController();

  TextEditingController _currentPassController = TextEditingController();
  TextEditingController _newPassController = TextEditingController();
  bool fishEye1 = true;
  bool fishEye2 = true;

  updatePage() {
    setState(() {
      futureStudent = serv.getDioStudentInfo(refresh: true);
    });
    setState(() {});
  }

  /////////////////////////////////// PROFILE PIC CODE STARTS ////////////////////////////////////

  String dpString;
  Io.File _image1;

  // Getting Profile Picture from Gallery
  Future getProfileImage() async {
    // ignore: deprecated_member_use
    final image1 = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 450.0,
      maxWidth: 450.0,
      imageQuality: 50,
    );
    setState(() {
      _image1 = image1;
    });
    // print(_image);
    await imgEncode1();
  }

  // Encoding Profile Picture to BASE64 string
  imgEncode1() async {
    final bytes = _image1.readAsBytesSync();
    dpString = base64Encode(bytes);
    print('ENCODED STRING: $dpString');
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('PROF_PIC_PREF', dpString);
  }

  /////////////////////////////////// PROFILE PIC CODE ENDS ////////////////////////////////////

  @override
  void initState() {
    super.initState();
    setState(() {
      futureStudent = serv.getDioStudentInfo(refresh: false);
    });
  }

  @override
  Widget build(BuildContext context) {
    //
    var sHeight = MediaQuery.of(context).size.height;
    var sWidth = MediaQuery.of(context).size.width;

    // var fishEye1 = true;
    // var fishEye2 = true;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      // endDrawer: widget.isPopable ? DrawerContent() : null,
      extendBody: true,
      // extendBodyBehindAppBar: true,
      body: Container(
        height: sHeight,
        width: sWidth,
        child: Stack(
          alignment: Alignment.topCenter,
          fit: StackFit.expand,
          overflow: Overflow.visible,
          children: [
            Positioned(
              top: 0,
              width: sWidth,
              child: Image.asset("assets/images/topBorder.png"),
            ),
            Positioned(
              bottom: 0,
              width: sWidth,
              child: Image.asset("assets/images/bottomBorder.png"),
            ),
            FutureBuilder(
              future: futureStudent,
              builder: (context, snapshot) {
                //
                StudentUserInfoModel apiData = snapshot.data;
                //
                if (snapshot.hasError) {
                  return Text('Something went wrong');
                } else if (snapshot.hasData) {
                  return SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.only(top: 20, bottom: 40),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),

                          //=========================== PROFILE PIC =========================//
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: FaIcon(
                                    FontAwesomeIcons.angleLeft,
                                    size: 35,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topCenter,
                                child: CircleAvatar(
                                  maxRadius: 40,
                                  backgroundImage: (apiData.data.dp == null)
                                      ? AssetImage("assets/images/home.png")
                                      : NetworkImage(apiData.data.dp),
                                ),
                              ),
                              Opacity(
                                opacity: 0,
                                child: Container(
                                  margin: EdgeInsets.only(right: 15),
                                  child: FaIcon(
                                    FontAwesomeIcons.angleLeft,
                                    size: 35,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          ProfileButton(
                            buttonName: "Change Photo",
                            sWidth: sWidth * 0.3,
                            sHeight: sHeight * 0.04,
                            bFontSize: 12.0,
                            function: () async {
                              EasyLoading.show(status: "Loading...");

                              try {
                                await getProfileImage();
                                await serv.studentDpUpload(
                                    dpString:
                                        "data:image/jpeg;base64," + dpString);
                                // EasyLoading.dismiss();
                                // EasyLoading.showSuccess("Upload Successful");
                                setState(() {
                                  futureStudent =
                                      serv.getDioStudentInfo(refresh: true);
                                });

                                Future.delayed(Duration(seconds: 1));

                                // profile pic to firebase
                                String uid =
                                    FirebaseAuth.instance.currentUser.uid;

                                String firebaseDP = apiData.data.dp;
                                print(firebaseDP);

                                Map<String, dynamic> userInfoMap = {
                                  "userName": apiData.data.name,
                                  "contactNo":
                                      apiData.data.contact.substring(3),
                                  "profilePic": firebaseDP,
                                };
                                await DatabaseMethods()
                                    .addUserInfoToDB(uid, userInfoMap);

                                // Done!

                                // EasyLoading.dismiss();
                                EasyLoading.showSuccess("Upload Successful");
                                setState(() {
                                  futureStudent =
                                      serv.getDioStudentInfo(refresh: true);
                                });
                              } catch (e) {
                                EasyLoading.dismiss();

                                print("CANCELED UPLOAD");
                                EasyLoading.showError("Upload Failed!");
                                // createAlertDialog3(context);
                              }
                            },
                          ),
                          Container(
                            margin: EdgeInsets.all(40),
                            width: sWidth,
                            // color: Colors.white,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 3,
                                    offset: Offset(0, 4)),
                              ],
                            ),
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 20,
                              ),

                              ///* PERSONAL DETAILS START ///
                              ///
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  FittedBox(
                                    child: Text(
                                      'PERSONAL DETAILS',
                                      style: _textStyle0(context),
                                    ),
                                  ),
                                  // GestureDetector(
                                  //   child: FaIcon(
                                  //     FontAwesomeIcons.edit,
                                  //     size: 18,
                                  //   ),
                                  //   onTap: () async {
                                  //     try {
                                  //       await serv
                                  //           .getDioStudentInfo(
                                  //               refresh: false)
                                  //           .then((value) =>
                                  //               studentPersonalInfoUpdate(
                                  //                   context: context,
                                  //                   studentUserInfoModel:
                                  //                       value));
                                  //       await updatePage();
                                  //       await Future.delayed(
                                  //           Duration(seconds: 1));
                                  //       await updatePage();
                                  //     } catch (e) {
                                  //       print(e);
                                  //     }
                                  //   },
                                  // ),
                                  Divider(
                                    color: AssetStrings.color4,
                                    height: 20,
                                    thickness: 2,
                                  ),
                                  //////////////////////////////// NAME //////////////////////////////////
                                  Text(
                                    'NAME',
                                    style: _textStyle1(context),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    apiData.data.name ?? "Not Available",
                                    style: _textStyle2(context),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  //////////////////////////////// EMAIL //////////////////////////////////
                                  Text(
                                    'EMAIL',
                                    style: _textStyle1(context),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    apiData.data.email ?? "Not Available",
                                    style: _textStyle2(context),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  //////////////////////////////// CONTACT //////////////////////////////////
                                  Text(
                                    'CONTACT NO',
                                    style: _textStyle1(context),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    apiData.data.contact ?? "Not Available",
                                    style: _textStyle2(context),
                                  ),
                                  // SizedBox(
                                  //   height: 20,
                                  // ),
                                  ////////////////////////////// ADDRESS //////////////////////////////////
                                  // Text(
                                  //   'ALTERNATE CONTACT NO',
                                  //   style: _textStyle1(context),
                                  // ),
                                  // SizedBox(
                                  //   height: 5,
                                  // ),
                                  // Text(
                                  //   apiData.data.altPhone ?? "Not Available",
                                  //   style: _textStyle2(context),
                                  // ),
                                  // SizedBox(
                                  //   height: 20,
                                  // ),
                                  // //////////////////////////////// LOCATION //////////////////////////////////
                                  // Text(
                                  //   'LOCATION REFERENCE (PLACE OF STUDY)',
                                  //   style: _textStyle1(context),
                                  // ),
                                  // SizedBox(
                                  //   height: 5,
                                  // ),
                                  // Text(
                                  //   'Dhanmondi',
                                  //   style: _textStyle2(context),
                                  // ),
                                ],
                              ),
                            ),
                          ),

                          //! EDUCATIONAL INFORTMAION //
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 40),
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            width: sWidth,
                            // color: Colors.white,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 3,
                                    offset: Offset(0, 4)),
                              ],
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: EdgeInsets.only(
                                      top: 20, right: 20, left: 20, bottom: 0),
                                  width: double.infinity,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          FittedBox(
                                            child: Text(
                                              'EDUCATIONAL INFORMATION',
                                              style: _textStyle0(context),
                                            ),
                                          ),
                                          Row(
                                            children: [
                                              //* Education Add Icons //
                                              //
                                              GestureDetector(
                                                child: FaIcon(
                                                  FontAwesomeIcons.plus,
                                                  size: 18,
                                                ),
                                                onTap: () async {
                                                  print(
                                                      'Add Education Clicked');
                                                  try {
                                                    // await serv
                                                    //     .addStudentEducation(
                                                    //   institutionName:
                                                    //       "Motijheel Govt Boys High School",
                                                    //   institutionType: "School",
                                                    //   grade: "10",
                                                    //   background: "Science",
                                                    //   medium: "Bangla",
                                                    //   detailAddress:
                                                    //       "Motijheel",
                                                    // );
                                                    await serv
                                                        .getDioStudentInfo(
                                                            refresh: false)
                                                        .then(
                                                          (value) =>
                                                              studentEducationAddDialog(
                                                            context: context,
                                                            studentUserInfoModel:
                                                                value,
                                                          ),
                                                        );
                                                    await updatePage();

                                                    await Future.delayed(
                                                        Duration(seconds: 1));
                                                    await updatePage();
                                                  } catch (e) {
                                                    print(
                                                        "ERROR OCCURED WHILE UPDATING EDUCATION");
                                                    print(e);
                                                  }
                                                },
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),

                                              //! Education Edit Icons //
                                              (apiData.data.educations.length <
                                                      2)
                                                  ? Container()
                                                  : GestureDetector(
                                                      child: FaIcon(
                                                        FontAwesomeIcons.edit,
                                                        size: 18,
                                                      ),
                                                      onTap: () async {
                                                        try {
                                                          await serv
                                                              .getDioStudentInfo(
                                                                  refresh:
                                                                      false)
                                                              .then((value) =>
                                                                  studentEducationListDialog(
                                                                      context:
                                                                          context,
                                                                      studentUserInfoModel:
                                                                          value));
                                                          await updatePage();

                                                          await Future.delayed(
                                                              Duration(
                                                                  seconds: 2));
                                                          await updatePage();
                                                        } catch (e) {
                                                          print(e);
                                                        }
                                                      },
                                                    ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Divider(
                                        color: AssetStrings.color4,
                                        height: 20,
                                        thickness: 2,
                                      ),
                                    ],
                                  ),
                                ),

                                ///! EDUCATION INFORMATION TILE ///
                                (apiData.data.educations.length < 1)
                                    ? Container(
                                        height: sHeight * 0.1,
                                        width: double.infinity,
                                        padding: EdgeInsets.only(bottom: 10),
                                        child: Center(
                                          child: Text('Not Available'),
                                        ),
                                      )
                                    : MediaQuery.removePadding(
                                        context: context,
                                        removeTop: true,
                                        removeBottom: true,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 10),
                                          child: ListView.builder(
                                            shrinkWrap: true,
                                            physics: BouncingScrollPhysics(),
                                            itemCount:
                                                apiData.data.educations.length,
                                            itemBuilder: (context, index) {
                                              return educationCard(
                                                context: context,
                                                institutionName: apiData
                                                    .data
                                                    .educations[index]
                                                    .institutionName,
                                                grade: apiData.data
                                                    .educations[index].grade,
                                                background: apiData
                                                    .data
                                                    .educations[index]
                                                    .background,
                                                medium: apiData.data
                                                    .educations[index].medium,
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                              ],
                            ),
                          ),
                          // SizedBox(
                          //   height: 40,
                          // ),
                          // ================================== ABOUT INFO START =========================================//
                          //
                          Container(
                            margin:
                                EdgeInsets.only(top: 40, left: 40, right: 40),
                            width: sWidth,
                            // color: Colors.white,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 3,
                                    offset: Offset(0, 4)),
                              ],
                            ),
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 20,
                              ),

                              ///
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      FittedBox(
                                        child: Text(
                                          'ABOUT',
                                          style: _textStyle0(context),
                                        ),
                                      ),

                                      //=============================== ABOUT UPDATE =============================//
                                      GestureDetector(
                                        child: FaIcon(
                                          FontAwesomeIcons.edit,
                                          size: 18,
                                        ),
                                        onTap: () async {
                                          aboutController.text =
                                              apiData.data.about;
                                          try {
                                            // DO SOMETHING
                                            await aboutUpdateDialog(
                                              context: context,
                                              inputController: aboutController,
                                            ).then((value) {
                                              setState(() {
                                                futureStudent =
                                                    serv.getDioStudentInfo(
                                                        refresh: true);
                                              });
                                            });
                                          } catch (e) {
                                            print(e);
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                  Divider(
                                    color: AssetStrings.color4,
                                    height: 20,
                                    thickness: 2,
                                  ),

                                  /////////////////////////// ABOUT TEXT /////////////////////////////
                                  FutureBuilder(
                                    future: futureStudent,
                                    builder: (context, snapshot) {
                                      var apiData = snapshot.data;
                                      if (snapshot.hasData) {
                                        return Container(
                                          child: Text(
                                            apiData.data.about ??
                                                "Not Available",
                                            style: _textStyle2(context),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        );
                                      }
                                      return Center(
                                          child: CircularProgressIndicator());
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),

                          //============================== CHANGE PASSWORD START =============================//

                          Container(
                            margin:
                                EdgeInsets.only(top: 40, left: 40, right: 40),
                            width: sWidth,
                            // color: Colors.white,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 3,
                                    offset: Offset(0, 4)),
                              ],
                            ),
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 20,
                              ),

                              ///
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      FittedBox(
                                        child: Text(
                                          'CHANGE PASSWORD',
                                          style: _textStyle0(context),
                                        ),
                                      ),

                                      //=============================== ABOUT UPDATE =============================//
                                      Opacity(
                                        opacity: 0,
                                        child: FaIcon(
                                          FontAwesomeIcons.edit,
                                          size: 18,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Divider(
                                    color: AssetStrings.color4,
                                    height: 20,
                                    thickness: 2,
                                  ),

                                  /////////////////////////// ABOUT TEXT /////////////////////////////
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 10,
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: AssetStrings.color4,
                                        width: 2,
                                      ),
                                    ),
                                    child: TextField(
                                      controller: _currentPassController,
                                      obscureText: fishEye1,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: "Current Password",
                                        hintStyle: TextStyle(
                                          fontSize: 14,
                                        ),
                                        suffixIcon: IconButton(
                                          icon: Icon(Icons.remove_red_eye),
                                          splashRadius: 1,
                                          onPressed: () {
                                            setState(() {
                                              fishEye1 = !fishEye1;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),

                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 10,
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: AssetStrings.color4,
                                        width: 2,
                                      ),
                                    ),
                                    child: TextField(
                                      controller: _newPassController,
                                      obscureText: fishEye2,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: "New Password",
                                        hintStyle: TextStyle(
                                          fontSize: 14,
                                        ),
                                        suffixIcon: IconButton(
                                          icon: Icon(Icons.remove_red_eye),
                                          splashRadius: 1,
                                          onPressed: () {
                                            setState(() {
                                              fishEye2 = !fishEye2;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),

                                  SizedBox(
                                    height: 10,
                                  ),

                                  Align(
                                    alignment: Alignment.center,
                                    child: MaterialButton(
                                      shape: StadiumBorder(
                                          // side: BorderSide(
                                          //   width: 2,
                                          //   color: Colors.grey,
                                          // ),
                                          ),
                                      color: AssetStrings.color4,
                                      child: Text(
                                        'Submit',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      highlightElevation: 0,
                                      onPressed: () async {
                                        try {
                                          EasyLoading.show(
                                              status: "Loading...");
                                          Future.delayed(Duration(seconds: 2));
                                          await serv.studentPassChange(
                                              currentPassword:
                                                  _currentPassController.text,
                                              newPassword:
                                                  _newPassController.text);
                                          EasyLoading.showSuccess(
                                              "Password Changed!");
                                          _currentPassController.clear();
                                          _newPassController.clear();
                                        } catch (e) {
                                          EasyLoading.showError("Error!");
                                          customToast(
                                              "Please check your current password!");
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }
                return CustomLoadingScreen();
              },
            ),
          ],
        ),
      ),
    );
  }

  _textStyle0(BuildContext context) {
    return TextStyle(
      color: Colors.black87,
      fontFamily: "QuickSandBold",
      fontSize: 13,
    );
  }

  _textStyle1(BuildContext context) {
    return TextStyle(
      color: AssetStrings.color4,
      fontFamily: "QuickSandBold",
      fontSize: 11,
    );
  }

  _textStyle2(BuildContext context) {
    return TextStyle(
      color: Colors.black,
      fontFamily: "QuickSandMedium",
      fontSize: 11,
    );
  }

  Container educationCard({
    @required BuildContext context,
    @required String institutionName,
    @required String grade,
    @required String background,
    @required String medium,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      padding: EdgeInsets.all(10),
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: AssetStrings.color4,
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //////////////////////////////// SCHOOL //////////////////////////////////
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ////////////////////! SCHOOL/ COLLEGE NAME ////////////////////
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'SCHOOL/COLLEGE',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      institutionName ?? "Not Avalable",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              //////////////////////! GRADE ////////////////////////////
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'GRADE',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      grade ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //////////////////////! BACKGROUND ////////////////////////////

              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'BACKGROUND',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      background ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),

              //////////////////////! MEDIUM ////////////////////////////

              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'MEDIUM',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      medium ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
