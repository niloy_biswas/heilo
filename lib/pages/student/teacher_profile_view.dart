import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_tutor_details_model.dart';
import 'package:Heilo/models/topic_model.dart' as tModel;
import 'package:Heilo/pages/common/chat_screen.dart';
import 'package:Heilo/pages/common/drawer_Content.dart';
import 'package:Heilo/services/firestore_database.dart';
// import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
// import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/review_tile.dart';
// import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class TeacherProfileView extends StatefulWidget {
  final int tutorId;
  final String tutorContact;
  final String tutorName;

  const TeacherProfileView({
    Key key,
    @required this.tutorId,
    @required this.tutorContact,
    @required this.tutorName,
  }) : super(key: key);
  @override
  _TeacherProfileViewState createState() => _TeacherProfileViewState();
}

class _TeacherProfileViewState extends State<TeacherProfileView> {
  Future<StudentTutorDetails> futureTutorDetail;
  Services serv = Services();
  List<Subjects> subjectList;

  _getData() async {
    futureTutorDetail = serv.getStudentHomeTutorDetails(
      tutorId: widget.tutorId,
      refresh: true,
    );

    setState(() {});
  }

  TextEditingController inputDateController = TextEditingController();
  TextEditingController inputToController = TextEditingController();
  TextEditingController inputFromController = TextEditingController();

  var myDateFormat = DateFormat('dd-MM-yyyy');
  // var myDay = DateFormat('dd');
  // var myMonth = DateFormat('MM');
  // var myYear = DateFormat('yyyy');

  var closingDateFormat = DateFormat('yyyy-MM-dd');
  var fromTime;
  var toTime;
  var date1;

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      endDrawer: DrawerContent(),
      extendBody: true,
      // body:
      body: FutureBuilder(
        future: futureTutorDetail,
        builder: (context, snapshot) {
          StudentTutorDetails apiData = snapshot.data;

          if (snapshot.hasError) {
            print("ERROR OCCURED WHILE FETCHING PROFILE DATA");
          } else if (snapshot.hasData) {
            /// SETTING UP SUBJECT LIST ///
            subjectList = apiData.data.subjects;

            /// ---------------------- ///
            return SafeArea(
              child: SingleChildScrollView(
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Container(
                          height: sHeight * 0.33,
                          width: sWidth,
                          decoration: BoxDecoration(
                            color: AssetStrings.color4,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                height: sWidth * 0.28,
                                width: sWidth * 0.28,
                                child: Stack(
                                  fit: StackFit.expand,
                                  children: [
                                    /////////////////////////////// PROFILE PICTURE ////////////////////////////////
                                    Align(
                                      alignment: Alignment.topCenter,
                                      child: CircleAvatar(
                                          maxRadius: 50,
                                          backgroundImage: (apiData.data.dp ==
                                                  null)
                                              ? AssetImage(
                                                  "assets/images/home.png")
                                              : NetworkImage(apiData.data.dp)),
                                    ),
                                    /////////////////////////////// RATING CHIP ////////////////////////////////
                                    Positioned(
                                      // top: 0,
                                      right: 0,
                                      top: 0,

                                      child: Container(
                                        height: 20,
                                        width: 33,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: AssetStrings.color4,
                                              width: 2),
                                        ),
                                        child: Center(
                                            child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            /////////////////////////////// RATING TEXT ////////////////////////////////

                                            Text(
                                              (apiData.data.rating == null)
                                                  ? "0"
                                                  : apiData.data.rating
                                                      .toStringAsFixed(1),
                                              style: TextStyle(fontSize: 12),
                                            ),
                                            Icon(
                                              Icons.star,
                                              size: 12,
                                              color: AssetStrings.color4,
                                            ),
                                          ],
                                        )),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),

                                ///-----------------------TUTOR NAME---------------------///
                                child: Text(
                                  apiData.data.name ?? 'Not Available',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54,
                                  ),
                                ),
                              ),

                              ///---------------------SCHOOL NAME----------------------///
                              (apiData.data.educations.length < 1)
                                  ? Container()
                                  : Text(
                                      apiData.data.educations[0]
                                              .institutionName ??
                                          'Not Available',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    FaIcon(
                                      FontAwesomeIcons.mapMarkerAlt,
                                      color: Colors.white,
                                      size: 14,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),

                                    ///---------------------LOCATION NAME----------------///
                                    Text(
                                      "${apiData.data.location.area}, ${apiData.data.location.district}" ??
                                          'Location Not Available',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                        SizedBox(
                          height: 30,
                        ),

                        /// RATING and COMPLETED TUTION COUNT ///
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 70,
                              width: 100,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 2),
                                    color: AssetStrings.color4.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 5,
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      ///---------------- HOURLY RATE NUMBER -------------------///
                                      Text(
                                        apiData.data.hourlyRate ?? '0',
                                        style: TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        "/hr",
                                        style: TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    'Hourly Rate',
                                    style: TextStyle(fontSize: 9),
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                            // SizedBox(
                            //   width: sWidth * 0.15,
                            // ),
                            Container(
                              height: 70,
                              width: 100,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 2),
                                    color: AssetStrings.color4.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 5,
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ///-----------------COMPLETED TUTION-----------------///
                                  Text(
                                    apiData.data.totalClassCompleted
                                            .toString() ??
                                        '0',
                                    style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'Completed Tutions',
                                    style: TextStyle(fontSize: 9),
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),

                            /////////////////////////// REFER BY ///////////////////////////
                            Container(
                              height: 70,
                              width: 100,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 2),
                                    color: AssetStrings.color4.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 5,
                                  )
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ////////////////////// REFER NO ///////////////////////
                                  Text(
                                    apiData.data.reviews.length.toString() ??
                                        '0',
                                    style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'Students Refer',
                                    style: TextStyle(fontSize: 9),
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          padding: EdgeInsets.all(16),
                          // height: sHeight * 0.2,
                          width: sWidth * 0.87,
                          alignment: Alignment.topLeft,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: AssetStrings.color4.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: Column(
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Skills:',
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  //////////////////////////// SKILL 1 ///////////////////////////
                                  ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: apiData.data.subjects.length,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemBuilder: (context, index) {
                                        return Container(
                                          alignment: Alignment.topLeft,
                                          padding: EdgeInsets.all(5),
                                          margin:
                                              EdgeInsets.symmetric(vertical: 5),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: AssetStrings.color4,
                                              width: 1,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                          ),

                                          ///------------------MEDIUM OF STUDY------------------///
                                          child: Text(
                                            apiData.data.subjects[index]
                                                    .subject ??
                                                'Not Available',
                                            style: TextStyle(fontSize: 20),
                                          ),
                                        );
                                      }),
                                ],
                              ),
                            ],
                          ),
                        ),

                        SizedBox(
                          height: 30,
                        ),

                        ///-------------------------ABOUT SECTION-------------------------///
                        Container(
                          padding: EdgeInsets.all(16),
                          height: sHeight * 0.2,
                          width: sWidth * 0.87,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: AssetStrings.color4.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: ListView(
                            // crossAxisAlignmet: CrossAxisAlignment.start,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            children: [
                              Text(
                                'About Tutor:',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),

                              ///--------------------------ABOUT AUTHOR PARAGRAPH---------------------///
                              (apiData.data.about == null)
                                  ? SizedBox(
                                      height: 100,
                                      child: Center(
                                        child: Text('Not Available'),
                                      ),
                                    )
                                  : Text(
                                      apiData.data.about,
                                    ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),

                        ///------------------------------ REVIEW SECTION ----------------------------///
                        Container(
                          padding: EdgeInsets.all(16),
                          // height: sHeight * 0.35,
                          width: sWidth * 0.87,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 2),
                                color: AssetStrings.color4.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: ListView(
                            // mainAxisSize: MainAxisSize.min,
                            shrinkWrap: true,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 5),
                                  child: Text(
                                    'Reviews:',
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                              (apiData.data.reviews.length < 1)
                                  ? Container(
                                      height: 200,
                                      child: Center(
                                        child: Text("Not Available"),
                                      ),
                                    )
                                  : SizedBox(
                                      height: 200,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: apiData.data.reviews.length,
                                        itemBuilder: (context, index) {
                                          // return Text(
                                          //     apiData.data.reviews[index].feedback);
                                          return Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 3.0),
                                            child: ReviewTile(
                                              dp: apiData.data.reviews[index]
                                                  .studentDetails.dp,
                                              title: apiData.data.reviews[index]
                                                  .studentDetails.name,
                                              subtitle: apiData
                                                  .data.reviews[index].feedback,
                                              rating: (apiData
                                                          .data
                                                          .reviews[index]
                                                          .rate ==
                                                      null)
                                                  ? "0"
                                                  : apiData
                                                      .data.reviews[index].rate
                                                      .toString(),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                            ],
                          ),
                          // child: ListView.builder(
                          //   itemCount: apiData.data.reviews.length,
                          //   itemBuilder: (context, index) {
                          //     return Text(apiData.data.reviews[index].feedback);
                          //   },
                          // ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                    Positioned(
                      top: 10,
                      left: 15,
                      child: GestureDetector(
                        ///-----------------------BACK BUTTON-----------------------///
                        child: FaIcon(
                          FontAwesomeIcons.angleLeft,
                          size: 35,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    // Positioned(
                    //   bottom: 10,
                    //   right: 10,
                    //   child: FloatingActionButton(
                    //     child: Icon(
                    //       CommunityMaterialIcons.comment_text,
                    //       color: Colors.white,
                    //       size: 30,
                    //     ),
                    //     backgroundColor: AssetStrings.color4,
                    //     onPressed: () {},
                    //   ),
                    // ),
                    //
                    /////////////////////////////////// HIRE BUTTON ///////////////////////////
                    Positioned(
                      top: sHeight * 0.3,
                      child: Container(
                        width: sWidth,
                        alignment: Alignment.center,
                        child: MaterialButton(
                          color: AssetStrings.color4,
                          height: 40,
                          minWidth: 100,
                          child: Text(
                            "HIRE",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          shape: StadiumBorder(
                            side: BorderSide(width: 2, color: Colors.white),
                          ),
                          onPressed: () async {
                            //
                            var tutorId = apiData.data.tutorId.toString();
                            hireDialog(
                              context: context,
                              tutorId: tutorId,
                              subjectList: subjectList,
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
          return Center(
              child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: AssetStrings.color4,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Loading...",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ));
        },
      ),
      floatingActionButton: FloatingActionButton(
        // child: Icon(
        //   CommunityMaterialIcons.comment_text,
        //   color: Colors.white,
        //   size: 30,
        // ),
        child: Image(
          width: 26,
          height: 26,
          color: Colors.white,
          image: Svg("assets/icons/chat.svg"),
        ),
        backgroundColor: AssetStrings.color4,
        onPressed: () async {
          var myContact = await SharedPreferenceHelper().getUserContact();
          myContact = myContact.substring(3);
          print("MY CONTACT: " + myContact);

          var chatWithContact = widget.tutorContact;
          chatWithContact = chatWithContact.substring(3);
          print("PARTNER CONTACT: " + chatWithContact);

          var myUserName = await SharedPreferenceHelper().getUserName();
          print("MY USERNAME: " + myUserName);

          var chatWithUserName = widget.tutorName;
          print("PARTNER USER NAME: " + chatWithUserName);

          //============================ main game start ========================//
          var chatRoomId =
              getChatRoomIdByUserContacts(myContact, chatWithContact);
          Map<String, dynamic> chatRoomInfoMap = {
            "users": [myUserName, chatWithUserName]
          };

          DatabaseMethods().createChatRoom(chatRoomId, chatRoomInfoMap);

          //==================================== GET THIS FOR NEW CHAT ====================================//
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChatScreen(
                chatWithName: chatWithUserName,
                chatWithContact: chatWithContact,
              ),
            ),
          );
        },
      ),
    );
  }

  //============================ CHAT FUNCTIONS ===============================//
  // Generate chatroom ID by using usernames
  getChatRoomIdByUserContacts(String a, String b) {
    if (int.parse(a) > int.parse(b)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }

  //===========================================================================//

  //============================================================================
  //============================================================================
  //============================================================================

  hireDialog({
    @required BuildContext context,
    @required String tutorId,
    @required List<Subjects> subjectList,
  }) async {
    var localizations = MaterialLocalizations.of(context);
    //
    String currentSub;
    var currentSubId;

    String currentTopic;
    var currentTopicId;

    tModel.TopicModel topicList;
    List<tModel.Data> topicDataList = [];

    return showDialog(
      context: context,
      barrierColor: Colors.black.withOpacity(0.3),
      builder: (context) {
        double sWidth = MediaQuery.of(context).size.width;
        // double sHeight = MediaQuery.of(context).size.height;
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
          child: StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                elevation: 10,
                backgroundColor: Colors.black.withOpacity(0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25),
                ),
                insetAnimationCurve: Curves.easeInOutCirc,
                insetAnimationDuration: Duration(milliseconds: 500),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  width: sWidth * 0.9,
                  decoration: BoxDecoration(
                    color: AssetStrings.color1,
                    borderRadius: BorderRadius.circular(20),
                  ),

                  //======================================== KHELA SHURU =======================================//
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(20),
                    child: ListView(
                      shrinkWrap: true,
                      // mainAxisSize: MainAxisSize.min,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 10,
                        ),

                        ///=========================== SUBJECTS DROPDOWN ============================== ///
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                  spreadRadius: 1),
                            ],
                          ),
                          child: DropdownButton<String>(
                            hint: Text('Select Subject'),
                            isExpanded: true,
                            underline: Container(),
                            value: currentSub,
                            items: subjectList.map((value) {
                              return DropdownMenuItem<String>(
                                value: value.subject,
                                child: new Text(value.subject),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                topicDataList = [];
                                currentTopic = null;

                                currentSub = value;
                              });
                              subjectList.forEach((element) {
                                if (element.subject == value) {
                                  setState(() {
                                    currentSubId = element.subjectId;
                                  });
                                }
                              });

                              var serv = Services();

                              serv
                                  .getDioTopics(
                                      subjectId: currentSubId.toString(),
                                      refresh: true)
                                  .then((value) {
                                setState(() {
                                  topicList = value;
                                  topicDataList = topicList.data;
                                });
                              });

                              print('>>>>> $value');
                              print('>>>>>ID $currentSubId');
                            },
                          ),
                        ),

                        SizedBox(
                          height: 15,
                        ),

                        /// ================================ TOPIC DROPDOWN ============================ ///
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                  spreadRadius: 1),
                            ],
                          ),
                          child: DropdownButton<String>(
                            hint: Text(
                              'Select Topic',
                            ),
                            isExpanded: true,
                            value: currentTopic,
                            underline: Container(),
                            items: topicDataList.map((value) {
                              return DropdownMenuItem<String>(
                                value: value.name,
                                child: new Text(value.name),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                currentTopic = value;
                              });
                              topicDataList.forEach((element) {
                                if (element.name == value) {
                                  setState(() {
                                    currentTopicId = element.topicId;
                                  });
                                }
                              });
                              print('TOPIC >>>>> $value');
                              print('TOPIC ID >>>>> $currentTopicId');
                            },
                          ),
                        ),
                        //=========================================================================//
                        SizedBox(
                          height: 15,
                        ),
                        // ============================== TIME PICKER (FROM) ==========================//
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                  spreadRadius: 1),
                            ],
                          ),
                          child: TextField(
                            controller: inputFromController,
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "FROM",
                            ),
                            readOnly: true,
                            onTap: () async {
                              // FocusScope.of(context).requestFocus(FocusNode());
                              FocusScope.of(context).unfocus();
                              await showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                                initialEntryMode: TimePickerEntryMode.input,
                                builder: (BuildContext context, Widget child) {
                                  return Theme(
                                    data: ThemeData.light().copyWith(
                                      primaryColor: Colors.white,
                                      accentColor: Colors.white,
                                      colorScheme: ColorScheme.light(
                                        primary: AssetStrings.color4,
                                        onPrimary: Colors.white,
                                        surface: AssetStrings.color1,
                                        onSurface: Colors.black,
                                      ),
                                      buttonTheme: ButtonThemeData(
                                        textTheme: ButtonTextTheme.primary,
                                      ),
                                    ),
                                    child: child,
                                  );
                                },
                              ).then((value) {
                                setState(() {
                                  fromTime = value;
                                  inputFromController.text =
                                      '${localizations.formatTimeOfDay(value)}';
                                  toTime = value.replacing(
                                    hour: value.hour + 1,
                                    minute: value.minute,
                                  );
                                  inputToController.text =
                                      '${localizations.formatTimeOfDay(toTime)}';
                                  print("FROM TIME: $fromTime");
                                  print("TO TIME: $toTime");
                                });
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        // ============================== TIME PICKER (TO) ==========================//
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                  spreadRadius: 1),
                            ],
                          ),
                          child: TextField(
                            controller: inputToController,
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "TO",
                            ),
                            readOnly: true,
                            onTap: () async {
                              // FocusScope.of(context).requestFocus(FocusNode());
                              FocusScope.of(context).unfocus();

                              await showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                                initialEntryMode: TimePickerEntryMode.input,
                                builder: (BuildContext context, Widget child) {
                                  return Theme(
                                    data: ThemeData.light().copyWith(
                                      primaryColor: Colors.white,
                                      accentColor: Colors.white,
                                      colorScheme: ColorScheme.light(
                                        primary: AssetStrings.color4,
                                        onPrimary: Colors.white,
                                        surface: AssetStrings.color1,
                                        onSurface: Colors.black,
                                      ),
                                      buttonTheme: ButtonThemeData(
                                        textTheme: ButtonTextTheme.primary,
                                      ),
                                    ),
                                    child: child,
                                  );
                                },
                              ).then((value) {
                                setState(() {
                                  toTime = value;
                                  inputToController.text =
                                      '${localizations.formatTimeOfDay(value)}';
                                });
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        //============================== DATE PICKER =========================//
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                  spreadRadius: 1),
                            ],
                          ),
                          child: TextField(
                            controller: inputDateController,
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Date",
                            ),
                            readOnly: true,
                            onTap: () async {
                              // FocusScope.of(context).requestFocus(FocusNode());
                              FocusScope.of(context).unfocus();
                              await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(DateTime.now().year - 50),
                                lastDate: DateTime(DateTime.now().year + 20),
                                builder: (BuildContext context, Widget child) {
                                  return Theme(
                                    data: ThemeData().copyWith(
                                      colorScheme: ColorScheme.light(
                                        primary: AssetStrings.color4,
                                        onPrimary: Colors.white,
                                        surface: AssetStrings.color1,
                                        onSurface: Colors.black,
                                      ),
                                      dialogBackgroundColor:
                                          AssetStrings.color1,
                                    ),
                                    child: child,
                                  );
                                },
                              ).then((value) {
                                setState(() {
                                  date1 = value;
                                  print(date1);
                                  inputDateController.text =
                                      '${myDateFormat.format(value)}';
                                });
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 60),
                          child: MaterialButton(
                            color: AssetStrings.color4,
                            height: 50,
                            minWidth: 100,
                            shape: StadiumBorder(),
                            child: Text(
                              "Submit",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                            onPressed: () async {
                              // var dFormat = new DateFormat('HH:mm a');
                              // print(DateTime.now());
                              var myYear = date1.year;
                              var myMonth = date1.month;
                              var myDay = date1.day;

                              try {
                                var fDate = DateTime(myYear, myMonth, myDay,
                                    fromTime.hour, fromTime.minute, 00);
                                var tDate = DateTime(myYear, myMonth, myDay,
                                    toTime.hour, toTime.minute, 00);

                                var baal = DateFormat('yyyy-MM-dd kk:mm:ss')
                                    .format(fDate);

                                var saal = DateFormat('yyyy-MM-dd kk:mm:ss')
                                    .format(tDate);

                                var diff = tDate.difference(fDate).inHours;
                                print(">>>>>> $diff");

                                if (diff < 1) {
                                  return customToast('Please fix your time');
                                }

                                print("Baal: " + baal);
                                print("Saal: " + saal);

                                var locId = await SharedPreferenceHelper()
                                    .getLocationId();
                                int subId = currentSubId;
                                int topId = currentTopicId;

                                print('From Date: $baal');
                                print("To Date: $saal");
                                print("Tutor Id: $tutorId");
                                print("Location Id: $locId");
                                print("Subject Id: $subId");
                                print("Topic Id: $topId");
                                Navigator.pop(context);
                                try {
                                  EasyLoading.show(status: "Please Wait!");
                                  Services serv = Services();
                                  await serv.addStudentClass(
                                    tutorId: tutorId,
                                    locationId: locId,
                                    subjectId: subId,
                                    topicId: topId ?? null,
                                    timeFrom: baal,
                                    timeTo: saal,
                                  );
                                  EasyLoading.showSuccess("Request Sent");
                                } catch (e) {
                                  print(e);
                                  EasyLoading.showError(
                                      "Request not Accepted!");
                                  customToast("Select all the items");
                                  // EasyLoading.dismiss();
                                }
                              } catch (e) {
                                customToast("Fill up all the forms!");
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }

// Future<void> selectDate(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//         context: context,
//         initialDate: selectedDate,
//         firstDate: DateTime(2015, 8),
//         lastDate: DateTime(2101));
//     if (picked != null && picked != selectedDate)
//       setState(() {
//         selectedDate = picked;
//       });
//   }

}
