import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_class_details_model.dart';
import 'package:Heilo/pages/student/payment_page.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:Heilo/widgets/rating/tutor_rating.dart';
import 'package:Heilo/widgets/student_home/tution_code_dialog.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class TimerPage extends StatefulWidget {
  final String code;
  final String classId;
  final int startHour;
  final int startMinute;

  const TimerPage({
    Key key,
    @required this.code,
    @required this.classId,
    @required this.startHour,
    @required this.startMinute,
  }) : super(key: key);
  @override
  _TimerPageState createState() => _TimerPageState();
}

class _TimerPageState extends State<TimerPage> {
  final CustomTimerController _controller = new CustomTimerController();
  Services serv = Services();
  StudentClassDetailsModel futureClass = StudentClassDetailsModel();
  bool _isLoading;

  /// GET API DATA ///
  _getData() async {
    setState(() {
      _isLoading = true;
    });
    await serv
        .getStudentClassDetails(refresh: false, classId: widget.classId)
        .then((value) {
      setState(() {
        futureClass = value;
      });
    });

    print('Ficking class id ${widget.classId}');
    print("FRICKING OUR ${futureClass.data.totalHour}");

    setState(() {
      _isLoading = false;
    });
  }

  /// FUNCTION FOR COMPLETE FUNCTION ///
  Future<void> _onCompleted() async {
    print('Timer Stoppppppp');
    print("CODE: ${widget.code}");
    print("CLASS ID: ${widget.classId}");
    print("TIMESTAMP: ${DateTime.now()}");
    try {
      EasyLoading.show(status: "Processing...");
      await serv.endTutionStudent(
        code: widget.code,
        status: "complete",
        endTime: DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now()),
        classId: widget.classId,
      );
      EasyLoading.dismiss();
      await tutorRatingDialog(
        context: context,
        classId: futureClass.data.classId.toString(),
        tutorId: futureClass.data.tutorId.toString(),
        prefer: "1",
      );

      String paymentAmount = futureClass.data.payment;
      print("PAYMENT AMOUNT : $paymentAmount");

      await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PaymentPage(
                    payment: futureClass.data.payment,
                  )));

      Navigator.pop(context);

      _controller.reset();

      setState(() {});

      customToast("Tution Ended");
      // Navigator.pop(context);
    } catch (e) {
      EasyLoading.dismiss();

      customToast("Server Error!");
    }
    EasyLoading.dismiss();
  }

  Future<void> _onStarted() async {
    EasyLoading.show(status: "Processing...");

    print('Timer Staaaaart');
    print("CODE: ${widget.code}");
    print("CLASS ID: ${widget.classId}");
    print("TIMESTAMP: ${DateTime.now()}");
    try {
      await serv.startTutionStudent(
        code: widget.code,
        status: "start",
        startTime: DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now()),
        classId: widget.classId,
      );

      EasyLoading.dismiss();
      _controller.start();
      setState(() {});
      customToast("Tution Started");
    } catch (e) {
      print(e.toString());

      EasyLoading.dismiss();

      customToast("Server Error!");
    }
    EasyLoading.dismiss();
  }

  Future<bool> _onBackPressed() async {
    if (_controller.state == CustomTimerState.counting) {
      customToast("Complete the tution first");
      return false;
    } else {
      return true;
    }
  }

  @override
  void initState() {
    super.initState();

    setState(() {
      _controller.state;
    });
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    //
    final sHeight = MediaQuery.of(context).size.height;
    final sWidth = MediaQuery.of(context).size.width;
    //
    TextStyle style1 = TextStyle(
      fontSize: sWidth * 0.13,
      color: AssetStrings.color4,
    );

    TextStyle style2 = TextStyle(
      fontWeight: FontWeight.normal,
    );

    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: _isLoading
          ? CustomLoading2()
          : WillPopScope(
              onWillPop: _onBackPressed,
              child: SafeArea(
                child: Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 100,
                          alignment: Alignment.topCenter,
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            "Heilo",
                            style: TextStyle(
                              fontSize: sHeight * 0.06,
                              fontFamily: 'Antonellie',
                              foreground: Paint()
                                ..shader = AssetStrings.linearGradient,
                            ),
                          ),
                        ),
                        Container(
                          width: sWidth * 0.75,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: AssetStrings.color4,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          padding: EdgeInsets.all(30),

                          ///----------------------CUSTOM TIMER LIBRARY CALL---------------------///
                          child: CustomTimer(
                            controller: _controller,
                            from: Duration(
                                hours: widget.startHour,
                                minutes: widget.startMinute),
                            to: Duration(hours: 0),
                            interval: Duration(seconds: 1),
                            onResetAction: CustomTimerAction.go_to_end,
                            onFinish: () => _onCompleted(),
                            builder: (CustomTimerRemainingTime remaining) {
                              // return Text(
                              //   "${remaining.hours}:${remaining.minutes}:${remaining.seconds}",
                              //   style: TextStyle(
                              //     fontSize: 50.0,
                              //     color: AssetStrings.color4,
                              //   ),
                              // );
                              return Row(
                                // mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ///-------------------------HOURS COUNT------------------------///
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "${remaining.hours}:",
                                        style: style1,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 8),
                                        child: Text(
                                          'Hr',
                                          style: style2,
                                        ),
                                      ),
                                    ],
                                  ),

                                  ///-------------------------MINUTES COUNT------------------------///
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "${remaining.minutes}:",
                                        style: style1,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 8),
                                        child: Text(
                                          'Min',
                                          style: style2,
                                        ),
                                      ),
                                    ],
                                  ),

                                  ///-------------------------SECONDS COUNT------------------------///
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "${remaining.seconds}",
                                        style: style1,
                                      ),
                                      Text(
                                        'Sec',
                                        style: style2,
                                      )
                                    ],
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),

                        ///----------------------------START BUTTON----------------------------///
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            MaterialButton(
                              child: Text(
                                "Start",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: (_controller.state ==
                                      CustomTimerState.counting)
                                  ? null
                                  : () async {
                                      // await _onStarted();
                                      // customToast("Tution Started");
                                      studentSecretCodeDialog(
                                        classId: widget.classId,
                                        code: widget.code,
                                        context: context,
                                      ).then((value) {
                                        print(value);
                                        if (value == true) {
                                          print("FUNCTION GOES HERE");
                                          _onStarted();
                                        } else {
                                          return;
                                        }
                                      });

                                      // setState(() {
                                      //   _controller.state;
                                      // });
                                    },
                              color: AssetStrings.color4,
                              disabledColor: Colors.grey,
                              shape: StadiumBorder(),
                              minWidth: 120,
                              elevation: 3,
                            ),
                            // FlatButton(
                            //   child: Text("Pause", style: TextStyle(color: Colors.white)),
                            //   onPressed: () => _controller.pause(),
                            //   color: Colors.blue,
                            // ),

                            ///----------------------RESET BUTTON----------------------------///
                            MaterialButton(
                              child: Text(
                                "Complete",
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: (_controller.state !=
                                      CustomTimerState.counting)
                                  ? null
                                  : () async {
                                      // customToast("Tution Stopped");
                                      await _onCompleted();
                                      print(_controller.state);
                                      setState(() {});
                                    },
                              color: Colors.white,
                              disabledColor: Colors.grey,
                              minWidth: 120,
                              shape: StadiumBorder(),
                              elevation: 3,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),

                        ///--------------------------PLACE------------------------------///
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: sWidth,
                            height: sHeight * 0.12,
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    'PLACE:',
                                    style: TextStyle(
                                      // fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    futureClass.data.tutor.location.area ??
                                        'Not Available',
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        ///------------------------SUBJECT & TOPIC------------------------------///
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: sWidth,
                            height: sHeight * 0.12,
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              color: AssetStrings.color1,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    'SUBJECT & TOPIC:',
                                    style: TextStyle(
                                      // fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    // 'Subject and topic names',
                                    futureClass.data.subject.subject ??
                                        'Not Available',

                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        // ///------------------------DESCRIPTION------------------------------///
                        // Expanded(
                        //   child: Container(
                        //     width: sWidth,
                        //     height: sHeight * 0.12,
                        //     padding: EdgeInsets.all(20),
                        //     decoration: BoxDecoration(
                        //       color: Colors.white,
                        //       borderRadius: BorderRadius.only(
                        //         topLeft: Radius.circular(30),
                        //       ),
                        //     ),
                        //     child: Column(
                        //       mainAxisAlignment: MainAxisAlignment.center,
                        //       children: [
                        //         Align(
                        //           alignment: Alignment.topLeft,
                        //           child: Text(
                        //             'DESCRIPTION:',
                        //             style: TextStyle(
                        //               // fontSize: 12,
                        //               fontWeight: FontWeight.bold,
                        //             ),
                        //           ),
                        //         ),
                        //         SizedBox(
                        //           height: 5,
                        //         ),
                        //         Align(
                        //           alignment: Alignment.centerLeft,
                        //           child: Text(
                        //             futureClass.data.subject.description ??
                        //                 'Not Available',
                        //             style: TextStyle(
                        //               fontSize: 12,
                        //             ),
                        //           ),
                        //         ),
                        //       ],
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                    Positioned(
                      top: 10,
                      left: 15,
                      child: GestureDetector(
                        ///-----------------------BACK BUTTON-----------------------///
                        child: FaIcon(
                          FontAwesomeIcons.angleLeft,
                          size: 35,
                          color: AssetStrings.color4,
                        ),
                        onTap: () {
                          if (_controller.state == CustomTimerState.counting) {
                            customToast("Complete the tution first");
                            return false;
                          } else {
                            Navigator.pop(context, true);
                            return true;
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
