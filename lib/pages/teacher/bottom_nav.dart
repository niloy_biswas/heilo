// import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/pages/common/contact_us.dart';
import 'package:Heilo/pages/teacher/profile_view.dart';
// import 'package:Heilo/pages/common/drawer_Content_home.dart';

import 'package:Heilo/pages/teacher/teacher_bell.dart';
import 'package:Heilo/pages/teacher/teacher_chat.dart';
import 'package:Heilo/pages/teacher/teacher_home.dart';
// import 'package:Heilo/pages/teacher/teacher_profile.dart';
import 'package:Heilo/pages/teacher/teacher_sidebar.dart';
// import 'package:Heilo/services/notifications.dart';
import 'package:community_material_icon/community_material_icon.dart';
// import 'package:Heilo/pages/teacher/teacher_home.dart';
import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:move_to_background/move_to_background.dart';

// import '../common/drawer_Content.dart';

class TeacherBottomNav extends StatefulWidget {
  @override
  _TeacherBottomNavState createState() => _TeacherBottomNavState();
}

class _TeacherBottomNavState extends State<TeacherBottomNav> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget _child;

  // onNotificationClick(String payload) {
  //   //
  //   print('Payload $payload');
  //   //

  //   Future.delayed(Duration(seconds: 1)).then((_) async {
  //     String classId =
  //         await SharedPreferenceHelper().getCompleteNotificationKey();

  //     if (classId != null && classId != "") {
  //       await Navigator.of(context)
  //           .push(MaterialPageRoute(builder: (context) => ContactUs()));
  //     }
  //   });
  // }

  @override
  void initState() {
    super.initState();
    // notificationPlugin.setOnNotificationClick(onNotificationClick);
    setState(() {
      _child = TeacherHome();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async {
          MoveToBackground.moveTaskToBack();
          return false;
        },
        child: Scaffold(
          backgroundColor: AssetStrings.color1,
          extendBody: true,
          // endDrawer: DrawerContent(),
          body: _child,
          bottomNavigationBar: Container(
            child: FluidNavBar(
              icons: [
                FluidNavBarIcon(
                    svgPath: "assets/icons/bell.svg",
                    // icon: Icons.notifications_none,
                    backgroundColor: AssetStrings.color4,
                    extras: {"label": "home"}),
                FluidNavBarIcon(
                    svgPath: "assets/icons/user.svg",
                    // icon: Icons.person,
                    backgroundColor: AssetStrings.color4,
                    extras: {"label": "bookmark"}),
                FluidNavBarIcon(
                    // svgPath: "assets/mainlogo.svg",
                    // icon: Icons.home_filled,
                    // iconPath: "assets/mainlogo.svg",
                    icon: CommunityMaterialIcons.school,
                    backgroundColor: AssetStrings.color4,
                    extras: {"label": "partner"}),
                FluidNavBarIcon(
                    svgPath: "assets/icons/chat.svg",
                    // icon: Icons.message,
                    backgroundColor: AssetStrings.color4,
                    extras: {"label": "conference"}),
                FluidNavBarIcon(
                    svgPath: "assets/icons/sidebar.svg",
                    // icon: Icons.sort,
                    backgroundColor: AssetStrings.color4,
                    extras: {"label": "conference"}),
              ],
              onChange: _handleNavigationChange,
              style: FluidNavBarStyle(
                iconUnselectedForegroundColor: Colors.white,
                barBackgroundColor: AssetStrings.color4,
              ),
              scaleFactor: 1.2,
              defaultIndex: 2,
              itemBuilder: (icon, item) => Semantics(
                label: icon.extras["label"],
                child: item,
              ),
              animationFactor: 0.1,
            ),
          ),
        ),
      ),
    );
  }

  void _handleNavigationChange(int index) {
    setState(
      () {
        switch (index) {
          case 0:
            _child = TutorNotification();
            break;
          case 1:
            _child = TutorOwnProfile(isPopable: false);
            break;
          case 2:
            _child = TeacherHome();
            break;
          case 3:
            _child = TutorChat();
            break;
          case 4:
            _child = TutorSideBar();
            // _child = dumdum();
            // Scaffold.of(context).openEndDrawer();
            break;
        }
        _child = AnimatedSwitcher(
          switchInCurve: Curves.easeOut,
          switchOutCurve: Curves.easeIn,
          duration: Duration(milliseconds: 100),
          child: _child,
        );
      },
    );
  }

  dumdum() {
    _scaffoldKey.currentState.openEndDrawer();
    // Scaffold.of(context).openEndDrawer();
    // print('sidebar');
  }
}
