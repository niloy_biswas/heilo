import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/pages/common/drawer_Content.dart';
import 'package:Heilo/services/notifications.dart';
import 'package:Heilo/services/notifications2.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:Heilo/widgets/rating/student_rating.dart';
import 'package:Heilo/widgets/review_tile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TutorOwnProfile extends StatefulWidget {
  final bool isPopable;

  const TutorOwnProfile({Key key, @required this.isPopable}) : super(key: key);
  @override
  _TutorOwnProfileState createState() => _TutorOwnProfileState();
}

class _TutorOwnProfileState extends State<TutorOwnProfile> {
  TutorUserInfoModel futureTutorDetail;
  Services serv = Services();
  bool _isLoading;

  _getData() async {
    try {
      setState(() {
        _isLoading = true;
      });
      await serv.getTutorUserInfo(refresh: false).then((value) {
        setState(() {
          futureTutorDetail = value;
        });
      });
      setState(() {
        _isLoading = false;
      });
      await serv.getTutorUserInfo(refresh: true).then((value) {
        setState(() {
          futureTutorDetail = value;
        });
      });
      // } on SocketException {
      //   customToast("No Internet Connection");
    } catch (e) {
      print(e);
      // customToast("Something Went Wrong!");
    }
  }

  onNotificationClick(String payload) {
    //
    print('Payload $payload');
    //
  }

  onNotificationClick2(String payload) async {
    //
    print('Payload $payload');
    //
    if (payload == "01") {
      return;
    } else {
      await studentRatingDialog(
        context: context,
        classId: payload,
        prefer: '1',
        checkBoxValue: false,
      );
    }
  }

  @override
  void initState() {
    notificationPlugin.setOnNotificationClick(onNotificationClick);
    notificationPlugin2.setOnNotificationClick(onNotificationClick2);

    _isLoading = true;
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      endDrawer: widget.isPopable ? DrawerContent() : null,
      extendBody: true,
      // body:
      body: _isLoading
          ? CustomLoading2()
          : SingleChildScrollView(
              child: Stack(
                children: [
                  Column(
                    children: [
                      Container(
                        height: sHeight * 0.33,
                        width: sWidth,
                        decoration: BoxDecoration(
                          color: AssetStrings.color4,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(
                              height: sWidth * 0.28,
                              width: sWidth * 0.28,
                              child: Stack(
                                fit: StackFit.expand,
                                children: [
                                  /////////////////////////////// PROFILE PICTURE ////////////////////////////////
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: CircleAvatar(
                                        maxRadius: 50,
                                        backgroundImage:
                                            (futureTutorDetail.data.dp == null)
                                                ? AssetImage(
                                                    "assets/images/home.png")
                                                : NetworkImage(
                                                    futureTutorDetail.data.dp)),
                                  ),
                                  /////////////////////////////// RATING CHIP ////////////////////////////////
                                  Positioned(
                                    // top: 0,
                                    right: 0,
                                    top: 0,

                                    child: Container(
                                      height: 20,
                                      width: 33,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(10),
                                        border: Border.all(
                                            color: AssetStrings.color4,
                                            width: 2),
                                      ),
                                      child: Center(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          /////////////////////////////// RATING TEXT ////////////////////////////////

                                          Text(
                                            futureTutorDetail.data.rating
                                                    .toStringAsFixed(1) ??
                                                '0',
                                            style: TextStyle(fontSize: 12),
                                          ),
                                          Icon(
                                            Icons.star,
                                            size: 12,
                                            color: AssetStrings.color4,
                                          ),
                                        ],
                                      )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),

                              ///-----------------------TUTOR NAME---------------------///
                              child: Text(
                                futureTutorDetail.data.name ?? 'Not Available',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black54,
                                ),
                              ),
                            ),

                            ///---------------------SCHOOL NAME----------------------///
                            (futureTutorDetail.data.educations.length < 1)
                                ? Container()
                                : Text(
                                    futureTutorDetail.data.educations[0]
                                            .institutionName ??
                                        'Not Available',
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                            // (futureTutorDetail.data.locations.length < 1)
                            //     ? Container()
                            //     : MediaQuery.removePadding(
                            //         context: context,
                            //         removeTop: true,
                            //         child: ListView.builder(
                            //           scrollDirection: Axis.vertical,
                            //           shrinkWrap: true,
                            //           itemCount: futureTutorDetail
                            //               .data.locations.length,
                            //           itemBuilder: (context, index) {
                            //             return locationWidget(
                            //               area: futureTutorDetail
                            //                   .data.locations[index].area,
                            //               district: futureTutorDetail
                            //                   .data.locations[index].district,
                            //             );
                            //           },
                            //         ),
                            //       ),
                          ],
                        ),
                      ),

                      SizedBox(
                        height: 30,
                      ),

                      /// RATING and COMPLETED TUTION COUNT ///
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            height: 70,
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 2),
                                  color: AssetStrings.color4.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 5,
                                )
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ///---------------- HOURLY RATE NUMBER -------------------///
                                    Text(
                                      futureTutorDetail.data.hourlyRate ?? '0',
                                      style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      "/hr",
                                      style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                  'Hourly Rate',
                                  style: TextStyle(fontSize: 9),
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                          // SizedBox(
                          //   width: sWidth * 0.15,
                          // ),
                          Container(
                            height: 70,
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 2),
                                  color: AssetStrings.color4.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 5,
                                )
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ///-----------------COMPLETED TUTION-----------------///
                                Text(
                                  futureTutorDetail.data.totalClassCompleted
                                          .toString() ??
                                      '0',
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  'Completed Tutions',
                                  style: TextStyle(fontSize: 9),
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),

                          /////////////////////////// REFER BY ///////////////////////////
                          Container(
                            height: 70,
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 2),
                                  color: AssetStrings.color4.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 5,
                                )
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ////////////////////// REFER NO ///////////////////////
                                Text(
                                  futureTutorDetail.data.reviews.length
                                          .toString() ??
                                      '0',
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  'Students Refer',
                                  style: TextStyle(fontSize: 9),
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),

                      Container(
                        padding: EdgeInsets.all(16),
                        // height: sHeight * 0.2,
                        width: sWidth * 0.87,
                        alignment: Alignment.topLeft,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 2),
                              color: AssetStrings.color4.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 5,
                            )
                          ],
                        ),
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.all(5),
                              margin: EdgeInsets.symmetric(vertical: 5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: AssetStrings.color4,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Flexible(
                                    flex: 2,
                                    child: Text(
                                      "Pending Income: ",
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    flex: 1,
                                    child: Text(
                                      (futureTutorDetail.data
                                                      .totalClassCompleted *
                                                  (double.parse(
                                                          futureTutorDetail.data
                                                              .hourlyRate) *
                                                      0.8))
                                              .toString() +
                                          " TK",
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  // Text(futureTutorDetail.data.totalClassCompleted),
                                ],
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.all(5),
                              margin: EdgeInsets.symmetric(vertical: 5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: AssetStrings.color4,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Flexible(
                                    flex: 2,
                                    child: Text(
                                      "Total Income: ",
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    flex: 1,
                                    child: Text(
                                      // (futureTutorDetail
                                      //                 .data.totalClassCompleted *
                                      //             int.tryParse(
                                      //                 "${futureTutorDetail.data.hourlyRate}"))
                                      //         .toString() +
                                      //     " TK",
                                      '0 TK',
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  // Text(futureTutorDetail.data.totalClassCompleted),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      SizedBox(
                        height: 30,
                      ),

                      //====================================== SKILL SET VIEW ==================================//
                      Container(
                        padding: EdgeInsets.all(16),
                        // height: sHeight * 0.2,
                        width: sWidth * 0.87,
                        alignment: Alignment.topLeft,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 2),
                              color: AssetStrings.color4.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 5,
                            )
                          ],
                        ),
                        child: Column(
                          children: [
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Skills:',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                //////////////////////////// SKILL 1 ///////////////////////////
                                MediaQuery.removePadding(
                                  context: context,
                                  removeBottom: true,
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount:
                                        futureTutorDetail.data.subjects.length,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      return Container(
                                        alignment: Alignment.topLeft,
                                        padding: EdgeInsets.all(5),
                                        margin:
                                            EdgeInsets.symmetric(vertical: 5),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: AssetStrings.color4,
                                            width: 1,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        child: Text(
                                          futureTutorDetail.data.subjects[index]
                                                  .subject ??
                                              'Not Available',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),

                      SizedBox(
                        height: 30,
                      ),

                      ///-------------------------ABOUT SECTION-------------------------///
                      Container(
                        padding: EdgeInsets.all(16),
                        height: sHeight * 0.2,
                        width: sWidth * 0.87,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 2),
                              color: AssetStrings.color4.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 5,
                            )
                          ],
                        ),
                        child: ListView(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          children: [
                            Text(
                              'About Tutor:',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),

                            ///--------------------------ABOUT AUTHOR PARAGRAPH---------------------///
                            (futureTutorDetail.data.about == null)
                                ? Container(
                                    height: 90,
                                    child: Center(
                                      child: Text('Not Available'),
                                    ),
                                  )
                                : Text(futureTutorDetail.data.about),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),

                      ///------------------------------ REVIEW SECTION ----------------------------///
                      Container(
                        padding: EdgeInsets.all(16),
                        // height: sHeight * 0.35,
                        width: sWidth * 0.87,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 2),
                              color: AssetStrings.color4.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 5,
                            )
                          ],
                        ),
                        child: Column(
                          // mainAxisSize: MainAxisSize.min,
                          // shrinkWrap: true,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Text(
                                  'Reviews:',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            (futureTutorDetail.data.reviews.length < 1)
                                ? Container(
                                    height: 200,
                                    margin: EdgeInsets.only(top: 90),
                                    child: Center(
                                      child: Text("Not Available"),
                                    ),
                                  )
                                : SizedBox(
                                    height: 200,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount:
                                          futureTutorDetail.data.reviews.length,
                                      itemBuilder: (context, index) {
                                        // return Text(
                                        //     apiData.data.reviews[index].feedback);
                                        return Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 3),
                                          child: ReviewTile(
                                            dp: futureTutorDetail
                                                .data
                                                .reviews[index]
                                                .studentDetails
                                                .dp,
                                            title: futureTutorDetail
                                                .data
                                                .reviews[index]
                                                .studentDetails
                                                .name,
                                            subtitle: futureTutorDetail
                                                .data.reviews[index].feedback,
                                            rating: futureTutorDetail
                                                .data.reviews[index].rate
                                                .toString(),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                          ],
                        ),
                        // child: ListView.builder(
                        //   itemCount: apiData.data.reviews.length,
                        //   itemBuilder: (context, index) {
                        //     return Text(apiData.data.reviews[index].feedback);
                        //   },
                        // ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                  Positioned(
                    top: 10,
                    left: 15,
                    child: widget.isPopable
                        ? GestureDetector(
                            ///-----------------------BACK BUTTON-----------------------///
                            child: FaIcon(
                              FontAwesomeIcons.angleLeft,
                              size: 35,
                              color: Colors.white,
                            ),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          )
                        : Container(
                            height: 35,
                            width: 35,
                          ),
                  ),
                ],
              ),
            ),
    );
  }

  Padding locationWidget({
    @required String area,
    @required String district,
  }) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FaIcon(
            FontAwesomeIcons.mapMarkerAlt,
            color: Colors.white,
            size: 14,
          ),
          SizedBox(
            width: 5,
          ),

          ///---------------------LOCATION NAME----------------///
          Text(
            "$area, $district" ?? 'Location Not Available',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
