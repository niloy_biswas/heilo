import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/location_model.dart';
import 'package:Heilo/pages/teacher/sign_up_skills.dart';
// import 'package:Heilo/pages/teacher/sign_up_skills.dart';
// import 'package:Heilo/pages/common/otp_page.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/customInputField.dart';
import 'package:Heilo/widgets/custom_toast.dart';
// import 'package:Heilo/widgets/customInputField.dart';
// import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/gradientButton.dart';
import 'package:Heilo/widgets/location/district_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:flutter_dropdown/flutter_dropdown.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:fluttertoast/fluttertoast.dart';

class SignUpLocation extends StatefulWidget {
  @override
  _SignUpLocationState createState() => _SignUpLocationState();
}

class _SignUpLocationState extends State<SignUpLocation> {
  GlobalKey _scaffoldKey = GlobalKey();
  String userType;
  // String regName;
  // String regEmail;
  // String regPasswd;
  // String regSegmentId;
  // String regMediumOfStudy;
  // String regMobNo;
  //
  String regDistrict;
  String regRoad;
  String regArea;
  // String regDetailAddress;
  //
  String selectDistrictButtonString = "Select Location";

  TextEditingController detailAddressController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  Services serv = Services();

  LocationModel locModel;
  List<String> areaList = [];
  List<String> districtList = [];
  List<String> roadList = [];

  String selectedDist;
  String selectedArea;
  String selectedRoad;

  // _customToast(String content) {
  //   return Fluttertoast.showToast(
  //       msg: content,
  //       toastLength: Toast.LENGTH_SHORT,
  //       gravity: ToastGravity.BOTTOM,
  //       timeInSecForIosWeb: 1,
  //       backgroundColor: Colors.black,
  //       textColor: Colors.white,
  //       fontSize: 16.0);
  // }

  _getData() async {
    userType = await SharedPreferenceHelper().getUserType();
    // regName = await SharedPreferenceHelper().getRegName();
    // regEmail = await SharedPreferenceHelper().getRegEmail();
    // regPasswd = await SharedPreferenceHelper().getRegPassword();
    // regSegmentId = await SharedPreferenceHelper().getRegSegmentId();
    // regMediumOfStudy = await SharedPreferenceHelper().getRegMedium();
    // regMobNo = await SharedPreferenceHelper().getRegMobile();
    selectedDist = await SharedPreferenceHelper().getRegDistrict();
    selectedArea = await SharedPreferenceHelper().getRegArea();
    selectedRoad = await SharedPreferenceHelper().getRegRoad();

    setState(() {});

    await serv.getDioLocations(refresh: true).then((value) {
      locModel = value;
    });

    for (int i = 0; i < locModel.data.length; i++) {
      areaList.add(locModel.data[i].area);
    }

    for (int i = 0; i < locModel.data.length; i++) {
      districtList.add(locModel.data[i].district);
    }

    for (int i = 0; i < locModel.data.length; i++) {
      roadList.add(locModel.data[i].road);
    }

    setState(() {});
    print(userType);
  }

  @override
  void initState() {
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //

    //
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AssetStrings.color1,
      // resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Row(
                children: [
                  // BackTextButton(
                  //   color: Colors.blueGrey[600],
                  // ),
                  //////////////////////////////// BACK BUTTON //////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        // Navigator.pushReplacement(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => LoginPage()));
                      },
                      child: Text(
                        '<',
                        style: TextStyle(
                          fontFamily: 'QuickSandMedium',
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                          color: Colors.blueGrey[600],
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              // Welcome message
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 40, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Welcome',
                          style: TextStyle(
                              fontFamily: 'QuickSandMedium',
                              fontSize: 20,
                              color: Colors.blueGrey),
                        ),
                        Text(
                          'SIGN UP',
                          style: TextStyle(
                              fontFamily: 'QuickSandMedium',
                              fontSize: 26,
                              color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: sHeight * 0.1,
              ),
              Expanded(
                child: Container(
                  height: sHeight * 0.6,
                  decoration: BoxDecoration(
                    color: AssetStrings.color4,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50.0),
                      topRight: Radius.circular(50.0),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: sHeight * 0.08,
                        ),

                        // SizedBox(
                        //   height: 20,
                        // ),
                        Container(
                          padding: EdgeInsets.only(bottom: 30),
                          child: Text(
                            "Enter Your Location",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 50),
                          height: 40,
                          width: double.infinity,

                          ////////////////////////////////// DISTRICT DROPDOWN /////////////////////////////
                          ///
                          child: Container(
                            height: 60,
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 5,
                                  offset: Offset(0, 7),
                                ),
                              ],
                            ),
                            child: TextField(
                              controller: locationController,
                              decoration: InputDecoration(
                                hintText: "Select Location",
                                hintStyle: TextStyle(
                                  color: Colors.black87,
                                ),
                                border: InputBorder.none,
                                alignLabelWithHint: true,
                              ),
                              textAlign: TextAlign.center,
                              readOnly: true,
                              onTap: () async {
                                List distList = [];
                                await serv
                                    .getDioLocations(refresh: false)
                                    .then((value) {
                                  for (int i = 0; i < value.data.length; i++) {
                                    // print("${value.data[i].district}");
                                    distList.add(value.data[i].district);
                                  }
                                  // distList.toSet().toList();
                                  // print(distList.toSet().toList());
                                });
                                await districtDialog(
                                  context: context,
                                  districtList: distList.toSet().toList(),
                                );

                                await _getData();
                                // await Future.delayed(Duration(seconds: 1), () {
                                //   setState(() {
                                //     locationController.text =
                                //         "$selectedRoad, $selectedArea, $selectedDist";
                                //     print(
                                //         "$selectedRoad, $selectedArea, $selectedDist");
                                //   });
                                // });
                                // setState(() {});

                                setState(() {
                                  locationController.text =
                                      "$selectedRoad, $selectedArea, $selectedDist";
                                });

                                setState(() {});
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        // Container(
                        //   padding: EdgeInsets.symmetric(horizontal: 50),
                        //   height: 40,
                        //   width: double.infinity,

                        //   ////////////////////////////////// AREA DROPDOWN /////////////////////////////
                        //   ///
                        //   child: Container(
                        //       padding: EdgeInsets.symmetric(horizontal: 20),
                        //       decoration: BoxDecoration(
                        //         borderRadius: BorderRadius.circular(50),
                        //         color: Colors.white,
                        //         boxShadow: [
                        //           BoxShadow(
                        //             color: Colors.grey.withOpacity(0.5),
                        //             blurRadius: 5,
                        //             offset: Offset(0, 7),
                        //           ),
                        //         ],
                        //       ),
                        //       child: DropDown(
                        //         items: areaList.toSet().toList(),
                        //         hint: Text("Select Area"),
                        //         showUnderline: false,
                        //         isExpanded: true,
                        //         onChanged: (value) {
                        //           setState(() {
                        //             regArea = value;
                        //           });
                        //           print("AREA: $regArea");
                        //         },
                        //       )),
                        // ),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        // Container(
                        //   padding: EdgeInsets.symmetric(horizontal: 50),
                        //   height: 40,
                        //   width: double.infinity,

                        //   ////////////////////////////////// ROAD DROPDOWN /////////////////////////////
                        //   ///
                        //   child: Container(
                        //     padding: EdgeInsets.symmetric(horizontal: 20),
                        //     decoration: BoxDecoration(
                        //       borderRadius: BorderRadius.circular(50),
                        //       color: Colors.white,
                        //       boxShadow: [
                        //         BoxShadow(
                        //           color: Colors.grey.withOpacity(0.5),
                        //           blurRadius: 5,
                        //           offset: Offset(0, 7),
                        //         ),
                        //       ],
                        //     ),
                        //     child: DropDown(
                        //       items: roadList.toSet().toList(),
                        //       hint: Text("Select Road"),
                        //       isExpanded: true,
                        //       showUnderline: false,
                        //       onChanged: (value) {
                        //         setState(() {
                        //           regRoad = value;
                        //         });
                        //         print("AREA: $regRoad");
                        //       },
                        //     ),
                        //   ),
                        // ),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        //////////////////////////////// DETAIL ADDRESS /////////////////////////
                        ///
                        CustomInputField(
                          fieldName: 'Detail Address (Optional)',
                          inputPhoneController: detailAddressController,
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        GradientButton(
                          buttonName: 'NEXT',
                          sWidth: sWidth,
                          sHeight: sHeight,
                          function: () async {
                            FocusScope.of(context).unfocus();

                            print(regDistrict);
                            print(regArea);
                            print(regRoad);
                            try {
                              // EasyLoading.show(status: "Please Wait...");
                              // await SharedPreferenceHelper()
                              //     .saveRegDistrict(regDistrict);
                              // await SharedPreferenceHelper()
                              //     .saveRegArea(regArea);
                              // await SharedPreferenceHelper()
                              //     .saveRegRoad(regRoad);
                              await SharedPreferenceHelper()
                                  .saveRegDetailAddress(
                                      detailAddressController.text);
                              if (locationController.text != "") {
                                // Navigator.pushReplacement(
                                //     context,
                                //     MaterialPageRoute(
                                //         builder: (context) => OtpPage()));
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SignUpSkills()));
                              } else {
                                // EasyLoading.showError("Fill up all the items");
                                customToast("Fill up all the items");
                              }

                              EasyLoading.dismiss();
                            } catch (e) {
                              print(e);
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
