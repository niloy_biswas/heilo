import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/segment_model.dart';
import 'package:Heilo/models/subject_model.dart';
import 'package:Heilo/pages/common/otp_page.dart';
// import 'package:Heilo/pages/common/otp_page.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/class_list_dialog.dart';
// import 'package:Heilo/pages/common/login_page.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/gradientButton.dart';
import 'package:Heilo/widgets/medium_list_dialog.dart';
import 'package:Heilo/widgets/subject_list_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpSkills extends StatefulWidget {
  @override
  _SignUpSkillsState createState() => _SignUpSkillsState();
}

class _SignUpSkillsState extends State<SignUpSkills> {
  List<String> dummyList = ['a', 'b'];
  List<String> mediumList = ['Bangla', 'English'];
  List<int> classList = [];
  List<String> subjectList = [];
  List<int> subjectIdList = [];
  SegmentModel segmentModel = SegmentModel();
  SubjectModel subjectModel = SubjectModel();

  Services serv = Services();

  String selectedMedium;
  int selectedSegmentId;
  String selectedSubject;

  //
  String mediumButtonString = "Select Medium";
  String classButtonString = "Select Class";
  String subjectButtonString = "Select Subject";

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //

    //
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      // resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Row(
                children: [
                  // BackTextButton(
                  //   color: Colors.blueGrey[600],
                  // ),
                  //////////////////////////////// BACK BUTTON //////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        // Navigator.pushReplacement(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => LoginPage()));
                      },
                      child: Text(
                        '<',
                        style: TextStyle(
                          fontFamily: 'QuickSandMedium',
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                          color: Colors.blueGrey[600],
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              // Welcome message
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 40, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Welcome',
                          style: TextStyle(
                              fontFamily: 'QuickSandMedium',
                              fontSize: 20,
                              color: Colors.blueGrey),
                        ),
                        Text(
                          'SIGN UP',
                          style: TextStyle(
                              fontFamily: 'QuickSandMedium',
                              fontSize: 26,
                              color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: sHeight * 0.1,
              ),
              Expanded(
                child: Container(
                  height: sHeight * 0.6,
                  decoration: BoxDecoration(
                    color: AssetStrings.color4,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50.0),
                      topRight: Radius.circular(50.0),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: sHeight * 0.08,
                        ),

                        // SizedBox(
                        //   height: 20,
                        // ),
                        Container(
                          padding: EdgeInsets.only(bottom: 30),
                          child: Column(
                            children: [
                              Text(
                                "Enter Preferred Subject",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5),
                                child: Text(
                                  "(Add more from your profile)",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.black87),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 50),
                          height: 40,
                          width: double.infinity,

                          ////////////////////////////////// SELECT MEDIUM /////////////////////////////
                          ///
                          child: GestureDetector(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 5,
                                    offset: Offset(0, 7),
                                  ),
                                ],
                              ),
                              child: Container(
                                // alignment: Alignment.centerLeft,
                                width: double.infinity,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      mediumButtonString ?? "Select Medium",
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black87,
                                      ),
                                    ),
                                    FaIcon(
                                      FontAwesomeIcons.caretDown,
                                      size: 14,
                                      color: Colors.black54,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            onTap: () async {
                              setState(() {
                                classList = [];
                                subjectList = [];
                                subjectIdList = [];
                                SharedPreferenceHelper()
                                    .saveRegSkillClass("Select Class");
                                SharedPreferenceHelper()
                                    .saveRegSkillSub("Select Subject");
                              });

                              classButtonString = await SharedPreferenceHelper()
                                  .getRegSkillClass();
                              subjectButtonString =
                                  await SharedPreferenceHelper()
                                      .getRegSkillSub();
                              setState(() {});

                              await mediumListDialog(
                                context: context,
                                mediumList: mediumList,
                              );
                              mediumButtonString =
                                  await SharedPreferenceHelper()
                                      .getRegSkillMedium();
                              setState(() {});

                              try {
                                await serv
                                    .getSegmentData(
                                        refresh: true,
                                        medium:
                                            mediumButtonString.toLowerCase())
                                    .then((value) {
                                  setState(() {
                                    segmentModel = value;
                                  });
                                });
                                for (int i = 0;
                                    i < segmentModel.data.length;
                                    i++) {
                                  print(segmentModel.data[i].segmentId);
                                  classList.add(segmentModel.data[i].segmentId);
                                }
                                classList.toSet();
                                setState(() {});
                              } catch (e) {
                                customToast("errorr");
                                print(e);
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ////////////////////////////classsss
                        // classWidget(),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 50),
                          height: 40,
                          width: double.infinity,

                          ////////////////////////////////// SELECT CLASS /////////////////////////////
                          ///
                          child: GestureDetector(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 5,
                                    offset: Offset(0, 7),
                                  ),
                                ],
                              ),
                              child: Container(
                                // alignment: Alignment.centerLeft,
                                width: double.infinity,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      classButtonString ?? "Select Class",
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black87,
                                      ),
                                    ),
                                    FaIcon(
                                      FontAwesomeIcons.caretDown,
                                      size: 14,
                                      color: Colors.black54,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            onTap: () async {
                              // subjectList.clear();
                              subjectList = [];
                              subjectIdList = [];
                              SharedPreferenceHelper()
                                  .saveRegSkillSub("Select Subject");
                              subjectButtonString =
                                  await SharedPreferenceHelper()
                                      .getRegSkillSub();
                              setState(() {});

                              if (classList.isNotEmpty) {
                                await classListDialog(
                                  context: context,
                                  classList: classList,
                                );
                                classButtonString =
                                    await SharedPreferenceHelper()
                                        .getRegSkillClass();
                                setState(() {});
                              } else {
                                customToast("Select Medium First!");
                              }

                              try {
                                await serv
                                    .getSubjects(segmentId: classButtonString)
                                    .then((value) {
                                  setState(() {
                                    subjectModel = value;
                                  });
                                });
                                for (int i = 0;
                                    i < subjectModel.data.length;
                                    i++) {
                                  print(subjectModel.data[i].subject);
                                  subjectList.add(subjectModel.data[i].subject);
                                  print(subjectModel.data[i].subjectId);
                                  subjectIdList
                                      .add(subjectModel.data[i].subjectId);
                                }
                                classList.toSet();
                                setState(() {});
                              } catch (e) {
                                customToast("errorr");
                                print(e);
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),

                        ///////////// SUBJECT //////////////
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 50),
                          height: 40,
                          width: double.infinity,

                          ////////////////////////////////// SELECT SUBJECT /////////////////////////////
                          ///
                          child: GestureDetector(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 5,
                                    offset: Offset(0, 7),
                                  ),
                                ],
                              ),
                              child: Container(
                                // alignment: Alignment.centerLeft,
                                width: double.infinity,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        subjectButtonString ?? "Select Subject",
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.black87,
                                        ),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    FaIcon(
                                      FontAwesomeIcons.caretDown,
                                      size: 14,
                                      color: Colors.black54,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            onTap: () async {
                              // subjectList.clear();
                              if (subjectList.isNotEmpty) {
                                await subjectListDialog(
                                  context: context,
                                  subjectList: subjectList,
                                  subjectIdList: subjectIdList,
                                );
                                subjectButtonString =
                                    await SharedPreferenceHelper()
                                        .getRegSkillSub();
                                setState(() {});
                              } else {
                                customToast("Select Class First!");
                                setState(() {});
                              }
                            },
                          ),
                        ),

                        SizedBox(
                          height: 30,
                        ),

                        GradientButton(
                          buttonName: 'NEXT',
                          sWidth: sWidth,
                          sHeight: sHeight,
                          function: () async {
                            try {
                              // EasyLoading.show(status: "Please Wait...");

                              if (mediumButtonString != "Select Medium" &&
                                  classButtonString != "Select Class" &&
                                  subjectButtonString != "Select Subject") {
                                // customToast("Sending");
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => OtpPage()));
                              } else {
                                EasyLoading.dismiss();
                                customToast("Fill up all the items");
                              }

                              // customToast("LOOKS GOOD!");
                              // EasyLoading.showError("Fill up all the items");
                            } catch (e) {
                              print(e);
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget classWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50),
      height: 40,
      width: double.infinity,

      ////////////////////////////////// SELECT CLASS /////////////////////////////
      ///
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 5,
              offset: Offset(0, 7),
            ),
          ],
        ),
        child: DropDown<dynamic>(
          items: classList.toSet().toList() ?? [],
          hint: Text("Select Class"),
          showUnderline: false,
          initialValue: selectedSegmentId,
          isExpanded: true,
          onChanged: (value) async {
            // print(selectedMedium.toLowerCase());
            setState(() {
              subjectList.clear();
              selectedSegmentId = value;
            });
            try {
              serv
                  .getDioSubjects(
                      segmentId: selectedSegmentId.toString(), refresh: true)
                  .then((value) {
                setState(() {
                  subjectModel = value;
                });
              });
              for (int i = 0; i < subjectModel.data.length; i++) {
                subjectList.add(subjectModel.data[i].subject);
              }
            } catch (e) {
              customToast("errorr");
            }
          },
        ),
      ),
    );
  }
}
