import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/services/notifications.dart';
import 'package:Heilo/services/notifications2.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:Heilo/widgets/rating/student_rating.dart';
import 'package:date_time_format/date_time_format.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TutorNotification extends StatefulWidget {
  @override
  _TutorNotificationState createState() => _TutorNotificationState();
}

class _TutorNotificationState extends State<TutorNotification> {
  TutorUserInfoModel futureTutorInfo;
  List<Classes> upPendingClass = [];

  Services serv = Services();
  bool _isLoading;

  _getData(bool refresh) async {
    try {
      setState(() {
        _isLoading = true;
      });
      await serv.getTutorUserInfo(refresh: refresh).then((value) {
        setState(() {
          futureTutorInfo = value;
          _isLoading = false;
        });
      });

      await _getUpcomingList();

      setState(() {
        _isLoading = false;
      });
      await serv.getTutorUserInfo(refresh: true).then((value) {
        setState(() {
          futureTutorInfo = value;
        });
      });
    } catch (e) {
      print(e);
    }
  }

  _getUpcomingList() async {
    upPendingClass.clear();
    await serv.getTutorUserInfo(refresh: false).then((value) {
      for (int i = 0; i < value.data.classes.length; i++) {
        print(value.data.classes[i].status);
        if (value.data.classes[i].status == "COMPLETE" &&
            value.data.classes[i].reviewStatus == false) {
          print('pending value');
          upPendingClass.add(value.data.classes[i]);
        }
      }
    });
  }

  onNotificationClick(String payload) async {
    //
    print('Payload $payload');
    //
  }

  onNotificationClick2(String payload) async {
    //
    print('Payload $payload');
    //

    if (payload == "01") {
      return;
    } else {
      await studentRatingDialog(
        context: context,
        classId: payload,
        prefer: '1',
        checkBoxValue: false,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    notificationPlugin.setOnNotificationClick(onNotificationClick);
    notificationPlugin2.setOnNotificationClick(onNotificationClick2);

    _getData(true);
  }

  @override
  Widget build(BuildContext context) {
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: RefreshIndicator(
        onRefresh: () => _getData(true),
        child: SafeArea(
          child: Container(
            child: Stack(
              fit: StackFit.loose,
              children: [
                Positioned(
                  top: 0,
                  child: Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    color: AssetStrings.color4,
                  ),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Opacity(
                          opacity: 0,
                          child: FaIcon(
                            FontAwesomeIcons.angleLeft,
                            size: 35,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "Activity",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Opacity(
                          opacity: 0,
                          child: FaIcon(
                            FontAwesomeIcons.angleLeft,
                            size: 35,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  // top: MediaQuery.of(context).size.width * 0.2,
                  child: Container(
                    // alignment: Alignment.bottomCenter,
                    height: sHeight,
                    width: sWidth,
                    // padding: EdgeInsets.only(
                    //   bottom: 10,
                    // ),
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.2,
                      // bottom: 20,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                      ),
                      color: AssetStrings.color1,
                    ),

                    ///==================================== BODY STARTS FROM HERE ====================================///
                    child: _isLoading
                        ? CustomLoading2()
                        : (upPendingClass.length < 1)
                            ? Center(
                                child: Container(
                                  // padding: const EdgeInsets.only(top: 20),
                                  child: Container(
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color: AssetStrings.color4,
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    child: Text(
                                      "No Activity",
                                      style: TextStyle(
                                        fontSize: 16,
                                        // fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : Container(
                                child: Stack(
                                  children: [
                                    // Positioned(
                                    //   top: 10,
                                    //   left: 15,
                                    //   child: Container(
                                    //     // width: 300,
                                    //     alignment: Alignment.topLeft,
                                    //     padding: EdgeInsets.symmetric(
                                    //       horizontal: 10,
                                    //       vertical: 5,
                                    //     ),
                                    //     decoration: BoxDecoration(
                                    //       borderRadius: BorderRadius.circular(30),
                                    //       color: Colors.white,
                                    //       boxShadow: [
                                    //         BoxShadow(
                                    //           color: AssetStrings.color4
                                    //               .withOpacity(0.5),
                                    //           blurRadius: 3,
                                    //           offset: Offset(0, 2),
                                    //           spreadRadius: 2,
                                    //         ),
                                    //       ],
                                    //     ),
                                    //     child: Center(
                                    //       child: Text(
                                    //         'UPCOMING',
                                    //         style: TextStyle(
                                    //           fontSize: 14,
                                    //           fontWeight: FontWeight.w500,
                                    //         ),
                                    //       ),
                                    //     ),
                                    //   ),
                                    // ),
                                    Positioned(
                                      top: 10,
                                      bottom: 10,
                                      width: sWidth,
                                      // height: sHeight * 0.78,
                                      child: Container(
                                        height: sHeight * 0.5,
                                        width: sWidth * 0.7,
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: upPendingClass.length,
                                          itemBuilder: (context, index) {
                                            return Container(
                                              padding: EdgeInsets.all(8),
                                              margin: EdgeInsets.symmetric(
                                                horizontal: 10,
                                                vertical: 5,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(0.5),
                                                    blurRadius: 3,
                                                    offset: Offset(0, 3),
                                                    spreadRadius: 2,
                                                  ),
                                                ],
                                              ),
                                              child: ListTile(
                                                leading: CircleAvatar(
                                                  maxRadius: 28,
                                                  backgroundImage: (upPendingClass[
                                                                  index]
                                                              .student
                                                              .dp ==
                                                          null)
                                                      ? AssetImage(
                                                          'assets/images/home.png')
                                                      : NetworkImage(
                                                          upPendingClass[index]
                                                              .student
                                                              .dp),
                                                ),
                                                title: Text(
                                                    upPendingClass[index]
                                                            .student
                                                            .name ??
                                                        "Not Available"),
                                                subtitle: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(upPendingClass[index]
                                                        .subject
                                                        .subject),
                                                    Text(
                                                      DateTimeFormat.format(
                                                          DateTime.parse(
                                                              upPendingClass[
                                                                      index]
                                                                  .timestamp),
                                                          format: 'M j, h:i a'),
                                                    ),
                                                  ],
                                                ),
                                                trailing: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Container(
                                                      padding:
                                                          EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                          color: AssetStrings
                                                              .color4,
                                                          width: 1,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      child: Text(
                                                        upPendingClass[index]
                                                            .status,
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    FittedBox(
                                                      child: Text(
                                                        'Waiting for review',
                                                        style: TextStyle(
                                                          fontSize: 8,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                onTap: () async {
                                                  await studentRatingDialog(
                                                    context: context,
                                                    classId:
                                                        upPendingClass[index]
                                                            .classId
                                                            .toString(),
                                                    prefer: '1',
                                                    checkBoxValue: false,
                                                  );

                                                  _getData(true);
                                                },
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
