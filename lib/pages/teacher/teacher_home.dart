import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/pages/common/terms_and_conditions.dart';
import 'package:Heilo/pages/teacher/student_profile_view.dart';
import 'package:Heilo/services/notifications.dart';
import 'package:Heilo/services/notifications2.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/rating/student_rating.dart';
// import 'package:Heilo/widgets/rating/tutor_rating.dart';
import 'package:Heilo/widgets/tutionRequestTile.dart';
// import 'package:Heilo/widgets/tutionRequestTile.dart';
import 'package:Heilo/widgets/tutionUpcomingTile.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:date_time_format/date_time_format.dart';
import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';

class TeacherHome extends StatefulWidget {
  @override
  _TeacherHomeState createState() => _TeacherHomeState();
}

class _TeacherHomeState extends State<TeacherHome> {
  TutorUserInfoModel futureTutorInfo;
  List<Classes> upPendingClass = [];
  List<Classes> upAcceptedClass = [];
  Services serv = Services();
  bool _isLoading = true;

  int alarmId = 1;
  int alarmId2 = 10;

  bool _onlineStatus;

  //================================ NOTIFICATION SERVICE CALL ==============================//
  startBackgroundService() async {
    //==== METHOD 2 =====//
    // await AndroidAlarmManager.initialize();
    // AndroidAlarmManager.periodic(Duration(seconds: 5), 1, fireAlarm);
    AndroidAlarmManager.periodic(Duration(seconds: 60), alarmId, fireAlarm);
  }

  startBackgroundService2() async {
    //==== METHOD 2 =====//
    try {
      AndroidAlarmManager.periodic(Duration(seconds: 70), alarmId2, fireAlarm2);
    } catch (e) {
      return;
    }
  }

  /////////////////////////// GET TUTOR DATA FOR HOMESCREEN ///////////////////////////
  ///
  _getData(bool refresh) async {
    try {
      setState(() {
        _isLoading = true;
        //
        upPendingClass.clear();
        upAcceptedClass.clear();
      });

      await serv.getTutorUserInfo(refresh: refresh).then((value) {
        setState(() {
          futureTutorInfo = value;
        });
      });

      await _getUpcomingList();
      await _getOnlineStatus();

      setState(() {
        _isLoading = false;
      });
      // await serv.getTutorUserInfo(refresh: true).then((value) {
      //   setState(() {
      //     futureTutorInfo = value;
      //   });
      // });
    } catch (e) {
      print(e);
    }
  }

  _getOnlineStatus() async {
    //
    if (futureTutorInfo.data.status == "active") {
      _onlineStatus = true;
    } else {
      _onlineStatus = false;
    }
  }

  /// Save data for Chat Window
  _saveUserInfo() async {
    await serv.getTutorUserInfo(refresh: false).then((value) {
      futureTutorInfo = value;
      setState(() {});
      SharedPreferenceHelper().saveUserName(futureTutorInfo.data.name);
      SharedPreferenceHelper().saveUserEmail(futureTutorInfo.data.email);
      SharedPreferenceHelper().saveUserProfilePic(futureTutorInfo.data.dp);
      SharedPreferenceHelper().saveUserContact(futureTutorInfo.data.contact);
    });
  }

  _getUpcomingList() async {
    await serv.getTutorUserInfo(refresh: true).then((value) {
      for (int i = 0; i < value.data.classes.length; i++) {
        print(value.data.classes[i].status);
        if (value.data.classes[i].status == "PENDING") {
          print('pending value');
          upPendingClass.add(value.data.classes[i]);
        } else if (value.data.classes[i].status == "ACCEPT") {
          upAcceptedClass.add(value.data.classes[i]);
          print('accept value');
        }
      }
    });
  }

  _checkUserValidity() async {
    await serv.getTutorUserInfo(refresh: false).then((value) {
      if (value.data.status == 'inactive') {
        customToast("Please go online to use our service!");
      } else if (value.data.status == 'blocked') {
        customToast("Please contact Heilo to use our service!");
        serv.userLogOut(context);
      }
    });
  }

  // //========================== PENDING TILE ACCEPT FUNCTION =========================//
  // acceptFunction({String code, String classId}) async {
  //   try {
  //     EasyLoading.show(status: "Processing...");

  //     // print("CODE: $code, STATUS: ACCEPT, CLASSID: $classId");
  //     await serv.tutorClassAccept(
  //       code: code,
  //       status: "accept",
  //       classId: classId,
  //     );
  //     _getData(true);
  //     EasyLoading.showSuccess("Accepted");

  //     // customToast("Tution Accepted");
  //   } catch (e) {
  //     print(e);
  //     EasyLoading.showError("Network Error");

  //     // customToast("Server Error");
  //   }
  // }

  //========================== PENDING TILE CANCEL FUNCTION =========================//
  // cancelFunction({String code, String classId}) async {
  //   try {
  //     EasyLoading.show(status: "Processing...");

  //     // print("CODE: $code, STATUS: CANCEL, CLASSID: $classId");
  //     await serv.tutorClassAccept(
  //       code: code,
  //       status: "cancel",
  //       classId: classId,
  //     );
  //     _getData(true);
  //     EasyLoading.showSuccess("Canceled");

  //     // customToast("Tution Accepted");
  //   } catch (e) {
  //     print(e);
  //     EasyLoading.showError("Network Error");

  //     // customToast("Server Error");
  //   }
  // }

  // =============================== PENDING TILE CHAT FUNCTION ============================ //
  // chatFunction({@required String chatWithContact}) async {
  //   var myContact = await SharedPreferenceHelper().getUserContact();
  //   myContact = myContact.substring(3);
  //   print("MY CONTACT: $myContact || PARTNER CONTACT: $chatWithContact");
  // }

  onNotificationClick(String payload) async {
    //
    print('Payload $payload');
    //
  }

  onNotificationClick2(String payload) async {
    //
    print('Payload $payload');

    //
    if (payload == "01") {
      return;
    } else {
      await studentRatingDialog(
        context: context,
        classId: payload,
        prefer: '1',
        checkBoxValue: false,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    notificationPlugin.setOnNotificationClick(onNotificationClick);
    notificationPlugin2.setOnNotificationClick(onNotificationClick2);

    _checkUserValidity();
    _getData(true);
    _saveUserInfo();
    startBackgroundService();
    startBackgroundService2();
    // _getOnlineStatus();
  }

  @override
  Widget build(BuildContext context) {
    //
    double sHeight = MediaQuery.of(context).size.height;
    double sWidth = MediaQuery.of(context).size.width;
    //
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Tutor Home'),
      // ),
      backgroundColor: AssetStrings.color1,
      body: SafeArea(
        child: !_isLoading
            ? RefreshIndicator(
                onRefresh: () => _getData(true),
                child: Container(
                  height: sHeight,
                  width: double.infinity,
                  // padding: EdgeInsets.all(10),
                  // color: Colors.white,
                  child: Stack(
                    alignment: Alignment.topCenter,
                    fit: StackFit.expand,
                    overflow: Overflow.clip,
                    children: [
                      //================================ TUTOR PROFILE STATUS ===================================//
                      Positioned(
                        top: 10,
                        child: GestureDetector(
                          child: Image.asset(
                            "assets/images/power.png",
                            height: 45,
                            width: 45,
                            color: !_onlineStatus ? Colors.red : Colors.green,
                          ),
                          onTap: () async {
                            // _onlineStatus = !_onlineStatus;
                            setState(() {});
                            print(_onlineStatus);
                            if (futureTutorInfo.data.status == "active" &&
                                _onlineStatus == true) {
                              try {
                                await serv.tutorProfileStatus(
                                    activeStatus: "inactive");
                                await _getData(true);
                                setState(() {
                                  _onlineStatus = false;
                                });
                                customToast("Offline");
                              } catch (e) {
                                customToast("Something went wrong!");
                                print(e);
                              }
                            } else if (futureTutorInfo.data.status ==
                                    "inactive" &&
                                _onlineStatus == false) {
                              try {
                                await serv.tutorProfileStatus(
                                    activeStatus: "active");
                                await _getData(true);
                                setState(() {
                                  _onlineStatus = true;
                                });
                                customToast("Online");
                              } catch (e) {
                                customToast("Something went wrong!");
                                print(e);
                              }
                            }
                          },
                        ),
                      ),

                      /// Tution Request Text ///
                      Positioned(
                        top: 50,
                        left: 15,
                        child: Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 5,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: Color(0xFFE76B6B),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                blurRadius: 3,
                                offset: Offset(0, 2),
                                spreadRadius: 2,
                              ),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              'TUTION REQUEST',
                              style: TextStyle(
                                // fontFamily: 'QuickSandRegular',
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),

                      /// Tution Request List ///
                      (upPendingClass.length < 1)
                          ? Positioned(
                              top: sHeight * 0.25,
                              child: Align(
                                  alignment: Alignment.topCenter,
                                  child: Center(
                                    child: Container(
                                      padding: EdgeInsets.all(8),
                                      decoration: BoxDecoration(
                                        color: AssetStrings.color4,
                                        borderRadius: BorderRadius.circular(25),
                                      ),
                                      child: Text(
                                        "No Pending Requests",
                                        style: TextStyle(
                                          fontSize: 16,
                                          // fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  )),
                            )
                          : Positioned(
                              top: 95,
                              left: 10,
                              height: sHeight * 0.31,
                              width: sWidth,
                              //================================== TUTION PENDING LIST START ==============================//
                              child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: upPendingClass.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    // padding: const EdgeInsets.only(bottom: 8.0),
                                    padding: EdgeInsets.only(
                                        left: 8, right: 8, bottom: 8),
                                    child: GestureDetector(
                                      child: FittedBox(
                                        child: TutionRequestTile(
                                          sWidth: sWidth,
                                          sHeight: sHeight,
                                          dp: upPendingClass[index].student.dp,
                                          address: null,
                                          subjects: upPendingClass[index]
                                              .subject
                                              .subject,
                                          studentName: upPendingClass[index]
                                              .student
                                              .name,
                                          studentContact: upPendingClass[index]
                                              .student
                                              .contact,
                                          timeDuration: DateTimeFormat.format(
                                              DateTime.parse(
                                                  upPendingClass[index]
                                                      .timestamp),
                                              format: 'M j, h:i a'),
                                          code: upPendingClass[index].code,
                                          classId: upPendingClass[index]
                                              .classId
                                              .toString(),
                                        ),
                                      ),
                                      onTap: () async {
                                        var studentId = upPendingClass[index]
                                            .studentId
                                            .toString();
                                        var studentName =
                                            upPendingClass[index].student.name;
                                        var studentContanct =
                                            upPendingClass[index]
                                                .student
                                                .contact;
                                        await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                TutorStudentProfileView(
                                              studentId: studentId,
                                              isPopable: true,
                                              studentName: studentName,
                                              studentContact: studentContanct,
                                            ),
                                          ),
                                        );
                                        _getData(true);
                                      },
                                    ),
                                  );
                                },
                              ),
                            ),

                      /// Upcoming Tution Table ///
                      Positioned(
                        top: sHeight * 0.465,
                        left: 0,
                        child: Container(
                          height: sHeight,
                          width: sWidth,
                          color: Colors.white,
                        ),
                      ),

                      Positioned(
                        top: sHeight * 0.5,
                        left: 15,
                        child: Container(
                          // width: 300,
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 5,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: AssetStrings.color4.withOpacity(0.5),
                                blurRadius: 3,
                                offset: Offset(0, 2),
                                spreadRadius: 2,
                              ),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              'UPCOMING',
                              style: TextStyle(
                                // fontFamily: 'QuickSandRegular',
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                // color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),

                      /// Custom Table ///
                      (upAcceptedClass.length < 1)
                          ? Positioned(
                              top: sHeight * 0.64,
                              child: Center(
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    color: AssetStrings.color4,
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  child: Text(
                                    "No Upcoming Requests",
                                    style: TextStyle(
                                      fontSize: 16,
                                      // fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Positioned(
                              top: sHeight * 0.555,
                              left: 15,
                              child: Container(
                                height: sHeight * 0.2,
                                width: sWidth,
                                child: ListView.builder(
                                  itemCount: upAcceptedClass.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      child: TutionUpcomingTile(
                                        sWidth: sWidth,
                                        subject:
                                            (upAcceptedClass[index].student ==
                                                    null)
                                                ? "N/A"
                                                : upAcceptedClass[index]
                                                        .student
                                                        .name ??
                                                    "N/A",
                                        address: upAcceptedClass[index]
                                            .subject
                                            .subject,
                                        secretCode: upAcceptedClass[index].code,

                                        // tutionTime: upAcceptedClass[index]
                                        //     .timestamp
                                        //     .substring(11, 16),
                                        tutionTime: DateTimeFormat.format(
                                            DateTime.parse(
                                                upAcceptedClass[index]
                                                    .timestamp),
                                            format: 'h:i a'),
                                        upcomingDate: DateTimeFormat.format(
                                            DateTime.parse(
                                                upAcceptedClass[index]
                                                    .timestamp),
                                            format: 'M j'),
                                      ),
                                      onTap: () async {
                                        var studentId = upAcceptedClass[index]
                                            .studentId
                                            .toString();
                                        var studentName =
                                            upAcceptedClass[index].student.name;
                                        var studentContanct =
                                            upAcceptedClass[index]
                                                .student
                                                .contact;
                                        await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                TutorStudentProfileView(
                                              studentId: studentId,
                                              isPopable: true,
                                              studentName: studentName,
                                              studentContact: studentContanct,
                                            ),
                                          ),
                                        );
                                        _getData(true);
                                      },
                                    );
                                  },
                                ),
                              ),
                            ),

                      /// Terms and Conditions ///
                      Positioned(
                        bottom: 0,
                        // top: sHeight * 0.8,
                        // height: sHeight,
                        child: Container(
                          width: sWidth,
                          height: 90,
                          decoration: BoxDecoration(
                            color: AssetStrings.color1,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(50),
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Image.asset(
                                'assets/images/arleft.png',
                                height: 35,
                              ),
                              GestureDetector(
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => TermsAndConditions(),
                                  ),
                                ),
                                child:
                                    // Text(
                                    // 'TERMS & CONDITIONS',
                                    // style: TextStyle(
                                    // fontSize: 18,
                                    // color: Colors.red,
                                    // ),
                                    // ),
                                    FittedBox(
                                  child: SizedBox(
                                    width: 250.0,
                                    child: AnimatedTextKit(
                                      isRepeatingAnimation: true,
                                      repeatForever: true,
                                      // stopPauseOnTap: true,

                                      // pause: const Duration(milliseconds: 2000),
                                      animatedTexts: [
                                        TypewriterAnimatedText(
                                          'Check Heilo in every hour to get hired.',
                                          textStyle: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black87,
                                            fontFamily: 'QuickSandMedium',
                                          ),
                                          speed:
                                              const Duration(milliseconds: 100),
                                          textAlign: TextAlign.center,
                                        ),
                                        TypewriterAnimatedText(
                                          'Longer absence in Heilo will decrease the probability of getting hired.',
                                          textStyle: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black87,
                                            fontFamily: 'QuickSandMedium',
                                          ),
                                          speed:
                                              const Duration(milliseconds: 100),
                                          textAlign: TextAlign.center,
                                        ),
                                        TypewriterAnimatedText(
                                          'Your student’s positive review will make a difference for you!',
                                          textStyle: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black87,
                                            fontFamily: 'QuickSandMedium',
                                          ),
                                          speed:
                                              const Duration(milliseconds: 100),
                                          textAlign: TextAlign.center,
                                        ),
                                        TypewriterAnimatedText(
                                          'Your effort in tuition can bring a positive impact on your students.',
                                          textStyle: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black87,
                                            fontFamily: 'QuickSandMedium',
                                          ),
                                          speed:
                                              const Duration(milliseconds: 100),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                      onTap: () {
                                        print("Tap Event");
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Image.asset(
                                'assets/images/arright.png',
                                height: 35,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            : CustomLoading2(),
      ),
    );
  }

  Widget loadingScreen() {
    return Center(
      child: Container(
        height: 95,
        width: 100,
        decoration: BoxDecoration(
          color: AssetStrings.color4,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Loading...",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

fireAlarm() async {
  print("PENDING NOTIFICATION SERVICE CALLED");
  // String name = await SharedPreferenceHelper().getUserName();
  // String phone = await SharedPreferenceHelper().getUserContact();
  String prevKey;
  String currentKey;

  searchList(List list, String item) {
    if (list.contains(item)) {
      return true;
    } else {
      return false;
    }
  }

  Services serv = Services();

  List<Classes> pendingTutorClassList = [];
  List<Classes> completeTutorClassList = [];
  List<Classes> acceptTutorClassList = [];
  List<Classes> cancelTutorClassList = [];

  List pendingTutorClassListID = [];
  List completeTutorClassListID = [];
  List acceptTutorClassListID = [];
  List cancelTutorClassListID = [];
  List doneIdList = [];

  try {
    pendingTutorClassList.clear();
    await serv.getTutorUserInfo(refresh: true).then((value) {
      for (int i = 0; i < value.data.classes.length; i++) {
        if (value.data.classes[i].status == "PENDING") {
          print(value.data.classes[i].status);
          pendingTutorClassList.add(value.data.classes[i]);
          pendingTutorClassListID.add(value.data.classes[i].classId.toString());
          //
        } else if (value.data.classes[i].status == "COMPLETE") {
          print(value.data.classes[i].status);
          completeTutorClassList.add(value.data.classes[i]);
          completeTutorClassListID
              .add(value.data.classes[i].classId.toString());
          //
        } else if (value.data.classes[i].status == "ACCEPT") {
          print(value.data.classes[i].status);
          acceptTutorClassList.add(value.data.classes[i]);
          acceptTutorClassListID.add(value.data.classes[i].classId.toString());
          //
        } else if (value.data.classes[i].status == "CANCEL") {
          print(value.data.classes[i].status);
          cancelTutorClassList.add(value.data.classes[i]);
          cancelTutorClassListID.add(value.data.classes[i].classId.toString());
          //
        } else {
          doneIdList.add(value.data.classes[i].classId.toString());
        }
      }
    });
  } catch (e) {
    return;
  }

  try {
    prevKey = await SharedPreferenceHelper().getNotificationKey();
    currentKey = pendingTutorClassList.last.classId.toString();
  } catch (e) {
    return;
  }

  if (prevKey == currentKey ||
      searchList(doneIdList, currentKey) ||
      searchList(completeTutorClassListID, currentKey) ||
      searchList(acceptTutorClassListID, currentKey) ||
      searchList(cancelTutorClassListID, currentKey)) {
    return;
  } else {
    String title = "New Request";
    String content =
        "You have a new tution request from ${pendingTutorClassList.last.student.name}";
    await notificationPlugin.initialize();
    await notificationPlugin.instantNotification(
      title: title,
      content: content,
    );
    await SharedPreferenceHelper().saveNotificationKey(currentKey);
  }
}

fireAlarm2() async {
  print("COMPLETE NOTIFICATION SERVICE CALLED");

  searchList(List list, String item) {
    if (list.contains(item)) {
      return true;
    } else {
      return false;
    }
  }

  Services serv = Services();

  List<Classes> pendingTutorClassList = [];
  List<Classes> completeTutorClassList = [];
  List<Classes> acceptTutorClassList = [];
  List<Classes> cancelTutorClassList = [];

  List pendingTutorClassListID = [];
  List completeTutorClassListID = [];
  List acceptTutorClassListID = [];
  List cancelTutorClassListID = [];
  List doneIdList = [];

  try {
    pendingTutorClassList.clear();
    await serv.getTutorUserInfo(refresh: true).then((value) {
      for (int i = 0; i < value.data.classes.length; i++) {
        if (value.data.classes[i].status == "PENDING") {
          print(value.data.classes[i].status);
          pendingTutorClassList.add(value.data.classes[i]);
          pendingTutorClassListID.add(value.data.classes[i].classId.toString());
          //
        } else if (value.data.classes[i].status == "COMPLETE" &&
            value.data.classes[i].reviewStatus == false) {
          print(value.data.classes[i].status);
          completeTutorClassList.add(value.data.classes[i]);
          completeTutorClassListID
              .add(value.data.classes[i].classId.toString());
          //
        } else if (value.data.classes[i].status == "ACCEPT") {
          print(value.data.classes[i].status);
          acceptTutorClassList.add(value.data.classes[i]);
          acceptTutorClassListID.add(value.data.classes[i].classId.toString());
          //
        } else if (value.data.classes[i].status == "CANCEL") {
          print(value.data.classes[i].status);
          cancelTutorClassList.add(value.data.classes[i]);
          cancelTutorClassListID.add(value.data.classes[i].classId.toString());
          //
        } else {
          doneIdList.add(value.data.classes[i].classId.toString());
        }
      }
    });
  } catch (e) {
    return;
  }

  String prevCompleteKey =
      await SharedPreferenceHelper().getCompleteNotificationKey();
  String currentCompleteKey = completeTutorClassListID.last.toString();

  if (prevCompleteKey == currentCompleteKey ||
      searchList(doneIdList, currentCompleteKey) ||
      searchList(pendingTutorClassListID, currentCompleteKey) ||
      searchList(acceptTutorClassListID, currentCompleteKey) ||
      searchList(cancelTutorClassListID, currentCompleteKey)) {
    return;
  } else {
    String title = "Thank you for using our service!";
    String content =
        "Would you like to rate ${completeTutorClassList.last.student.name}?";

    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // await prefs.setStringList('DESPERATE', [
    //   completeTutorClassList.last.studentId.toString(),
    //   completeTutorClassList.last.classId.toString()
    // ]);
    await notificationPlugin2.initialize();
    await notificationPlugin2.instantNotification2(
      title: title,
      content: content,
      payload: currentCompleteKey,
    );
    await SharedPreferenceHelper()
        .saveCompleteNotificationKey(currentCompleteKey);
    await SharedPreferenceHelper().saveCompleteClassStudentIdKey(
        completeTutorClassList.last.studentId.toString());

    String rand1 = await SharedPreferenceHelper().getCompleteNotificationKey();
    String rand2 =
        await SharedPreferenceHelper().getCompleteClassStudentIdKey();

    Idk.classId = rand1;
    Idk.studentId = rand2;

    print('HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH');
    print("STUDENT ID: $rand2 and CLASS ID: $rand1");
  }
}

class Idk {
  static String classId;
  static String studentId;
}
