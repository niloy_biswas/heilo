import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/services/firestore_database.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/ProfileButton.dart';
import 'package:Heilo/widgets/about_update_dialog.dart';
import 'package:Heilo/widgets/custom_loading_screen_2.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:Heilo/widgets/location/district_dialog.dart';
import 'package:Heilo/widgets/signup/hourly_rate_dialog.dart';
import 'package:Heilo/widgets/tutor/tutor_education_add_dialog.dart';
import 'package:Heilo/widgets/tutor/tutor_education_list_dialog.dart';
import 'package:Heilo/widgets/tutor/tutor_location_list_dialog.dart';
import 'package:Heilo/widgets/tutor/tutor_profile_skill_add.dart';
import 'package:Heilo/widgets/tutor/tutor_skill_update_list.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io' as Io;

import 'package:shared_preferences/shared_preferences.dart';

class TutorProfile extends StatefulWidget {
  @override
  _TutorProfileState createState() => _TutorProfileState();
}

class _TutorProfileState extends State<TutorProfile> {
  TutorUserInfoModel futureTutorUserInfo;
  Services serv = Services();
  bool _isLoading;

  TextEditingController emailController = TextEditingController();
  TextEditingController locationReferenceController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController schoolCollegeController = TextEditingController();
  TextEditingController gradeController = TextEditingController();
  TextEditingController backgroundController = TextEditingController();
  TextEditingController mediumController = TextEditingController();

  TextEditingController _currentPassController = TextEditingController();
  TextEditingController _newPassController = TextEditingController();
  bool fishEye1 = true;
  bool fishEye2 = true;

  //
  TextEditingController aboutController = TextEditingController();

  _getData() async {
    try {
      setState(() {
        _isLoading = true;
      });
      await serv.getTutorUserInfo(refresh: false).then((value) {
        setState(() {
          futureTutorUserInfo = value;
        });
      });
      setState(() {
        _isLoading = false;
      });
      await serv.getTutorUserInfo(refresh: true).then((value) {
        setState(() {
          futureTutorUserInfo = value;
        });
      });
    } on SocketException {
      EasyLoading.showError("No Internet Connection");
    } catch (e) {
      print(e);
    }
  }

  // _getControllerData() async {
  //   String email = futureTutorUserInfo.data.email ?? "N/A";
  //   String location = futureTutorUserInfo.data.location.detailAddress ?? "N/A";
  //   print(email);
  //   print(location);
  //   setState(() {});
  //   Future.delayed(Duration(milliseconds: 500));

  //   setState(() {
  //     emailController.text = email;
  //     locationReferenceController.text = location;
  //   });
  // }

  /////////////////////////////////// PROFILE PIC CODE STARTS ////////////////////////////////////

  String dpString;
  Io.File _image1;

  // Getting Profile Picture from Gallery
  Future getProfileImage() async {
    // ignore: deprecated_member_use
    final image1 = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 450.0,
      maxWidth: 450.0,
      imageQuality: 50,
    );
    setState(() {
      _image1 = image1;
    });
    // print(_image);
    await imgEncode1();
  }

  // Encoding Profile Picture to BASE64 string
  imgEncode1() async {
    final bytes = _image1.readAsBytesSync();
    dpString = base64Encode(bytes);
    print('ENCODED STRING: $dpString');
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('PROF_PIC_PREF', dpString);
  }

  /////////////////////////////////// PROFILE PIC CODE ENDS ////////////////////////////////////

  /////////////////////////////////// MAIN WIDGET CODE START ////////////////////////////////////

  @override
  void initState() {
    super.initState();
    _getData();
    // _getControllerData();
    EasyLoading.dismiss();
  }

  @override
  Widget build(BuildContext context) {
    //
    var sHeight = MediaQuery.of(context).size.height;
    var sWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AssetStrings.color1,
      extendBody: true,
      // extendBodyBehindAppBar: true,
      body: _isLoading
          ? CustomLoading2()
          : SafeArea(
              child: Container(
                height: sHeight,
                width: sWidth,
                child: Stack(
                  alignment: Alignment.topCenter,
                  fit: StackFit.expand,
                  overflow: Overflow.clip,
                  children: [
                    Positioned(
                      top: 0,
                      width: sWidth,
                      child: Image.asset("assets/images/topBorder.png"),
                    ),
                    Positioned(
                      bottom: 0,
                      width: sWidth,
                      child: Image.asset("assets/images/bottomBorder.png"),
                    ),
                    SingleChildScrollView(
                      child: SafeArea(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),

                            //=============================== PROFILE PIC ============================//
                            // ProfilePictureWidget(
                            //     assetString: "assets/images/hulk.jpg"),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  ///-----------------------BACK BUTTON-----------------------///
                                  child: Container(
                                    margin: const EdgeInsets.only(left: 15),
                                    child: FaIcon(
                                      FontAwesomeIcons.angleLeft,
                                      size: 35,
                                      color: Colors.white,
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: CircleAvatar(
                                    maxRadius: 40,
                                    backgroundImage: (futureTutorUserInfo
                                                .data.dp ==
                                            null)
                                        ? AssetImage("assets/images/home.png")
                                        : NetworkImage(
                                                futureTutorUserInfo.data.dp) ??
                                            AssetImage(
                                                'assets/images/home.png'),
                                  ),
                                ),
                                Opacity(
                                  opacity: 0,
                                  child: Container(
                                    margin: EdgeInsets.only(right: 15),
                                    child: FaIcon(
                                      FontAwesomeIcons.angleLeft,
                                      size: 35,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),

                            //================================== PHOTO CHANGE ===================================//
                            ProfileButton(
                              buttonName: "Change Photo",
                              sWidth: sWidth * 0.3,
                              sHeight: sHeight * 0.04,
                              bFontSize: 12.0,
                              function: () async {
                                EasyLoading.show(status: "Processing");

                                try {
                                  await getProfileImage();
                                  await serv.tutorDpUpload(
                                      dpString:
                                          "data:image/jpeg;base64," + dpString);

                                  await _getData();

                                  String uid =
                                      FirebaseAuth.instance.currentUser.uid;

                                  String firebaseDP =
                                      futureTutorUserInfo.data.dp;
                                  print(firebaseDP);

                                  Map<String, dynamic> userInfoMap = {
                                    "userName": futureTutorUserInfo.data.name,
                                    "contactNo": futureTutorUserInfo
                                        .data.contact
                                        .substring(3),
                                    "profilePic": firebaseDP,
                                  };
                                  await DatabaseMethods()
                                      .addUserInfoToDB(uid, userInfoMap);

                                  EasyLoading.showSuccess("Upload Successful");
                                  _getData();
                                } catch (e) {
                                  print("CANCELED UPLOAD");
                                  EasyLoading.showError("Upload Failed!");
                                  // createAlertDialog3(context);
                                }
                              },
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 40,
                                left: 40,
                                right: 40,
                              ),
                              width: sWidth,
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 3,
                                      offset: Offset(0, 4)),
                                ],
                              ),
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: 30,
                                  vertical: 20,
                                ),

                                ///////////////////////// PERSONAL DETAILS /////////////////////////
                                ///
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    FittedBox(
                                      child: Text(
                                        'PERSONAL DETAILS',
                                        style: _textStyle0(context),
                                      ),
                                    ),

                                    Divider(
                                      color: AssetStrings.color4,
                                      height: 20,
                                      thickness: 2,
                                    ),
                                    //////////////////////////// NAME //////////////////////////////
                                    ///
                                    Text(
                                      'NAME',
                                      style: _textStyle1(context),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      futureTutorUserInfo.data.name ??
                                          'Not Available',
                                      style: _textStyle2(context),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    ////////////////////////////// EMAIL ////////////////////////////
                                    ///
                                    Text(
                                      'EMAIL',
                                      style: _textStyle1(context),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      futureTutorUserInfo.data.email ??
                                          'Not Available',
                                      style: _textStyle2(context),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),

                                    ////////////////////////////// CONTACT NO ///////////////////////////
                                    ///
                                    Text(
                                      'CONTACT NO',
                                      style: _textStyle1(context),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      futureTutorUserInfo.data.contact ??
                                          'Not Available',
                                      style: _textStyle2(context),
                                    ),
                                    // SizedBox(
                                    //   height: 20,
                                    // ),
                                  ],
                                ),
                              ),
                            ),

                            // // ================================== LOCATION INFO START =========================================//
                            // //
                            // Container(
                            //   margin: EdgeInsets.only(
                            //     top: 40,
                            //     left: 40,
                            //     right: 40,
                            //   ),
                            //   width: sWidth,
                            //   // color: Colors.white,
                            //   decoration: BoxDecoration(
                            //     color: Colors.white,
                            //     boxShadow: [
                            //       BoxShadow(
                            //           color: Colors.grey.withOpacity(0.5),
                            //           spreadRadius: 2,
                            //           blurRadius: 3,
                            //           offset: Offset(0, 4)),
                            //     ],
                            //   ),
                            //   child: Container(
                            //     margin: EdgeInsets.symmetric(
                            //       horizontal: 30,
                            //       vertical: 20,
                            //     ),

                            //     ///
                            //     child: Column(
                            //       crossAxisAlignment: CrossAxisAlignment.start,
                            //       children: [
                            //         Row(
                            //           mainAxisAlignment:
                            //               MainAxisAlignment.spaceBetween,
                            //           children: [
                            //             Text(
                            //               'LOCATION DETAILS',
                            //               style: _textStyle0(context),
                            //             ),
                            //             GestureDetector(
                            //               child: FaIcon(
                            //                 FontAwesomeIcons.edit,
                            //                 size: 18,
                            //               ),
                            //               onTap: () async {
                            //                 try {
                            //                   List distList = [];
                            //                   await SharedPreferenceHelper()
                            //                       .saveLocationTrigger(
                            //                           "HONDATRIGGER");
                            //                   await serv
                            //                       .getDioLocations(
                            //                           refresh: false)
                            //                       .then((value) {
                            //                     for (int i = 0;
                            //                         i < value.data.length;
                            //                         i++) {
                            //                       // print("${value.data[i].district}");
                            //                       distList.add(
                            //                           value.data[i].district);
                            //                     }
                            //                     // distList.toSet().toList();
                            //                     // print(distList.toSet().toList());
                            //                   });
                            //                   await districtDialog(
                            //                     context: context,
                            //                     districtList:
                            //                         distList.toSet().toList(),
                            //                   );
                            //                 } catch (e) {
                            //                   print(e);
                            //                 }
                            //               },
                            //             ),
                            //           ],
                            //         ),
                            //         Divider(
                            //           color: AssetStrings.color4,
                            //           height: 20,
                            //           thickness: 2,
                            //         ),

                            //         /////////////////////////// USER ADDRESS /////////////////////////////
                            //         Text(
                            //           'DISTRICT',
                            //           style: _textStyle1(context),
                            //         ),
                            //         SizedBox(
                            //           height: 5,
                            //         ),
                            //         Text(
                            //           futureTutorUserInfo
                            //                   .data.location.district ??
                            //               'Not Available',
                            //           style: _textStyle2(context),
                            //         ),
                            //         SizedBox(
                            //           height: 20,
                            //         ),
                            //         Text(
                            //           // 'LOCATION REFERENCE (PLACE OF STUDY)',
                            //           'LOCATION REFERENCE',

                            //           style: _textStyle1(context),
                            //         ),
                            //         SizedBox(
                            //           height: 5,
                            //         ),
                            //         Text(
                            //           futureTutorUserInfo.data.location.area ??
                            //               'Not Available',
                            //           style: _textStyle2(context),
                            //         ),
                            //         SizedBox(
                            //           height: 20,
                            //         ),
                            //         Text(
                            //           // 'DETAIL ADDRESS',
                            //           'ROAD',

                            //           style: _textStyle1(context),
                            //         ),
                            //         SizedBox(
                            //           height: 5,
                            //         ),
                            //         Text(
                            //           futureTutorUserInfo.data.location.road ??
                            //               'Not Available',
                            //           style: _textStyle2(context),
                            //         ),
                            //       ],
                            //     ),
                            //   ),
                            // ),

                            SizedBox(
                              height: 40,
                            ),

                            //+++++++++++++++++++++++++++++++++++ TEST ++++++++++++++++++++++++++++++++++++++//
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 40),
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              width: sWidth,
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 3,
                                      offset: Offset(0, 4)),
                                ],
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(
                                        top: 20,
                                        right: 20,
                                        left: 20,
                                        bottom: 0),
                                    width: double.infinity,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            FittedBox(
                                              child: Text(
                                                'LOCATION DETAILS',
                                                style: _textStyle0(context),
                                              ),
                                            ),
                                            Row(
                                              children: [
                                                //* Location Add Icons //
                                                //
                                                GestureDetector(
                                                  child: FaIcon(
                                                    FontAwesomeIcons.plus,
                                                    size: 18,
                                                  ),
                                                  onTap: () async {
                                                    try {
                                                      List distList = [];
                                                      await SharedPreferenceHelper()
                                                          .saveLocationTrigger(
                                                              "HONDATRIGGER");
                                                      await serv
                                                          .getDioLocations(
                                                              refresh: false)
                                                          .then((value) {
                                                        for (int i = 0;
                                                            i <
                                                                value.data
                                                                    .length;
                                                            i++) {
                                                          // print("${value.data[i].district}");
                                                          distList.add(value
                                                              .data[i]
                                                              .district);
                                                        }
                                                        // distList.toSet().toList();
                                                        // print(distList.toSet().toList());
                                                      });
                                                      await districtDialog(
                                                        context: context,
                                                        districtList: distList
                                                            .toSet()
                                                            .toList(),
                                                      );
                                                    } catch (e) {
                                                      print(e);
                                                    }
                                                  },
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),

                                                //! Location Edit Icons //
                                                (futureTutorUserInfo.data
                                                            .locations.length <
                                                        2)
                                                    ? Container()
                                                    : GestureDetector(
                                                        child: FaIcon(
                                                          FontAwesomeIcons.edit,
                                                          size: 18,
                                                        ),
                                                        onTap: () async {
                                                          try {
                                                            await tutorLocationsListDialog(
                                                              context: context,
                                                              tutorUserInfoModel:
                                                                  futureTutorUserInfo,
                                                            );

                                                            await _getData();
                                                          } catch (e) {
                                                            throw Exception(e);
                                                          }
                                                        },
                                                      ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Divider(
                                          color: AssetStrings.color4,
                                          height: 20,
                                          thickness: 2,
                                        ),
                                      ],
                                    ),
                                  ),

                                  ///=======================! LOCATION INFORMATION TILE ==============================///
                                  ///
                                  (futureTutorUserInfo.data.locations.length <
                                          1)
                                      ? Container(
                                          height: sHeight * 0.1,
                                          width: double.infinity,
                                          padding: EdgeInsets.only(bottom: 10),
                                          child: Center(
                                            child: Text(
                                              'Not Available',
                                              style: _textStyle2(context),
                                            ),
                                          ),
                                        )
                                      : MediaQuery.removePadding(
                                          context: context,
                                          removeTop: true,
                                          removeBottom: true,
                                          child: Padding(
                                            padding:
                                                EdgeInsets.only(bottom: 15),
                                            child: ListView.builder(
                                              shrinkWrap: true,
                                              physics: BouncingScrollPhysics(),
                                              itemCount: futureTutorUserInfo
                                                  .data.locations.length,
                                              itemBuilder: (context, index) {
                                                return locationCard(
                                                  context: context,
                                                  district: futureTutorUserInfo
                                                      .data
                                                      .locations[index]
                                                      .district,
                                                  area: futureTutorUserInfo.data
                                                      .locations[index].area,
                                                  road: futureTutorUserInfo.data
                                                      .locations[index].road,
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                ],
                              ),
                            ),

                            // ================================== TUTION RATE INFO START =========================================//
                            //
                            Container(
                              margin: EdgeInsets.only(
                                left: 40,
                                right: 40,
                                top: 40,
                              ),
                              width: sWidth,
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 3,
                                      offset: Offset(0, 4)),
                                ],
                              ),
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: 30,
                                  vertical: 20,
                                ),

                                ///
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        FittedBox(
                                          child: Text(
                                            'HOURLY TUTION RATE',
                                            style: _textStyle0(context),
                                          ),
                                        ),
                                        GestureDetector(
                                          child: FaIcon(
                                            FontAwesomeIcons.edit,
                                            size: 18,
                                          ),
                                          onTap: () async {
                                            try {
                                              await hourlyRateDialog(
                                                      context: context)
                                                  .then((value) {
                                                debugPrint('>>>>> $value');
                                                Services serv = Services();
                                                serv.tutorHourlyRateUpdate(
                                                    hourlyRate: value);
                                                customToast('Profile Updated');
                                                _getData();
                                              });
                                            } catch (e) {
                                              customToast('Update Failed!');
                                              print(e);
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                    Divider(
                                      color: AssetStrings.color4,
                                      height: 20,
                                      thickness: 2,
                                    ),

                                    /////////////////////////// ABOUT TEXT /////////////////////////////
                                    Container(
                                      child: Row(
                                        children: [
                                          Text(
                                            'CURRENT RATE:',
                                            style: _textStyle1(context),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            futureTutorUserInfo
                                                    .data.hourlyRate ??
                                                "Not Available",
                                            style: _textStyle2(context),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),

                            /// TUTION RATE INFO FINISH ///

                            // ================================== SKILL INFO START =========================================//
                            //
                            Container(
                              margin: EdgeInsets.all(40),
                              width: sWidth,
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 3,
                                      offset: Offset(0, 4)),
                                ],
                              ),
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: 30,
                                  vertical: 20,
                                ),

                                ///
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        FittedBox(
                                          child: Text(
                                            'SKILL NAMES',
                                            style: _textStyle0(context),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            GestureDetector(
                                              child: FaIcon(
                                                FontAwesomeIcons.plus,
                                                size: 18,
                                              ),
                                              onTap: () async {
                                                try {
                                                  tutorProfileSkillAddDialog(
                                                          context: context,
                                                          tutorUserInfoModel:
                                                              futureTutorUserInfo)
                                                      .then((_) => _getData());
                                                } catch (e) {
                                                  print(e);
                                                }
                                              },
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            (futureTutorUserInfo
                                                        .data.subjects.length <
                                                    2)
                                                ? Container()
                                                : GestureDetector(
                                                    child: FaIcon(
                                                      FontAwesomeIcons.edit,
                                                      size: 18,
                                                    ),

                                                    //=============================== SKILL UPDATE LIST ============================///
                                                    onTap: () async {
                                                      try {
                                                        // await serv
                                                        //     .getTutorUserInfo(
                                                        //         refresh: true)
                                                        //     .then((value) {
                                                        //   tutorSkillListDialog(
                                                        //     context: context,
                                                        //     tutorUserInfoModel: value,
                                                        //   );
                                                        // });
                                                        await tutorSkillListDialog(
                                                          context: context,
                                                          tutorUserInfoModel:
                                                              futureTutorUserInfo,
                                                        ).then(
                                                            (_) => _getData());
                                                      } catch (e) {
                                                        print(e);
                                                      }
                                                    },
                                                  ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Divider(
                                      color: AssetStrings.color4,
                                      height: 20,
                                      thickness: 2,
                                    ),

                                    /////////////////////////// SKILL LIST /////////////////////////////
                                    (futureTutorUserInfo.data.subjects.length <
                                            1)
                                        ? Container(
                                            height: sHeight * 0.1,
                                            width: double.infinity,
                                            padding:
                                                EdgeInsets.only(bottom: 10),
                                            child: Center(
                                              child: Text(
                                                'Not Available',
                                                style: _textStyle2(context),
                                              ),
                                            ),
                                          )
                                        : ListView.builder(
                                            shrinkWrap: true,
                                            physics:
                                                NeverScrollableScrollPhysics(),
                                            itemCount: futureTutorUserInfo
                                                .data.subjects.length,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                padding: EdgeInsets.all(5),
                                                margin:
                                                    EdgeInsets.only(bottom: 5),
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    width: 2,
                                                    color: AssetStrings.color4,
                                                  ),
                                                ),
                                                child: Text(
                                                  futureTutorUserInfo.data
                                                      .subjects[index].subject,
                                                  style: _textStyle2(context),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  textAlign: TextAlign.center,
                                                ),
                                              );
                                            }),
                                  ],
                                ),
                              ),
                            ),

                            // ================================== EDUCATIONAL INFORMATION STARTS ==================================//
                            //
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 40),
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              width: sWidth,
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 3,
                                      offset: Offset(0, 4)),
                                ],
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(
                                        top: 20,
                                        right: 20,
                                        left: 20,
                                        bottom: 0),
                                    width: double.infinity,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            FittedBox(
                                              child: Text(
                                                'EDUCATIONAL INFORMATION',
                                                style: _textStyle0(context),
                                              ),
                                            ),
                                            Row(
                                              children: [
                                                //* Education Add Icons //
                                                //
                                                GestureDetector(
                                                  child: FaIcon(
                                                    FontAwesomeIcons.plus,
                                                    size: 18,
                                                  ),
                                                  onTap: () async {
                                                    print(
                                                        'Add Education Clicked');
                                                    // try {

                                                    //   await serv
                                                    //       .getDioStudentInfo(
                                                    //           refresh: false)
                                                    //       .then(
                                                    //         (value) =>
                                                    //             studentEducationAddDialog(
                                                    //           context: context,
                                                    //           studentUserInfoModel:
                                                    //               value,
                                                    //         ),
                                                    //       );
                                                    //   await updatePage();

                                                    //   await Future.delayed(
                                                    //       Duration(seconds: 1));
                                                    //   await updatePage();
                                                    // } catch (e) {
                                                    //   print(
                                                    //       "ERROR OCCURED WHILE UPDATING EDUCATION");
                                                    //   print(e);
                                                    // }
                                                    try {
                                                      await tutorEducationAddDialog(
                                                          context: context,
                                                          tutorUserInfoModel:
                                                              futureTutorUserInfo);
                                                      await _getData();
                                                    } catch (e) {
                                                      throw Exception(e);
                                                    }
                                                  },
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),

                                                //! Education Edit Icons //
                                                (futureTutorUserInfo.data
                                                            .educations.length <
                                                        2)
                                                    ? Container()
                                                    : GestureDetector(
                                                        child: FaIcon(
                                                          FontAwesomeIcons.edit,
                                                          size: 18,
                                                        ),
                                                        onTap: () async {
                                                          // try {
                                                          //   await serv
                                                          //       .getDioStudentInfo(
                                                          //           refresh:
                                                          //               false)
                                                          //       .then((value) =>
                                                          //           studentEducationListDialog(
                                                          //               context:
                                                          //                   context,
                                                          //               studentUserInfoModel:
                                                          //                   value));
                                                          //   await updatePage();

                                                          //   await Future.delayed(
                                                          //       Duration(
                                                          //           seconds: 2));
                                                          //   await updatePage();
                                                          // } catch (e) {
                                                          //   print(e);
                                                          // }
                                                          try {
                                                            await tutorEducationListDialog(
                                                              context: context,
                                                              tutorUserInfoModel:
                                                                  futureTutorUserInfo,
                                                            );

                                                            await _getData();
                                                          } catch (e) {
                                                            throw Exception(e);
                                                          }
                                                        },
                                                      ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Divider(
                                          color: AssetStrings.color4,
                                          height: 20,
                                          thickness: 2,
                                        ),
                                      ],
                                    ),
                                  ),

                                  ///=======================! EDUCATION INFORMATION TILE ==============================///
                                  ///
                                  (futureTutorUserInfo.data.educations.length <
                                          1)
                                      ? Container(
                                          height: sHeight * 0.1,
                                          width: double.infinity,
                                          padding: EdgeInsets.only(bottom: 10),
                                          child: Center(
                                            child: Text(
                                              'Not Available',
                                              style: _textStyle2(context),
                                            ),
                                          ),
                                        )
                                      : MediaQuery.removePadding(
                                          context: context,
                                          removeTop: true,
                                          removeBottom: true,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 10),
                                            child: ListView.builder(
                                              shrinkWrap: true,
                                              physics: BouncingScrollPhysics(),
                                              itemCount: futureTutorUserInfo
                                                  .data.educations.length,
                                              itemBuilder: (context, index) {
                                                return educationCard(
                                                  context: context,
                                                  institutionName:
                                                      futureTutorUserInfo
                                                          .data
                                                          .educations[index]
                                                          .institutionName,
                                                  grade: futureTutorUserInfo
                                                      .data
                                                      .educations[index]
                                                      .grade,
                                                  background:
                                                      futureTutorUserInfo
                                                          .data
                                                          .educations[index]
                                                          .background,
                                                  medium: futureTutorUserInfo
                                                      .data
                                                      .educations[index]
                                                      .medium,
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                ],
                              ),
                            ),

                            // ================================== ABOUT INFO START =========================================//
                            //
                            Container(
                              margin: EdgeInsets.only(
                                left: 40,
                                right: 40,
                                top: 40,
                              ),
                              width: sWidth,
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 3,
                                      offset: Offset(0, 4)),
                                ],
                              ),
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: 30,
                                  vertical: 20,
                                ),

                                ///
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        FittedBox(
                                          child: Text(
                                            'ABOUT',
                                            style: _textStyle0(context),
                                          ),
                                        ),
                                        GestureDetector(
                                          child: FaIcon(
                                            FontAwesomeIcons.edit,
                                            size: 18,
                                          ),
                                          onTap: () async {
                                            try {
                                              try {
                                                aboutController.text =
                                                    futureTutorUserInfo
                                                        .data.about;
                                                // DO SOMETHING
                                                await aboutUpdateDialog(
                                                  context: context,
                                                  inputController:
                                                      aboutController,
                                                ).then((_) => _getData());
                                              } catch (e) {
                                                print(e);
                                              }
                                            } catch (e) {
                                              print(e);
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                    Divider(
                                      color: AssetStrings.color4,
                                      height: 20,
                                      thickness: 2,
                                    ),

                                    /////////////////////////// ABOUT TEXT /////////////////////////////
                                    Container(
                                      child: Text(
                                        futureTutorUserInfo.data.about ??
                                            "Not Available",
                                        style: _textStyle2(context),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),

                            //============================== CHANGE PASSWORD START =============================//

                            Container(
                              margin:
                                  EdgeInsets.only(top: 40, left: 40, right: 40),
                              width: sWidth,
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 3,
                                      offset: Offset(0, 4)),
                                ],
                              ),
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: 30,
                                  vertical: 20,
                                ),

                                ///
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        FittedBox(
                                          child: Text(
                                            'CHANGE PASSWORD',
                                            style: _textStyle0(context),
                                          ),
                                        ),

                                        //=============================== ABOUT UPDATE =============================//
                                        Opacity(
                                          opacity: 0,
                                          child: FaIcon(
                                            FontAwesomeIcons.edit,
                                            size: 18,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Divider(
                                      color: AssetStrings.color4,
                                      height: 20,
                                      thickness: 2,
                                    ),

                                    /////////////////////////// ABOUT TEXT /////////////////////////////
                                    Container(
                                      padding: EdgeInsets.only(
                                        left: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: AssetStrings.color4,
                                          width: 2,
                                        ),
                                      ),
                                      child: TextField(
                                        controller: _currentPassController,
                                        obscureText: fishEye1,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "Current Password",
                                          hintStyle: TextStyle(
                                            fontSize: 14,
                                          ),
                                          suffixIcon: IconButton(
                                            icon: Icon(Icons.remove_red_eye),
                                            splashRadius: 1,
                                            onPressed: () {
                                              setState(() {
                                                fishEye1 = !fishEye1;
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(
                                        left: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: AssetStrings.color4,
                                          width: 2,
                                        ),
                                      ),
                                      child: TextField(
                                        controller: _newPassController,
                                        obscureText: fishEye2,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "New Password",
                                          hintStyle: TextStyle(
                                            fontSize: 14,
                                          ),
                                          suffixIcon: IconButton(
                                            icon: Icon(Icons.remove_red_eye),
                                            splashRadius: 1,
                                            onPressed: () {
                                              setState(() {
                                                fishEye2 = !fishEye2;
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                    ),

                                    SizedBox(
                                      height: 10,
                                    ),

                                    Align(
                                      alignment: Alignment.center,
                                      child: MaterialButton(
                                        shape: StadiumBorder(
                                            // side: BorderSide(
                                            //   width: 2,
                                            //   color: Colors.grey,
                                            // ),
                                            ),
                                        color: AssetStrings.color4,
                                        child: Text(
                                          'Submit',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        highlightElevation: 0,
                                        onPressed: () async {
                                          try {
                                            EasyLoading.show(
                                                status: "Loading...");
                                            Future.delayed(
                                                Duration(seconds: 2));
                                            await serv.tutorPassChange(
                                                currentPassword:
                                                    _currentPassController.text,
                                                newPassword:
                                                    _newPassController.text);
                                            EasyLoading.showSuccess(
                                                "Password Changed!");
                                            _currentPassController.clear();
                                            _newPassController.clear();
                                          } catch (e) {
                                            EasyLoading.showError("Error!");
                                            customToast(
                                                "Please check your current password!");
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),

                            SizedBox(
                              height: 40,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  _textStyle0(BuildContext context) {
    return TextStyle(
      color: Colors.black87,
      fontFamily: "QuickSandBold",
      fontSize: 13,
    );
  }

  _textStyle1(BuildContext context) {
    return TextStyle(
      color: AssetStrings.color4,
      fontFamily: "QuickSandBold",
      fontSize: 11,
    );
  }

  _textStyle2(BuildContext context) {
    return TextStyle(
      color: Colors.black,
      fontFamily: "QuickSandMedium",
      fontSize: 11,
    );
  }

  //============================= CUSTOM EDUCATION CARD =================================//
  Container educationCard({
    @required BuildContext context,
    @required String institutionName,
    @required String grade,
    @required String background,
    @required String medium,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      padding: EdgeInsets.all(10),
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: AssetStrings.color4,
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //////////////////////////////// SCHOOL //////////////////////////////////
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ////////////////////! SCHOOL/ COLLEGE NAME ////////////////////
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'SCHOOL/COLLEGE',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      institutionName ?? "Not Avalable",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              //////////////////////! GRADE ////////////////////////////
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'GRADE',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      grade ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //////////////////////! BACKGROUND ////////////////////////////

              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'BACKGROUND',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      background ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),

              //////////////////////! MEDIUM ////////////////////////////

              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'MEDIUM',
                      style: _textStyle1(context),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      medium ?? "Not Available",
                      style: _textStyle2(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  //============================= CUSTOM EDUCATION CARD =================================//
  Container locationCard({
    @required BuildContext context,
    @required String district,
    @required String area,
    @required String road,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 5,
      ),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: AssetStrings.color4,
        ),
      ),
      child: Text(
        '$area, $road, $district',
        style: _textStyle2(context),
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
      ),
    );
  }
}
