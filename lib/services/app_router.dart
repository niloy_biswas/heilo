import 'package:Heilo/pages/common/initial_page.dart';
import 'package:Heilo/pages/common/login_page.dart';
import 'package:Heilo/pages/common/otp_page.dart';
import 'package:Heilo/pages/common/signup_page.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
// import 'package:Heilo/pages/student/payment_page.dart';
import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:flutter/material.dart';

class AppRouter {
  Route onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => InitialPage());
        break;

      case '/loginPage':
        return MaterialPageRoute(builder: (_) => LoginPage());
        break;

      case '/signUpPage':
        return MaterialPageRoute(builder: (_) => SignUpPage());
        break;

      case '/otpPage':
        return MaterialPageRoute(builder: (_) => OtpPage());
        break;

      case '/bottomNav':
        return MaterialPageRoute(builder: (_) => StudentBottomNav());
        break;

      case '/tutorBottomNav':
        return MaterialPageRoute(builder: (_) => TeacherBottomNav());
        break;

      // case '/paymentPage':
      //   return MaterialPageRoute(builder: (_) => PaymentPage());
      //   break;

      default:
        return null;
    }
  }
}
