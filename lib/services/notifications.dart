// import 'package:Heilo/main.dart';
// import 'package:Heilo/pages/common/contact_us.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:math';

class NotificationService {
  NotificationService._() {
    initialize();
  }
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  var initializationSettings;

  // doSomething() async {
  //   // Navigating to next screen
  //   Services serv = Services();
  //   try {
  //     await serv.getTutorUserInfo(refresh: true);
  //   } catch (e) {
  //     return;
  //   }
  // }

  // doSomething() {
  //   Future.delayed(Duration(seconds: 1));
  //   // navKey.currentState.pushNamed('contactUs');
  //   navKey.currentState.push(MaterialPageRoute(builder: (_) => ContactUs()));
  // }

  // Initialize
  Future initialize() async {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings("ic_launcher");

    IOSInitializationSettings iosInitializationSettings =
        IOSInitializationSettings();

    initializationSettings = InitializationSettings(
      android: androidInitializationSettings,
      iOS: iosInitializationSettings,
    );

    // await flutterLocalNotificationsPlugin.initialize(
    // initializationSettings,
    // onSelectNotification: doSomething(),
    // );
  }

  setOnNotificationClick(Function onNotificationClick) async {
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
      onNotificationClick(payload);
    });
  }

  // // INSTANT NOTIFICATION
  // Future instantNotification() async {
  //   // int MAX = 100;
  //   // print(new Random().nextInt(MAX));
  //   var android = AndroidNotificationDetails(
  //     "Id",
  //     "Channel",
  //     "Description",
  //     priority: Priority.high,
  //     importance: Importance.max,
  //   );

  //   var ios = IOSNotificationDetails();

  //   var platform = new NotificationDetails(
  //     android: android,
  //     iOS: ios,
  //   );

  //   await _flutterLocalNotificationsPlugin.show(
  //     // id, title, body, notificationdetail
  //     // 0,
  //     Random().nextInt(100),
  //     "Demo Notification",
  //     "Tap to do something",
  //     platform,
  //     payload: "WELCOME TO FLUTTER NOTIFICATION APP",
  //   );
  // }

  // INSTANT NOTIFICATION
  Future instantNotification({
    @required String title,
    @required String content,
  }) async {
    // int MAX = 100;
    // print(new Random().nextInt(MAX));
    var android = AndroidNotificationDetails(
      "Id",
      "Channel",
      "Description",
      priority: Priority.high,
      importance: Importance.max,
    );

    var ios = IOSNotificationDetails();

    var platform = new NotificationDetails(
      android: android,
      iOS: ios,
    );

    await flutterLocalNotificationsPlugin.show(
      // id, title, body, notificationdetail
      // 0,
      // "Demo Notification",
      // "Tap to do something",
      Random().nextInt(100),
      title,
      content,
      platform,
      payload: "01",
    );
  }

  // IMAGE NOTIFICATION
  Future imageNotification() async {
    var bigPicture = BigPictureStyleInformation(
      DrawableResourceAndroidBitmap("ic_launcher"),
      largeIcon: DrawableResourceAndroidBitmap("ic_launcher"),
      contentTitle: "Demo Image Notification",
      summaryText: "This is summary text",
      htmlFormatContent: true,
      htmlFormatTitle: true,
    );
    var android = AndroidNotificationDetails(
      "Id",
      "Channel",
      "Description",
      styleInformation: bigPicture,
    );

    var platform = new NotificationDetails(
      android: android,
    );

    await flutterLocalNotificationsPlugin.show(
      // id, title, body, notificationdetail
      0,
      "Demo Image Notification",
      "Tap to do something",
      platform,
      payload: "WELCOME TO FLUTTER NOTIFICATION APP",
    );
  }

  // Stylish Notification
  //
  Future stylishNotification() async {
    var android = AndroidNotificationDetails(
      "id",
      "channelName",
      "channelDescription",
      color: Colors.deepOrange,
      enableLights: true,
      enableVibration: true,
      largeIcon: DrawableResourceAndroidBitmap("ic_launcher"),
      styleInformation: MediaStyleInformation(
        htmlFormatContent: true,
        htmlFormatTitle: true,
      ),
    );

    var platform = new NotificationDetails(
      android: android,
    );

    await flutterLocalNotificationsPlugin.show(
      // id, title, body, notificationdetail
      0,
      "Demo Stylish Notification",
      "Tap to do something",
      platform,
    );
  }

  // Scheduled Notification
  Future scheduleNotification() async {
    var interval = RepeatInterval.everyMinute;

    var bigPicture = BigPictureStyleInformation(
      DrawableResourceAndroidBitmap("ic_launcher"),
      largeIcon: DrawableResourceAndroidBitmap("ic_launcher"),
      contentTitle: "Demo Image Notification",
      summaryText: "This is summary text",
      htmlFormatContent: true,
      htmlFormatTitle: true,
    );
    var android = AndroidNotificationDetails(
      "Id",
      "Channel",
      "Description",
      styleInformation: bigPicture,
    );

    var platform = new NotificationDetails(
      android: android,
    );

    await flutterLocalNotificationsPlugin.periodicallyShow(
      // id, title, body, notificationdetail
      0,
      "Demo Scheduled Notification",
      "Tap to do something",
      interval,
      platform,
    );
  }

  // Cancel Notification
  Future cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }
}

NotificationService notificationPlugin = NotificationService._();
