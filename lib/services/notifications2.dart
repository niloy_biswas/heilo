// import 'package:Heilo/main.dart';
// import 'package:Heilo/pages/common/contact_us.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:math';

class NotificationService2 {
  NotificationService2._() {
    initialize();
  }
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin2;
  var initializationSettings;

  // doSomething() async {
  //   // Navigating to next screen
  //   Services serv = Services();
  //   try {
  //     await serv.getTutorUserInfo(refresh: true);
  //   } catch (e) {
  //     return;
  //   }
  // }

  // doSomething() {
  //   Future.delayed(Duration(seconds: 1));
  //   // navKey.currentState.pushNamed('contactUs');
  //   navKey.currentState.push(MaterialPageRoute(builder: (_) => ContactUs()));
  // }

  // Initialize
  Future initialize() async {
    flutterLocalNotificationsPlugin2 = FlutterLocalNotificationsPlugin();

    AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings("ic_launcher");

    IOSInitializationSettings iosInitializationSettings =
        IOSInitializationSettings();

    initializationSettings = InitializationSettings(
      android: androidInitializationSettings,
      iOS: iosInitializationSettings,
    );

    // await flutterLocalNotificationsPlugin.initialize(
    // initializationSettings,
    // onSelectNotification: doSomething(),
    // );
  }

  setOnNotificationClick(Function onNotificationClick) async {
    await flutterLocalNotificationsPlugin2.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
      onNotificationClick(payload);
    });
  }

  Future instantNotification2({
    @required String title,
    @required String content,
    @required String payload,
  }) async {
    // int MAX = 100;
    // print(new Random().nextInt(MAX));
    var android = AndroidNotificationDetails(
      "Id2",
      "Channel2",
      "Description2",
      priority: Priority.high,
      importance: Importance.max,
    );

    var ios = IOSNotificationDetails();

    var platform = new NotificationDetails(
      android: android,
      iOS: ios,
    );

    await flutterLocalNotificationsPlugin2.show(
      // id, title, body, notificationdetail
      // 0,
      // "Demo Notification",
      // "Tap to do something",
      Random().nextInt(100),
      title,
      content,
      platform,
      payload: payload,
    );
  }

  // Cancel Notification
  Future cancelNotification() async {
    await flutterLocalNotificationsPlugin2.cancelAll();
  }
}

NotificationService2 notificationPlugin2 = NotificationService2._();
