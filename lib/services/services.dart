import 'dart:convert';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/location_model.dart';
import 'package:Heilo/models/pending_tution_request_model.dart';
import 'package:Heilo/models/segment_model.dart';
import 'package:Heilo/models/student_class_details_model.dart';
import 'package:Heilo/models/student_home_tutor_list_model.dart';
import 'package:Heilo/models/student_tution_class_list.dart';
import 'package:Heilo/models/student_tutor_details_model.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/models/subject_model.dart';
import 'package:Heilo/models/topic_model.dart';
import 'package:Heilo/models/tutor_class_detail.dart';
import 'package:Heilo/models/tutor_student_view_model.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/pages/common/initial_page.dart';
import 'package:Heilo/pages/common/otp_page.dart';
import 'package:Heilo/pages/common/sign_in_otp_page.dart';
import 'package:Heilo/pages/teacher/sign_up_location.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
// import 'package:Heilo/pages/common/signup_page.dart';
// import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Services {
  // ignore: non_constant_identifier_names
  String URL = "http://api.heilo.com.bd";
  // String URL = "http://heilo.bornonit.com";

  //////////////////////////////// CHECK STUDENT USER EXISTENCE //////////////////////////////////
  Future getStudentUserExist(BuildContext context, String contact) async {
    Map data = {
      'contact': contact,
    };
    var jsonData;

    var response =
        await http.post("$URL/api/auth/student/userExistByPhone", body: data);

    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);

      if (jsonData["exist"] == true) {
        customToast('User already exists!');
      } else {
        print(contact);
        print(jsonData);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => OtpPage()));
        // Navigator.pushReplacement(
        //     context, MaterialPageRoute(builder: (context) => SignUpLocation()));
      }
    } else {
      print(jsonData);
    }
  }

  //////////////////////////////// CHECK TUTOR USER EXISTENCE //////////////////////////////////
  Future getTutorUserExist(BuildContext context, String contact) async {
    Map data = {
      'contact': contact,
    };
    var jsonData;

    var response =
        await http.post("$URL/api/auth/tutor/userExistByPhone", body: data);

    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);

      if (jsonData["exist"] == true) {
        customToast('User already exists!');
      } else {
        print(contact);
        print(jsonData);
        // Navigator.pushReplacement(
        //     context, MaterialPageRoute(builder: (context) => OtpPage()));
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignUpLocation()));
      }
    } else {
      print(jsonData);
    }
  }

  //////////////////////////////// STUDENT LOGIN //////////////////////////////////
  Future studentLogin(
      BuildContext context, String contact, String password) async {
    Map data = {
      'contact': contact,
      'password': password,
    };
    var jsonData;

    var response = await http.post("$URL/api/auth/student/login", body: data);

    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);
      print(jsonData["access_token"]);
      SharedPreferenceHelper().saveUserAccessToken(jsonData["access_token"]);
      // Navigator.pushReplacement(
      //     context, MaterialPageRoute(builder: (context) => StudentBottomNav()));
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => SignInOtp()));
    } else {
      print('LOGIN ERROR');
      customToast("Login Error");
    }
  }

  //////////////////////////////// TUTOR LOGIN ////////////////////////////////////
  Future tutorLogin(
      BuildContext context, String contact, String password) async {
    Map data = {
      'contact': contact,
      'password': password,
    };
    var jsonData;

    var response = await http.post("$URL/api/auth/tutor/login", body: data);

    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);
      print(jsonData["access_token"]);
      SharedPreferenceHelper().saveUserAccessToken(jsonData["access_token"]);
      // Navigator.pushReplacement(
      //     context, MaterialPageRoute(builder: (context) => TeacherBottomNav()));
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => SignInOtp()));
    } else {
      print('LOGIN ERROR');
      customToast("Login Error");
    }
  }

  ////////////////////////// STUDENT SIGN UP //////////////////////////
  Future studentSignUp({
    @required String name,
    @required String email,
    @required String mobile,
    @required String password,
    @required String uid,
    @required String medium,
    @required String segmentId,
  }) async {
    Map data = {
      "contact": mobile,
      "password": password,
      "name": name,
      "email": email,
      "medium": medium,
      "segment_id": segmentId,
      "uid": uid
    };

    var response = await http.post("$URL/api/auth/student/signup", body: data);

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      // print(jsonData);
      SharedPreferenceHelper().saveUserAccessToken(jsonData["access_token"]);
      print(jsonData["access_token"]);
      customToast('Sign Up Successful');
    } else {
      var jsonData = json.decode(response.body);
      print(jsonData);
      customToast('Sign Up Failed');
    }
  }

  ////////////////////////// TUTOR SIGN UP //////////////////////////
  Future tutorSignUp({
    @required String name,
    @required String email,
    @required String mobile,
    @required String password,
    // @required String otpToken,
    @required String uid,
    @required String hourlyRate,
    @required String medium,
  }) async {
    Map data = {
      "name": name,
      "email": email,
      "contact": mobile,
      "password": password,
      "uid": uid,
      "hourly_rate": hourlyRate,
      "medium": medium
    };

    var response = await http.post("$URL/api/auth/tutor/signup", body: data);

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      print(jsonData["access_token"]);
      SharedPreferenceHelper().saveUserAccessToken(jsonData["access_token"]);
      customToast('Sign Up Successful');
    } else {
      var jsonData = json.decode(response.body);
      print(jsonData);
      customToast('Sign Up Failed');
    }
  }

  ////////////////////////// STUDENT USER INFO //////////////////////////
  Future<StudentUserInfoModel> getStudentInfo() async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    final response =
        await http.get("$URL/api/auth/student/user", headers: headers);

    if (response.statusCode == 200) {
      print('USER INFO SUCCESS');
      var jsonData = json.decode(response.body);
      print(jsonData);
      return StudentUserInfoModel.fromJson(json.decode(response.body));
    } else {
      var jsonData = json.decode(response.body);
      print(jsonData);
      // throw Exception("USER INFO NOT WORKING");
      return null;
    }
  }

  ////////////////////////// USER LOGOUT //////////////////////////
  userLogOut(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await FirebaseAuth.instance.signOut();
    await prefs.clear();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    if (_dioCacheManager != null) {
      // bool res = await _dioCacheManager.deleteByPrimaryKey(
      //     "https://jsonplaceholder.typicode.com/users");
      bool res = await _dioCacheManager.clearAll();
      print(res);
    }
    await AndroidAlarmManager.cancel(1);
    await AndroidAlarmManager.cancel(2);
    await AndroidAlarmManager.cancel(3);
    await AndroidAlarmManager.cancel(4);
    await AndroidAlarmManager.cancel(10);

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => InitialPage()));
    customToast("User Logged Out");
  }

  ////////////////////////// LOCATIONS //////////////////////////
  Future<LocationModel> getLocations() async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    final response =
        await http.get('$URL/api/content/locations', headers: headers);

    if (response.statusCode == 200) {
      print('Location Ready');
      print(json.decode(response.body));
      return LocationModel.fromJson(json.decode(response.body));
    } else {
      customToast("Location Api not working");
      throw Exception('Location Api not working');
    }
  }

  ////////////////////////// SUBJECTS //////////////////////////
  Future<SubjectModel> getSubjects({
    @required String segmentId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    final response = await http.get('$URL/api/content/subjects/$segmentId',
        headers: headers);

    if (response.statusCode == 200) {
      print('Subject Ready');
      print(json.decode(response.body));
      return SubjectModel.fromJson(json.decode(response.body));
    } else {
      customToast("Location Api not working");
      throw Exception('Location Api not working');
    }
  }

  ////////////////////////// IT IS I, DIO //////////////////////////////////
  /////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////
  Future<LocationModel> getDioLocations({@required bool refresh}) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );

    // var headers = {
    // 'Authorization': 'Bearer $accessToken',
    // };

    var apiData;

    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response = await _dio.get(
      '$URL/api/content/locations',
      options: _cacheOptions,
    );

    apiData = LocationModel.fromJson(response.data);

    return apiData;

    // Previously in try catch
  }

  Future<StudentUserInfoModel> getDioStudentInfo(
      {@required bool refresh}) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );
    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    final response = await _dio.get(
      "$URL/api/auth/student/user",
      options: _cacheOptions,
    );

    print('USER INFO SUCCESS');
    var jsonData = response.data;
    print(jsonData);
    print(jsonData['data']['segment']['segment_id']);
    await SharedPreferenceHelper()
        .saveSegmentId(jsonData['data']['segment']['segment_id'].toString());
    return StudentUserInfoModel.fromJson(jsonData);
  }

  Future<SubjectModel> getDioSubjects({
    @required String segmentId,
    @required bool refresh,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );
    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    final response = await _dio.get(
      '$URL/api/content/subjects/$segmentId',
      options: _cacheOptions,
    );

    print(response.data);

    return SubjectModel.fromJson(response.data);
  }

  Future<TopicModel> getDioTopics({
    @required String subjectId,
    @required bool refresh,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );
    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    final response = await _dio.get(
      '$URL/api/content/topics/$subjectId',
      options: _cacheOptions,
    );

    print(response.data);

    return TopicModel.fromJson(response.data);
  }

  //! Tutor List for Student Home Screen
  Future<StudentHomeTutorListModel> getDioStudentHomeTutorList({
    @required String locationId,
    @required String subjectId,
    @required String topicId,
    @required bool refresh,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    if (locationId == null) {
      locationId = '';
    }

    if (subjectId == null) {
      subjectId = '';
    }

    if (topicId == null) {
      topicId = '';
    }

    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );
    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    final response = await _dio.get(
      '$URL/api/tutors?loc_id=$locationId&subject_id=$subjectId&topic_id=$topicId',
      options: _cacheOptions,
    );

    // print(response.data);
    return StudentHomeTutorListModel.fromJson(response.data);
  }

  //! Tutor Details from Student Home Screen
  Future<StudentTutorDetails> getStudentHomeTutorDetails({
    @required int tutorId,
    @required bool refresh,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );
    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    final response = await _dio.get(
      "$URL/api/tutors/$tutorId",
      options: _cacheOptions,
    );
    print(response.data);

    return StudentTutorDetails.fromJson(response.data);
  }

  //! Student Education Add //
  Future<void> addStudentEducation({
    String institutionName,
    String background,
    String medium,
    String institutionType,
    String grade,
    String detailAddress,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    var data = {
      "institution_type": institutionType,
      "institution_name": institutionName,
      "background": background,
      "grade": grade,
      "medium": medium,
      "detail_address": detailAddress
    };
    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    await _dio.post("$URL/api/auth/student/user/education",
        data: jsonEncode(data));
  }

  //! Student Education Delete
  Future deleteStudentEducation({@required int eduId}) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    final responseData =
        await _dio.delete("$URL/api/auth/student/user/education/$eduId");
    print(responseData.data);
  }

  ///* Student Education Info Update
  ///
  Future<void> updateStudentEducationDetails({
    @required String institutionName,
    @required String background,
    @required String medium,
    @required String institutionType,
    @required String grade,
    @required String detailAddress,
    @required String eduId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    var data = {
      "institution_type": institutionType,
      "institution_name": institutionName,
      "background": background,
      "grade": grade,
      "medium": medium,
      "detail_address": detailAddress
    };
    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    await _dio.put("$URL/api/auth/student/user/education/$eduId",
        data: jsonEncode(data));
  }

  ///* Student Personal Info Update ///
  ///
  Future<void> updateStudentPersonalDetails({
    @required String name,
    @required String email,
    @required String contactNo,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {"name": name, "email": email, "alt_phone": contactNo};

    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    final responseData =
        await _dio.put("$URL/api/auth/student/user", data: jsonEncode(data));
    print(responseData.data);
  }

  //////////////////////////// TUTOR PENDING TUTION REQUESTS /////////////////////////////
  ///
  Future<PendingTutionRequestsModel> getPendingTutionList(
      {@required bool refresh}) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );
    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response =
        await _dio.get("$URL/api/tutor/classes", options: _cacheOptions);
    print("API DATA: ${response.data}");
    return PendingTutionRequestsModel.fromJson(response.data);
  }

  /////////////////////////// TUTOR USER INFO GET ///////////////////////////////////
  ///
  Future<TutorUserInfoModel> getTutorUserInfo({@required bool refresh}) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );

    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response = await _dio.get(
      "$URL/api/auth/tutor/user",
      options: _cacheOptions,
    );

    print("USER DATA: ${response.data}");
    return TutorUserInfoModel.fromJson(response.data);
  }

  ///////////////////////// TUTOR PERSONAL DETAIL UPDATE /////////////////////////
  ///
  // Future updateTutorPersonalDetail({
  //   @required String name,
  //   @required String email,
  //   @required String area,
  //   @required String detailAddress,
  // }) async {
  //   var data = {
  //     "name": name,
  //     "email": email,
  //     "hourly_rate": "",
  //     "about": "",
  //   };
  // }
  //
  //
  // //////////////////////////// TUTOR LOCATION ADD ////////////////////////////////
  // ///
  // Future<void> addTutorLocation({
  //   @required String district,
  //   @required String area,
  //   @required String road,
  //   @required String detailAddress,
  // }) async {
  //   Map data = {
  //     "long": "",
  //     "lat": "",
  //     "district": district,
  //     "area": area,
  //     "road": road,
  //     "detail_address": detailAddress
  //   };
  //   String accessToken = await SharedPreferenceHelper().getUserAccessToken();
  //   print(accessToken);

  //   var headers = {
  //     'Authorization': 'Bearer $accessToken',
  //   };

  //   var response = await http.post("$URL/api/auth/tutor/user/location",
  //       body: data, headers: headers);
  //   print("TUTOR LOCATION RESPONSE DATA: ${json.decode(response.body)}");
  // }

  /////////////////////////// GET SEGMENT ID ///////////////////////////////////
  ///
  Future<SegmentModel> getSegmentData({
    @required bool refresh,
    @required String medium,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );

    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response = await _dio.get(
      "$URL/api/content/segments?medium=$medium",
      options: _cacheOptions,
    );

    print("SEGMENT API DATA: ${response.data}");
    return SegmentModel.fromJson(response.data);
  }

  /////////////////////////// SAVE TUTOR SKILL INFORMATION /////////////////////////////
  ///
  Future<void> saveTutorSkills({
    @required int tutorId,
    @required int segmentId,
    @required int subjectId,
    @required String topicId,
  }) async {
    Map data = {
      "tutor_id": tutorId,
      "segment_id": segmentId,
      "subject_id": subjectId,
      "topic_id": topicId
    };

    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    final responseData = await _dio.post("$URL/api/auth/tutor/user/subjects",
        data: jsonEncode(data));
    print(responseData.data);
  }

  ////////////////////////////////// SAVE TUTOR DP ////////////////////////////////////
  ///
  Future<void> tutorDpUpload({@required String dpString}) async {
    Map data = {
      "dp": dpString,
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.put('$URL/api/auth/tutor/user',
        headers: headers, body: data);

    print(json.decode(response.body));

    if (response.statusCode == 202) {
      print('It worked');
      var jsonData = json.decode(response.body);
      print(jsonData);
    } else {
      print('Something went wrong');
      print(response.body);
    }
  }

  ////////////////////////////////// SAVE STUDENT DP ////////////////////////////////////
  ///
  Future<void> studentDpUpload({@required String dpString}) async {
    Map data = {
      "dp": dpString,
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.put('$URL/api/auth/student/user',
        headers: headers, body: data);

    print(json.decode(response.body));

    if (response.statusCode == 202) {
      print('It worked');
      var jsonData = json.decode(response.body);
      print(jsonData);
    } else {
      print('Something went wrong');
      print(response.body);
    }
  }

  /////////////////////////// GET STUDENT CLASS LIST ///////////////////////////////////
  ///
  Future<StudentTutionClassList> getStudentClassList({
    @required bool refresh,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );

    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response = await _dio.get(
      "$URL/api/student/classes",
      options: _cacheOptions,
    );

    print("STUDENT CLASS LIST API DATA: ${response.data}");
    return StudentTutionClassList.fromJson(response.data);
  }

  ///========================== DELETE CLASS (STUDENT) ==========================///
  Future deleteClassStudent({
    @required String code,
    @required String status,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {"code": code, "status": status};

    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    final responseData = await _dio.put("$URL/api/student/classes/$classId",
        data: jsonEncode(data));
    print(responseData.data);
  }

  ///========================= STUDENT CLASS DETAILS =============================///
  ///
  Future<StudentClassDetailsModel> getStudentClassDetails({
    @required bool refresh,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );

    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response = await _dio.get(
      "$URL/api/student/classes/$classId",
      options: _cacheOptions,
    );

    print("STUDENT CLASS LIST API DATA: ${response.data}");
    return StudentClassDetailsModel.fromJson(response.data);
  }

  ///========================= TUTOR CLASS DETAILS =============================///
  ///
  Future<TutorClassDetailModel> getTutorClassDetails({
    @required bool refresh,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );

    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response = await _dio.get(
      "$URL/api/tutor/classes/$classId",
      options: _cacheOptions,
    );

    print("STUDENT CLASS LIST API DATA: ${response.data}");
    return TutorClassDetailModel.fromJson(response.data);
  }

  ///========================== START TUTION (STUDENT) ==========================///
  ///
  Future startTutionStudent({
    @required String code,
    @required String status,
    @required String startTime,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {"code": code, "status": status, "start_time": startTime};

    // Dio _dio = Dio();
    // _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    // final responseData = await _dio.put("$URL/api/student/classes/$classId",
    //     data: jsonEncode(data));
    // print(responseData.data);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.put('$URL/api/student/classes/$classId',
        headers: headers, body: data);

    print(json.decode(response.body));
  }

  ///========================== END TUTION (STUDENT) ==========================///
  ///
  Future endTutionStudent({
    @required String code,
    @required String status,
    @required String endTime,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    Map data = {"code": code, "status": status, "ending_time": endTime};

    // Dio _dio = Dio();
    // _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    // final responseData = await _dio.put("$URL/api/student/classes/$classId",
    //     data: jsonEncode(data));
    // print(responseData.data);
    //
    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.put('$URL/api/student/classes/$classId',
        headers: headers, body: data);

    print(json.decode(response.body));
  }

  ///========================== TUTOR CLASS ACCEPT (STUDENT) ==========================///
  ///
  Future tutorClassAccept({
    @required String code,
    @required String status,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {"code": code, "status": status};

    // Dio _dio = Dio();
    // _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    // final responseData = await _dio.put("$URL/api/tutor/classes/$classId",
    //     data: jsonEncode(data));
    // print(responseData.data);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.put('$URL/api/tutor/classes/$classId',
        headers: headers, body: data);

    print(json.decode(response.body));
  }

  ///========================== TUTOR CLASS ADD (STUDENT) ==========================///
  ///
  Future<void> addStudentClass({
    @required String tutorId,
    @required String locationId,
    @required int subjectId,
    @required int topicId,
    @required String timeFrom,
    @required String timeTo,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {
      "tutor_id": tutorId,
      "loc_id": locationId,
      "subject_id": subjectId,
      "topic_id": topicId,
      "time_from": timeFrom,
      "time_to": timeTo
    };

    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    final responseData =
        await _dio.post("$URL/api/student/classes", data: jsonEncode(data));
    print(responseData.data);

    // var headers = {
    //   'Authorization': 'Bearer $accessToken',
    // };

    // var response = await http.post("$URL/api/student/classes",
    //     headers: headers, body: data);

    // print(json.decode(response.body));
  }

  //////////////////////////// STUDENT ABOUT ADD ////////////////////////////////
  ///
  Future<void> addStudentAbout({
    @required String about,
  }) async {
    Map data = {
      "about": about,
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    try {
      var response = await http.put("$URL/api/auth/student/user",
          body: data, headers: headers);
      print("TUTOR LOCATION RESPONSE DATA: ${json.decode(response.body)}");
    } catch (e) {
      throw e;
    }
  }

  //========================= TUTOR SKILL DELETE ==============================//
  //
  ///========================== DELETE CLASS (STUDENT) ==========================///
  Future<void> deleteTutorSkill({
    @required String tutorId,
    @required String segmentId,
    @required String subjectId,
    @required String topicId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {
      "tutor_id": tutorId,
      "segment_id": segmentId,
      "subject_id": subjectId,
      "topic_id": topicId
    };

    // Dio _dio = Dio();
    // _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    // final responseData = await _dio.post(
    //     "$URL/api/auth/tutor/user/subjects/delete",
    //     data: jsonEncode(data));
    // print(responseData.data);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    try {
      var response = await http.post("$URL/api/auth/tutor/user/subjects/delete",
          body: data, headers: headers);
      print("TUTOR LOCATION RESPONSE DATA: ${json.decode(response.body)}");
    } catch (e) {
      throw e;
    }
  }

  //////////////////////////// TUTOR ABOUT ADD ////////////////////////////////
  ///
  Future<void> addTutorAbout({
    @required String about,
  }) async {
    Map data = {
      "about": about,
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    try {
      var response = await http.put("$URL/api/auth/tutor/user",
          body: data, headers: headers);
      print("TUTOR LOCATION RESPONSE DATA: ${json.decode(response.body)}");
    } catch (e) {
      throw e;
    }
  }

  ///========================== RATING TUTORS CLASS (STUDENT) ==========================///
  Future<void> ratingTutorSession({
    @required String tutorId,
    @required String prefer,
    @required String rate,
    @required String feedback,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {
      "tutor_id": tutorId,
      "prefer": prefer,
      "rate": rate,
      "feedback": feedback
    };
    // Dio _dio = Dio();
    // _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    // final responseData = await _dio.post(
    //     "$URL/api/auth/tutor/user/subjects/delete",
    //     data: jsonEncode(data));
    // print(responseData.data);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    try {
      var response = await http.post("$URL/api/student/classes/$classId/review",
          body: data, headers: headers);
      print("TUTOR REVIEW RESPONSE DATA: ${json.decode(response.body)}");
    } catch (e) {
      throw e;
    }
  }

  ///========================== RATING STUDENT (TUTOR) ==========================///
  Future<void> ratingStudentSession({
    @required String studentId,
    @required String prefer,
    @required String rate,
    @required String feedback,
    @required String classId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var data = {
      "student_id": studentId,
      "prefer": prefer,
      "rate": rate,
      "feedback": feedback
    };
    // Dio _dio = Dio();
    // _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    // final responseData = await _dio.post(
    //     "$URL/api/auth/tutor/user/subjects/delete",
    //     data: jsonEncode(data));
    // print(responseData.data);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    try {
      var response = await http.post("$URL/api/tutor/classes/$classId/review",
          body: data, headers: headers);
      print("TUTOR REVIEW RESPONSE DATA: ${json.decode(response.body)}");
    } catch (e) {
      throw e;
    }
  }

  ///========================= STUDENT PROFILE VIEW FOR TUTOR (FROM TUTOR CLASS LIST/DETAILS) =============================///
  ///
  Future<TutorStudentViewModel> getStudentForTutor({
    @required bool refresh,
    @required String studentId,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
    Options _cacheOptions = buildCacheOptions(
      Duration(days: 3),
      forceRefresh: refresh,
    );

    _dio.interceptors.add(_dioCacheManager.interceptor);
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';

    var response = await _dio.get(
      "$URL/api/students/$studentId",
      options: _cacheOptions,
    );

    print("STUDENT CLASS LIST API DATA: ${response.data}");
    return TutorStudentViewModel.fromJson(response.data);
  }

  //================================ TUTOR EDUCATION ADD =================================//
  Future<void> addTutorEducation({
    @required String institutionName,
    @required String background,
    @required String medium,
    @required String institutionType,
    @required String grade,
  }) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    var data = {
      "institution_type": institutionType,
      "institution_name": institutionName,
      "background": background,
      "grade": grade,
      "medium": medium,
    };
    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    await _dio.post("$URL/api/auth/tutor/user/education",
        data: jsonEncode(data));
  }

  //======================================== TUTOR EDUCATION DELETE ====================================//
  //
  Future deleteTutorEducation({@required int eduId}) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    final responseData =
        await _dio.delete("$URL/api/auth/tutor/user/education/$eduId");
    print(responseData.data);
  }

  //======================================== TUTOR ONLINE/OFFLINE MODE ====================================//
  //
  Future tutorProfileStatus({@required String activeStatus}) async {
    Map data = {
      "status": activeStatus,
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.put('$URL/api/auth/tutor/user',
        headers: headers, body: data);

    print(json.decode(response.body));

    // if (response.statusCode == 202) {
    //   print('It worked');
    //   var jsonData = json.decode(response.body);
    //   print(jsonData);
    // } else {
    //   print('Something went wrong');
    //   print(response.body);
    // }
  }

  //================================= TUTOR LOCATION DELETE =============================//
  //
  Future deleteTutorLocation({@required int recordId}) async {
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);
    Dio _dio = Dio();
    _dio.options.headers["Authorization"] = 'Bearer $accessToken';
    final responseData =
        await _dio.delete("$URL/api/auth/tutor/user/location/$recordId");
    print(responseData.data);
  }

  //////////////////////////// TUTOR LOCATION ADD ////////////////////////////////
  ///
  Future<void> addTutorLocation({
    @required String locId,
  }) async {
    Map data = {
      "loc_id": locId,
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.post("$URL/api/auth/tutor/user/location",
        body: data, headers: headers);

    if (response.statusCode == 200) {
      print("TUTOR LOCATION RESPONSE DATA: ${json.decode(response.body)}");
    } else {
      throw Exception("FAILED TO UPDATE LOCATION");
    }
  }

  //////////////////////////// STUDENT CHANGE PASSWORD ////////////////////////////////
  ///
  Future<void> studentPassChange({
    @required String currentPassword,
    @required String newPassword,
  }) async {
    Map data = {
      "current_password": currentPassword,
      "new_password": newPassword
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.post("$URL/api/auth/student/user/passowrd/reset",
        body: data, headers: headers);

    if (response.statusCode == 202) {
      print(
          "TUTOR PASSWORD CHANGE RESPONSE DATA: ${json.decode(response.body)}");
    } else {
      throw Exception("FAILED TO CHANGE PASSWORD PASSWORD");
    }
  }

  //////////////////////////// TUTOR CHANGE PASSWORD ////////////////////////////////
  ///
  Future<void> tutorPassChange({
    @required String currentPassword,
    @required String newPassword,
  }) async {
    Map data = {
      "current_password": currentPassword,
      "new_password": newPassword
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.post("$URL/api/auth/tutor/user/passowrd/reset",
        body: data, headers: headers);

    if (response.statusCode == 202) {
      print(
          "TUTOR PASSWORD CHANGE RESPONSE DATA: ${json.decode(response.body)}");
    } else {
      throw Exception("FAILED TO CHANGE PASSWORD");
    }
  }

  //////////////////////////// STUDENT RESET PASSWORD ////////////////////////////////
  ///
  Future<void> studentPassReset({
    @required String password,
    @required String token,
  }) async {
    Map data = {"password": password, "token": token};

    var response = await http.post(
      "$URL/api/auth/student/user/password_reset",
      body: data,
    );

    print(response.body);

    if (response.statusCode == 202) {
      print(
          "TUTOR PASSWORD RESET RESPONSE DATA: ${json.decode(response.body)}");
    } else {
      throw Exception("FAILED TO RESET PASSWORD");
    }
  }

  //////////////////////////// TUTOR RESET PASSWORD ////////////////////////////////
  ///
  Future<void> tutorPassReset({
    @required String password,
    @required String token,
  }) async {
    Map data = {"password": password, "token": token};

    var response = await http.post(
      "$URL/api/auth/tutor/user/password_reset",
      body: data,
    );

    print(response.body);

    if (response.statusCode == 202) {
      print(
          "TUTOR PASSWORD RESET RESPONSE DATA: ${json.decode(response.body)}");
    } else {
      throw Exception("FAILED TO RESET PASSWORD");
    }
  }

  //======================================== TUTOR ONLINE/OFFLINE MODE ====================================//
  //
  Future tutorHourlyRateUpdate({@required String hourlyRate}) async {
    Map data = {
      "hourly_rate": hourlyRate,
    };
    String accessToken = await SharedPreferenceHelper().getUserAccessToken();
    print(accessToken);

    var headers = {
      'Authorization': 'Bearer $accessToken',
    };

    var response = await http.put('$URL/api/auth/tutor/user',
        headers: headers, body: data);

    print(json.decode(response.body));

    // if (response.statusCode == 202) {
    //   print('It worked');
    //   var jsonData = json.decode(response.body);
    //   print(jsonData);
    // } else {
    //   print('Something went wrong');
    //   print(response.body);
    // }
  }
}
