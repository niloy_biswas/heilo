import 'package:flutter/material.dart';

class ProfileButton extends StatelessWidget {
  const ProfileButton({
    Key key,
    @required this.buttonName,
    @required this.sWidth,
    @required this.sHeight,
    @required this.bFontSize,
    @required this.function,
  }) : super(key: key);

  final String buttonName;
  final double sWidth;
  final double sHeight;
  final double bFontSize;
  final Function function;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: sHeight,
      width: sWidth,
      child: MaterialButton(
        // color: AssetStrings.color3,
        child: FittedBox(
          child: Text(
            buttonName,
            style: TextStyle(
              fontFamily: 'QuickSandMedium',
              color: Colors.black,
              fontSize: bFontSize,
              shadows: <Shadow>[
                Shadow(
                  offset: Offset(0.0, 2.0),
                  blurRadius: 5.0,
                  color: Colors.blueGrey.withOpacity(0.5),
                ),
              ],
            ),
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
          side: BorderSide(
            color: Colors.white,
            width: 3,
          ),
        ),
        elevation: 8,
        onPressed: function,
      ),
      // decoration: BoxDecoration(
      //   // gradient: LinearGradient(
      //   //   colors: [Color(0xff7EDD27), Color(0xff26D892)],
      //   //   begin: Alignment.centerLeft,
      //   //   end: Alignment.centerRight,
      //   // ),
      //   borderRadius: BorderRadius.circular(50.0),
      // ),
      decoration: BoxDecoration(
        // color: Colors.yellow,
        color: Color(0xFFBFFF90),
        borderRadius: BorderRadius.circular(50),
      ),
    );
  }
}
