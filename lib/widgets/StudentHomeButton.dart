import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';

class StudentHomeButton extends StatelessWidget {
  const StudentHomeButton({
    Key key,
    @required this.buttonName,
    @required this.sHeight,
    @required this.sWidth,
    @required this.function,
  }) : super(key: key);

  final String buttonName;
  final double sHeight;
  final double sWidth;
  final Function function;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        height: sHeight * 0.08,
        width: sWidth * 0.26,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: AssetStrings.color4.withOpacity(0.5),
              blurRadius: 5,
              offset: Offset(0, 7),
            ),
          ],
          color: AssetStrings.color4,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Center(
          child: Text(
            buttonName,
            style: TextStyle(
              fontFamily: 'QuickSandMedium',
              fontSize: 16,
            ),
          ),
        ),
      ),
      onTap: function,
    );
  }
}
