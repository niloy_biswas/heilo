import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/services/services.dart';
// import 'package:Heilo/widgets/custom_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

Future aboutUpdateDialog({
  @required BuildContext context,
  @required TextEditingController inputController,
}) async {
  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.3),
    builder: (context) {
      double sWidth = MediaQuery.of(context).size.width;
      // double sHeight = MediaQuery.of(context).size.height;
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(milliseconds: 500),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            width: sWidth * 0.9,
            decoration: BoxDecoration(
              color: AssetStrings.color1,
              borderRadius: BorderRadius.circular(20),
            ),
            child: ListView(
              shrinkWrap: true,
              children: [
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text('About You',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 6,
                    top: 10,
                    bottom: 10,
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 5,
                          color: Colors.grey.withOpacity(0.3),
                          offset: Offset(0, 0),
                          // spreadRadius: 5,
                        ),
                      ]),
                  child: TextField(
                    controller: inputController,
                    textAlign: TextAlign.left,
                    textAlignVertical: TextAlignVertical.top,
                    maxLines: 10,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 70,
                  ),
                  child: MaterialButton(
                    height: 40,
                    color: AssetStrings.color4,
                    shape: StadiumBorder(),
                    child: Text('Update',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        )),
                    onPressed: () async {
                      //
                      try {
                        print("ABOUT: ${inputController.text}");
                        Services serv = Services();
                        var userType =
                            await SharedPreferenceHelper().getUserType();
                        if (userType == 'student') {
                          await serv.addStudentAbout(
                              about: inputController.text);
                        } else {
                          await serv.addTutorAbout(about: inputController.text);
                        }
                        Navigator.pop(context);
                        EasyLoading.showSuccess("Updated");
                      } catch (e) {
                        print(e);
                        EasyLoading.showError("Failed");
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
