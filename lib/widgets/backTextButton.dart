import 'package:flutter/material.dart';

class BackTextButton extends StatelessWidget {
  const BackTextButton({
    Key key,
    @required this.color,
  }) : super(key: key);

  final Color color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Text(
          '<',
          style: TextStyle(
            fontFamily: 'QuickSandMedium',
            fontWeight: FontWeight.bold,
            fontSize: 40,
            color: color,
          ),
        ),
      ),
    );
  }
}
