import 'package:Heilo/pages/common/chat_screen.dart';
import 'package:Heilo/services/firestore_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_time_format/date_time_format.dart';
// import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class ChatRoomListTile extends StatefulWidget {
  // final String profilePicUrl;
  // final String name;
  // final String profilePic;
  final String myUserName;
  final String myContact;
  final String lastMessage;
  final String chatRoomId;
  final Timestamp lastMessageSendTs;

  const ChatRoomListTile({
    // this.profilePicUrl,
    // this.name,
    // @required this.profilePic,
    @required this.myUserName,
    @required this.lastMessage,
    @required this.chatRoomId,
    @required this.lastMessageSendTs,
    @required this.myContact,
  });
  @override
  _ChatRoomListTileState createState() => _ChatRoomListTileState();
}

class _ChatRoomListTileState extends State<ChatRoomListTile> {
  String name = '';
  String profilePicUrl = '';
  // String lastMessage = '';
  String username = '';
  String contact = '';
  String profilePic = '';

  getThisUserInfo() async {
    try {
      contact = widget.chatRoomId
          .replaceAll(widget.myContact, "")
          .replaceAll("_", "");
      QuerySnapshot querySnapshot =
          await DatabaseMethods().getUserInfo(contact);
      name = await querySnapshot.docs[0]["userName"];
      profilePic = await querySnapshot.docs[0]["profilePic"];
      // username = await querySnapshot.docs[0]["userName"];
      // print('something ${querySnapshot.docs[0].id}');
      // profilePicUrl = querySnapshot.docs[0]["profileUrl"];
      setState(() {});
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    getThisUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatScreen(
              chatWithContact: contact,
              chatWithName: name,
            ),
          ),
        );
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          (profilePic == null || profilePic == '')
              ? Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: CircleAvatar(
                    maxRadius: 25,
                    backgroundImage: AssetImage(
                      "assets/images/home.png",
                    ),
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: CircleAvatar(
                    maxRadius: 25,
                    backgroundImage: NetworkImage(profilePic),
                  ),
                ),
          SizedBox(
            width: 12,
          ),
          Flexible(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  child: Text(
                    widget.lastMessage,
                    style: TextStyle(fontSize: 14),
                    maxLines: 1,
                    // textDirection: TextDirection.ltr,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  // formatDate(
                  //     DateTime.parse(
                  //         widget.lastMessageSendTs.toDate().toString()),
                  //     [dd, '/', mm, '/', yy, ' - ', hh, ':', mm, ' ', am]),

                  DateTimeFormat.format(widget.lastMessageSendTs.toDate(),
                      // format: DateTimeFormats.american),
                      // format: 'D, M j, h:i a'),
                      format: 'M j, h:i a'),

                  style: TextStyle(fontSize: 10, color: Colors.grey),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
