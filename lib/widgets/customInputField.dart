import 'package:flutter/material.dart';

class CustomInputField extends StatelessWidget {
  const CustomInputField({
    Key key,
    @required this.fieldName,
    @required this.inputPhoneController,
  }) : super(key: key);

  final TextEditingController inputPhoneController;
  final String fieldName;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 50,
        vertical: 0,
      ),
      child: Container(
        height: 40,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 5,
              offset: Offset(0, 7),
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(50),
        ),
        child: TextField(
          controller: inputPhoneController,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: fieldName,
            hintStyle: TextStyle(
              fontFamily: 'QuickSandMedium',
              color: Colors.black87,
            ),
            border: InputBorder.none,
            // contentPadding: EdgeInsets.all(10),
          ),
        ),
      ),
    );
  }
}
