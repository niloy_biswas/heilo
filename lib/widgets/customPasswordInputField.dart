import 'package:flutter/material.dart';

class CustomPasswordInputField extends StatefulWidget {
  const CustomPasswordInputField({
    Key key,
    @required this.fieldName,
    @required this.inputPhoneController,
    // @required this.obscure,
  }) : super(key: key);

  final TextEditingController inputPhoneController;
  final String fieldName;
  // final bool obscure;

  @override
  _CustomPasswordInputFieldState createState() =>
      _CustomPasswordInputFieldState();
}

class _CustomPasswordInputFieldState extends State<CustomPasswordInputField> {
  // boolean value of obscureText / hide password
  bool fisheye = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 50,
        vertical: 0,
      ),
      child: Container(
        height: 40,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 5,
              offset: Offset(0, 7),
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(50),
        ),
        child: TextField(
          controller: widget.inputPhoneController,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.center,

          obscureText: fisheye,
          // onSubmitted: null,
          decoration: InputDecoration(
            hintText: widget.fieldName,
            hintStyle: TextStyle(
              fontFamily: 'QuickSandMedium',
              color: Colors.black87,
            ),
            border: InputBorder.none,
            // contentPadding: EdgeInsets.fromLTRB(50, 0, 0, 0),
            isDense: false,
            prefixIcon: Opacity(
              opacity: 0,
              child: IconButton(
                icon: Icon(Icons.remove_red_eye),
                onPressed: null,
              ),
            ),
            suffixIcon: IconButton(
              icon: Icon(Icons.remove_red_eye),
              splashRadius: 1,
              onPressed: () {
                setState(() {
                  fisheye = !fisheye;
                });
              },
            ),
          ),
        ),
      ),
    );
  }
}
