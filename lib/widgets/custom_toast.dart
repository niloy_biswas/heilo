import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';

////////////////////////// CUSTOM TOAST /////////////////////////
Future customToast(String content) {
  return Fluttertoast.showToast(
    msg: content,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    backgroundColor: Colors.black.withOpacity(0.7),
    textColor: Colors.white,
    fontSize: 16.0,
  );
}
