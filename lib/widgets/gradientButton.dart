import 'package:flutter/material.dart';

class GradientButton extends StatelessWidget {
  const GradientButton({
    Key key,
    @required this.buttonName,
    @required this.sWidth,
    @required this.sHeight,
    @required this.function,
  }) : super(key: key);

  final String buttonName;
  final double sWidth;
  final double sHeight;
  final Function function;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: sHeight * 0.07,
      width: sWidth * 0.5,
      child: MaterialButton(
        // color: AssetStrings.color3,
        child: Text(
          buttonName,
          style: TextStyle(
            fontFamily: 'QuickSandMedium',
            color: Colors.white,
            fontSize: 20,
            shadows: <Shadow>[
              Shadow(
                offset: Offset(0.0, 2.0),
                blurRadius: 5.0,
                color: Colors.blueGrey.withOpacity(0.5),
              ),
            ],
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
          side: BorderSide(
            color: Colors.white,
            width: 3,
          ),
        ),
        elevation: 8,
        onPressed: function,
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xff7EDD27), Color(0xff26D892)],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
        borderRadius: BorderRadius.circular(50.0),
      ),
    );
  }
}
