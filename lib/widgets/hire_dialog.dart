// import 'dart:ui';

// import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/models/subject_model.dart';
// import 'package:Heilo/widgets/custom_toast.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';

// Future hireDialog({
//   @required BuildContext context,
//   @required TextEditingController topicController,
//   @required TextEditingController fromController,
//   @required TextEditingController toController,
//   @required TextEditingController dateController,
//   @required List<SubjectModel> subjectList,
// }) async {
//   // TextEditingController topicController = TextEditingController();
//   // TextEditingController fromController = TextEditingController();
//   // TextEditingController toController = TextEditingController();
//   // TextEditingController dateController = TextEditingController();
//   String fromText = "From";
//   String toText = "To";
//   // String dateText = "Date";
//   // String TopicText = "Topic";
//   return showDialog(
//     context: context,
//     barrierColor: Colors.black.withOpacity(0.7),
//     builder: (context) {
//       double sWidth = MediaQuery.of(context).size.width;
//       // double sHeight = MediaQuery.of(context).size.height;
//       return BackdropFilter(
//         filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
//         child: Dialog(
//           elevation: 10,
//           backgroundColor: Colors.black.withOpacity(0),
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(25),
//           ),
//           insetAnimationCurve: Curves.easeInOutCirc,
//           insetAnimationDuration: Duration(milliseconds: 500),
//           child: Container(
//             padding: EdgeInsets.symmetric(vertical: 5),
//             width: sWidth * 0.9,
//             decoration: BoxDecoration(
//               color: AssetStrings.color1,
//               borderRadius: BorderRadius.circular(20),
//             ),
//             child: Container(
//               width: MediaQuery.of(context).size.width,
//               padding: EdgeInsets.all(20),
//               child: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   //======================= Text Field for topic =========================//
//                   Container(
//                     height: 40,
//                     decoration: BoxDecoration(
//                       color: Colors.white,
//                       borderRadius: BorderRadius.circular(50),
//                       boxShadow: [
//                         BoxShadow(
//                             color: Colors.grey.withOpacity(0.5),
//                             blurRadius: 5,
//                             offset: Offset(0, 2),
//                             spreadRadius: 1),
//                       ],
//                     ),
//                     child: TextField(
//                       controller: topicController,
//                       textAlign: TextAlign.center,
//                       decoration: InputDecoration(
//                         hintText: 'Topic',
//                         hintStyle: TextStyle(
//                           fontSize: 16,
//                           color: Colors.black87,
//                         ),
//                         border: InputBorder.none,
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     height: 30,
//                   ),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       GestureDetector(
//                         child: Container(
//                           width: 100,
//                           height: 40,
//                           decoration: BoxDecoration(
//                             color: Colors.white,
//                             borderRadius: BorderRadius.circular(50),
//                             boxShadow: [
//                               BoxShadow(
//                                   color: Colors.grey.withOpacity(0.5),
//                                   blurRadius: 5,
//                                   offset: Offset(0, 2),
//                                   spreadRadius: 1),
//                             ],
//                           ),
//                           //=============== FROM CONTROLLER ============///
//                           child: Center(
//                             child: Text(
//                               fromText,
//                               style: TextStyle(
//                                 fontSize: 16,
//                                 color: Colors.black87,
//                               ),
//                             ),
//                           ),
//                         ),
//                         onTap: () async {
//                           //========================= TIME PICKER ===========================//
//                           // var localizations = MaterialLocalizations.of(context);
//                           //
//                           // await showTimePicker(
//                           //   context: context,
//                           //   initialTime: TimeOfDay.now(),
//                           //   initialEntryMode: TimePickerEntryMode.input,
//                           // ).then((value) {
//                           //   fromController.text =
//                           //       localizations.formatTimeOfDay(value);
//                           // });

//                           //========================= CUSTOMIZED TIME PICKER ===================//

//                           try {
//                             await showTimePicker(
//                               context: context,
//                               initialTime: TimeOfDay.now(),
//                               initialEntryMode: TimePickerEntryMode.input,
//                               builder: (BuildContext context, Widget child) {
//                                 return Theme(
//                                   data: ThemeData.light().copyWith(
//                                     primaryColor: Colors.white,
//                                     accentColor: Colors.white,
//                                     colorScheme: ColorScheme.light(
//                                       primary: AssetStrings.color4,
//                                       onPrimary: Colors.white,
//                                       surface: AssetStrings.color1,
//                                       onSurface: Colors.black,
//                                     ),
//                                     buttonTheme: ButtonThemeData(
//                                       textTheme: ButtonTextTheme.primary,
//                                     ),
//                                   ),
//                                   child: child,
//                                 );
//                               },
//                             ).then((value) {
//                               fromController.text = value.toString();
//                               fromText = fromController.text.toString();
//                             });
//                           } catch (e) {
//                             customToast("Error!");
//                           }
//                         },
//                       ),

//                       //================================ Text Field for "To" ===============================//
//                       GestureDetector(
//                         child: Container(
//                           width: 100,
//                           height: 40,
//                           decoration: BoxDecoration(
//                             color: Colors.white,
//                             borderRadius: BorderRadius.circular(50),
//                             boxShadow: [
//                               BoxShadow(
//                                   color: Colors.grey.withOpacity(0.5),
//                                   blurRadius: 5,
//                                   offset: Offset(0, 2),
//                                   spreadRadius: 1),
//                             ],
//                           ),
//                           child: Center(
//                             child: Text(
//                               toText,
//                               style: TextStyle(
//                                 fontSize: 16,
//                                 color: Colors.black87,
//                               ),
//                             ),
//                           ),
//                         ),
//                         onTap: () async {
//                           //========================= TIME PICKER ===========================//
//                           var localizations = MaterialLocalizations.of(context);
//                           //
//                           await showTimePicker(
//                             context: context,
//                             initialTime: TimeOfDay.now(),
//                             initialEntryMode: TimePickerEntryMode.input,
//                           ).then((value) {
//                             toController.text =
//                                 localizations.formatTimeOfDay(value);
//                           });
//                         },
//                       ),
//                     ],
//                   ),
//                   SizedBox(
//                     height: 30,
//                   ),
//                   // Text Field for Date and Time
//                   GestureDetector(
//                     child: Container(
//                       // width: 100,
//                       width: double.infinity,
//                       height: 40,
//                       decoration: BoxDecoration(
//                         color: Colors.white,
//                         borderRadius: BorderRadius.circular(50),
//                         boxShadow: [
//                           BoxShadow(
//                               color: Colors.grey.withOpacity(0.5),
//                               blurRadius: 5,
//                               offset: Offset(0, 2),
//                               spreadRadius: 1),
//                         ],
//                       ),
//                       child: Center(
//                         child: Text(
//                           'Date',
//                           style: TextStyle(
//                             fontSize: 16,
//                             color: Colors.black87,
//                           ),
//                         ),
//                       ),
//                     ),
//                     onTap: () async {
//                       //========================= DATE PICKER ===========================//
//                       var myDateFormat = DateFormat('dd-MM-yyyy');
//                       //

//                       // await showDatePicker(
//                       //   context: context,
//                       //   initialDate: DateTime.now(),
//                       //   firstDate: DateTime(DateTime.now().year - 50),
//                       //   lastDate: DateTime(DateTime.now().year + 20),
//                       // ).then((value) {
//                       //   dateController.text = '${myDateFormat.format(value)}';
//                       // });

//                       await showDatePicker(
//                         context: context,
//                         initialDate: DateTime.now(),
//                         firstDate: DateTime(DateTime.now().year - 50),
//                         lastDate: DateTime(DateTime.now().year + 20),
//                         builder: (BuildContext context, Widget child) {
//                           return Theme(
//                             data: ThemeData().copyWith(
//                               colorScheme: ColorScheme.light(
//                                 primary: AssetStrings.color4,
//                                 onPrimary: Colors.white,
//                                 surface: AssetStrings.color1,
//                                 onSurface: Colors.black,
//                               ),
//                               dialogBackgroundColor: AssetStrings.color1,
//                             ),
//                             child: child,
//                           );
//                         },
//                       ).then((value) {
//                         dateController.text = '${myDateFormat.format(value)}';
//                       });
//                     },
//                   ),
//                   SizedBox(
//                     height: 30,
//                   ),

//                   //======================= SUBMIT BUTTON ======================//
//                   MaterialButton(
//                     child: Text(
//                       "Submit",
//                       style: TextStyle(
//                         fontSize: 20,
//                         color: Colors.white,
//                       ),
//                     ),
//                     height: 40,
//                     minWidth: 150,
//                     shape: StadiumBorder(),
//                     color: AssetStrings.color4,
//                     onPressed: () async {
//                       print(topicController.text);
//                       print(fromController.text);
//                       print(toController.text);
//                       print(dateController.text);
//                       // // var myTimeFormat = TimeFormats.standard('hh-mm');
//                       // FocusScope.of(context).requestFocus(FocusNode());
//                       // //========================= DATE PICKER ===========================//
//                       // var myDateFormat = DateFormat('dd-MM-yyyy');
//                       // //

//                       // await showDatePicker(
//                       //   context: context,
//                       //   initialDate: DateTime.now(),
//                       //   firstDate: DateTime(DateTime.now().year - 50),
//                       //   lastDate: DateTime(DateTime.now().year + 20),
//                       // ).then((value) {
//                       //   dateController.text = '${myDateFormat.format(value)}';
//                       // });

//                       // //========================= TIME PICKER ===========================//
//                       // var localizations = MaterialLocalizations.of(context);
//                       // //
//                       // await showTimePicker(
//                       //   context: context,
//                       //   initialTime: TimeOfDay.now(),
//                       //   initialEntryMode: TimePickerEntryMode.input,
//                       // ).then((value) {
//                       //   fromController.text =
//                       //       localizations.formatTimeOfDay(value);
//                       // });

//                       print(topicController.text);
//                       print(fromController.text);
//                       print(toController.text);
//                       print(dateController.text);

//                       // DATE TIME TEST
//                     },
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       );
//     },
//   );
// }

// // Future<void> selectDate(BuildContext context) async {
// //     final DateTime picked = await showDatePicker(
// //         context: context,
// //         initialDate: selectedDate,
// //         firstDate: DateTime(2015, 8),
// //         lastDate: DateTime(2101));
// //     if (picked != null && picked != selectedDate)
// //       setState(() {
// //         selectedDate = picked;
// //       });
// //   }
