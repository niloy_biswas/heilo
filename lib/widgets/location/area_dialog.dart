import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/location/road_dialog.dart';
// import 'package:Heilo/services/services.dart';
import 'package:flutter/material.dart';

// LocationModel locModel = LocationModel();
// Services serv = Services();

Future areaDialog({
  @required BuildContext context,
  @required List areaList,
}) async {
  ScrollController _controller = ScrollController();

  return showDialog(
    // barrierDismissible: false,
    barrierColor: Colors.black.withOpacity(0.3),
    context: context,
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Dialog(
            elevation: 10,
            backgroundColor: Colors.black.withOpacity(0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0)),
            insetAnimationCurve: Curves.easeInOutCirc,
            insetAnimationDuration: Duration(seconds: 3),
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                // height: MediaQuery.of(context).size.height * 0.45,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                  color: AssetStrings.color1,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Scrollbar(
                  isAlwaysShown: false,
                  controller: _controller,
                  radius: Radius.circular(50),
                  thickness: 10,
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                      color: Colors.black.withOpacity(0.3),
                    ),
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: areaList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 16,
                            horizontal: 20,
                          ),
                          child: Text(
                            areaList[index],
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () async {
                          print("Area: ${areaList[index]}");
                          // print("Location Id: ${locModel.data[index].locId}");
                          // await SharedPreferenceHelper().saveLocationId(
                          // locModel.data[index].locId.toString());
                          Services serv = Services();
                          List roadList = [];
                          List locIdList = [];
                          await serv.getDioLocations(refresh: false).then(
                            (value) {
                              for (int i = 0; i < value.data.length; i++) {
                                if (value.data[i].area == areaList[index]) {
                                  roadList.add(value.data[i].road);
                                  locIdList.add(value.data[i].locId);
                                }
                              }
                              // print(area.toSet().toList());
                            },
                          );

                          setState(() {
                            SharedPreferenceHelper()
                                .saveRegArea(areaList[index]);
                          });

                          // Navigator.pop(context, areaList[index]);
                          await roadDialog(
                            context: context,
                            roadList: roadList.toSet().toList(),
                            locIdList: locIdList.toSet().toList(),
                          );
                          Navigator.pop(context, areaList[index]);

                          // print(roadList.toSet().toList());
                        },
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        );
      });
    },
  );
}
