import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:Heilo/pages/teacher/teacher_profile.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
// import 'package:Heilo/services/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:transition/transition.dart';

// LocationModel locModel = LocationModel();
// Services serv = Services();

Future roadDialog({
  @required BuildContext context,
  @required List roadList,
  @required List locIdList,
}) async {
  ScrollController _controller = ScrollController();

  return showDialog(
    // barrierDismissible: false,
    barrierColor: Colors.black.withOpacity(0.3),
    context: context,
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Dialog(
            elevation: 10,
            backgroundColor: Colors.black.withOpacity(0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0)),
            insetAnimationCurve: Curves.easeInOutCirc,
            insetAnimationDuration: Duration(seconds: 3),
            child: SingleChildScrollView(
              child: Container(
                // padding: EdgeInsets.symmetric(vertical: 5),
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),

                // height: MediaQuery.of(context).size.height * 0.45,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                  color: AssetStrings.color1,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Scrollbar(
                  isAlwaysShown: false,
                  controller: _controller,
                  radius: Radius.circular(50),
                  thickness: 10,
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                      color: Colors.black.withOpacity(0.3),
                    ),
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: roadList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 16,
                            horizontal: 20,
                          ),
                          child: Text(
                            roadList[index],
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () async {
                          print("Road: ${roadList[index]}");
                          print("loc ID: ${locIdList[index]}");
                          // print("Location Id: ${locModel.data[index].locId}");
                          await SharedPreferenceHelper()
                              .saveRegRoad(roadList[index]);
                          await SharedPreferenceHelper()
                              .saveLocationId(locIdList[index].toString());
                          await SharedPreferenceHelper()
                              .saveChipLocation(roadList[index]);
                          var trigger = await SharedPreferenceHelper()
                              .getLocationTrigger();

                          setState(() {
                            SharedPreferenceHelper()
                                .saveRegRoad(roadList[index]);
                          });

                          if (trigger == "HONDATRIGGER") {
                            // DO SOMETHING
                            try {
                              EasyLoading.show(status: "Processing...");
                              Services serv = Services();
                              // var district =
                              //     await SharedPreferenceHelper().getRegDistrict();
                              // var area =
                              //     await SharedPreferenceHelper().getRegArea();
                              // var road =
                              //     await SharedPreferenceHelper().getRegRoad();
                              await serv.addTutorLocation(
                                locId: locIdList[index].toString(),
                              );
                              Navigator.pop(context);
                              customToast("Location Updated");
                              await Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => TutorProfile()));
                            } catch (e) {
                              EasyLoading.showError("Failed");
                            }
                          }

                          var userType =
                              await SharedPreferenceHelper().getUserType();
                          // Navigator.pop(context, roadList[index]);
                          if (userType == 'student') {
                            // Navigator.pushReplacement(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => StudentBottomNav()));
                            // Navigator.pushAndRemoveUntil(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => StudentBottomNav()),
                            //     (route) => false);
                            Navigator.pushAndRemoveUntil(
                                context,
                                Transition(
                                        child: StudentBottomNav(),
                                        transitionEffect: TransitionEffect.fade)
                                    .builder(),
                                (route) => false);
                          } else {
                            Navigator.pop(context, roadList[index]);
                          }
                        },
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        );
      });
    },
  );
}
