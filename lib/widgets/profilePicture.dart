import 'package:flutter/material.dart';

class ProfilePictureWidget extends StatelessWidget {
  const ProfilePictureWidget({
    @required this.assetString,
    Key key,
  }) : super(key: key);

  final String assetString;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 2,
              offset: Offset(0, 3)),
        ],
      ),
      child: Container(
        padding: EdgeInsets.all(2),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              color: Colors.white,
              spreadRadius: 1,
              // blurRadius: 2,
            ),
          ],
        ),
        child: CircleAvatar(
          radius: 35,
          backgroundImage: AssetImage(assetString),
          // backgroundImage: AssetImage(chat.sender.imageUrl),
        ),
      ),
    );
  }
}
