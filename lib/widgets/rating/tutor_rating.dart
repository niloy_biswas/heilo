import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

Future tutorRatingDialog({
  @required BuildContext context,
  String tutorId,
  String classId,
  String prefer,
  bool checkBoxValue = false,
}) async {
  double ratingValue;
  TextEditingController rateController = TextEditingController();

  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.7),
    builder: (context) {
      // double sHeight = MediaQuery.of(context).size.height;
      double sWidth = MediaQuery.of(context).size.width;
      return StatefulBuilder(builder: (context, setState) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Dialog(
            elevation: 10,
            backgroundColor: Colors.black.withOpacity(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                bottomRight: Radius.circular(25),
              ),
            ),
            insetAnimationCurve: Curves.easeInOutCirc,
            insetAnimationDuration: Duration(milliseconds: 500),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              // height: sHeight * 0.45,
              width: sWidth * 0.9,
              decoration: BoxDecoration(
                color: AssetStrings.color1,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                ),
              ),
              //===============================================================================//
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 100,
                      child: Image.asset('assets/images/logo2.png'),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Thanks for using our service',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      // color: Colors.white,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Now it\'s time for your valuable feedback',
                              style: TextStyle(
                                fontSize: 10,
                                color: Colors.grey[700],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: RatingBar.builder(
                              initialRating: 1,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: false,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 4.0),
                              itemSize: 35,
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                                setState(() {
                                  ratingValue = rating;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: Row(
                        children: [
                          Text(
                            'Share your feedback',
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.grey[700],
                              fontSize: 17,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      // color: Colors.grey,
                      width: MediaQuery.of(context).size.width,
                      height: 150,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      // color: Colors.white,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: TextField(
                        controller: rateController,
                        keyboardType: TextInputType.multiline,
                        maxLines: 50,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.all(20),
                        ),
                        onSubmitted: (_) {
                          FocusScope.of(context).unfocus();
                        },
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          flex: 1,
                          child: Checkbox(
                            value: checkBoxValue,
                            checkColor: Colors.white,
                            activeColor: Theme.of(context).primaryColor,
                            onChanged: (bool value) {
                              print(value);
                              setState(() {
                                checkBoxValue = value;
                                if (checkBoxValue == true) {
                                  setState(() {
                                    prefer = "1";
                                  });
                                } else {
                                  setState(() {
                                    prefer = "0";
                                  });
                                }
                              });
                            },
                          ),
                        ),
                        Flexible(
                          flex: 6,
                          child: Text(
                            "Would you like to refer this tutor?",
                          ),
                        ),
                      ],
                    ),
                    // SizedBox(
                    //   height: 20,
                    // ),
                    MaterialButton(
                      color: AssetStrings.color4,
                      shape: StadiumBorder(),
                      minWidth: 150,
                      child: Text(
                        "Submit",
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                      onPressed: () async {
                        try {
                          print(ratingValue);
                          print(rateController.text);
                          print(prefer);
                          Services serv = Services();
                          await serv.ratingTutorSession(
                              tutorId: tutorId,
                              prefer: prefer,
                              rate: ratingValue.toString(),
                              feedback: rateController.text,
                              classId: classId);
                          //
                          customToast("Review Complete!");
                          Navigator.pop(context);
                        } catch (e) {
                          print(e);
                          throw Exception('Cannot finish the rating task');
                        }
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      });
    },
  );
}
