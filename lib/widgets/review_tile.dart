import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ReviewTile extends StatelessWidget {
  final String dp;
  final String title;
  final String subtitle;
  final String rating;

  const ReviewTile({
    Key key,
    this.dp,
    this.title,
    this.subtitle,
    this.rating,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: 60,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: AssetStrings.color4,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(100),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ///------------------------ Student PROFILE PICTURE -------------------------///
            Flexible(
              flex: 2,
              child: Container(
                padding: const EdgeInsets.all(3.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: CircleAvatar(
                    maxRadius: 25,
                    backgroundImage: (dp == null)
                        ? AssetImage("assets/images/home.png")
                        : NetworkImage(dp),
                  ),
                ),
              ),
            ),
            // SizedBox(
            //   width: 5,
            // ),
            Flexible(
              flex: 7,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ///-------------------------- STUDENT NAME -----------------------///
                      Text(
                        title ?? 'Student Name',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ///---------------------- RATING NUMBER -------------------///
                            Text(
                              rating ?? '4.8',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            FaIcon(
                              FontAwesomeIcons.solidStar,
                              color: AssetStrings.starColor,
                              size: 10,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    width: 180,

                    ///------------------------------ USER REVIEW -------------------------///
                    child: Text(
                      subtitle ?? "Not Available",
                      style: TextStyle(fontSize: 10),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
