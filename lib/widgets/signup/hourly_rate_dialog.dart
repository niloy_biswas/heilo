import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/services/services.dart';
import 'package:flutter/material.dart';

Future hourlyRateDialog({
  @required BuildContext context,
}) {
  //
  double rating = 200;
  String label = "200";
  //
  return showDialog(
    barrierDismissible: false,
    barrierColor: Colors.black.withOpacity(0.3),
    context: context,
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Dialog(
            elevation: 10,
            backgroundColor: Colors.black.withOpacity(0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0)),
            insetAnimationCurve: Curves.easeInOutCirc,
            insetAnimationDuration: Duration(milliseconds: 300),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              // height: MediaQuery.of(context).size.height * 0.35,
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                color: AssetStrings.color1,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  // Text(
                  //   'Set your hourly rate',
                  //   style: TextStyle(fontSize: 18),
                  // ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: SliderTheme(
                      data: SliderThemeData(
                        activeTrackColor: AssetStrings.color4,
                        inactiveTrackColor: Colors.grey,
                        overlayColor: AssetStrings.color4.withOpacity(0.2),
                        thumbColor: AssetStrings.color4,
                        valueIndicatorColor: AssetStrings.color4,
                        activeTickMarkColor: AssetStrings.color4,
                      ),
                      child: Slider(
                        max: 400,
                        min: 200,
                        divisions: 20,
                        value: rating,
                        onChanged: (value) {
                          setState(() {
                            rating = value;
                            label = rating.toInt().toString();
                          });
                        },
                        label: label,
                      ),
                    ),
                  ),
                  Text(
                    "Rate: ${rating.toInt().toString()} tk/hr",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  MaterialButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    minWidth: 150,
                    color: AssetStrings.color4,
                    textColor: Colors.white,
                    // elevation: 0,
                    focusElevation: 0,
                    highlightElevation: 0,
                    shape: StadiumBorder(),
                    onPressed: () {
                      Navigator.pop(context, label);
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        );
      });
    },
  );
}
