import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/services/services.dart';
import 'package:flutter/material.dart';

Future mediumDialog({
  @required BuildContext context,
  @required List mediumList,
}) {
  // serv.getLocations().then((value) => locModel = value);

  return showDialog(
    barrierDismissible: false,
    barrierColor: Colors.black.withOpacity(0.7),
    context: context,
    builder: (context) {
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(milliseconds: 300),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              // height: MediaQuery.of(context).size.height * 0.45,
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                color: AssetStrings.color1,
                borderRadius: BorderRadius.circular(20),
              ),
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Colors.black.withOpacity(0.3),
                ),
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: mediumList.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 16,
                        horizontal: 20,
                      ),
                      child: Text(
                        mediumList[index],
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    onTap: () async {
                      Navigator.pop(context, mediumList[index]);
                    },
                  );
                },
              ),
            ),
          ),
        ),
      );
    },
  );
}
