import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/services/services.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

Future studentEducationAddDialog({
  @required BuildContext context,
  @required StudentUserInfoModel studentUserInfoModel,
}) async {
  TextEditingController schoolController = TextEditingController();
  // TextEditingController gradeController = TextEditingController();
  // TextEditingController backgroundController = TextEditingController();
  // TextEditingController mediumController = TextEditingController();
  String classNo;
  String background;
  String medium;

  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.3),
    builder: (context) {
      double sWidth = MediaQuery.of(context).size.width;
      // double sHeight = MediaQuery.of(context).size.height;
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(milliseconds: 500),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            width: sWidth * 0.9,
            decoration: BoxDecoration(
              color: AssetStrings.color1,
              borderRadius: BorderRadius.circular(20),
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    // margin: EdgeInsets.only(top: 20),
                    child: Text(
                      "Education Information",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),

                  ///* SCHOOL/COLLEGE INPUT ///
                  //
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: Container(
                  //     margin: EdgeInsets.only(left: 20, bottom: 5),
                  //     child: Text('School/College Name'),
                  //   ),
                  // ),
                  UpdateInputField(
                    controller: schoolController,
                    initialValue: "School/College Name",
                  ),

                  ///* GRADE INPUT ///
                  //
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: Container(
                  //     margin: EdgeInsets.only(left: 20, bottom: 5),
                  //     child: Text('Grade'),
                  //   ),
                  // ),
                  // UpdateInputField(
                  //   controller: gradeController,
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 30.0,
                      right: 30,
                      bottom: 20,
                    ),
                    child: Container(
                      // padding: EdgeInsets.all(20),
                      height: 40,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 3,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: DropDown(
                          items: [
                            "1",
                            "2",
                            "3",
                            "4",
                            "5",
                            "6",
                            "7",
                            "8",
                            "9",
                            "10",
                            "11",
                            "12"
                          ],
                          hint: Text("Select Class"),
                          showUnderline: false,
                          onChanged: (value) {
                            classNo = value;
                          },
                        ),
                      ),
                    ),
                  ),

                  ///* BACKGROUND INPUT ///
                  ///
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: Container(
                  //     margin: EdgeInsets.only(left: 20, bottom: 5),
                  //     child: Text('Background'),
                  //   ),
                  // ),
                  // UpdateInputField(
                  //   controller: backgroundController,
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 30.0,
                      right: 30,
                      bottom: 20,
                    ),
                    child: Container(
                      // padding: EdgeInsets.all(20),
                      height: 40,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 3,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: DropDown(
                          items: [
                            "Science",
                            "Commerce",
                            "Arts",
                          ],
                          hint: Text("Select Background"),
                          showUnderline: false,
                          onChanged: (value) {
                            background = value;
                          },
                        ),
                      ),
                    ),
                  ),

                  ///* MEDIUM INPUT ///
                  ///
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: Container(
                  //     margin: EdgeInsets.only(left: 20, bottom: 5),
                  //     child: Text('Medium of Study'),
                  //   ),
                  // ),
                  // UpdateInputField(
                  //   controller: mediumController,
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 30.0,
                      right: 30,
                      bottom: 20,
                    ),
                    child: Container(
                      // padding: EdgeInsets.all(20),
                      height: 40,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 3,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: DropDown(
                          items: [
                            "Bangla",
                            "English",
                          ],
                          hint: Text("Select Medium"),
                          showUnderline: false,
                          onChanged: (value) {
                            medium = value;
                          },
                        ),
                      ),
                    ),
                  ),

                  ///* SUBMIT BUTTON ///
                  ///
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    child: MaterialButton(
                      child: Text(
                        'Add',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                      minWidth: 150,
                      shape: StadiumBorder(),
                      elevation: 3,
                      color: AssetStrings.color4,
                      onPressed: () async {
                        Services serv = Services();

                        Navigator.pop(context);

                        EasyLoading.show(status: "Please Wait...");

                        try {
                          if (schoolController.text != '' &&
                              classNo != null &&
                              background != null &&
                              medium != null) {
                            print(schoolController.text);
                            // print(gradeController.text);
                            // print(backgroundController.text);
                            // print(mediumController.text);
                            print(classNo);
                            print(background);
                            print(medium);
                            await serv.addStudentEducation(
                              institutionName: schoolController.text.toString(),
                              grade: classNo,
                              background: background,
                              medium: medium,
                              detailAddress: "_",
                              institutionType: "_",
                            );
                            EasyLoading.showSuccess("Updated");
                            EasyLoading.dismiss();
                          } else {
                            EasyLoading.showError("Empty Fields Not Allowed!");
                            Future.delayed(Duration(milliseconds: 1500));
                            EasyLoading.dismiss();
                          }
                        } catch (e) {
                          print(e);
                          EasyLoading.showError("Error Occured");
                          EasyLoading.dismiss();
                        }
                      },
                    ),
                  ),
                  // SizedBox(
                  //   height: 20,
                  // ),
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}

class UpdateInputField extends StatelessWidget {
  const UpdateInputField({
    Key key,
    @required this.controller,
    this.initialValue,
  }) : super(key: key);

  final TextEditingController controller;
  final String initialValue;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 30.0,
        right: 30,
        bottom: 20,
      ),
      child: Container(
        // padding: EdgeInsets.all(20),
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 3,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: TextField(
            controller: controller,
            // textAlign: TextAlign.center,
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: initialValue ?? '',
              // contentPadding: EdgeInsets.symmetric(horizontal: 10),
            ),
          ),
        ),
      ),
    );
  }
}
