import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/services/services.dart';
// import 'package:Heilo/widgets/student_education_update_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Future studentEducationListDialog({
  BuildContext context,
  StudentUserInfoModel studentUserInfoModel,
}) async {
  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.7),
    builder: (context) {
      // double sHeight = MediaQuery.of(context).size.height;
      double sWidth = MediaQuery.of(context).size.width;
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(seconds: 3),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            // height: sHeight * 0.45,
            width: sWidth * 0.9,
            decoration: BoxDecoration(
              color: AssetStrings.color1,
              borderRadius: BorderRadius.circular(20),
            ),
            child: ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black.withOpacity(0.3),
              ),
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              itemCount: studentUserInfoModel.data.educations.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 16,
                    horizontal: 20,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 4,
                        child: Text(
                          studentUserInfoModel
                              .data.educations[index].institutionName,
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            ///
                            ///* EDIT BUTTON ///
                            ///
                            // GestureDetector(
                            //   child: FaIcon(
                            //     FontAwesomeIcons.edit,
                            //     size: 18,
                            //   ),
                            //   onTap: () async {
                            //     Services serv = Services();
                            //     try {
                            //       // Navigator.pop(context);
                            //       // EasyLoading.show(status: "Please Wait...");

                            //       print(studentUserInfoModel
                            //           .data.educations[index].eduId);
                            //       await serv
                            //           .getDioStudentInfo(refresh: false)
                            //           .then((value) =>
                            //               studentEducationUpdateDialog(
                            //                 context: context,
                            //                 studentUserInfoModel: value,
                            //                 eduId: studentUserInfoModel
                            //                     .data.educations[index].eduId,
                            //                 classNo: studentUserInfoModel
                            //                     .data.educations[index].grade,
                            //                 background: studentUserInfoModel
                            //                     .data
                            //                     .educations[index]
                            //                     .background,
                            //                 medium: studentUserInfoModel
                            //                     .data.educations[index].medium,
                            //                 institutionName:
                            //                     studentUserInfoModel
                            //                         .data
                            //                         .educations[index]
                            //                         .institutionName,
                            //               ));
                            //       Navigator.pop(context);

                            //       // EasyLoading.dismiss();
                            //     } catch (e) {
                            //       print(e);
                            //     }
                            //   },
                            // ),
                            // SizedBox(
                            //   width: 10,
                            // ),

                            ///
                            ///* DELETE BUTTON ///
                            ///
                            GestureDetector(
                              child: FaIcon(
                                FontAwesomeIcons.trash,
                                size: 18,
                              ),
                              onTap: () async {
                                Services serv = Services();
                                Navigator.pop(context);
                                EasyLoading.show(status: "Please Wait...");
                                try {
                                  print(studentUserInfoModel
                                      .data.educations[index].eduId);
                                  await serv.deleteStudentEducation(
                                      eduId: studentUserInfoModel
                                          .data.educations[index].eduId);
                                  EasyLoading.showSuccess(
                                    'Delete Successful',
                                  );
                                  // await Future.delayed(Duration(seconds: 2));
                                  EasyLoading.dismiss();
                                } catch (e) {
                                  print(e);
                                  // Navigator.pop(context);

                                  EasyLoading.showError(
                                    'Delete Failed',
                                  );
                                  EasyLoading.dismiss();
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      );
    },
  );
}
