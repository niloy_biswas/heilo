import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/student_user_info_model.dart';
import 'package:Heilo/services/services.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

Future studentPersonalInfoUpdate({
  @required BuildContext context,
  @required StudentUserInfoModel studentUserInfoModel,
}) async {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController contactController = TextEditingController();
  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.7),
    builder: (context) {
      double sWidth = MediaQuery.of(context).size.width;
      // double sHeight = MediaQuery.of(context).size.height;
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(milliseconds: 500),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            width: sWidth * 0.9,
            decoration: BoxDecoration(
              color: AssetStrings.color1,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    "Personal Details",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),

                ///* NAME INPUT ///
                //
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 20, bottom: 5),
                    child: Text('Enter Name'),
                  ),
                ),
                UpdateInputField(
                  controller: nameController,
                  initialValue: studentUserInfoModel.data.name,
                  keyboardType: TextInputType.text,
                ),

                ///* EMAIL INPUT ///
                //
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 20, bottom: 5),
                    child: Text('Enter Email'),
                  ),
                ),
                UpdateInputField(
                  controller: emailController,
                  initialValue:
                      studentUserInfoModel.data.email ?? "No Email Found",
                  keyboardType: TextInputType.emailAddress,
                ),

                ///* CONTACT INPUT ///
                ///
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 20, bottom: 5),
                    child: Text('Enter Alternate Contact'),
                  ),
                ),
                UpdateInputField(
                  controller: contactController,
                  initialValue:
                      studentUserInfoModel.data.altPhone ?? "No Contacts Found",
                  keyboardType: TextInputType.phone,
                ),

                ///* SUBMIT BUTTON ///
                ///
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: RaisedButton(
                    child: Text(
                      'Update',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                    shape: StadiumBorder(),
                    elevation: 3,
                    color: AssetStrings.color4,
                    onPressed: () async {
                      Navigator.pop(context);

                      if (nameController.text != '' &&
                          emailController.text != '' &&
                          contactController.text != '') {
                        try {
                          print(nameController.text);
                          print(emailController.text);
                          print(contactController.text);
                          Services serv = Services();
                          if (EmailValidator.validate(emailController.text)) {
                            EasyLoading.show(status: "Please Wait...");

                            await serv.updateStudentPersonalDetails(
                              name: nameController.text,
                              email: emailController.text,
                              contactNo: "+88${contactController.text}",
                            );
                            EasyLoading.showSuccess("Updated");
                          } else {
                            EasyLoading.showError("Invalid Email");
                          }
                        } catch (e) {
                          print(e);
                          EasyLoading.showError("Error Occured");
                        }
                      } else {
                        EasyLoading.showError("Empty Fields Not Allowed!");
                        Future.delayed(Duration(milliseconds: 1500));
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

class UpdateInputField extends StatefulWidget {
  const UpdateInputField({
    Key key,
    @required this.controller,
    @required this.initialValue,
    @required this.keyboardType,
  }) : super(key: key);

  final TextEditingController controller;
  final String initialValue;
  final TextInputType keyboardType;

  @override
  _UpdateInputFieldState createState() => _UpdateInputFieldState();
}

class _UpdateInputFieldState extends State<UpdateInputField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20.0,
        right: 20,
        bottom: 20,
      ),
      child: Container(
        // padding: EdgeInsets.all(20),
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 3,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: TextField(
          controller: widget.controller,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.center,
          keyboardType: widget.keyboardType,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: widget.initialValue ?? '',
          ),
        ),
      ),
    );
  }
}
