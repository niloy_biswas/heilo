// import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/pages/student/timer_page.dart';
// import 'package:Heilo/widgets/tution_confirm_dialog_student.dart';
import 'package:Heilo/widgets/custom_toast.dart';
// import 'package:date_time_format/date_time_format.dart';
import 'package:flutter/material.dart';

class StudentTutionUpcomingTile extends StatelessWidget {
  const StudentTutionUpcomingTile({
    Key key,
    @required this.sWidth,
    @required this.upcomingDate,
    @required this.subject,
    @required this.address,
    @required this.tutionTime,
    @required this.secretCode,
    @required this.tutorId,
    @required this.classId,
    @required this.isPending,
    @required this.isAccepted,
    @required this.startFunc,
    @required this.cancelFunc,
    @required this.date,
  }) : super(key: key);

  final double sWidth;
  final String upcomingDate;
  final String subject;
  final String address;
  final String tutionTime;
  final String secretCode;
  final String tutorId;
  final String classId;
  final bool isPending;
  final bool isAccepted;
  final Function startFunc;
  final Function cancelFunc;
  final String date;

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.all(20),
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                height: 35,
                width: sWidth * 0.16,
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                  color: Color(0xFFE2DEDE),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    topLeft: Radius.circular(30),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 3,
                      offset: Offset(0, 3),
                      spreadRadius: 3,
                    ),
                  ],
                ),
                child: Center(
                  ///////////////////////////// TUTION DATE //////////////////////////////
                  ///
                  child: Text(
                    upcomingDate ?? 'N/A',
                    style: TextStyle(
                      color: Colors.blueGrey,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Container(
                width: sWidth * 0.72,
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 10,
                      offset: Offset(0, 6),
                      spreadRadius: 3,
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: sWidth * 0.16,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ///////////////////////////// TUTION SUBJECT //////////////////////////////
                          ///
                          Container(
                            width: sWidth * 0.16,
                            child: Text(
                              subject ?? 'N/A',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              textDirection: TextDirection.ltr,
                              textAlign: TextAlign.center,
                            ),
                          ),

                          ///////////////////////////// TUTION ADDRESS //////////////////////////////
                          ///
                          Container(
                            width: sWidth * 0.16,
                            child: Text(
                              address ?? 'N/A',
                              style: TextStyle(fontSize: 10),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 20,
                      child: Image.asset('assets/images/line1.png'),
                    ),

                    ///////////////////////////// TUTION TIME //////////////////////////////
                    ///
                    Container(
                      width: 40,
                      child: Text(
                        tutionTime ?? 'N/A',
                        style: TextStyle(
                          fontSize: 10,
                        ),
                        // maxLines: 1,
                        // overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      height: 20,
                      child: Image.asset('assets/images/line1.png'),
                    ),

                    if ((isAccepted || isPending) &&
                            (DateTime.parse(date)
                                    .difference(DateTime.now())
                                    .inHours >
                                4) ||
                        isPending)
                      GestureDetector(
                        child: Container(
                          width: 50,
                          height: 30,
                          padding: EdgeInsets.symmetric(
                            horizontal: 5,
                            vertical: 5,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: Text(
                              "Cancel",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ),
                        onTap: () => cancelFunc(),
                      ),

                    if ((isAccepted) &&
                        (DateTime.now().isBefore(DateTime.parse(date))))
                      GestureDetector(
                        onTap: () {
                          customToast("You can access this at $tutionTime");
                        },
                        child: Container(
                          width: 50,
                          height: 30,
                          padding: EdgeInsets.symmetric(
                            horizontal: 5,
                            vertical: 5,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: Text(
                              "Start",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ),
                      ),

                    if (isAccepted &&
                        DateTime.now().isAfter(DateTime.parse(date)))
                      GestureDetector(
                        child: Container(
                          width: 50,
                          height: 30,
                          padding: EdgeInsets.symmetric(
                            horizontal: 5,
                            vertical: 5,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: Text(
                              "Start",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ),
                        onTap: () => startFunc(),
                      ),

                    //============================= START BUTTON ==============================//
                    // ((isAccepted || isPending) &&
                    //         (DateTime.now()
                    //                 .difference(DateTime.parse(date))
                    //                 .inHours >
                    //             4))
                    //     ? GestureDetector(
                    //         child: Container(
                    //           width: 50,
                    //           height: 30,
                    //           padding: EdgeInsets.symmetric(
                    //             horizontal: 5,
                    //             vertical: 5,
                    //           ),
                    //           decoration: BoxDecoration(
                    //             color: Colors.red,
                    //             borderRadius: BorderRadius.circular(10),
                    //           ),
                    //           child: Center(
                    //             child: Text(
                    //               "Cancel",
                    //               style: TextStyle(
                    //                 color: Colors.white,
                    //                 fontSize: 10,
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //         onTap: () => cancelFunc(),
                    //       )
                    //     :

                    // discarded
                    // ((DateTimeFormat.format(DateTime.parse(date),
                    //             format: 'M j') !=
                    //         DateTimeFormat.format(DateTime.now(),
                    //             format: 'M j'))

                    // (DateTime.now().isBefore(DateTime.parse(date)
                    //         .subtract(Duration(hours: 4)))
                    //     ? GestureDetector(
                    //         child: Container(
                    //           width: 50,
                    //           height: 30,
                    //           padding: EdgeInsets.symmetric(
                    //             horizontal: 5,
                    //             vertical: 5,
                    //           ),
                    //           decoration: BoxDecoration(
                    //             color: Colors.grey,
                    //             borderRadius: BorderRadius.circular(10),
                    //           ),
                    //           child: Center(
                    //             child: Text(
                    //               // "Cancel",
                    //               "Start",
                    //               style: TextStyle(
                    //                 color: Colors.white,
                    //                 fontSize: 10,
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //         onTap: () {
                    //           // return cancelFunc();
                    //           customToast(
                    //               'You cannot cancel this class anymore!');
                    //         },
                    //       )
                    //     :
                    // (isAccepted &&
                    //         (DateTime.now().isBefore(DateTime.parse(date)))
                    //     ? GestureDetector(
                    //         onTap: () {
                    //           customToast(
                    //               "You can access this at $tutionTime");
                    //         },
                    //         child: Container(
                    //           width: 50,
                    //           height: 30,
                    //           padding: EdgeInsets.symmetric(
                    //             horizontal: 5,
                    //             vertical: 5,
                    //           ),
                    //           decoration: BoxDecoration(
                    //             color: Colors.grey,
                    //             borderRadius: BorderRadius.circular(10),
                    //           ),
                    //           child: Center(
                    //             child: Text(
                    //               "Start",
                    //               style: TextStyle(
                    //                 color: Colors.white,
                    //                 fontSize: 10,
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //       )
                    // : GestureDetector(
                    //     child: Container(
                    //       width: 50,
                    //       height: 30,
                    //       padding: EdgeInsets.symmetric(
                    //         horizontal: 5,
                    //         vertical: 5,
                    //       ),
                    //       decoration: BoxDecoration(
                    //         color: Colors.green,
                    //         borderRadius: BorderRadius.circular(10),
                    //       ),
                    //       child: Center(
                    //         child: Text(
                    //           "Start",
                    //           style: TextStyle(
                    //             color: Colors.white,
                    //             fontSize: 10,
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     onTap: () => startFunc(),
                    // {
                    // print("start button pressed");
                    // print("tutor id: $tutorId");
                    // print("secrect code: $secretCode");
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => TimerPage(
                    //               classId: classId,
                    //               code: secretCode,
                    //             )));
                    // },
                    // )),

                    //============================== CANCEL BUTTON ==========================//
                    // !isPending
                    //     ? Container()
                    //     :
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
