import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/subject_model.dart';
import 'package:Heilo/pages/student/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:transition/transition.dart';

Future subjectDialog(BuildContext context, SubjectModel subModel) {
  return showDialog(
    // barrierDismissible: false,
    barrierColor: Colors.black.withOpacity(0.3),
    context: context,
    builder: (context) {
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(seconds: 3),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              height: MediaQuery.of(context).size.height * 0.45,
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                color: AssetStrings.color1,
                borderRadius: BorderRadius.circular(20),
              ),
              child: MediaQuery.removePadding(
                context: context,
                removeBottom: true,
                child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                    color: Colors.black.withOpacity(0.3),
                  ),
                  shrinkWrap: false,
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: subModel.data.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 20,
                        ),
                        child: Text(
                          subModel.data[index].subject,
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ),
                      onTap: () async {
                        print("Subject Name: ${subModel.data[index].subject}");
                        print("Subject ID: ${subModel.data[index].subjectId}");
                        await SharedPreferenceHelper().saveSubjectId(
                            subModel.data[index].subjectId.toString());
                        await SharedPreferenceHelper()
                            .saveChipSubject(subModel.data[index].subject);
                        // Navigator.pop(context);
                        Navigator.pushAndRemoveUntil(
                            context,
                            Transition(
                                    child: StudentBottomNav(),
                                    transitionEffect: TransitionEffect.fade)
                                .builder(),
                            (route) => false);
                      },
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      );
    },
  );
}
