import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';

Future subjectListDialog({
  @required BuildContext context,
  @required List<String> subjectList,
  @required List<int> subjectIdList,
}) async {
  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.7),
    builder: (context) {
      // double sHeight = MediaQuery.of(context).size.height;
      double sWidth = MediaQuery.of(context).size.width;
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(seconds: 3),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            // height: sHeight * 0.45,
            width: sWidth * 0.9,
            decoration: BoxDecoration(
              color: AssetStrings.color1,
              borderRadius: BorderRadius.circular(20),
            ),
            child: ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black.withOpacity(0.3),
              ),
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              itemCount: subjectList.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Container(
                    margin: const EdgeInsets.symmetric(
                      vertical: 16,
                      horizontal: 20,
                    ),
                    child: Text(
                      subjectList[index],
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  onTap: () {
                    SharedPreferenceHelper()
                        .saveRegSkillSub(subjectList[index]);
                    print("SELECTED SUBJECT: ${subjectList[index]}");
                    SharedPreferenceHelper()
                        .saveRegSkillSubjectId(subjectIdList[index].toString());
                    print(
                        "SELECTED SUBJECT ID: ${subjectIdList[index].toString()}");
                    Navigator.pop(context);
                  },
                );
              },
            ),
          ),
        ),
      );
    },
  );
}
