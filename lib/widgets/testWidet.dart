import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TestWidget extends StatefulWidget {
  @override
  _TestWidgetState createState() => _TestWidgetState();
}

class _TestWidgetState extends State<TestWidget> {
  TextEditingController inputDateController = TextEditingController();
  TextEditingController inputToController = TextEditingController();
  TextEditingController inputFromController = TextEditingController();

  var myDateFormat = DateFormat('dd-MM-yyyy');
  var closingDateFormat = DateFormat('yyyy-MM-dd');
  var fromTime1;
  var toTime1;
  var date1;

  @override
  Widget build(BuildContext context) {
    var localizations = MaterialLocalizations.of(context);

    return Scaffold(
      backgroundColor: AssetStrings.color1,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // ============================== TIME PICKER (TO) ==========================//
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(0, 2),
                      spreadRadius: 1),
                ],
              ),
              child: TextField(
                controller: inputFromController,
                textAlign: TextAlign.center,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "FROM",
                ),
                onTap: () async {
                  FocusScope.of(context).requestFocus(FocusNode());
                  await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.now(),
                    initialEntryMode: TimePickerEntryMode.input,
                    builder: (BuildContext context, Widget child) {
                      return Theme(
                        data: ThemeData.light().copyWith(
                          primaryColor: Colors.white,
                          accentColor: Colors.white,
                          colorScheme: ColorScheme.light(
                            primary: AssetStrings.color4,
                            onPrimary: Colors.white,
                            surface: AssetStrings.color1,
                            onSurface: Colors.black,
                          ),
                          buttonTheme: ButtonThemeData(
                            textTheme: ButtonTextTheme.primary,
                          ),
                        ),
                        child: child,
                      );
                    },
                  ).then((value) {
                    setState(() {
                      fromTime1 = value;
                      inputFromController.text =
                          '${localizations.formatTimeOfDay(value)}';
                    });
                  });
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            // ============================== TIME PICKER (TO) ==========================//
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(0, 2),
                      spreadRadius: 1),
                ],
              ),
              child: TextField(
                controller: inputToController,
                textAlign: TextAlign.center,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "TO",
                ),
                onTap: () async {
                  FocusScope.of(context).requestFocus(FocusNode());
                  await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.now(),
                    initialEntryMode: TimePickerEntryMode.input,
                    builder: (BuildContext context, Widget child) {
                      return Theme(
                        data: ThemeData.light().copyWith(
                          primaryColor: Colors.white,
                          accentColor: Colors.white,
                          colorScheme: ColorScheme.light(
                            primary: AssetStrings.color4,
                            onPrimary: Colors.white,
                            surface: AssetStrings.color1,
                            onSurface: Colors.black,
                          ),
                          buttonTheme: ButtonThemeData(
                            textTheme: ButtonTextTheme.primary,
                          ),
                        ),
                        child: child,
                      );
                    },
                  ).then((value) {
                    setState(() {
                      toTime1 = value;
                      inputToController.text =
                          '${localizations.formatTimeOfDay(value)}';
                    });
                  });
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            //============================== DATE PICKER =========================//
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(0, 2),
                      spreadRadius: 1),
                ],
              ),
              child: TextField(
                controller: inputDateController,
                textAlign: TextAlign.center,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Date",
                ),
                onTap: () async {
                  FocusScope.of(context).requestFocus(FocusNode());
                  await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(DateTime.now().year - 50),
                    lastDate: DateTime(DateTime.now().year + 20),
                    builder: (BuildContext context, Widget child) {
                      return Theme(
                        data: ThemeData().copyWith(
                          colorScheme: ColorScheme.light(
                            primary: AssetStrings.color4,
                            onPrimary: Colors.white,
                            surface: AssetStrings.color1,
                            onSurface: Colors.black,
                          ),
                          dialogBackgroundColor: AssetStrings.color1,
                        ),
                        child: child,
                      );
                    },
                  ).then((value) {
                    setState(() {
                      date1 = value;
                      inputDateController.text =
                          '${myDateFormat.format(value)}';
                    });
                  });
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            MaterialButton(
              color: AssetStrings.color4,
              height: 40,
              minWidth: 100,
              shape: StadiumBorder(),
              textColor: Colors.white,
              child: Text("Submit"),
              onPressed: () async {
                var fromDate =
                    '${closingDateFormat.format(date1)} ${fromTime1.hour}:${fromTime1.minute}:00';

                var toDate =
                    '${closingDateFormat.format(date1)} ${toTime1.hour}:${toTime1.minute}:00';

                var locId = await SharedPreferenceHelper().getLocationId();
                var subId = await SharedPreferenceHelper().getSubjectId();
                var topId = await SharedPreferenceHelper().getTopicId();

                print('From Date: $fromDate');
                print("To Date: $toDate");
                print("Location Id: $locId");
                print("Subject Id: $subId");
                print("Topic Id: $topId");
              },
            ),
          ],
        ),
      ),
    );
  }
}
