import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/topic_model.dart';
import 'package:flutter/material.dart';

Future topicDialog(BuildContext context, TopicModel topModel) {
  return showDialog(
    // barrierDismissible: false,
    barrierColor: Colors.black.withOpacity(0.3),
    context: context,
    builder: (context) {
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(seconds: 3),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              height: MediaQuery.of(context).size.height * 0.45,
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                color: AssetStrings.color1,
                borderRadius: BorderRadius.circular(20),
              ),
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Colors.black.withOpacity(0.3),
                ),
                shrinkWrap: false,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: topModel.data.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 20),
                      child: Text(
                        topModel.data[index].name,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    onTap: () async {
                      print("Subject Name: ${topModel.data[index].name}");
                      print("Topic ID: ${topModel.data[index].topicId}");
                      await SharedPreferenceHelper()
                          .saveTopicId(topModel.data[index].topicId.toString());
                      await SharedPreferenceHelper()
                          .saveChipTopic(topModel.data[index].name);
                      Navigator.pop(context);
                    },
                  );
                },
              ),
            ),
          ),
        ),
      );
    },
  );
}
