import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/pages/common/chat_screen.dart';
import 'package:Heilo/pages/teacher/bottom_nav.dart';
import 'package:Heilo/services/firestore_database.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
// import 'package:Heilo/widgets/custom_toast.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class TutionRequestTile extends StatefulWidget {
  TutionRequestTile({
    @required this.dp,
    @required this.sHeight,
    @required this.sWidth,
    @required this.studentName,
    @required this.studentContact,
    @required this.timeDuration,
    @required this.address,
    @required this.subjects,
    @required this.classId,
    @required this.code,
  });

  final String dp;
  final double sWidth;
  final double sHeight;
  final String studentName;
  final String studentContact;
  final String timeDuration;
  final String address;
  final String subjects;
  final String classId;
  final String code;

  @override
  _TutionRequestTileState createState() => _TutionRequestTileState();
}

class _TutionRequestTileState extends State<TutionRequestTile> {
  Services serv = Services();

  //============================ CHAT FUNCTIONS ===============================//
  // Generate chatroom ID by using usernames
  getChatRoomIdByUserContacts(String a, String b) {
    if (int.parse(a) > int.parse(b)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: widget.sHeight * 0.09,
          width: widget.sWidth * 0.77,
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 3,
                offset: Offset(0, 3),
                spreadRadius: 2,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
                    child: CircleAvatar(
                      radius: 25,
                      backgroundImage: (widget.dp == null)
                          ? AssetImage('assets/images/home.png')
                          : NetworkImage(widget.dp),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ////////////////////// STUDENT NAME //////////////////////////
                      ///
                      Text(widget.studentName ?? 'Not Available'),
                      ////////////////////// TUTION TIME //////////////////////////
                      ///
                      Text(
                        widget.timeDuration ?? 'Duration',
                        style: TextStyle(fontSize: 12),
                      ),
                      ////////////////////// STUDENT ADDRESS //////////////////////////
                      ///
                      Text(
                        widget.subjects ?? 'Not Available',
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        child: Icon(
                          CommunityMaterialIcons.check_circle,
                          color: Colors.green,
                          size: 30,
                        ),
                        onTap: () async {
                          try {
                            // EasyLoading.show(status: "Processing...");

                            print(
                                "CODE: ${widget.code}, STATUS: ACCEPT, CLASSID: ${widget.classId}");
                            await serv.tutorClassAccept(
                              code: widget.code,
                              status: "accept",
                              classId: widget.classId,
                            );
                            // EasyLoading.showSuccess("Accepted");
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TeacherBottomNav()));

                            customToast("Tution Accepted");
                          } catch (e) {
                            print(e);
                            EasyLoading.showError("Failed");

                            customToast("Please Try Again Later!");
                          }
                        },
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        child: Icon(
                          CommunityMaterialIcons.close_circle,
                          color: Colors.red,
                          size: 30,
                        ),
                        onTap: () async {
                          try {
                            // EasyLoading.show(status: "Processing...");
                            print(
                                "CODE: ${widget.code}, STATUS: CANCEL, CLASSID: ${widget.classId}");
                            await serv.tutorClassAccept(
                              code: widget.code,
                              status: "cancel",
                              classId: widget.classId,
                            );
                            // EasyLoading.showSuccess("Cancelled");
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TeacherBottomNav()));
                            customToast("Tution Cancelled");
                          } catch (e) {
                            print(e);
                            EasyLoading.showError("Network Error");
                            customToast("Please try again later");
                          }
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  // ////////////////////// TUTION SUBJECTS //////////////////////////
                  // ///
                  // Text(
                  //   widget.subjects ?? 'Subjects',
                  //   style: TextStyle(fontSize: 10),
                  // ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
          alignment: Alignment.centerRight,
          padding: EdgeInsets.only(right: 10),
          child: GestureDetector(
            child: Image.asset(
              "assets/images/chat.png",
              height: 55,
              width: 55,
            ),
            onTap: () async {
              var myContact = await SharedPreferenceHelper().getUserContact();
              myContact = myContact.substring(3);
              print("MY CONTACT: " + myContact);

              var chatWithContact = widget.studentContact;
              chatWithContact = chatWithContact.substring(3);
              print("PARTNER CONTACT: " + chatWithContact);

              var myUserName = await SharedPreferenceHelper().getUserName();
              print("MY USERNAME: " + myUserName);

              var chatWithUserName = widget.studentName;
              print("PARTNER USER NAME: " + chatWithUserName);

              //============================ main game start ========================//
              var chatRoomId =
                  getChatRoomIdByUserContacts(myContact, chatWithContact);
              Map<String, dynamic> chatRoomInfoMap = {
                "users": [myUserName, chatWithUserName]
              };

              DatabaseMethods().createChatRoom(chatRoomId, chatRoomInfoMap);

              //==================================== GET THIS FOR NEW CHAT ====================================//
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChatScreen(
                    chatWithName: chatWithUserName,
                    chatWithContact: chatWithContact,
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
