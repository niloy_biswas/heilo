// import 'package:Heilo/models/asset_strings.dart';
// import 'package:Heilo/models/asset_strings.dart';
import 'package:flutter/material.dart';

class TutionUpcomingTile extends StatelessWidget {
  const TutionUpcomingTile(
      {Key key,
      @required this.sWidth,
      @required this.upcomingDate,
      @required this.subject,
      @required this.address,
      @required this.tutionTime,
      @required this.secretCode})
      : super(key: key);

  final double sWidth;
  final String upcomingDate;
  final String subject;
  final String address;
  final String tutionTime;
  final String secretCode;

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.all(20),
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                height: 35,
                width: sWidth * 0.16,
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                  color: Color(0xFFE2DEDE),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    topLeft: Radius.circular(30),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 3,
                      offset: Offset(0, 3),
                      spreadRadius: 3,
                    ),
                  ],
                ),
                child: Center(
                  ///////////////////////////// TUTION DATE //////////////////////////////
                  ///
                  child: Text(
                    upcomingDate ?? 'N/A',
                    style: TextStyle(
                      color: Colors.blueGrey,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Container(
                width: sWidth * 0.64,
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 10,
                      offset: Offset(0, 6),
                      spreadRadius: 3,
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ///////////////////////////// TUTION SUBJECT //////////////////////////////
                        ///
                        Container(
                          width: sWidth * 0.16,
                          child: Text(
                            subject ?? 'N/A',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.center,
                          ),
                        ),

                        ///////////////////////////// TUTION ADDRESS //////////////////////////////
                        ///
                        Container(
                          width: sWidth * 0.16,
                          child: Text(
                            address ?? 'N/A',
                            style: TextStyle(fontSize: 10),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 20,
                      child: Image.asset('assets/images/line1.png'),
                    ),

                    ///////////////////////////// TUTION TIME //////////////////////////////
                    ///
                    Text(
                      tutionTime ?? 'N/A',
                      style: TextStyle(
                        fontSize: 10,
                      ),
                    ),
                    Container(
                      height: 20,
                      child: Image.asset('assets/images/line1.png'),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 5,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      ),

                      ///////////////////////////// SECRET CODE //////////////////////////////
                      ///
                      child: Text(
                        secretCode ?? 'N/A',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
