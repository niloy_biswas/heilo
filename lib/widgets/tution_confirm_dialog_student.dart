import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

Future studentTutionConfirmDialog({
  @required BuildContext context,
  @required String code,
  @required String classId,
}) {
  return showDialog(
      context: context,
      builder: (context) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Dialog(
            elevation: 10,
            backgroundColor: Colors.black.withOpacity(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                bottomRight: Radius.circular(25),
              ),
            ),
            insetAnimationCurve: Curves.easeInOutCirc,
            insetAnimationDuration: Duration(seconds: 3),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(vertical: 5),
              height: MediaQuery.of(context).size.height * 0.2,
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                color: AssetStrings.color1,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Text(
                      "Are you sure you want to cancel?",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MaterialButton(
                        shape: StadiumBorder(),
                        color: Colors.green,
                        child: Text(
                          'YES',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () async {
                          print("CLASS ID: $classId, SECRET CODE: $code");

                          try {
                            EasyLoading.show(status: "Please Wait...");
                            Services serv = Services();
                            print(
                              "CLASS ID: $classId, SECRET CODE: $code",
                            );
                            await serv.deleteClassStudent(
                              code: code,
                              status: "cancel",
                              classId: classId,
                            );
                            Navigator.pop(context);
                            EasyLoading.showSuccess("Done");
                          } catch (e) {
                            await EasyLoading.showError("Failed");
                            customToast("Please try again later!");
                            print("FAILED to cancel class");
                          }
                        },
                      ),
                      SizedBox(
                        width: 50,
                      ),
                      MaterialButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        shape: StadiumBorder(),
                        child: Text('NO'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      });
}
