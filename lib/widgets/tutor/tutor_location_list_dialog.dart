import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/custom_toast.dart';
// import 'package:Heilo/widgets/student_education_update_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Future tutorLocationsListDialog({
  @required BuildContext context,
  @required TutorUserInfoModel tutorUserInfoModel,
}) async {
  ScrollController _controller = ScrollController();

  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.3),
    builder: (context) {
      // double sHeight = MediaQuery.of(context).size.height;
      double sWidth = MediaQuery.of(context).size.width;
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(seconds: 3),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            // height: sHeight * 0.45,
            width: sWidth * 0.9,
            decoration: BoxDecoration(
              color: AssetStrings.color1,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Scrollbar(
              isAlwaysShown: false,
              controller: _controller,
              radius: Radius.circular(50),
              thickness: 10,
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Colors.black.withOpacity(0.3),
                ),
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                itemCount: tutorUserInfoModel.data.locations.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 16,
                      horizontal: 20,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 4,
                          child: Text(
                            "${tutorUserInfoModel.data.locations[index].district}, ${tutorUserInfoModel.data.locations[index].area}, ${tutorUserInfoModel.data.locations[index].road}",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              ///
                              ///* DELETE BUTTON ///
                              ///
                              GestureDetector(
                                child: FaIcon(
                                  FontAwesomeIcons.trash,
                                  size: 18,
                                ),
                                onTap: () async {
                                  Services serv = Services();
                                  EasyLoading.show(status: "Please Wait...");
                                  try {
                                    print(tutorUserInfoModel
                                        .data.locations[index].locId);
                                    await serv.deleteTutorLocation(
                                        recordId: tutorUserInfoModel
                                            .data.locations[index].recordId);
                                    // EasyLoading.showSuccess(
                                    //   'Delete Successful',
                                    // );
                                    EasyLoading.dismiss();
                                    customToast("Location Updated");
                                    Navigator.pop(context);

                                    // await Future.delayed(Duration(seconds: 2));
                                  } catch (e) {
                                    print(e);

                                    EasyLoading.showError(
                                      'Delete Failed',
                                    );
                                    EasyLoading.dismiss();
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      );
    },
  );
}
