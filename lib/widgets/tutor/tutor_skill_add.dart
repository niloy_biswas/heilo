import 'dart:ui';

import 'package:Heilo/helper/sharedpref_helper.dart';
import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/segment_model.dart';
import 'package:Heilo/models/subject_model.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:Heilo/services/services.dart';
import 'package:Heilo/widgets/class_list_dialog.dart';
import 'package:Heilo/widgets/medium_list_dialog.dart';
import 'package:Heilo/widgets/subject_list_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../custom_toast.dart';

Future tutorSkillAddDialog({
  BuildContext context,
  TutorUserInfoModel tutorUserInfoModel,
}) async {
  // ScrollController _controller = ScrollController();

  List<String> mediumList = ['Bangla', 'English'];
  List<int> classList = [];
  List<String> subjectList = [];
  List<int> subjectIdList = [];
  SegmentModel segmentModel = SegmentModel();
  SubjectModel subjectModel = SubjectModel();

  Services serv = Services();

  // String selectedMedium;
  // int selectedSegmentId;
  // String selectedSubject;

  //
  String mediumButtonString = "Select Medium";
  String classButtonString = "Select Class";
  String subjectButtonString = "Select Subject";

  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.3),
    builder: (context) {
      double sHeight = MediaQuery.of(context).size.height;
      double sWidth = MediaQuery.of(context).size.width;
      return StatefulBuilder(builder: (context, setState) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Dialog(
            elevation: 10,
            backgroundColor: Colors.black.withOpacity(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
            ),
            insetAnimationCurve: Curves.easeInOutCirc,
            insetAnimationDuration: Duration(seconds: 3),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              height: sHeight * 0.45,
              width: sWidth * 0.9,
              decoration: BoxDecoration(
                color: AssetStrings.color1,
                borderRadius: BorderRadius.circular(20),
              ),
              //===============================================================================//
              child: ListView(
                children: [
                  SizedBox(
                    height: 30,
                  ),

                  // SizedBox(
                  //   height: 20,
                  // ),
                  //==================================== HEADER =======================================//
                  // Container(
                  //   padding: EdgeInsets.only(bottom: 30),
                  //   child: Column(
                  //     children: [
                  //       Text(
                  //         "Enter Preferred Subject",
                  //         style: TextStyle(
                  //           fontSize: 20,
                  //           fontWeight: FontWeight.bold,
                  //           color: Colors.white,
                  //         ),
                  //       ),
                  //       Padding(
                  //         padding: const EdgeInsets.only(top: 5),
                  //         child: Text(
                  //           "(Add more from your profile)",
                  //           style: TextStyle(
                  //               fontSize: 12, color: Colors.black87),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Center(
                    child: Text('Skill Information',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    height: 40,
                    width: double.infinity,

                    ////////////////////////////////// SELECT MEDIUM /////////////////////////////
                    ///
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 3,
                              offset: Offset(0, 3),
                            ),
                          ],
                        ),
                        child: Container(
                          // alignment: Alignment.centerLeft,
                          width: double.infinity,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(
                                  mediumButtonString ?? "Select Medium",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black54,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              FaIcon(
                                FontAwesomeIcons.caretDown,
                                size: 14,
                                color: Colors.black54,
                              )
                            ],
                          ),
                        ),
                      ),
                      onTap: () async {
                        setState(() {
                          classList = [];
                          subjectList = [];
                          subjectIdList = [];
                          SharedPreferenceHelper()
                              .saveRegSkillClass("Select Class");
                          SharedPreferenceHelper()
                              .saveRegSkillSub("Select Subject");
                        });

                        classButtonString =
                            await SharedPreferenceHelper().getRegSkillClass();
                        subjectButtonString =
                            await SharedPreferenceHelper().getRegSkillSub();
                        setState(() {});

                        await mediumListDialog(
                          context: context,
                          mediumList: mediumList,
                        );
                        mediumButtonString =
                            await SharedPreferenceHelper().getRegSkillMedium();
                        setState(() {});

                        try {
                          await serv
                              .getSegmentData(
                                  refresh: true,
                                  medium: mediumButtonString.toLowerCase())
                              .then((value) {
                            setState(() {
                              segmentModel = value;
                            });
                          });
                          for (int i = 0; i < segmentModel.data.length; i++) {
                            print(segmentModel.data[i].segmentId);
                            classList.add(segmentModel.data[i].segmentId);
                          }
                          classList.toSet();
                          setState(() {});
                        } catch (e) {
                          print(e);
                          throw Exception(e);
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ////////////////////////////classsss
                  // classWidget(),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    height: 40,
                    width: double.infinity,

                    ////////////////////////////////// SELECT CLASS /////////////////////////////
                    ///
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 3,
                              offset: Offset(0, 3),
                            ),
                          ],
                        ),
                        child: Container(
                          // alignment: Alignment.centerLeft,
                          width: double.infinity,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(
                                  classButtonString ?? "Select Class",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black54,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              FaIcon(
                                FontAwesomeIcons.caretDown,
                                size: 14,
                                color: Colors.black54,
                              )
                            ],
                          ),
                        ),
                      ),
                      onTap: () async {
                        // subjectList.clear();
                        subjectList = [];
                        subjectIdList = [];
                        SharedPreferenceHelper()
                            .saveRegSkillSub("Select Subject");
                        subjectButtonString =
                            await SharedPreferenceHelper().getRegSkillSub();
                        setState(() {});

                        if (classList.isNotEmpty) {
                          await classListDialog(
                            context: context,
                            classList: classList,
                          );
                          classButtonString =
                              await SharedPreferenceHelper().getRegSkillClass();
                          setState(() {});
                        } else {
                          customToast("Select Medium First!");
                        }

                        try {
                          await serv
                              .getSubjects(segmentId: classButtonString)
                              .then((value) {
                            setState(() {
                              subjectModel = value;
                            });
                          });
                          for (int i = 0; i < subjectModel.data.length; i++) {
                            print(subjectModel.data[i].subject);
                            subjectList.add(subjectModel.data[i].subject);
                            print(subjectModel.data[i].subjectId);
                            subjectIdList.add(subjectModel.data[i].subjectId);
                          }
                          classList.toSet();
                          setState(() {});
                        } catch (e) {
                          customToast("errorr");
                          print(e);
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),

                  ///////////// SUBJECT //////////////
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    height: 40,
                    width: double.infinity,

                    ////////////////////////////////// SELECT SUBJECT /////////////////////////////
                    ///
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 3,
                              offset: Offset(0, 3),
                            ),
                          ],
                        ),
                        child: Container(
                          // alignment: Alignment.centerLeft,
                          width: double.infinity,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 10,
                                child: Text(
                                  subjectButtonString ?? "Select Subject",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black54,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              FaIcon(
                                FontAwesomeIcons.caretDown,
                                size: 14,
                                color: Colors.black54,
                              )
                            ],
                          ),
                        ),
                      ),
                      onTap: () async {
                        // subjectList.clear();
                        if (subjectList.isNotEmpty) {
                          await subjectListDialog(
                            context: context,
                            subjectList: subjectList,
                            subjectIdList: subjectIdList,
                          );
                          subjectButtonString =
                              await SharedPreferenceHelper().getRegSkillSub();
                          setState(() {});
                        } else {
                          customToast("Select Class First!");
                          setState(() {});
                        }
                      },
                    ),
                  ),

                  SizedBox(
                    height: 30,
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 70,
                    ),
                    child: MaterialButton(
                      height: 40,
                      color: AssetStrings.color4,
                      shape: StadiumBorder(),
                      child: Text('Add',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          )),
                      onPressed: () async {
                        try {
                          EasyLoading.show(status: "Please Wait...");

                          if (mediumButtonString != "Select Medium" &&
                              classButtonString != "Select Class" &&
                              subjectButtonString != "Select Subject") {
                            // customToast("Sending");
                            // Navigator.pushReplacement(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => OtpPage()));
                            print(mediumButtonString);
                            print(classButtonString);
                            print(subjectButtonString);

                            var finalTutorId =
                                tutorUserInfoModel.data.tutorId.toString();
                            var finalSegmentId = await SharedPreferenceHelper()
                                .getRegSkillClass();
                            var finalSubjectId = await SharedPreferenceHelper()
                                .getRegSkillSubjectId();
                            print("TUTOR ID: $finalTutorId");
                            print("SEGMENT ID: $finalSegmentId");
                            print("SUBJECT ID: $finalSubjectId");

                            await serv.saveTutorSkills(
                                tutorId: int.parse(finalTutorId),
                                segmentId: int.parse(finalSegmentId),
                                subjectId: int.parse(finalSubjectId),
                                topicId: '');

                            Navigator.pop(context);
                            EasyLoading.showSuccess("Updated");
                            // customToast("Profile Updated");
                          } else {
                            EasyLoading.dismiss();
                            customToast("Fill up all the items");
                          }

                          // customToast("LOOKS GOOD!");
                          // EasyLoading.showError("Fill up all the items");
                        } catch (e) {
                          EasyLoading.dismiss();

                          print(e);
                        }
                      },
                    ),
                  ),

                  // GradientButton(
                  //   buttonName: 'NEXT',
                  //   sWidth: sWidth,
                  //   sHeight: sHeight,
                  //   function: () async {},
                  // ),
                ],
              ),
            ),
          ),
        );
      });
    },
  );
}
