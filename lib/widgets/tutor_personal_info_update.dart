import 'dart:ui';

import 'package:Heilo/models/asset_strings.dart';
import 'package:Heilo/models/tutor_user_info_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

Future tutorPersonalInfoUpdate({
  @required BuildContext context,
  @required TutorUserInfoModel tutorUserInfoModel,
  @required TextEditingController emailController,
  @required TextEditingController locationReferenceController,
  // @required TextEditingController addressController,
}) async {
  return showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(0.7),
    builder: (context) {
      double sWidth = MediaQuery.of(context).size.width;
      // double sHeight = MediaQuery.of(context).size.height;
      return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
        child: Dialog(
          elevation: 10,
          backgroundColor: Colors.black.withOpacity(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          insetAnimationCurve: Curves.easeInOutCirc,
          insetAnimationDuration: Duration(milliseconds: 500),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            width: sWidth * 0.9,
            decoration: BoxDecoration(
              color: AssetStrings.color1,
              borderRadius: BorderRadius.circular(20),
            ),
            child: ListView(
              // mainAxisSize: MainAxisSize.min,
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Text(
                    "Personal Details",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),

                ///* NAME INPUT ///
                //
                // Align(
                //   alignment: Alignment.centerLeft,
                //   child: Container(
                //     margin: EdgeInsets.only(left: 20, bottom: 5),
                //     child: Text('Enter Name'),
                //   ),
                // ),
                // UpdateInputField(
                //   controller: null,
                //   // initialValue: tutorUserInfoModel.data.name,
                //   keyboardType: TextInputType.text,
                // ),

                ///* EMAIL INPUT ///
                //
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 20, bottom: 5),
                    child: Text('Enter Email'),
                  ),
                ),
                UpdateInputField(
                  controller: emailController,
                  // initialValue:
                  //     tutorUserInfoModel.data.email ?? "No Email Found",
                  keyboardType: TextInputType.emailAddress,
                ),

                // ///* CONTACT INPUT ///
                // ///
                // Align(
                //   alignment: Alignment.centerLeft,
                //   child: Container(
                //     margin: EdgeInsets.only(left: 20, bottom: 5),
                //     child: Text('Enter Alternate Contact'),
                //   ),
                // ),
                // UpdateInputField(
                //   controller: contactController,
                //   initialValue:
                //       tutorUserInfoModel.data.altPhone ?? "No Contacts Found",
                //   keyboardType: TextInputType.phone,
                // ),
                ///* CONTACT INPUT ///
                ///
                ///

                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 20, bottom: 5),
                    child: Text('Location of Reference (Area)'),
                  ),
                ),
                UpdateInputField(
                  controller: locationReferenceController,
                  // initialValue: tutorUserInfoModel.data.location.area ??
                  //     "No Contacts Found",
                  // keyboardType: TextInputType.phone,
                ),

                // Align(
                //   alignment: Alignment.centerLeft,
                //   child: Container(
                //     margin: EdgeInsets.only(left: 20, bottom: 5),
                //     child: Text('Enter Detail Address'),
                //   ),
                // ),
                // UpdateInputField(
                //   controller: addressController,
                // ),

                ///* SUBMIT BUTTON ///
                ///
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  padding: EdgeInsets.symmetric(horizontal: 80),
                  child: RaisedButton(
                    child: Text(
                      'Update',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                    shape: StadiumBorder(),
                    elevation: 3,
                    color: AssetStrings.color4,
                    onPressed: () async {
                      Navigator.pop(context);

                      if (emailController.text != '' &&
                          locationReferenceController.text != '') {
                        try {
                          // print(addressController.text);
                          // print(emailController.text);
                          // print(locationReferenceController.text);
                          // Services serv = Services();
                          // if (EmailValidator.validate(emailController.text)) {
                          //   EasyLoading.show(status: "Please Wait...");

                          //   EasyLoading.showSuccess("Updated");
                          // } else {
                          //   EasyLoading.showError("Invalid Email");
                          // }
                        } catch (e) {
                          print(e);
                          EasyLoading.showError("Error Occured");
                        }
                      } else {
                        EasyLoading.showError("Empty Fields Not Allowed!");
                        Future.delayed(Duration(milliseconds: 1500));
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

class UpdateInputField extends StatefulWidget {
  const UpdateInputField({
    Key key,
    @required this.controller,
    this.initialValue,
    this.keyboardType,
  }) : super(key: key);

  final TextEditingController controller;
  final String initialValue;
  final TextInputType keyboardType;

  @override
  _UpdateInputFieldState createState() => _UpdateInputFieldState();
}

class _UpdateInputFieldState extends State<UpdateInputField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20.0,
        right: 20,
        bottom: 20,
      ),
      child: Container(
        // padding: EdgeInsets.all(20),
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 3,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: TextField(
          controller: widget.controller,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.center,
          keyboardType: widget.keyboardType,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: widget.initialValue ?? '',
          ),
        ),
      ),
    );
  }
}
